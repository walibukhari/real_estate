<div class="sidebar" data-color="white" data-active-color="danger">
   <div class="logo">
      <a href="#" class="simple-text logo-mini">
         <div class="logo-image-small">
            <!--  <img src="{{ asset('paper') }}/img/logo-small.png"> -->
         </div>
      </a>
      <a href="#" class="simple-text logo-normal">
      Real Estate
      </a>
   </div>
   <div class="sidebar-wrapper">
      <ul class="nav">
         <li class="{{ $elementActive == 'dashboard' ? 'active' : '' }}">
            <a href="{{ route('page.index', 'dashboard') }}">
               <i class="nc-icon nc-bank"></i>
               <p>{{ __('Dashboard') }}</p>
            </a>
         </li>
         <li class="{{ $elementActive == 'user' || $elementActive == 'profile' ? 'active' : '' }}">
            <a data-toggle="collapse" aria-expanded="true" href="#laravelExamples">
               <i class="nc-icon"><i class="fa fa-users"></i></i>
               <p>
                  Management
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse" id="laravelExamples">
               <ul class="nav">
                  <li class="{{ $elementActive == 'profile' ? 'active' : '' }}">
                     <a href="{{ route('profile.edit') }}">
                     <span class="sidebar-mini-icon">{{ __('UP') }}</span>
                     <span class="sidebar-normal">{{ __(' User Profile ') }}</span>
                     </a>
                  </li>
                  <li class="{{ $elementActive == 'user' ? 'active' : '' }}">
                     <a href="{{ route('page.index', 'user') }}">
                     <span class="sidebar-mini-icon">{{ __('U') }}</span>
                     <span class="sidebar-normal">{{ __(' User Management ') }}</span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>
         <li>
            <a data-toggle="collapse"  href="#Projects_M">
               <i class="nc-icon"><i class="fa fa-building"></i></i>
               <p>
                  Master
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse " id="Projects_M">
               <ul class="nav">
                  <li class="#">
                     <a href="{{url('/Add_Project')}}">
                     <span class="sidebar-mini-icon">{{ __('RNP') }}</span>
                     <span class="sidebar-normal">Projects</span>
                     </a>
                  </li>
                  <li class="#">
                     <a href="{{url('/Add_Building')}}">
                     <span class="sidebar-mini-icon">{{ __('RB') }}</span>
                     <span class="sidebar-normal"> Buildings</span>
                     </a>
                  </li>
                  <li class="#">
                     <a href="{{url('/Add_Floor')}}">
                     <span class="sidebar-mini-icon">{{ __('RF') }}</span>
                     <span class="sidebar-normal">Floors</span>
                     </a>
                  </li>
                  <li class="#">
                     <a href="{{url('/Add_Flat')}}">
                     <span class="sidebar-mini-icon">{{ __('RF') }}</span>
                     <span class="sidebar-normal">Flats</span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>

          <li>
            <a data-toggle="collapse"  href="#accounts">
               <i class="nc-icon">   <i class="nc-icon nc-air-baloon"></i></i>
               <p>
                  Accounts
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse " id="accounts">
               <ul class="nav">
                  <li class="#">
                     <a href="{{route('Admin.Add_Account')}}">
                     <span class="sidebar-mini-icon">{{ __('RNP') }}</span>
                     <span class="sidebar-normal">Add New</span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>







         <li>
            <a data-toggle="collapse"  href="#tenants_M">
               <i class="nc-icon">  <i class="nc-icon nc-chart-pie-36"></i></i>
               <p>
                  Tenants
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse " id="tenants_M">
               <ul class="nav">
                  <li class="#">
                     <a href="{{url('/Add_tenants')}}">
                     <span class="sidebar-mini-icon">{{ __('RNP') }}</span>
                     <span class="sidebar-normal">Add New</span>
                     </a>
                  </li>
                  <!--  <li class="#">
                     <a href="{{url('/Add_Building')}}">
                         <span class="sidebar-mini-icon">{{ __('RB') }}</span>
                         <span class="sidebar-normal"> Buildings</span>
                     </a>
                     </li>
                     <li class="#">
                     <a href="{{url('/Add_Floor')}}">
                         <span class="sidebar-mini-icon">{{ __('RF') }}</span>
                         <span class="sidebar-normal">Floors</span>
                     </a>
                     </li>
                     <li class="#">
                     <a href="{{url('/Add_Flat')}}">
                         <span class="sidebar-mini-icon">{{ __('RF') }}</span>
                         <span class="sidebar-normal">Flats</span>
                     </a>
                     </li> -->
               </ul>
            </div>
         </li>
         <li>
            <a data-toggle="collapse"  href="#Landlords_M">
               <i class="nc-icon">   <i class="nc-icon nc-air-baloon"></i></i>
               <p>
                  Landlords
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse " id="Landlords_M">
               <ul class="nav">
                  <li class="#">
                     <a href="{{url('/Add_Landlords')}}">
                     <span class="sidebar-mini-icon">{{ __('RNP') }}</span>
                     <span class="sidebar-normal">Add New</span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>
         <li>
            <a data-toggle="collapse"  href="#Contract_M">
               <i class="nc-icon">   <i class="nc-icon nc-air-baloon"></i></i>
               <p>
                  Contract's
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse " id="Contract_M">
               <ul class="nav">
                  <li class="#">
                     <a href="{{url('/Add_Contracts')}}">
                     <span class="sidebar-mini-icon">{{ __('CN') }}</span>
                     <span class="sidebar-normal">Long Term Contract</span>
                     </a>
                  </li>
                  <li class="#">
                     <a href="{{url('/Add_Short_Contracts')}}">
                     <span class="sidebar-mini-icon">{{ __('CN') }}</span>
                     <span class="sidebar-normal">Short Term Contract</span>
                     </a>
                  </li>
                  <li class="#">
                     <a href="{{url('/Sale_Purchase_Contract')}}">
                     <span class="sidebar-mini-icon">{{ __('CN') }}</span>
                     <span class="sidebar-normal">Sale Purchase Contract</span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>


           <li>
            <a data-toggle="collapse"  href="#Voucher_M">
               <i class="nc-icon">   <i class="nc-icon nc-air-baloon"></i></i>
               <p>
                  Journal Voucher
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse " id="Voucher_M">
               <ul class="nav">
                  <li class="#">
                     <a href="{{url('/Add_journal_voucher')}}">
                     <span class="sidebar-mini-icon">{{ __('CN') }}</span>
                     <span class="sidebar-normal">Add New</span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>







           <li>
            <a data-toggle="collapse"  href="#maintenance">
               <i class="nc-icon">   <i class="nc-icon nc-tablet-2"></i></i>
               <p>
                  Nature Of Maintenance
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse " id="maintenance">
               <ul class="nav">
                  <li class="#">
                     <a href="{{url('/Nature_maintenance')}}">
                     <span class="sidebar-mini-icon">{{ __('CN') }}</span>
                     <span class="sidebar-normal">Maintenance Head List</span>
                     </a>
                  </li>

                  <li class="#">
                     <a href="{{url('/Questions')}}">
                     <span class="sidebar-mini-icon">{{ __('CN') }}</span>
                     <span class="sidebar-normal">Questions  List</span>
                     </a>
                  </li>

               </ul>
            </div>
         </li>



             <li>
            <a data-toggle="collapse"  href="#maintenance1">
               <i class="nc-icon">   <i class="nc-icon nc-tablet-2"></i></i>
               <p>
                  Voucher  Maintenance
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse " id="maintenance1">
               <ul class="nav">
                  <li class="#">
                     <a href="{{url('/Voucher_Detail')}}">
                     <span class="sidebar-mini-icon">{{ __('CN') }}</span>
                     <span class="sidebar-normal">Payment Voucher</span>
                     </a>
                  </li>

                  <li class="#">
                     <a href="{{url('/Receipt_Voucher')}}">
                     <span class="sidebar-mini-icon">{{ __('CN') }}</span>
                     <span class="sidebar-normal">Receipt Voucher</span>
                     </a>
                  </li>

               </ul>
            </div>
         </li>









         <li >
            <a href="{{url('/Utility_Management_Show')}}">
               <i class="nc-icon nc-bank"></i>
               <p>Utility Management</p>
            </a>
         </li>

         <li >
            <a href="{{url('/Maintaince_List')}}">
               <i class="nc-icon nc-tap-01"></i>
               <p>Maintaince List</p>
            </a>
         </li>

          <li >
            <a href="{{url('/supplier_list')}}">
               <i class="nc-icon nc-support-17"></i>
               <p>Suppliers List</p>
            </a>
         </li>

         <li >
            <a href="{{url('/Create_Post_Dated_Cheques')}}">
               <i class="nc-icon nc-support-17"></i>
               <p>PDC Management</p>
            </a>
         </li>

         <li>
            <a data-toggle="collapse"  href="#Cheques_M">
               <i class="nc-icon">   <i class="nc-icon nc-air-baloon"></i></i>
               <p>
                  Cheques Management
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse " id="Cheques_M">
               <ul class="nav">
                  <li class="#">
                     <a href="{{url('/Add_Contracts')}}">
                     <span class="sidebar-mini-icon">{{ __('CM') }}</span>
                     <span class="sidebar-normal">Add New</span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>
         <li>
            <a data-toggle="collapse"  href="#APPART_M">
               <i class="nc-icon">   <i class="nc-icon nc-air-baloon"></i></i>
               <p>
                  Assinge Appartments
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse " id="APPART_M">
               <ul class="nav">
                  <li class="#">
                     <a href="{{url('/Add_Assign_APPART')}}">
                     <span class="sidebar-mini-icon">{{ __('RNP') }}</span>
                     <span class="sidebar-normal">Add New</span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>




          <li>
            <a data-toggle="collapse"  href="#reports">
               <i class="nc-icon">   <i class="nc-icon nc-tablet-2"></i></i>
               <p>
                  Reports
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse " id="reports">
               <ul class="nav">
                  <li class="#">
                     <a href="{{route('admin.owner_report')}}">
                     <span class="sidebar-mini-icon">{{ __('CN') }}</span>
                     <span class="sidebar-normal">Owner Report</span>
                     </a>
                  </li>



               </ul>
            </div>
         </li>







         <li>
            <a data-toggle="collapse"  href="#Settings">
               <i class="nc-icon"><i class="fa fa-cog"></i></i>
               <p>
                  Settings
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse " id="Settings">
               <ul class="nav">
                  <li class="#">
                     <a href="{{url('/Add_building_types')}}">
                     <span class="sidebar-mini-icon">{{ __('SS') }}</span>
                     <span class="sidebar-normal">Building Types</span>
                     </a>
                  </li>
                  <li class="#">
                     <a href="{{url('/Add_building_facilties')}}">
                     <span class="sidebar-mini-icon">{{ __('SS') }}</span>
                     <span class="sidebar-normal">Building Facilties</span>
                     </a>
                  </li>
                  <li class="#">
                     <a href="{{url('/Add_flat_types')}}">
                     <span class="sidebar-mini-icon">{{ __('SS') }}</span>
                     <span class="sidebar-normal">Falt Types</span>
                     </a>
                  </li>
                  <li class="#">
                     <a href="{{url('/Add_flat_pro_types')}}">
                     <span class="sidebar-mini-icon">{{ __('SS') }}</span>
                     <span class="sidebar-normal">Flat Property Types</span>
                     </a>
                  </li>
                  <li class="#">
                     <a href="{{url('/Add_flat_facilties')}}">
                     <span class="sidebar-mini-icon">{{ __('SS') }}</span>
                     <span class="sidebar-normal">Flat Facilties</span>
                     </a>
                  </li>
                  <li class="#">
                     <a href="{{url('/Add_floor_types')}}">
                     <span class="sidebar-mini-icon">{{ __('SS') }}</span>
                     <span class="sidebar-normal">Floor Type</span>
                     </a>
                  </li>
                  <li class="#">
                     <a href="{{url('/Add_admin')}}">
                     <span class="sidebar-mini-icon">{{ __('SS') }}</span>
                     <span class="sidebar-normal">Admins</span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>
      </ul>
   </div>
</div>
?>

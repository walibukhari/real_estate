@extends('layouts.app', [
'class' => '',
'elementActive' => 'add_building_facilties'
])
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="content">
   @if (session('status'))
   <div class="alert alert-success" role="alert">
      {{ session('status') }}
   </div>
   @endif
   @if (session('password_status'))
   <div class="alert alert-success" role="alert">
      {{ session('password_status') }}
   </div>
   @endif
   @if (Session::has('error'))
   <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{!! Session('error') !!}</strong>
   </div>
   @endif
   @if ($errors->any())
   <div class="alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
   @endif
   <style type="text/css">
      .filelabel {
      width: 100%;
      border: 2px dashed grey;
      border-radius: 5px;
      display: block;
      padding: 5px;
      transition: border 300ms ease;
      cursor: pointer;
      text-align: center;
      margin: 0;
      }
      .filelabel i {
      display: block;
      font-size: 30px;
      padding-bottom: 5px;
      }
      .filelabel i,
      .filelabel .title {
      color: grey;
      transition: 200ms color;
      }
      .filelabel:hover {
      border: 2px solid #1665c4;
      }
      .filelabel:hover i,
      .filelabel:hover .title {
      color: #1665c4;
      }
      #FileInput{
      display:none;
      }
   </style>
   <div class="row">
      <div class="col-12">
         <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-2">
               <!--<button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Create New</button>-->
            </div>
         </div>
         <div class="material-card card">
            <div class="card-body">
               <h4 class="card-title">Utility Management</h4>
               <h6 class="card-subtitle">
               </h6>
               <br>
               <form class="form" method="POST" action="{{url('/Admin/Save_Utility_management')}}" enctype="multipart/form-data">
                  @csrf
                  <div class="container">
                     <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary">Project</label>
                           <select class="form-control" name="project" id="selectProject" onchange="selectproject(this.value)">
                              <option value="" selected disabled>Select project</option>
                              @foreach($projects as $pro)
                              <option value="{{$pro->id}}">{{$pro->name}}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="col-md-6">
                           <label class="text-primary"> Building</label>
                           <select class="form-control" name="building" id="selectBuilding" onchange="selectbuilding(this.value)">
                           </select>
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary"> Floor</label>
                           <select class="form-control" name="floor" id="selectFloor" onchange="selectfloor(this.value)">
                           </select>
                        </div>
                        <div class="col-md-6">
                           <label class="text-primary"> Flat</label>
                           <select class="form-control" name="flat" id="selectFlat" onchange="selectflat(this.value)" >
                           </select>
                        </div>
                     </div>
                     <br>
                     <div class="row">
                         <div class="col-md-6">
                             <label class="text-primary">Owner Information</label>
                             <div class="container" style="border: 1px solid grey;"> 
                             <br>
                                <div class="row">
                                    <div class="col-md-3"><lable class="text-primary">Name</lable></div>
                                    <div class="col-md-9">
                                        <select class="form-control" name="owner_id">
                                            <option value="" selected disabled> Select Owner</option>
                                             @foreach($owner as $owners)
                                                <option value="{{$owners->id}}">{{$owners->name}}</option>
                                             @endforeach
                                        </select>
                                    </div>
                                </div> <br>
                                
                                <div class="row">
                                    <div class="col-md-3"><lable class="text-primary">Contact No.</lable></div>
                                    <div class="col-md-9">
                                        <input type="number" class="form-control" name="owner_contact_no" />
                                    </div>
                                </div> <br>
                                
                                <div class="row">
                                    <div class="col-md-3"><lable class="text-primary">Email</lable></div>
                                    <div class="col-md-9">
                                        <input type="email" class="form-control" name="owner_email" />
                                    </div>
                                </div>
                                <br>
                             </div>
                         </div>
                         
                         
                           <div class="col-md-6">
                             <label class="text-primary">Tenant Information</label>
                             <div class="container" style="border: 1px solid grey;"> 
                             <br>
                                <div class="row">
                                    <div class="col-md-3"><lable class="text-primary">Name</lable></div>
                                    <div class="col-md-9">
                                        <select class="form-control" name="tenant_id">
                                            <option value="" selected disabled> Select Tenant</option>
                                             @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                             @endforeach
                                        </select>
                                    </div>
                                </div> <br>
                                
                                <div class="row">
                                    <div class="col-md-3"><lable class="text-primary">Contact No.</lable></div>
                                    <div class="col-md-9">
                                        <input type="number" class="form-control" name="tenant_contact_no" />
                                    </div>
                                </div> <br>
                                
                                <div class="row">
                                    <div class="col-md-3"><lable class="text-primary">Email</lable></div>
                                    <div class="col-md-9">
                                        <input type="email" class="form-control" name="tenant_email" />
                                    </div>
                                </div>
                                <br>
                             </div>
                         </div>
                     </div>
                     
                     <!--<div class="row">-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">LanLord</label>-->
                     <!--      <input type="text" id="LanLord" name="lanlord_name" class="form-control" />-->
                     <!--   </div>-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Select Tenant</label>-->
                     <!--      <select class="form-control" name="tenant_name">-->
                     <!--         <option>Select Tenant</option>-->
                     <!--         @foreach($users as $user)-->
                     <!--         <option value="{{$user->id}}">{{$user->name}}</option>-->
                     <!--         @endforeach-->
                     <!--      </select>-->
                     <!--   </div>-->
                     <!--</div>-->
                     <!--<br>-->
                     <!--<div class="row">-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Name</label>-->
                     <!--      <input type="name" class="form-control" name="name" placeholder="Name">-->
                     <!--   </div>-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Email</label>-->
                     <!--      <input type="email" class="form-control" name="email1" placeholder="Email">-->
                     <!--   </div>-->
                     <!--</div>-->
                     <!--<br>-->
                     <!--<div class="row">-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Contact 1</label>-->
                     <!--      <input type="number" class="form-control" name="contact1" placeholder="Contact 1">-->
                     <!--   </div>-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Contact 2</label>-->
                     <!--      <input type="number" class="form-control" name="contact2" placeholder="Contact 2">-->
                     <!--   </div>-->
                     <!--</div>-->
                     <!--<br>-->
                     <!--<div class="row">-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Check In Date</label>-->
                     <!--      <input type="date" class="form-control" name="check_in_date" id="check_in_date" onchange="assign_date_in()"  placeholder="Flat Security">-->
                     <!--   </div>-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Check Out Date</label>-->
                     <!--      <input type="date" class="form-control" name="check_out_date" id="check_out_date" onchange="assign_date_out()" placeholder="Flat Security">-->
                     <!--   </div>-->
                     <!--</div>-->
                     <!--<br>-->
                     <!--<div class="row">-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Number of Person</label>-->
                     <!--      <input type="number" class="form-control" name="num_of_person" id="num_of_person"  placeholder="Flat Security">-->
                     <!--   </div>-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Adults</label>-->
                     <!--      <input type="number" class="form-control" name="adults" placeholder="Adults">-->
                     <!--   </div>-->
                     <!--</div>-->
                     <!--<br>-->
                     <!--<div class="row">-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Childrens</label>-->
                     <!--      <input type="number" id="childrens" name="childrens" class="form-control" placeholder="Childrens">-->
                     <!--   </div>-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Managemnet Fee</label>-->
                     <!--      <input class="form-control" value="" id="managment_fee" name="managment_fee" />-->
                     <!--   </div>-->
                     <!--</div>-->
                     <!--<br>-->
                     <!--<div class="row">-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Terms and conditions</label>-->
                     <!--      <textarea class="form-control" name="terms_conditions" placeholder="Terms and conditions"></textarea>-->
                     <!--   </div>-->
                     <!--   <div class="col-md-6">-->
                     <!--      <label class="text-primary">Renew Terms and conditions</label>-->
                     <!--      <textarea class="form-control" name="renew_terms_conditions" placeholder="Renew Terms and conditions"></textarea>-->
                     <!--   </div>-->
                     <!--</div>-->
                     <br>
                     
                     
                     <div class="row">
                        <div class="col-md-12">
                           <label class="text-primary">On Account of For Tenants (External Use)</small></label>
                           <div style="border: 1px solid grey;">
                              <center>
                                 <!--<input type="button" id="security-row" class="btn btn-primary"  value="Add More" />-->
                                 <input id='security-row' class='btn btn-primary' type='button' value='Add More' />
                              </center>
                              <div class="table-responsive">
                                 <table id="complex_header" class="table table-striped table-bordered display"
                                    style="width:100%">
                                    <thead>
                                       <tr>
                                          <th>Sr #</th>
                                          <th>Utility Type</th>
                                          <th>Particular</th>
                                          <th>Ref No.</th>
                                          <th>Amount</th>
                                          <th>From</th>
                                          <th>To</th>
                                          <th>Dua Date</th>
                                          <th>Next Billing Date</th>
                                          <th>Frequency</th>
                                          <th>Charge To Owner</th>
                                          <th>Equal to Owner</th>
                                          <th>
                                             <center>Attachment</center>
                                          </th>
                                          <th>
                                             <center>Charge to Tenant</center>
                                          </th>
                                          <th>
                                             <center>Email to Owner</center>
                                          </th>
                                          <th>
                                             <center>Chargr to Company</center>
                                          </th>
                                          <th>
                                             <center>Company to Head</center>
                                          </th>
                                          <th> - </th>
                                       </tr>
                                    </thead>
                                    <tbody id="test-body1">
                                       <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='1' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                               
                                                <option  selected  value="Empower">Empower</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' placeholder="Particular" class='form-control' />
                                          </td>
                                          
                                          <td>
                                             <input name='ref_no[]'  id="" type='text' placeholder="Ref No."  class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='amount[]' placeholder="Amount"  type='number' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='from[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='to[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                            <input name='due_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='next_billing_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td >
                                             <input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td >
                                             <input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>
                                          </td>
                                     
                                          <td>
                                             <input name='company_to_head[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                         <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='2' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                               
                                                <option  selected  value="Emicool">Emicool</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' placeholder="Particular" class='form-control' />
                                          </td>
                                          
                                          <td>
                                             <input name='ref_no[]'  id="" type='text' placeholder="Ref No."  class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='amount[]' placeholder="Amount"  type='number' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='from[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='to[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                            <input name='due_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='next_billing_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td >
                                             <input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td >
                                             <input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>
                                          </td>
                                     
                                          <td>
                                             <input name='company_to_head[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                        <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='3' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                               
                                                <option  selected  value="Dewa">Dewa</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' placeholder="Particular" class='form-control' />
                                          </td>
                                          
                                          <td>
                                             <input name='ref_no[]'  id="" type='text' placeholder="Ref No."  class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='amount[]' placeholder="Amount"  type='number' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='from[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='to[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                            <input name='due_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='next_billing_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td >
                                             <input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td >
                                             <input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>
                                          </td>
                                     
                                          <td>
                                             <input name='company_to_head[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                         <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='4' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                               
                                                <option  selected  value="Du">Du</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' placeholder="Particular" class='form-control' />
                                          </td>
                                          
                                          <td>
                                             <input name='ref_no[]'  id="" type='text' placeholder="Ref No."  class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='amount[]' placeholder="Amount"  type='number' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='from[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='to[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                            <input name='due_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='next_billing_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td >
                                             <input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td >
                                             <input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>
                                          </td>
                                     
                                          <td>
                                             <input name='company_to_head[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                         <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='5' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                               
                                                <option  selected  value="Etisalat">Etisalat</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' placeholder="Particular" class='form-control' />
                                          </td>
                                          
                                          <td>
                                             <input name='ref_no[]'  id="" type='text' placeholder="Ref No."  class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='amount[]' placeholder="Amount"  type='number' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='from[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='to[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                            <input name='due_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='next_billing_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td >
                                             <input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td >
                                             <input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>
                                          </td>
                                     
                                          <td>
                                             <input name='company_to_head[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                         <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='6' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                               
                                                <option  selected  value="My Ista">My Ista</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' placeholder="Particular" class='form-control' />
                                          </td>
                                          
                                          <td>
                                             <input name='ref_no[]'  id="" type='text' placeholder="Ref No."  class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='amount[]' placeholder="Amount"  type='number' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='from[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='to[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                            <input name='due_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='next_billing_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td >
                                             <input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td >
                                             <input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>
                                          </td>
                                     
                                          <td>
                                             <input name='company_to_head[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                        <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='7' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                               
                                                <option  selected  value="Service Charges">Service Charges</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' placeholder="Particular" class='form-control' />
                                          </td>
                                          
                                          <td>
                                             <input name='ref_no[]'  id="" type='text' placeholder="Ref No."  class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='amount[]' placeholder="Amount"  type='number' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='from[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='to[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                            <input name='due_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='next_billing_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td >
                                             <input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td >
                                             <input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>
                                          </td>
                                     
                                          <td>
                                             <input name='company_to_head[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                        <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='8' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                               
                                                <option  selected  value="New Access Card">New Access Card</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' placeholder="Particular" class='form-control' />
                                          </td>
                                          
                                          <td>
                                             <input name='ref_no[]'  id="" type='text' placeholder="Ref No."  class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='amount[]' placeholder="Amount"  type='number' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='from[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='to[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                            <input name='due_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='next_billing_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td >
                                             <input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td >
                                             <input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>
                                          </td>
                                     
                                          <td>
                                             <input name='company_to_head[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                        <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='9' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                               
                                                <option  selected  value="Lootha Gas">Lootha Gas</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' placeholder="Particular" class='form-control' />
                                          </td>
                                          
                                          <td>
                                             <input name='ref_no[]'  id="" type='text' placeholder="Ref No."  class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='amount[]' placeholder="Amount"  type='number' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='from[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='to[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                            <input name='due_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='next_billing_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td >
                                             <input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td >
                                             <input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>
                                          </td>
                                     
                                          <td>
                                             <input name='company_to_head[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                        <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='10' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                               
                                                <option  selected  value="Al-Fanar Gas">Al-Fanar Gas</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' placeholder="Particular" class='form-control' />
                                          </td>
                                          
                                          <td>
                                             <input name='ref_no[]'  id="" type='text' placeholder="Ref No."  class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='amount[]' placeholder="Amount"  type='number' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='from[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='to[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                            <input name='due_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='next_billing_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td >
                                             <input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td >
                                             <input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>
                                          </td>
                                     
                                          <td>
                                             <input name='company_to_head[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                        <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='11' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                               
                                                <option  selected  value="Aqua Cool">Aqua Cool</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' placeholder="Particular" class='form-control' />
                                          </td>
                                          
                                          <td>
                                             <input name='ref_no[]'  id="" type='text' placeholder="Ref No."  class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='amount[]' placeholder="Amount"  type='number' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td>
                                             <input name='from[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='to[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                            <input name='due_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                          <td>
                                             <input name='next_billing_date[]'  type='date' class='form-control ' style="width:150px;"/>
                                          </td>
                                         
                                          <td >
                                             <input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td >
                                             <input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>
                                          </td>
                                           <td>
                                             <input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>
                                          </td>
                                     
                                          <td>
                                             <input name='company_to_head[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>




                     <br>
                     


                     <div class="row">
                        <div class="col-md-12">
                           <label class="text-primary">For Internal Use</small></label>
                           <div style="border: 1px solid grey;">
                              <center>
                                 <!--<input type="button" id="security-row" class="btn btn-primary"  value="Add More" />-->
                                 <input id='internaluse-row' class='btn btn-primary' type='button' value='Add More' />
                              </center>
                              <div class="table-responsive">
                                 <table id="complex_header" class="table table-striped table-bordered display"
                                    style="width:100%">
                                    <thead>
                                       <tr>
                                          <th>Sr #</th>
                                          <th>Accounts Head</th>
                                          <th>Particulars</th>
                                          <th>Party</th>
                                          <th>Terms</th>
                                          <th>Percentage/Fixed Amount</th>
                                          <th>Rent Amount</th>
                                          <th>
                                             <center>Commission Amount</center>
                                          </th>
                                          <th>Commission Terms</th>
                                          <th>Email</th>
                                          <th>
                                             <center>VAT</center>
                                          </th>
                                          <th>
                                             <center>Net Amount</center>
                                          </th>
                                          <th>
                                             <center>VAT Amount</center>
                                          </th>
                                          <th>
                                             <center>With VAT Amount</center>
                                          </th>
                                          <th>
                                             <center>charge to owner or no</center>
                                          </th>
                                       </tr>
                                    </thead>
                                     <tbody id="internaluse-body">
                                       <tr id="row0">
                                          <td>
                                             <input name='serial_no[]' readonly  value='1' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                              <select class="form-control" name="accounts_head[]">
                                                  <option selected value="Commission">Commission</option>
                                              </select>
                                          </td>
                                          <td>
                                            <input type="text" class="form-control" name="particulars_internal_use[]" placeholder="Particulars">
                                          </td>
                                          <td>
                                             <input type="text" class="form-control" name="party[]" placeholder="Party">
                                          </td>
                                          <td >
                                            <select class="form-control" name="terms[]">
                                                <option value="" selected disabled >Select One</option>
                                              <option value="Fixed Amount">Fixed Amount</option>
                                              <option value="Percentage">Percentage</option>
                                           </select>
                                          </td>
                                          <td ><input type="number" class="form-control" name="percentage_fixed_amount[]" placeholder="Enter Percentage or Enter Fixed Amount">
                                          </td>
                                          <td >
                                             <input type="number" class="form-control" name="rent_amount[]" placeholder="Rent Amount">
                                          </td>
                                          <td >
                                              <input type="number" class="form-control" name="commision_amount[]" placeholder="Commission Amount">
                                          </td>
                                          <td>
                                            <select class="form-control" name="vat_tax_type[]" >
                                              <option value="" selected disabled>Select Vat Type</option>
                                              <option value="Vat Included">Vat Included</option>
                                              <option value="Vat Not Included">Vat Not Included</option>
                                              <option value="No Vat">No Vat</option>
                                           </select>
                                          </td>
                                          <td>
                                             <input type="email" class="form-control" name="email[]" placeholder="Email">
                                          </td>
                                          <td>
                                            <input type="number" class="form-control" name="vat1[]" placeholder="VAT Amount">
                                          </td>
                                          <td id="vat_amount">
                                             <input type="number" class="form-control" name="net_amount1[]" placeholder="Net Amount">
                                          </td>
                                          <td>
                                               <input type="number" class="form-control" name="vat_amount1[]" placeholder="VAT Amount">
                                          </td>
                                          <td>
                                               <input type="number" class="form-control" name="with_vat_amount1[]" placeholder="Without VAT Amount">
                                          </td>
                                          <td >
                                             <input type="checkbox" class="form-control" name="charge_to_owner[]" >
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>

                                        <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='2' readonly type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                              <select class="form-control" name="accounts_head[]">
                                                  <option selected value="Management Fee">Management Fee</option>
                                              </select>
                                          </td>
                                          <td>
                                            <input type="text" class="form-control" name="particulars_internal_use[]" placeholder="Particular">
                                          </td>
                                          <td>
                                             <input type="text" class="form-control" name="party[]" placeholder="Party">
                                          </td>
                                          <td id="date_from">
                                            <select class="form-control" name="terms[]">
                                                  <option value="" selected disabled>Select One</option>
                                              <option value="Fixed Amount">Fixed Amount</option>
                                              <option value="Percentage">Percentage</option>
                                           </select>
                                          </td>
                                          <td ><input type="number" class="form-control" name="percentage_fixed_amount[]" placeholder="Enter Percentage or Enter Fixed Amount">
                                          </td>
                                          <td >
                                             <input type="number" class="form-control" name="rent_amount[]" placeholder="Rent Amount">
                                          </td>
                                          <td >
                                              <input type="number" class="form-control" name="commision_amount[]" placeholder="Commission Amount">
                                          </td>
                                          <td>
                                            <select class="form-control" name="vat_tax_type[]" >
                                              <option value="" selected disabled>Select Vat Type</option>
                                              <option value="Vat Included">Vat Included</option>
                                              <option value="Vat Not Included">Vat Not Included</option>
                                              <option value="No Vat">No Vat</option>
                                           </select>
                                          </td>
                                          <td>
                                             <input type="email" class="form-control" name="email[]" placeholder="Email">
                                          </td>
                                          <td>
                                            <input type="number" class="form-control" name="vat1[]" placeholder="VAT Amount">
                                          </td>
                                          <td id="vat_amount">
                                             <input type="number" class="form-control" name="net_amount1[]" placeholder="Net Amount">
                                          </td>
                                          <td>
                                               <input type="number" class="form-control" name="vat_amount1[]" placeholder=" VAT Amount">
                                          </td>
                                           <td>
                                               <input type="number" class="form-control" name="with_vat_amount1[]" placeholder="Without VAT Amount">
                                          </td>
                                          <td >
                                             <input type="checkbox" class="form-control" name="charge_to_owner[]" >
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                    
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>



                     <br>
                   
                  </div>
                  <br>
                  <div class="row">
                     <div class="col-md-12">
                        <div style="border: 1px solid grey;">
                           <center>
                              <input id='add-row' class='btn btn-primary' type='button' value='Add More' />
                           </center>
                           <table id="test-table" class="table table-condensed">
                              <thead>
                                 <tr>
                                    <th>Attachments</th>
                                    <th>Comment</th>
                                 </tr>
                              </thead>
                              <tbody id="test-body">
                                 <tr id="row0">
                                    <td>
                                       <input name='attachment[]'  type='file' class='form-control' />
                                    </td>
                                    <td>
                                       <input name='comments[]'  placeholder="Comment" type='text' class='form-control input-md' />
                                    </td>
                                    <td>
                                       <input class='delete-row btn btn-primary' type='button' value='Delete' />
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div class="container">
                      <div class="row">
                        <div class="text-center mt-4 col-md-2">
                         <button class="btn btn-started">Save</button>
                        </div>
                        <div class="text-center mt-4 col-md-2">
                         <button class="btn btn-started">Email To Owner</button>
                        </div>
                        <div class="text-center mt-4 col-md-1">
                       
                        </div>
                        <div class="text-center mt-4 col-md-3">
                         <button class="btn btn-started">Email To Company Manager</button>
                        </div>
                        <div class="text-center mt-4 col-md-2">
                         <button class="btn btn-started">Email To </button>
                        </div>
                        <div class="text-center mt-4 col-md-2">
                         <button class="btn btn-started">Email To Tenant</button>
                        </div>
                      </div>
                  </div>
                  
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
   let url = '/get/buildings';
   let url1 = '/get/floors';
   let url2 = '/get/flats';
   let url3 = '/get/flats/assign/lanlord';
   // $(document).on("change" , "#selectProject" , function () {
   //     let value = $(this).val();
   //     console.log('value');
   //     console.log(value);
   //     $('#selectBuilding').html('');
   //     $.ajax({
   //         url:url+'/'+value,
   //         Type:'GET',
   //         success: function (response) {
   //             if(response.status == true) {
   //                 let html = '<option>Select Building</option>';
   //                 $('#selectBuilding').append(html);
   //                 response.data.map((d) => {
   //                     html = '<option value="'+ d.id +'">'+ d.name +'</option>';
   //                     $('#selectBuilding').append(html);
   //                 });
   //             }
   //         },
   //         error: function (error) {
   //             console.log(error);
   //             console.log(error);
   //         }
   //     })
   // });
   function selectproject(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_buildings')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, pro_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectBuilding').html(msg);
   });
   }
   
   
      function selectbuilding(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_floors')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, building_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectFloor').html(msg);
   });
   }
   
   
    function selectfloor(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_flats')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, building_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectFlat').html(msg);
   });
   }
   
   
   
        function selectflat(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_lanlords')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, flat_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#LanLord').val(msg);
   });
   }
   
   
   $(document).on("change" , "#no_installments" , function () {
       $('#rowTable').html('');
       let no_installments = $(this).val();
       let totalAmount = $('#totalAmount').val();
       console.log('value');
       console.log(no_installments);
       console.log(totalAmount);
       let totalA = totalAmount/no_installments;
       for (var i = 1; i <= no_installments; i++) {
           console.log('count iiiii');
           console.log(i);
           let Tblhtml = '<tr>' +
               '<td><input type="number" name="no_installment[]" value="'+ i +'" /></td>' +
               '<td><input type="number" name="amount[]" value="'+ totalA +'" /></td>' +
               '<td><input type="text" name="particular[]" value="" /></td>' +
               '<td><select name="pay_type" style="width:150px;" class="form-control">' +
                   '<option> select type </otpion>' +
                   '<option value="cash"> Cash </otpion>' +
                   '<option value="check"> Check </otpion>' +
               '</select></td>' +
               '<td><input type="number" name="check_no[]" /></td>' +
               '<td><input type="date" name="check_date[]" /></td>' +
               '<td><input type="text" name="check_issue[]" /></td>' +
               '<td><input type="text" name="party_name[]" /></td>' +
               '<td>' +
                   '<select id="selectOpt" name="selectOpt[]">' +
                       '<option>select option</option>' +
                       '<option>company</option>' +
                       '<option>landlord</option>' +
                   '</select>' +
               '</td>' +
               '</tr>';
           $('#rowTable').append(Tblhtml);
       }
       $('#rowTableDisplay').show();
   
   });
   
   
</script>
<script type="text/javascript">
   // Add row
   var row=1;
   $(document).on("click", "#add-row", function () {
       var new_row = '<tr id="row' + row + '"><td><input name="attachment[]" type="file" class="form-control" /></td><td><input name="comments[]" type="text" placeholder="Comment" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';
   
       $('#test-body').append(new_row);
       row++;
       return false;
   });
   
   // Remove criterion
   $(document).on("click", ".delete-row", function () {
       //  alert("deleting row#"+row);
       if(row>1) {
           $(this).closest('tr').remove();
           row--;
       }
       return false;
   });
   
   // Add row
   var row1=1;
   $(document).on("click", "#security-row", function () { 
       
      
       
       var new_row = '<tr id="row' + row1 + '">'
           +
               '<td><input name="serial_no[]"  value="1" type="text" class="form-control" style="width:40px;"/>'+
               '</td>'+
                '<td>'+
                     '<select class="form-control" name="on_ac_of[]" style="width:200px;">'+
                      '<option  selected  disabled value="">Select One</option>'+
                      '<option  value="Empower">Empower</option>'+
                      '<option    value="Emicool">Emicool</option>'+
                      '<option  value="Dewa">Dewa</option>'+
                      '<option  value="Du">Du</option>'+
                      '<option  value="Etisalat">Etisalat</option>'+
                      '<option  value="My Ista">My Ista</option>'+
                      '<option  value="Service Charges">Service Charges</option>'+
                      '<option  value="New Access Card">New Access Card</option>'+
                      '<option  value="Lootha Gas">Lootha Gas</option>'+
                      '<option  value="Al-Fanar Gas">Al-Fanar Gas</option>'+
                      '<option  value="Aqua Cool">Aqua Cool</option>'+
                     '</select>'+
                                         '</td>'+
                                          '<td>'+
                                             '<input name="particular[]"  style="width:150px;" type="text" placeholder="Particular" class="form-control" />'+
                                          '</td>'+
                                          '<td>'+
                                             '<input name="ref_no[]"  id="" type="text" placeholder="Ref No."  class="form-control " style="width:150px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input name="amount[]" placeholder="Amount"  type="number" class="form-control " style="width:150px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input name="from[]"  type="date" class="form-control " style="width:150px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input name="to[]"  type="date" class="form-control " style="width:150px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                            '<input name="due_date[]"  type="date" class="form-control " style="width:150px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input name="next_billing_date[]"  type="date" class="form-control " style="width:150px;"/>'+
                                          '</td>'+
                                          '<td >'+
                                             '<input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                           '<td >'+
                                             '<input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                           '<td>'+
                                             '<input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                           '<td>'+
                                             '<input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                           '<td>'+
                                             '<input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                           '<td>'+
                                             '<input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                           '<td>'+
                                             '<input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input name="company_to_head[]""  type="checkbox" Placeholder="With Vat" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input class="security btn btn-primary" type="button" value="Delete" />'+
                                          '</td>'+
           '</tr>';
       $('#test-body1').append(new_row);
       row1++;
       return false;
   });
   
   // Remove criterion
   $(document).on("click", ".security", function () {
       //  alert("deleting row#"+row);
       if(row1>1) {
           $(this).closest('tr').remove();
           row1--;
       }
       return false;
   });
   
   
   
   // Add row
   var row2=1;
   $(document).on("click", "#other-row", function () {
       var new_row = '<tr id="row' + row2 + '">'
           +
           '<td>' +
           '<input name="other_serial_no[]" type="text" class="form-control" /></td>' +
           '<td><input name="other_particular[]" type="text"  class="form-control" /></td>' +
           '<td><input name="other_amount[]" type="number"  class="form-control" /></td>' +
           '<td><input name="other_vat[]" type="number" class="form-control" /></td>' +
           '<td><input class="other-row btn btn-primary" type="button" value="Delete" /></td>'
           +
           '</tr>';
       $('#test-body2').append(new_row);
       row1++;
       return false;
   });
   
   // Remove criterion
   $(document).on("click", ".other-row", function () {
       //  alert("deleting row#"+row);
       if(row1>1) {
           $(this).closest('tr').remove();
           row1--;
       }
       return false;
   });
   
   $(document).ready(function(){
       $('#on_ac_of').on('change', function() {
           if ( this.value == 'rent')
               //.....................^.......
   
           {
               $("#date_from").show();
               $("#date_from_empty").hide();
               $("#date_to").show();
               $("#date_to_empty").hide();
               
           }
           else
           {
               $("#date_from").hide();
               $("#date_from_empty").show();
               $("#date_to").hide();
               $("#date_to_empty").show();
               
           }
       });
   
   });
   
      $(document).ready(function(){
       $('#vat_tax').on('change', function() {
           if ( this.value == 'no_vat')
               //.....................^.......
   
           {
              
               
                $("#with_vat_amount").hide();
               $("#with_vat_amount_empty").show();
               $("#vat_amount").hide();
               $("#vat_amount_empty").show();
               
           }
           else
           {
                $("#with_vat_amount").show();
               $("#with_vat_amount_empty").hide();
               $("#vat_amount").show();
               $("#vat_amount_empty").hide();
               
           }
       });
   
   });
   
   
      $(document).on("click", "#internaluse-row", function () { 
       
      
       
       var new_row = '<tr id="row' + row1 + '">'
           +
               '<td><input name="serial_no[]" readonly   type="text" class="form-control" style="width:40px;"/>'+
                '</td>'+
                 '<td>'+
                      '<select class="form-control" name="accounts_head[]">'+
                      '<option  value="" selected disabled >Select One</option>'+
                           '<option  value="Commission">Commission</option>'+
                           '<option  value="Management Fee">Management Fee</option>'+
                                              '</select>'+
                '</td>'+
                 '<td>'+
                   '<input type="text" class="form-control" name="particulars_internal_use[]" placeholder="Particulars">'+
                 '</td>'+
                 '<td>'+
                   '<input type="text" class="form-control" name="party[]" placeholder="Party">'+
                 '</td>'+
                  '<td id="date_from">'+
                  '<select class="form-control" name="terms[]">'+
                  '<option value="" selected disabled>Select One</option>'+
                    '<option value="Fixed Amount">Fixed Amount</option>'+
                     '<option value="Percentage">Percentage</option>'+
                                           '</select>'+
                                          '</td>'+
                                          '<td >'+
                '<input type="number" class="form-control" name="percentage_fixed_amount[]" placeholder="Enter Percentage or Enter Fixed Amount">'+
                                          '</td>'+
                                          '<td >'+
                        '<input type="number" class="form-control" name="rent_amount[]" placeholder="Rent Amount">'+
                                          '</td>'+
                                          '<td >'+
                                    '<input type="number" class="form-control" name="commision_amount[]" placeholder="Commission Amount">'+
                                          '</td>'+
                                          '<td>'+
                                        '<select class="form-control" name="vat_tax_type[]" >'+    
                                              '<option value="" selected disabled>Select Vat Type</option>'+
                                              '<option value="Vat Included">Vat Included</option>'+
                                              '<option value="Vat Not Included">Vat Not Included</option>'+
                                              '<option value="No Vat">No Vat</option>'+
                                           '</select>'+
                                          '</td>'+
                                          '<td>'+
                                            '<input type="email" class="form-control" name="email[]" placeholder="Email">'+
                                          '</td>'+
                                          '<td>'+
                                            '<input type="number" class="form-control" name="vat1[]" placeholder="VAT Amount">'+
                                          '</td>'+
                                          '<td id="vat_amount">'+
                                             '<input type="number" class="form-control" name="net_amount1[]" placeholder="Net Amount">'+
                                          '</td>'+
                                          '<td>'+
                                               '<input type="number" class="form-control" name="vat_amount1[]" placeholder=" VAT Amount">'+
                                          '</td>'+
                                         
                                          '<td>'+
                                               '<input type="number" class="form-control" name="with_vat_amount1[]" placeholder="Without VAT Amount">'+
                                          '</td>'+
                                           '<td >'+
                                             '<input type="checkbox" class="form-control" name="charge_to_owner[]">'+
                                          '</td>'+
                                          '<td>'+
                                             '<input class="security btn btn-primary" type="button" value="Delete" />'+
                                          '</td>'+
           '</tr>';
       $('#internaluse-body').append(new_row);
       row1++;
       return false;
   });
   
   
   
   
   
   
   
   
   
   
   
       
       
       
   function assign_date_in()
   {
   
   var a =$('#check_in_date').val();
   $('.check_date_security').val(a);
   }
   
   function assign_date_out(){
      var a =$('#check_out_date').val();
   $('.check_issue_security').val(a);
   }
   
   function assign_vat_tax(value)
   {
   //   <option value="Vat Included">Vat Included</option>
   //         <option value="Vat Not Included">Vat Not Included</option>
   //         <option value="No Vat">No Vat</option>
   var amount= $('#party_name_security').val();
   if(amount=="" || amount==null){
       alert("Please Enter the Amount");
       ('#party_name_security').focus();
       return false;
   }
   if(value=="Vat Included"){
       var net_amount = (amount*5)/100;
       var vat_amount = amount-net_amount;
     }   
   }
   
</script>
@endsection
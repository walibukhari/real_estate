@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'add_contracts'
])

@section('content')
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
             @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('error') !!}</strong>
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
        <style type="text/css">
            .filelabel {
    width: 100%;
    border: 2px dashed grey;
    border-radius: 5px;
    display: block;
    padding: 5px;
    transition: border 300ms ease;
    cursor: pointer;
    text-align: center;
    margin: 0;
}
.filelabel i {
    display: block;
    font-size: 30px;
    padding-bottom: 5px;
}
.filelabel i,
.filelabel .title {
  color: grey;
  transition: 200ms color;
}
.filelabel:hover {
  border: 2px solid #1665c4;
}
.filelabel:hover i,
.filelabel:hover .title {
  color: #1665c4;
}
#FileInput{
    display:none;
}
        </style>
            <div class="row">
  <div class="col-12">
  <div class="row">
      <div class="col-md-3">

      </div>
       <div class="col-md-3">

      </div>
       <div class="col-md-4">

      </div>
       <div class="col-md-2">
            <a href="{{route('admin.create_new_contract')}}" type="button" class="btn btn-info " > Create New</a>
      </div>
  </div>
    <div class="material-card card">
      <div class="card-body">

        <h4 class="card-title">Long term Rental Contracts List</h4>
        <h6 class="card-subtitle">
        </h6><br>
        <div class="table-responsive">
          <table id="complex_header" class="table table-striped table-bordered display"
          style="width:100%">
          <thead>

            <tr>
              <th>Sr#</th>
              <th>Building</th>
              <th>Floor</th>
              <th>Flat</th>
              <th>Tenant</th>
              <th>Flat Security</th>
              <th>From</th>
              <th>To</th>
              <th>Contract Amount</th>
              <th>No of Installments</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
          @foreach($contracts as $contract)
            <tr>
              <td>{{$contract->id}}</td>
              <td>{{$contract->building_id}}</td>
              <td>{{$contract->floor_id}}</td>
              <td>{{$contract->flat_id}}</td>
              <td>{{$contract->tenant}}</td>
              <td>{{$contract->flat_security}}</td>
              <td>{{$contract->contract_from}}</td>
              <td>{{$contract->contract_to}}</td>
              <td>{{$contract->installments_no}}</td>
              <td>{{$contract->installments_no}}</td>
              <td>{{$contract->status}}</td>
              <td><a href="{{route('admin.edit_contract',[$contract->id])}}" class="btn btn-primary btn-sm">Edit</a>-
              <a href="{{route('admin.view_contract',[$contract->id])}}"  class="btn btn-info btn-sm">Print</a>
              </td>
            </tr>
          @endforeach
      </tbody>
      <tfoot>
        <tr>
            <th>Sr#</th>
              <th>Building</th>
              <th>Floor</th>
              <th>Flat</th>
              <th>Tenant</th>
              <th>Flat Security</th>
              <th>From</th>
              <th>To</th>
              <th>Contract Amount</th>
              <th>No of Installments</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
      </tfoot>
     </table>
    </div>

            </div>
   </div>
  </div>
 </div>
</div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
    // Add row
  var row=1;
  $(document).on("click", "#add-row", function () {
  var new_row = '<tr id="row' + row + '"><td><input name="from_value' + row + '" type="file" class="form-control" /></td><td><input name="to_value' + row + '" type="text" placeholder="Comment" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';

  $('#test-body').append(new_row);
  row++;
  return false;
  });

  // Remove criterion
  $(document).on("click", ".delete-row", function () {
  //  alert("deleting row#"+row);
    if(row>1) {
      $(this).closest('tr').remove();
      row--;
    }
  return false;
  });

</script>

@endsection

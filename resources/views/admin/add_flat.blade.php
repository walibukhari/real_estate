@extends('layouts.app', [
'class' => '',
'elementActive' => 'add_flat'
])

@section('content')
<div class="content">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  @if (session('password_status'))
  <div class="alert alert-success" role="alert">
    {{ session('password_status') }}
  </div>
  @endif
  @if (Session::has('error'))
  <div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{!! Session('error') !!}</strong>
  </div>
  @endif

  @if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div class="row">
    <div class="col-12">    
      <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-3">

        </div>
        <div class="col-md-4">

        </div>
        <div class="col-md-2">
          <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Create New</button>
        </div>
      </div>     
      <div class="material-card card">
        <div class="card-body">

          <h4 class="card-title">Apartments / Offices List</h4>
          <h6 class="card-subtitle">
          </h6><br>
          <div class="table-responsive">
            <table id="complex_header" class="table table-striped table-bordered display"
            style="width:100%">
            <thead>

              <tr>
                <th>#</th>
                <th>Building</th>
                <th>Apartment No.</th>
                <th>Description</th>
                <th>Status</th>
                <th>Facilities</th>
                <th>Actions</th>
              </tr>

            </thead>
            <tbody>
             @foreach($flatlist as $flatlists)
             <tr>

              <td>{{$counter++}}</td>
              <td>{{$flatlists->Building_name->name}}</td>
              <td>{{$flatlists->unit_code}}</td>
              <td>{{$flatlists->rent_range}}</td>
              <td>{{$flatlists->status}}</td>

              <td>
               <center> 
                <button type="button" class="btn btn-info " data-toggle="modal" data-target="#View_Variations{{$flatlists->id}}">View Facilities</button>
                <!-- Variations Modal start-->
                <div class="modal fade" id="View_Variations{{$flatlists->id}}" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Variations Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">

                        <div class="container">
                          <div class="row">
                            <div class="col-12">

                              <table id="complex_header" class="table table-striped table-bordered display">
                                <thead>
                                  <tr>
                                    <th>Flat Facilties</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($flatlists->Flat_facilitie as $r)
                                  <tr>
                                   <td>{{$r->Facilitie_name->name}}</td>
                                 </tr>
                                 @endforeach
                               </tbody>  
                             </table>

                           </div>
                         </div>
                       </div>

                     </div>
                     <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Variations Modal End-->

            </center>  
          </td>
          <td>

            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editModal{{$flatlists->id}}">Eidt </button>

          <!--  </td>
          </tr> -->
          <div class="modal fade" id="editModal{{$flatlists->id}}"  role="dialog" >
            <div class="modal-dialog modal-lg" >
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Falts</h5>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                 <div class="p-5">
                   <form class="col-md-12" action="{{url('Admin/edit_flats')}}" method="POST" enctype="multipart/form-data">
                     @csrf
                     <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">Building Name</label>
                        <input type="hidden" name="flatlists_id" value="{{$flatlists->id}}">
                        <select class="form-control" name="building_id" required="">
                          <option value="" selected disabled>Select Building</option>
                          @foreach($buildinglist as $buildinglists)
                          <option @if($flatlists->building_id==$buildinglists->id) selected @endif value="{{$buildinglists->id}}">{{$buildinglists->name}}-{{$buildinglists->status}}</option>
                        @endforeach                        </select>
                      </div>
                        <div class="col-md-6">
            <label class="text-primary">Floor Name</label>
            <select class="form-control" name="floor_id" required="">
              <option value="" selected  disabled>Select Floor</option>
              @foreach($floor_list as $floor_lists)
              <option @if($flatlists->floor_id==$floor_lists->id) selected @endif value="{{$floor_lists->id}}">{{$floor_lists->name}}</option>
            @endforeach                    
             </select>
          </div>
        </div>
                    <div class="form-group row">
                         
                     <div class="col-md-6">
                      <label class="text-primary">Flat Property Type</label>
                      <select class="form-control" name="flat_type_id" required="">
                        <option value="" selected  disabled>Select Flat Type</option>
                        @foreach($flat_type as $flat_types)
                        <option @if($flatlists->flat_type_id==$flat_types->id) selected @endif value="{{$flat_types->id}}">{{$flat_types->name}}</option>
                      @endforeach                     </select>
                    </div>
                    <div class="col-md-6">
                      <label class="text-primary">Flat  Type</label>
                      <select class="form-control" name="flat_pro_type_id" required="">
                       <option value="" selected  disabled>Select Flat Property Type</option>
                       @foreach($flat_pro_type as $flat_pro_types)
                       <option @if($flatlists->flat_pro_type_id==$flat_pro_types->id) selected @endif value="{{$flat_pro_types->id}}">{{$flat_pro_types->name}}</option>
                     @endforeach                          </select>
                   </div>
                 </div>
                 <div class="form-group row">
                     
                     
        <div class="col-md-6">
              <label class="text-primary">Apartment/Office Number</label>
              <input type="text"  class="form-control"  name="unit_code" placeholder="For Example: 110" value="{{$flatlists->unit_code}}" required="">
        </div>
         <div class="col-md-6">
          <label class="text-primary">Minimum Rent Range <small>(AED)</small></label>
          <input type="text"  class="form-control"  name="rent_range" value="{{$flatlists->rent_range}}" required="">
        </div>
                     
        </div> 
         <div class="form-group row">
           
         <div class="col-md-6">
          <label class="text-primary">Property Size</label>
          <input type="text"  class="form-control"  name="property_size" value="{{$flatlists->property_size}}" required="">
        </div>
      </div>    
       <div class="form-group row">
           <div class="col-md-6">
              <label class="text-primary">Management Fee Type</label>
              <select class="form-control" name="management_fee_type">
                  <option value="" selected disabled>Select Type</option>
                  @if($flatlists->management_fee_type=='Fixed Amount')
                  <option selected value="Fixed Amount">Fixed Amount</option>
                  <option value="In Percentage">In Percentage</option>
                  
                  @elseif($flatlists->management_fee_type=='In Percentage')
                  <option  value="Fixed Amount">Fixed Amount</option>
                  <option selected value="In Percentage">In Percentage</option>
                  @else
                   <option  value="Fixed Amount">Fixed Amount</option>
                  <option  value="In Percentage">In Percentage</option>
                  @endif
              </select>
            </div>
         <div class="col-md-6">
          <label class="text-primary">Management Fee </label>
          <input type="number"  class="form-control" placeholder="Enter Number only"  name="management_fee" value="{{$flatlists->management_fee}}" required="">
        </div>
      </div> 
                <div class="form-group row">
                    <div class="col-md-6">
              <label class="text-primary">Assign Owner</label>
              <select class="form-control" name="owner_id">
                  <option value="" selected disabled>Select Owner</option>
                  @foreach($landlored as $landloreds)
                  <option @if($flatlists->owner_id==$landloreds->id) selected @endif value="{{$landloreds->id}}">{{$landloreds->name}} - (Landlord)</option>
                  @endforeach
              </select>
             
            </div>
                    <div class="col-md-6">
                    <label class="text-primary">Status</label>
                    <select class="form-control" name="status">
                      <option value="" disabled selected>Select Status</option>
                      @if($flatlists->status=='Pending')
                      <option selected value="Pending">Pending</option>
                      <option  value="Approved">Approved</option>
                      <option  value="Disapproved">Disapproved</option>
                      @endif
                      @if($flatlists->status=='Approved')
                      <option selected value="Approved">Approved</option>
                      <option  value="Disapproved">Disapproved</option>
                      <option  value="Pending">Pending</option>
                      
                      @endif
                        @if($flatlists->status=='Disapproved')
                      <option selected value="Disapproved">Disapproved</option>
                      <option  value="Pending">Pending</option>
                      <option  value="Approved">Approved</option>
                      @endif
                   </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="text-primary">Flat Facilities</label>
                  <table id="fac_fieds_tbl2{{$flatlists->id}}" class="table table-bordered">
                    <thead>
                      <tr class="text-center">
                        <td colspan="2">Select</td>
                      </tr>
                    </thead>
                    <tfoot>
                        @foreach($flatlists->Flat_facilitie as $r)
                      <tr>
                        <td>
                          <select id="fac_type_id" name="facilitie_type[]" required="" class="form-control">
                          
                         @foreach($faciltie_of_flat as $faciltie_of_flats)

                         <option @if($r->flat_fac_id == $faciltie_of_flats->id) selected @endif value="{{$faciltie_of_flats->id}}">{{$faciltie_of_flats->name}}
                         </option>
                          @endforeach
                            
                               </select></td><td class="text-center">
                                <button type="button" class="btn btn-danger remove-fac-row2">X</button>
                          </td>
                      </tr>
                      @endforeach
                    <tr><td colspan="2">  <button type="button" class="btn btn-primary add-more-fac2" id="add-more-fac2{{$flatlists->id}}" value="{{$flatlists->id}}" onclick="addmore(this.value)"><b>+ Add More</b></button></td></tr>
                    </tfoot>
                  
                  </table>
                </div>
                      <hr>
      
       <div class="row">
                                    <div class="col-md-12"><label class="text-primary">Attachments</label>
                                        <div >
                                           
                                            <table id="test-table" class="table table table-bordered">
                                                <thead>
                                                <tr>
                                                    
                                                  
                                                </tr>
                                                </thead>
                                                
                                                  @if($flatlists->Flat_Attachment)
                                                    @foreach($flatlists->Flat_Attachment  as $attachments)
                                                <tbody id="test-body2{{$flatlists->id}}">
                                                  
                                                <tr id="row1">
                                                    <td>
                                                        <input name='flat_attachments[]'  type='file' class='form-control' />
                                                        <input name='' value="{{$attachments->attachment}}"  type='hidden' class='form-control' />
                                                    </td>
                                                    <td>
                                                        <input name='comments[]'  placeholder="Comment" type='text' value="{{$attachments->comment}}" class='form-control input-md' />
                                                    </td>
                                                    <td>
                                                        <input class='delete-row2 btn btn-primary' type='button' value='Delete' />
                                                    </td>
                                                </tr>
                                              
                                                </tbody>
                                                 @endforeach
                                                 @endif
                                            </table>
                                             <center>
                                                 <button id='add-row2{{$flatlists->id}}' type="button" class='btn btn-primary' value="{{$flatlists->id}}" onclick="addmore_attch(this.value)">Add More</button>
                                                <!--<input id='add-row2' class='btn btn-primary' type='button' value='Add More' />-->
                                                </center>
                                        </div>
                                    </div>
                                </div>
                        <br><br>
                <button  type="submit" name="createNewFlat" class="btn btn-primary btn-user btn-block">
                 Update
               </button>
             </form>
           </div>

         </div>
         <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>

  @endforeach
</tbody>
<tfoot>
  <tr>
   <th>#</th>
   <th>Building</th>
   <th>Unit Code</th>
   <th>Description</th>
   <th>Status</th>
   <th>Facilities</th>
   <th>Actions</th>
 </tr>
</tfoot>
</table>
</div>
</div>
</div>
</div>
</div>




<div class="modal fade" id="myModal"  role="dialog" >
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>New Apartment / Office Registration Form</b></h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="p-5">
         <form class="col-md-12" action="{{url('Admin/save_flats')}}" method="POST" enctype="multipart/form-data">
           @csrf
           <div class="form-group row">
             <div class="col-md-6">
              <label class="text-primary">Building Name</label>
              <select class="form-control" name="building_id" required="">
                <option value="" selected disabled>Select Building</option>
                @foreach($buildinglist as $buildinglists)
                <option value="{{$buildinglists->id}}">{{$buildinglists->name}}-{{$buildinglists->status}}</option>
              @endforeach                        
            </select>
            </div>
            <div class="col-md-6">
            <label class="text-primary">Floor Name</label>
            <select class="form-control" name="floor_id" required="">
              <option value="" selected  disabled>Select Floor</option>
              @foreach($floor_list as $floor_lists)
              <option value="{{$floor_lists->id}}">{{$floor_lists->name}}</option>
            @endforeach                    
             </select>
          </div>
             
           
          </div><br>
          <div class="form-group row">
              
              <div class="col-md-6">
            <label class="text-primary">Flat Property Type</label>
            <select class="form-control" name="flat_type_id" required="">
              <option value="" selected  disabled>Select Flat Type</option>
              @foreach($flat_type as $flat_types)
              <option value="{{$flat_types->id}}">{{$flat_types->name}}</option>
            @endforeach                    
             </select>
          </div>
          
                <div class="col-md-6">
            <label class="text-primary">Flat Type</label>
            <select class="form-control" name="flat_pro_type_id" required="">
             <option value="" selected  disabled>Select Flat Property Type</option>
             @foreach($flat_pro_type as $flat_pro_types)
             <option value="{{$flat_pro_types->id}}">{{$flat_pro_types->name}}</option>
           @endforeach                          </select>
         </div>
           
        
       </div><br>
       <div class="form-group row">
           
           <div class="col-md-6">
              <label class="text-primary">Apartment/Office Number</label>
              <input type="text"  class="form-control"  name="unit_code" placeholder="For Example: 110" value="" required="">
            </div>
            
         <div class="col-md-6">
          <label class="text-primary">Minimum Rent Range <small>(AED)</small></label>
          <input type="text"  class="form-control"  name="rent_range" value="" required="">
        </div>
      </div>    
      
      
      <div class="form-group row">
           
         <div class="col-md-6">
          <label class="text-primary">Property Size</label>
          <input type="text"  class="form-control"  name="property_size" value="" required="">
        </div>
      </div>    
      
      
      <br>
       <div class="form-group row">
           <div class="col-md-6">
              <label class="text-primary">Management Fee Type</label>
              <select class="form-control" name="management_fee_type">
                  <option value="" selected disabled>Select Type</option>
                  <option value="Fixed Amount">Fixed Amount</option>
                  <option value="In Percentage">In Percentage</option>
              </select>
            </div>
         <div class="col-md-6">
          <label class="text-primary">Management Fee </label>
          <input type="number"  class="form-control" placeholder="Enter Number only"  name="management_fee" value="" required="">
        </div>
      </div> 
      
       <br>
       <div class="form-group row">
           <div class="col-md-6">
              <label class="text-primary">Assign Owner</label>
              <select class="form-control" name="owner_id">
                  <option value="" selected disabled>Select Owner</option>
                  @foreach($landlored as $landloreds)
                  <option value="{{$landloreds->id}}">{{$landloreds->name}} - (Landlord)</option>
                  @endforeach
              </select>
             
            </div>
            <div class="col-md-2"><lable>.</lable> <a href="{{url('/Add_Landlords')}}" class="btn btn-info" target="_blank">Add New Owner</a></div>
         
      </div> 
      
      <hr>

      <div class="form-group row">
          <div class="col-md-12">
        <label class="text-primary">Apartment/Office Facilities</label>
        <table id="fac_fieds_tbl" class="table table-bordered">
          <thead>
            <tr class="text-center">
              <td colspan="2">Select</td>
            </tr>
          </thead>
          <tfoot>
            <tr class="text-center">
            
              <td colspan="2" ><button type="button" class="btn btn-primary add-more-fac" ><b>+ Add More</b></button></td>
     
            </tr>
          </tfoot>
          <!-- Coming form ajax-->
        </table>
      </div>
      </div>
      
      <hr>
      
       <div class="row">
                                    <div class="col-md-12"><label class="text-primary">Attachments</label>
                                        <div >
                                           
                                            <table id="test-table" class="table table table-bordered">
                                                <thead>
                                                <tr>
                                                    
                                                  
                                                </tr>
                                                </thead>
                                                <tbody id="test-body">
                                                <tr id="row0">
                                                    <td>
                                                        <input name='flat_attachments[]'  type='file' class='form-control' />
                                                    </td>
                                                    <td>
                                                        <input name='comments[]'  placeholder="Comment" type='text' class='form-control input-md' />
                                                    </td>
                                                    <td>
                                                        <input class='delete-row btn btn-primary' type='button' value='Delete' />
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                             <center>
                                                <input id='add-row' class='btn btn-primary' type='button' value='Add More' /></center>
                                        </div>
                                    </div>
                                </div>
                                
                                <br><br>

      <button  type="submit" name="createNewFlat" class="btn btn-primary btn-user btn-block">
       Create
     </button>
   </form>
 </div>

</div>
<div class="modal-footer">
  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
</div>
</div>
</div>
</div>

</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
     //add new amount field
     add_fac_row();
  //   add_fac_row2();
     $(document).on('click', '.add-more-fac', function(){
      add_fac_row();
    });

 
     //function to add new row from amount
     function add_fac_row(){
       var html = '';
       html +='<tr>';
       html +='<td>';
       html +='<select id="fac_type_id" name="facilitie_type[]" required="" class="form-control">';
       html +='<option value="" selected disabled>Select Facilities</option> @foreach($faciltie_of_flat as $faciltie_of_flats) <option value="{{$faciltie_of_flats->id}}">{{$faciltie_of_flats->name}}</option> @endforeach';
       html +='</select>';
       html +='</td>';
       html +='<td class="text-center">';
       html +='<button type="button" class="btn btn-danger remove-fac-row" >X</button>';
       html +='</td>';
       html +='</tr>';
       $('#fac_fieds_tbl').append(html);
     }







      var row=1;
        $(document).on("click", "#add-row", function () {
            var new_row = '<tr id="row' + row + '"><td><input name="flat_attachments[]" type="file" class="form-control" /></td><td><input name="comments[]" placeholder="Comment" type="text" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';

            $('#test-body').append(new_row);
            row++;
            return false;
        });
        
        //for the edit section
        //   $(document).on("click", "#add-row2", function () {
        //     var new_row = '<tr id="row' + row + '"><td><input name="flat_attachments[]" type="file" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';

        //     $('#test-body2').append(new_row);
        //     row++;
        //     return false;
        // });

        // Remove criterion
        $(document).on("click", ".delete-row", function () {
            //  alert("deleting row#"+row);
            if(row>1) {
                $(this).closest('tr').remove();
                row--;
            }
            return false;
        });

     
     //remove amount row
     $(document).on('click', '.remove-fac-row', function(){
       $(this).closest('tr').remove();
     });
       $(document).on('click', '.remove-fac-row2', function(){
       $(this).closest('tr').remove();
     });
   });
   
   
   function addmore(value)
   {
        var html = '';
       html +='<tr>';
       html +='<td>';
       html +='<select id="fac_type_id" name="facilitie_type[]" required="" class="form-control">';
       html +='<option value="" selected disabled>Select Facilities</option> @foreach($faciltie_of_flat as $faciltie_of_flats) <option value="{{$faciltie_of_flats->id}}">{{$faciltie_of_flats->name}}</option> @endforeach';
       html +='</select>';
       html +='</td>';
       html +='<td class="text-center">';
       html +='<button type="button" class="btn btn-danger remove-fac-row2" >X</button>';
       html +='</td>';
       html +='</tr>';
       $('#fac_fieds_tbl2'+value).append(html);  
   }
   
   function addmore_attch(val){
         var new_row = '<tr ><td><input name="flat_attachments[]" type="file" class="form-control" /></td><td><input name="comments[]" placeholder="Comment" type="text" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';

            $('#test-body2'+val).append(new_row);
   }
 </script>
 @endsection

@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'owner_report'
])

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('error') !!}</strong>
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <style type="text/css">
            .filelabel {
                width: 100%;
                border: 2px dashed grey;
                border-radius: 5px;
                display: block;
                padding: 5px;
                transition: border 300ms ease;
                cursor: pointer;
                text-align: center;
                margin: 0;
            }
            .filelabel i {
                display: block;
                font-size: 30px;
                padding-bottom: 5px;
            }
            .filelabel i,
            .filelabel .title {
                color: grey;
                transition: 200ms color;
            }
            .filelabel:hover {
                border: 2px solid #1665c4;
            }
            .filelabel:hover i,
            .filelabel:hover .title {
                color: #1665c4;
            }
            #FileInput{
                display:none;
            }
            .select2-container .select2-selection--single {
                box-sizing: border-box;
                cursor: pointer;
                display: block;
                height: 36px !important;
                user-select: none;
                -webkit-user-select: none;
            }
            h4, .h4{
                margin: 0px;
                font-size: 22px;
                font-weight: 500;
            }
        </style>
        <div class="row">
            <div class="col-12">

                <div class="material-card card" >
                    <div class="card-body" >

                        <!--<center><h2 class="card-title">Owner Report</h2></center>-->
                        <!--<div class="row">-->
                        <!--    <div class="col-md-1"></div>-->
                        <!--     <div class="col-md-2">-->
                        <!--       <input onclick="dd()" type="radio" name="rad" id="bank" />-->
                        <!--       <label><b>Bank Name</b></label>-->
                        <!--     </div>-->

                        <!--     <div class="col-md-2">-->
                        <!--       <input onclick="dd()" type="radio" name="rad" id="account" />-->
                        <!--       <label><b>Amount</b></label>-->
                        <!--     </div>-->

                        <!--     <div class="col-md-2">-->
                        <!--       <input onclick="dd()" type="radio" name="rad" id="voucher" />-->
                        <!--       <label><b>Voucher No.</b></label>-->
                        <!--     </div>-->

                        <!--     <div class="col-md-2">-->
                        <!--       <input onclick="dd()" type="radio" name="rad" id="cheque" />-->
                        <!--       <label><b>Cheque No.</b></label>-->
                        <!--       <input type="hidden" name="no_of_years" name="rad" id="val">-->
                        <!--     </div>-->

                        <!--       <div class="col-md-2">-->
                        <!--       <input onclick="dd()" type="radio" name="rad" id="all" />-->
                        <!--       <label><b>All</b></label>-->
                        <!--     </div>-->

                        <!--</div>-->





                        <div class="row">
                            <div class="col-md-3">
                                <label><b>Owner</b></label>
                                <select id="cmbIdioma" class="form-control" required onchange="getOwnerFlats()" name="owner_id">
                                    <option disabled selected>
                                        Select Owner
                                    </option>
                                    @foreach($owners as $owner)
                                        <option value="{{$owner->id}}">
                                            {{$owner->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label><b>Flat</b></label>
                                <select id="cmbIdioma2" class="form-control" required name="flat_id">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label><b>Start Date</b></label>
                                <input type="date" value="" class="form-control" required name="day1" id="day1"/>
                            </div>
                            <div class="col-md-3">
                                <label><b>End Date</b></label>
                                <input type="date" value="" class="form-control" required name="day2" id="day2"/>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-success" onclick="filterReport()">Generate</button>
                                <button class="btn btn-success" id="buttonPrint" style="display: none;">Print</button>
                            </div>
                        </div>
                    </div>
                    <br><br>



                    <!--class="card-body"-->

                </div>
                <div class="material-card card" >
                    <div class="card-body" >

{{--                        <div class="row">--}}
{{--                            <div class="col-md-4">--}}
{{--                                <h3>Owner Report</h3>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-4">--}}

{{--                                <!--<br>-->--}}
{{--                                <!--<p>Office No. 1508 Damac Smart Heights, Tecom, Dubai. U.A.E</p>-->--}}
{{--                            </div>--}}
{{--                            <div class="col-md-4">--}}
{{--                                <table>--}}
{{--                                    <thead>--}}
{{--                                    <br>--}}
{{--                                    <tr>--}}
{{--                                        <th>Voucher Type:<th>--}}
{{--                                        <td></td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th>TRN:<th>--}}
{{--                                        <td></td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th>Date:<th>--}}
{{--                                        <td></td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th>Voucher No:<th>--}}
{{--                                        <td></td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th>Ref No:<th>--}}
{{--                                        <td></td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th>Method of Payment :<th>--}}
{{--                                        <td></td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th>Status : <th>--}}
{{--                                        <td></td>--}}
{{--                                    </tr>--}}
{{--                                    </thead>--}}
{{--                                </table>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="table-responsive" id="print_summery">
                            <div class="table-responsive" style="border:1px solid #000; ">
                                <table class="" id="" width="100%" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td colspan="2" ><img src="https://gfre.alphasmspk.com/uploads/370X180_20200829072449.png" width="80px" height="80px" alt="banner" style="margin-right:5%;"></td>
                                            <td colspan="8" style="text-align:center;">
                                                <h3 style="">Real Estate Broker LLC</h3><br>
                                                <p style="font-size:10px">Office No. 1508 Damac Smart Heights, Tecom, Dubai. U.A.E</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="10" style="border-bottom: 1px solid black;"></td>
                                        </tr>
                                        <tr style="text-align:center; background-color:#bcbaba;border-bottom: 1px solid #000;">
                                            <td colspan="2" style="background-color: #bcbaba;border-left:1px solid #000;"></td>
                                            <td colspan="8" style="background-color: #bcbaba;"> <h4>Funds Inflow and Outlflow Statement</h4></td>
                                        </tr>
                                        <tr>
                                            <td colspan="10" style="border-top:1px solid #000;"><br>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid #000;background-color:#bcbaba;text-align:center; font-size:12px;">
                                            <td colspan="" style="border-top: 1px solid #000;" width="5%">Date</td>
                                            <td colspan="" style="border-top: 1px solid #000;">#</td>
                                            <td colspan="" style="border-top: 1px solid #000;">Name</td>
                                            <td colspan="" style="border-top: 1px solid #000;">Flat Code</td>
                                            <td colspan="" style="border-top: 1px solid #000;">Debit (AED)</td>
                                            <td colspan="" style="border-top: 1px solid #000;">Credit (AED)</td>
                                        </tr>
                                    </tbody>
                                    <tbody id="renderData">
                                    </tbody></table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script type="text/javascript">
        function printData()
        {
            var divToPrint=document.getElementById("print_summery");
            newWin= window.open("");
            newWin.document.write(divToPrint.outerHTML);
            newWin.print();
            newWin.close();
        }

        $('#buttonPrint').on('click',function(){
            printData();
        })
        $("#cmbIdioma").select2({
            templateResult: function (idioma) {
                var $span = $("<span>" + idioma.text + "</span>");
                return $span;
            },
            templateSelection: function (idioma) {
                var $span = $("<span>" +idioma.text+ "</span>");
                return $span;
            }
        });

        function getOwnerFlats(){
            let owner_id = $('#cmbIdioma').val();
            console.log('owner_id');
            console.log(owner_id);
            $('#cmbIdioma2').html('');
            $('#cmbIdioma2').append('');
            // /DEMO/Real_Estate/public
            $.ajax({
                url:'/DEMO/Real_Estate/public/Owner/Flats/'+owner_id,
                method:'GET',

                success: function (response) {
                    console.log('response flat_id');
                    console.log(response);
                    $('#cmbIdioma2').append('<option value="all">All</option>');
                    response.data.map(function (data){
                       let  html = '<option value="'+ data.id +'">'+data.unit_code+'</option>'
                        $('#cmbIdioma2').append(html);
                    });
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                }
            })
        }

        $("#cmbIdioma2").select2({
            templateResult: function (idioma) {
                var $span = $("<span>" + idioma.text + "</span>");
                return $span;
            },
            templateSelection: function (idioma) {
                var $span = $("<span>" +idioma.text+ "</span>");
                return $span;
            }
        });

        function filterReport(){
            let owner_id = $('#cmbIdioma').val();
            let flat_id = $('#cmbIdioma2').val();
            let start_date = $('#day1').val();
            let end_date = $('#day2').val();
            if(owner_id == '') {
                alert('please select owner');
            } else if(flat_id == '') {
                alert('please select flat');
            } else if (start_date == '') {
                alert('please select start date');
            } else if(end_date == '') {
                alert('please select end date');
            } else {
                console.log(owner_id);
                console.log(flat_id);
                console.log(start_date);
                console.log(end_date);
                let data = {
                    'owner_id' : owner_id,
                    'flat_id' : flat_id,
                    'start_date' : start_date,
                    'end_date' : end_date
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{route('admin.owner_post')}}',
                    method: 'POST',
                    data: data,

                    success: function (response) {
                        console.log('response');
                        console.log(response.data);
                        $('#renderData').html('');
                        $('#renderData').append('');
                        if (response.data != '') {
                            $('#buttonPrint').show();
                        }
                        response.data.map(function (data) {
                            console.log('data');
                            console.log(data);
                            let html = '<tr style="border: 1px solid #000;text-align:center; font-size:12px;">\n' +
                                '                                        <td colspan="" style="border-top: 1px solid #000;">' + data.created_at + '</td>\n' +
                                '                                        <td colspan="" style="border-top: 1px solid #000;">' + data.id + '</td>\n' +
                                '                                        <td colspan="" style="border-top: 1px solid #000;">' + data.owner_name + '</td>\n' +
                                '                                        <td colspan="" style="border-top: 1px solid #000;">' + data.flat_code + '</td>\n' +
                                '                                        <td colspan="" style="border-top: 1px solid #000;">' + data.debit + '</td>\n' +
                                '                                        <td colspan="" style="border-top: 1px solid #000;">' + data.credit + '</td>\n' +
                                '                                    </tr>';
                            $('#renderData').append(html);
                        });
                    },
                    error: function (error) {
                        console.log('error');
                        console.log(error);
                    }
                })
            }
        }
    </script>
@endsection

@extends('layouts.app', [
'class' => '',
'elementActive' => 'short_term_contract'
])
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="content">
   @if (session('status'))
   <div class="alert alert-success" role="alert">
      {{ session('status') }}
   </div>
   @endif
   @if (session('password_status'))
   <div class="alert alert-success" role="alert">
      {{ session('password_status') }}
   </div>
   @endif
   @if (Session::has('error'))
   <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{!! Session('error') !!}</strong>
   </div>
   @endif
   @if ($errors->any())
   <div class="alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
   @endif
   <style type="text/css">
      .filelabel {
      width: 100%;
      border: 2px dashed grey;
      border-radius: 5px;
      display: block;
      padding: 5px;
      transition: border 300ms ease;
      cursor: pointer;
      text-align: center;
      margin: 0;
      }
      .filelabel i {
      display: block;
      font-size: 30px;
      padding-bottom: 5px;
      }
      .filelabel i,
      .filelabel .title {
      color: grey;
      transition: 200ms color;
      }
      .filelabel:hover {
      border: 2px solid #1665c4;
      }
      .filelabel:hover i,
      .filelabel:hover .title {
      color: #1665c4;
      }
      #FileInput{
      display:none;
      }
   </style>
   <div class="row">
      <div class="col-12">
         <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-2">
               <!--<button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Create New</button>-->
            </div>
         </div>
         <div class="material-card card">
            <div class="card-body">
               <h4 class="card-title">Short Term Tenancy  Contract</h4>
               <h6 class="card-subtitle">
               </h6>
               <br>
               <form class="form" method="POST" action="{{url('/Admin/Save_Short_Contracts')}}" enctype="multipart/form-data">
                  @csrf
                  <div class="container">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="row">
                                            
                                            <div class="col-md-6">
                                                <label class="text-primary">Select Project</label>        
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <a target="_blank" href="{{url('/Add_Project')}}" style="text-decoration:none;" class="pull-right bg-success p-1 text-white">Add New</a>        
                                            </div>
                                            
                                        </div>
                           <select class="form-control" name="project" id="selectProject" onchange="selectproject(this.value)">
                              <option value="" selected disabled>Select project</option>
                              @foreach($projects as $pro)
                              <option value="{{$pro->id}}">{{$pro->name}}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                                            
                                            <div class="col-md-6">
                                               <label class="text-primary">Select Building</label>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <a target="_blank" href="{{url('/Add_Building')}}" style="text-decoration:none;" class="pull-right bg-success p-1 text-white">Add New</a>        
                                            </div>
                                            
                                        </div>
                           <select class="form-control" name="building" id="selectBuilding" onchange="selectbuilding(this.value)">
                           </select>
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                            
                                            <div class="col-md-6">
                                               <label class="text-primary">Select Floor</label>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <a target="_blank" href="{{url('/Add_Floor')}}" style="text-decoration:none;" class="pull-right bg-success p-1 text-white">Add New</a>        
                                            </div>
                                            
                                        </div>
                           <select class="form-control" name="floor" id="selectFloor" onchange="selectfloor(this.value)">
                           </select>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                            
                                            <div class="col-md-6">
                                               <label class="text-primary">Select Flat</label>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <a target="_blank" href="{{url('/Add_Flat')}}" style="text-decoration:none;" class="pull-right bg-success p-1 text-white">Add New</a>        
                                            </div>
                                            
                                        </div>
                           <select class="form-control" name="flat" id="selectFlat" onchange="selectflat(this.value)" >
                           </select>
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-md-6">
                          <div class="row">
                                            
                                            <div class="col-md-6">
                                               <label class="text-primary">Owner Name</label>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <a target="_blank" href="{{url('/Add_Landlords')}}" style="text-decoration:none;" class="pull-right bg-success p-1 text-white">Add New</a>        
                                            </div>
                                            
                                        </div>
                           <input type="text" id="LanLord" name="lanlord_name" class="form-control" />
                        </div>
                        <div class="col-md-6">
                           <div class="row">
                                            
                                            <div class="col-md-6">
                                               <label class="text-primary">Select Tenant</label>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <a target="_blank" href="{{url('/Add_tenants')}}" style="text-decoration:none;" class="pull-right bg-success p-1 text-white">Add New</a>        
                                            </div>
                                            
                                        </div>
                           <select class="form-control" name="tenant_name">
                              <option value="" selected disabled>Select Tenant</option>
                              @foreach($users as $user)
                              <option value="{{$user->id}}">{{$user->name}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary">Name</label>
                           <input type="name" class="form-control" name="name" placeholder="Name">
                        </div>
                        <div class="col-md-6">
                           <label class="text-primary">Email</label>
                           <input type="email" class="form-control" name="email1" placeholder="Email">
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary">Contact 1</label>
                           <input type="number" class="form-control" name="contact1" placeholder="Contact 1">
                        </div>
                        <div class="col-md-6">
                           <label class="text-primary">Contact 2</label>
                           <input type="number" class="form-control" name="contact2" placeholder="Contact 2">
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary">Check In Date</label>
                           <input type="date" class="form-control" name="check_in_date" id="check_in_date" onchange="assign_date_in()"  placeholder="Flat Security">
                        </div>
                        <div class="col-md-6">
                           <label class="text-primary">Check Out Date</label>
                           <input type="date" class="form-control" name="check_out_date" id="check_out_date" onchange="assign_date_out()" placeholder="Flat Security">
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary">Number of Person</label>
                           <input type="number" class="form-control" name="num_of_person" id="num_of_person"  placeholder="Flat Security">
                        </div>
                        <div class="col-md-6">
                           <label class="text-primary">Adults</label>
                           <input type="number" class="form-control" name="adults" placeholder="Adults">
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary">Childrens</label>
                           <input type="number" id="childrens" name="childrens" class="form-control" placeholder="Childrens">
                        </div>
                        <div class="col-md-6">
                           <label class="text-primary">Managemnet Fee</label>
                           <input class="form-control" readonly id="management_fee" name="managment_fee" />
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary">Terms and conditions</label>
                           <textarea class="form-control" name="terms_conditions" placeholder="Terms and conditions"></textarea>
                        </div>
                        <div class="col-md-6">
                           <label class="text-primary">Renew Terms and conditions</label>
                           <textarea class="form-control" name="renew_terms_conditions" placeholder="Renew Terms and conditions"></textarea>
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-md-12">
                           <label class="text-primary">On Account of For Tenants (External Use)</small></label>
                           <div style="border: 1px solid grey;">
                              <center>
                                 <!--<input type="button" id="security-row" class="btn btn-primary"  value="Add More" />-->
                                 <input id='security-row' class='btn btn-primary' type='button' value='Add More' />
                              </center>
                              <div class="table-responsive">
                                 <table id="complex_header" class="table table-striped table-bordered display"
                                    style="width:100%">
                                    <thead>
                                       <tr>
                                          <th>Sr #</th>
                                          <th>On A/C of</th>
                                          <th>Particular</th>
                                          <th>From</th>
                                          <th>To</th>
                                          <th>Amount</th>
                                          <th>
                                             <center>VAT</center>
                                          </th>
                                          <th>
                                             <center>Net Amount</center>
                                          </th>
                                          <th>
                                             <center>VAT Amount</center>
                                          </th>
                                          <th>
                                             <center>With VAT Amount</center>
                                          </th>
                                          <th>
                                             <center>Credit to Owner</center>
                                          </th>
                                       </tr>
                                    </thead>
                                    <tbody id="test-body1">
                                       <tr id="row0">
                                          <td>
                                             <input name="serial_no[]"  value="1" type="text" class="form-control" style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class="form-control" name="on_ac_of[]" style="width:200px;">
                                               
                                                <option  selected  value="Rent">Rent</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name="particular[]"  style="width:150px;" type="text" class="form-control" />
                                          </td>
                                          <td id="date_from">
                                             <input name="check_date_security[]"  id="check_date_security" type="date" class="form-control check_date_security" style="width:150px;"/>
                                          </td>
                                          <td id="date_from_empty" style="display:none;"></td>
                                          <td id="date_to">
                                             <input name="check_issue_security[]"  type="date" class="form-control check_issue_security" style="width:150px;"/>
                                          </td>
                                          <td id="date_to_empty" style="display:none;"></td>
                                          <td>
                                             <input name="amount[]" id="amount" type="number" Placeholder="Amount" class="form-control party_name_security" style="width:100px;"/>
                                          </td>
                                          <td>
                                             <select class="form-control" id="vat_tax" name="vat_tax[]" style="width:100px;" onchange="assign_vat_tax(this.value)">
                                                <option value="" selected disabled>Select Vat Type</option>
                                                <option value="Vat Included">Vat Included</option>
                                                <option value="Vat Not Included">Vat Not Included</option>
                                                <option value="No Vat">No Vat</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name="net_amount[]"  type="number" Placeholder="Net Amount" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount">
                                             <input name="vat_amount[]"  type="number" Placeholder="With Vat" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount_empty" style="display:none;"></td>
                                          <td id="with_vat_amount">
                                             <input name="with_vat_amount[]"  type="number" Placeholder="With Vat" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="with_vat_amount_empty" style="display:none;"></td>
                                          <td>
                                             <input name="credit_to_owner[]"  readonly  checked type="checkbox"  class="form-control" style="width:100px;"/>
                                          </td>
                                          <td>
                                             <input class="security btn btn-primary" type="button" value="Delete" />
                                          </td>
                                       </tr>
                                       <tr id="row0">
                                          <td>
                                             <input name='serial_no[]' value="2"  type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                            
                                                <option selected value="DTCM">DTCM</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' class='form-control' />
                                          </td>
                                          <td id="date_from">
                                             <input name='check_date_security[]'  id="check_date_security" type='date' class='form-control check_date_security' style="width:150px;"/>
                                          </td>
                                          <td id="date_from_empty" style="display:none;"></td>
                                          <td id="date_to">
                                             <input name='check_issue_security[]'  type='date' class='form-control check_issue_security' style="width:150px;"/>
                                          </td>
                                          <td id="date_to_empty" style="display:none;"></td>
                                          <td>
                                             <input name='amount[]' id="amount" type='number' Placeholder='Amount' class="form-control party_name_security" style="width:100px;"/>
                                          </td>
                                          <td>
                                             <select class="form-control" id="vat_tax" name="vat_tax[]" style="width:100px;" onchange="assign_vat_tax(this.value)">
                                                <option value="" selected disabled>Select Vat Type</option>
                                                <option value="Vat Included">Vat Included</option>
                                                <option value="Vat Not Included">Vat Not Included</option>
                                                <option value="No Vat">No Vat</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name="net_amount[]"  type="number" Placeholder="Net Amount" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount">
                                             <input name="vat_amount[]"  type='number' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount_empty" style="display:none;"></td>
                                          <td id="with_vat_amount">
                                             <input name="with_vat_amount[]"  type="number" Placeholder="With Vat" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="with_vat_amount_empty" style="display:none;"></td>
                                          <td>
                                             <input name='credit_to_owner[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                       <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='3' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                                <option value="Security Deposit Received">Security Deposit Received</option>
                                             </select>
                                          </td>
                                           <td>
                                             <input name='particular[]'  style="width:150px;" type='text' class='form-control' />
                                          </td>
                                          <td >
                                            
                                          </td>
                                          <td id="date_from_empty" style="display:none;"></td>
                                          <td >
                                            
                                          </td>
                                          <td id="date_to_empty" style="display:none;"></td>
                                          <td>
                                             <input name='amount[]' id="amount" type='number' Placeholder='Amount' class="form-control party_name_security" style="width:100px;"/>
                                          </td>
                                          <td>
                                             <select class="form-control" id="vat_tax" name="vat_tax[]" style="width:100px;" onchange="assign_vat_tax(this.value)">
                                                <option value="" selected disabled>Select Vat Type</option>
                                                <option value="Vat Included">Vat Included</option>
                                                <option value="Vat Not Included">Vat Not Included</option>
                                                <option value="No Vat">No Vat</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name="net_amount[]"  type="number" Placeholder="Net Amount" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount">
                                             <input name="vat_amount[]"  type='number' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount_empty" style="display:none;"></td>
                                          <td id="with_vat_amount">
                                             <input name="with_vat_amount[]"  type="number" Placeholder="With Vat" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="with_vat_amount_empty" style="display:none;"></td>
                                          <td>
                                             <input name='credit_to_owner[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                       <tr id="row0">
                                          <td>
                                             <input name='serial_no[]' value='4'  type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                                <option value="Security Deposit Refund">Security Deposit Refund</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' class='form-control' />
                                          </td>
                                          <td >
                                             
                                          </td>
                                          <td id="date_from_empty" style="display:none;"></td>
                                          <td >
                                         
                                          </td>
                                          <td id="date_to_empty" style="display:none;"></td>
                                          <td>
                                             <input name='amount[]' id="amount" type='number' Placeholder='Amount' class="form-control party_name_security" style="width:100px;"/>
                                          </td>
                                          <td>
                                             <select class="form-control" id="vat_tax" name="vat_tax[]" style="width:100px;" onchange="assign_vat_tax(this.value)">
                                                <option value="" selected disabled>Select Vat Type</option>
                                                <option value="Vat Included">Vat Included</option>
                                                <option value="Vat Not Included">Vat Not Included</option>
                                                <option value="No Vat">No Vat</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name="net_amount[]"  type="number" Placeholder="Net Amount" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount">
                                             <input name="vat_amount[]"  type='number' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount_empty" style="display:none;"></td>
                                          <td id="with_vat_amount">
                                             <input name="with_vat_amount[]"  type="number" Placeholder="With Vat" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="with_vat_amount_empty" style="display:none;"></td>
                                          <td>
                                             <input name='credit_to_owner[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                       <tr id="row0">
                                          <td>
                                             <input name='serial_no[]' value='5' type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control'name="on_ac_of[]"style="width:200px;">
                                                <option value="Balance Rent Amount">Balance Rent Amount</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' class='form-control' />
                                          </td>
                                          <td >
                                            
                                          </td>
                                          <td id="date_from_empty" style="display:none;"></td>
                                          <td >
                                         
                                          </td>
                                          <td id="date_to_empty" style="display:none;"></td>
                                          <td>
                                             <input name='amount[]' id="amount" type='number' Placeholder='Amount' class="form-control party_name_security" style="width:100px;"/>
                                          </td>
                                          <td>
                                             <select class="form-control" id="vat_tax" name="vat_tax[]" style="width:100px;" onchange="assign_vat_tax(this.value)">
                                                <option value="" selected disabled>Select Vat Type</option>
                                                <option value="Vat Included">Vat Included</option>
                                                <option value="Vat Not Included">Vat Not Included</option>
                                                <option value="No Vat">No Vat</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name="net_amount[]"  type="number" Placeholder="Net Amount" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount">
                                             <input name="vat_amount[]"  type='number' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount_empty" style="display:none;"></td>
                                          <td id="with_vat_amount">
                                             <input name="with_vat_amount[]"  type="number" Placeholder="With Vat" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="with_vat_amount_empty" style="display:none;"></td>
                                          <td>
                                             <input name='credit_to_owner[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                       <tr id="row0">
                                          <td>
                                             <input name='serial_no[]' value='6'  type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                                <option value="Miscellaneous">Miscellaneous</option>
                                             </select>
                                          </td>
                                           <td>
                                             <input name='particular[]'  style="width:150px;" type='text' class='form-control' />
                                          </td>
                                          <td >
                                            
                                          </td>
                                          <td id="date_from_empty" style="display:none;"></td>
                                          <td >
                                            
                                          </td>
                                          <td id="date_to_empty" style="display:none;"></td>
                                          <td>
                                             <input name='amount[]' id="amount" type='number' Placeholder='Amount' class="form-control party_name_security" style="width:100px;"/>
                                          </td>
                                          <td>
                                             <select class="form-control" id="vat_tax" name="vat_tax[]" style="width:100px;" onchange="assign_vat_tax(this.value)">
                                                <option value="" selected disabled>Select Vat Type</option>
                                                <option value="Vat Included">Vat Included</option>
                                                <option value="Vat Not Included">Vat Not Included</option>
                                                <option value="No Vat">No Vat</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name="net_amount[]"  type="number" Placeholder="Net Amount" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount">
                                             <input name="vat_amount[]"  type='number' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount_empty" style="display:none;"></td>
                                          <td id="with_vat_amount">
                                             <input name="with_vat_amount[]"  type="number" Placeholder="With Vat" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="with_vat_amount_empty" style="display:none;"></td>
                                          <td>
                                             <input name='credit_to_owner[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                       <tr id="row0">
                                          <td>
                                             <input name='serial_no[]' value='7'  type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class='form-control' name="on_ac_of[]" style="width:200px;">
                                                <!--<option>Miscellaneous</option>-->
                                                
                                                <option value="Miscellaneous Charges">Miscellaneous Charges</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name='particular[]'  style="width:150px;" type='text' class='form-control' />
                                          </td>
                                          <td >
                                             
                                          </td>
                                          <td id="date_from_empty" style="display:none;"></td>
                                          <td >
                                             
                                          </td>
                                          <td id="date_to_empty" style="display:none;"></td>
                                          <td>
                                             <input name='amount[]' id="amount" type='number' Placeholder='Amount' class="form-control party_name_security" style="width:100px;"/>
                                          </td>
                                          <td>
                                             <select class="form-control" id="vat_tax" name="vat_tax[]" style="width:100px;" onchange="assign_vat_tax(this.value)">
                                                <option value="" selected disabled>Select Vat Type</option>
                                                <option value="Vat Included">Vat Included</option>
                                                <option value="Vat Not Included">Vat Not Included</option>
                                                <option value="No Vat">No Vat</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input name="net_amount[]"  type="number" Placeholder="Net Amount" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount">
                                             <input name="vat_amount[]"  type='number' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          <td id="vat_amount_empty" style="display:none;"></td>
                                          <td id="with_vat_amount">
                                             <input name="with_vat_amount[]"  type="number" Placeholder="With Vat" class="form-control" style="width:100px;"/>
                                          </td>
                                          <td id="with_vat_amount_empty" style="display:none;"></td>
                                          <td>
                                             <input name='credit_to_owner[]'  type='checkbox' Placeholder='With Vat' class='form-control' style="width:100px;"/>
                                          </td>
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>




                     <br>
                     


                     <div class="row">
                        <div class="col-md-12">
                           <label class="text-primary">For Internal Use</small></label>
                           <div style="border: 1px solid grey;">
                              <center>
                                 <!--<input type="button" id="security-row" class="btn btn-primary"  value="Add More" />-->
                                 <input id='internaluse-row' class='btn btn-primary' type='button' value='Add More' />
                              </center>
                              <div class="table-responsive">
                                 <table id="complex_header" class="table table-striped table-bordered display"
                                    style="width:100%">
                                    <thead>
                                       <tr>
                                          <th>Sr #</th>
                                          <th>Accounts Head</th>
                                          <th>Particulars</th>
                                          <th>Party</th>
                                          <th>Terms</th>
                                          <th>Percentage/Fixed Amount</th>
                                          <th>Rent Amount</th>
                                          <th>
                                             <center>Commission Amount</center>
                                          </th>
                                          <th>Commission Terms</th>
                                          <th>Email</th>
                                          <th>
                                             <center>VAT</center>
                                          </th>
                                          <th>
                                             <center>Net Amount</center>
                                          </th>
                                          <th>
                                             <center>VAT Amount</center>
                                          </th>
                                          <th>
                                             <center>With VAT Amount</center>
                                          </th>
                                          <th>
                                             <center>charge to owner or no</center>
                                          </th>
                                          <center><th>-<th></center>
                                       </tr>
                                    </thead>
                                    <tbody id="internaluse-body">
                                       <tr id="row0">
                                          <td>
                                             <input name='serial_no[]' readonly  value="1" type="text" class="form-control" style="width:40px;"/>
                                          </td>
                                          <td>
                                              <select class="form-control" name="accounts_head[]">
                                                  <option selected value="Commission">Commission</option>
                                              </select>
                                          </td>
                                          <td>
                                            <input type="text" class="form-control" style="width:100px;" name="particulars_internal_use[]" placeholder="Particulars">
                                          </td>
                                          <td>
                                             <input type="text" class="form-control" style="width:100px;" name="party[]" placeholder="Party">
                                          </td>
                                          <td >
                                            <select class="form-control" name="terms[]" style="width:100px;">
                                                <option value="" selected disabled >Select One</option>
                                              <option value="Fixed Amount">Fixed Amount</option>
                                              <option value="Percentage">Percentage</option>
                                           </select>
                                          </td>
                                          <td ><input type="number" class="form-control" name="percentage_fixed_amount[]" placeholder="Enter Percentage or Enter Fixed Amount">
                                          </td>
                                          <td >
                                             <input type="number" class="form-control" style="width:100px;" name="rent_amount[]" placeholder="Rent Amount">
                                          </td>
                                          <td >
                                              <input type="number" class="form-control" style="width:100px;" name="commision_amount[]" placeholder="Commission Amount">
                                          </td>
                                          <td>
                                            <select class="form-control" style="width:100px;" name="vat_tax_type[]" >
                                              <option value="" selected disabled>Select Vat Type</option>
                                              <option value="Vat Included">Vat Included</option>
                                              <option value="Vat Not Included">Vat Not Included</option>
                                              <option value="No Vat">No Vat</option>
                                           </select>
                                          </td>
                                          <td>
                                             <input type="email" class="form-control" style="width:100px;" name="email[]" placeholder="Email">
                                          </td>
                                          <td>
                                            <input type="number" class="form-control" style="width:100px;"   name="vat1[]"  placeholder="0">
                                          </td>
                                          <td id="vat_amount">
                                             <input type="number" class="form-control" style="width:100px;" name="net_amount1[]" placeholder="0">
                                          </td>
                                          <td>
                                               <input type="number" class="form-control" style="width:100px;" name="vat_amount1[]" placeholder="0">
                                          </td>
                                           <td>
                                               <input type="number" class="form-control" style="width:100px;" name="with_vat_amount1[]" placeholder="0">
                                          </td>
                                          <td >
                                             <input type="checkbox" class="form-control" style="width:100px;" name="charge_to_owner1[]" >
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>

                                        <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='2' readonly type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                              <select class="form-control" name="accounts_head[]">
                                                  <option selected value="Management Fee">Management Fee</option>
                                              </select>
                                          </td>
                                          <td>
                                            <input type="text" class="form-control" name="particulars_internal_use[]" placeholder="Particular">
                                          </td>
                                          <td>
                                             <input type="text" class="form-control" name="party[]" placeholder="Party">
                                          </td>
                                          <td id="date_from">
                                            <select class="form-control" name="terms[]">
                                                  <option value="" selected disabled>Select One</option>
                                              <option value="Fixed Amount">Fixed Amount</option>
                                              <option value="Percentage">Percentage</option>
                                           </select>
                                          </td>
                                          <td ><input type="number" class="form-control" name="percentage_fixed_amount[]" placeholder="Enter Percentage or Enter Fixed Amount">
                                          </td>
                                          <td >
                                             <input type="number" class="form-control" name="rent_amount[]" placeholder="Rent Amount">
                                          </td>
                                          <td >
                                              <input type="number" class="form-control" name="commision_amount[]" placeholder="Commission Amount">
                                          </td>
                                          <td>
                                            <select class="form-control" name="vat_tax_type[]" >
                                              <option value="" selected disabled>Select Vat Type</option>
                                              <option value="Vat Included">Vat Included</option>
                                              <option value="Vat Not Included">Vat Not Included</option>
                                              <option value="No Vat">No Vat</option>
                                           </select>
                                          </td>
                                          <td>
                                             <input type="email" class="form-control" name="email[]" placeholder="Email">
                                          </td>
                                          <td>
                                            <input type="number" class="form-control"   name="vat1[]"  placeholder="0">
                                          </td>
                                          <td id="vat_amount">
                                             <input type="number" class="form-control" name="net_amount1[]" placeholder="0">
                                          </td>
                                          <td>
                                               <input type="number" class="form-control" name="vat_amount1[]" placeholder="0">
                                          </td>
                                           <td>
                                               <input type="number" class="form-control" name="with_vat_amount1[]" placeholder="0">
                                          </td>
                                          <td >
                                             <input type="checkbox" class="form-control" name="charge_to_owner1[]" >
                                          </td>
                                          
                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>
                                    
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>



                     <br>
                   
                  </div>
                  <br>
                  <div class="row">
                     <div class="col-md-12">
                        <div style="border: 1px solid grey;">
                           <center>
                              <input id='add-row' class='btn btn-primary' type='button' value='Add More' />
                           </center>
                           <table id="test-table" class="table table-condensed">
                              <thead>
                                 <tr>
                                    <th>Attachments</th>
                                    <th>Comment</th>
                                 </tr>
                              </thead>
                              <tbody id="test-body">
                                 <tr id="row0">
                                    <td>
                                       <input name='attachment[]'  type='file' class='form-control' />
                                    </td>
                                    <td>
                                       <input name='comments[]'  placeholder="Comment" type='text' class='form-control input-md' />
                                    </td>
                                    <td>
                                       <input class='delete-row btn btn-primary' type='button' value='Delete' />
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div class="text-center mt-4">
                     <button class="btn btn-started"> Save</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
   let url = '/get/buildings';
   let url1 = '/get/floors';
   let url2 = '/get/flats';
   let url3 = '/get/flats/assign/lanlord';
   // $(document).on("change" , "#selectProject" , function () {
   //     let value = $(this).val();
   //     console.log('value');
   //     console.log(value);
   //     $('#selectBuilding').html('');
   //     $.ajax({
   //         url:url+'/'+value,
   //         Type:'GET',
   //         success: function (response) {
   //             if(response.status == true) {
   //                 let html = '<option>Select Building</option>';
   //                 $('#selectBuilding').append(html);
   //                 response.data.map((d) => {
   //                     html = '<option value="'+ d.id +'">'+ d.name +'</option>';
   //                     $('#selectBuilding').append(html);
   //                 });
   //             }
   //         },
   //         error: function (error) {
   //             console.log(error);
   //             console.log(error);
   //         }
   //     })
   // });
   function selectproject(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_buildings')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, pro_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectBuilding').html(msg);
   });
   }
   
   
      function selectbuilding(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_floors')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, building_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectFloor').html(msg);
   });
   }
   
   
    function selectfloor(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_flats')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, building_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectFlat').html(msg);
   });
   }
   
   
   
        function selectflat(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_management_fee')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, flat_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
//   alert(msg);
  $('#management_fee').val(msg);
   });
   }
   
   
   $(document).on("change" , "#no_installments" , function () {
       $('#rowTable').html('');
       let no_installments = $(this).val();
       let totalAmount = $('#totalAmount').val();
       console.log('value');
       console.log(no_installments);
       console.log(totalAmount);
       let totalA = totalAmount/no_installments;
       for (var i = 1; i <= no_installments; i++) {
           console.log('count iiiii');
           console.log(i);
           let Tblhtml = '<tr>' +
               '<td><input type="number" name="no_installment[]" value="'+ i +'" /></td>' +
               '<td><input type="number" name="amount[]" value="'+ totalA +'" /></td>' +
               '<td><input type="text" name="particular[]" value="" /></td>' +
               '<td><select name="pay_type" style="width:150px;" class="form-control">' +
                   '<option> select type </otpion>' +
                   '<option value="cash"> Cash </otpion>' +
                   '<option value="check"> Check </otpion>' +
               '</select></td>' +
               '<td><input type="number" name="check_no[]" /></td>' +
               '<td><input type="date" name="check_date[]" /></td>' +
               '<td><input type="text" name="check_issue[]" /></td>' +
               '<td><input type="text" name="party_name[]" /></td>' +
               '<td>' +
                   '<select id="selectOpt" name="selectOpt[]">' +
                       '<option>select option</option>' +
                       '<option>company</option>' +
                       '<option>landlord</option>' +
                   '</select>' +
               '</td>' +
               '</tr>';
           $('#rowTable').append(Tblhtml);
       }
       $('#rowTableDisplay').show();
   
   });
   
   
</script>
<script type="text/javascript">
   // Add row
   var row=1;
   $(document).on("click", "#add-row", function () {
       var new_row = '<tr id="row' + row + '"><td><input name="attachment[]" type="file" class="form-control" /></td><td><input name="comments[]" type="text" placeholder="Comment" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';
   
       $('#test-body').append(new_row);
       row++;
       return false;
   });
   
   // Remove criterion
   $(document).on("click", ".delete-row", function () {
       //  alert("deleting row#"+row);
       if(row>1) {
           $(this).closest('tr').remove();
           row--;
       }
       return false;
   });
   
   // Add row
   var row1=1;
   $(document).on("click", "#security-row", function () { 
       
      
       
       var new_row = '<tr id="row' + row1 + '">'
           +
               '<td>' +
               '<input name="serial_no[]" type="text" class="form-control" /></td>' +
               '<td><select class="form-control" name="on_ac_of[]" id="on_ac_of" style="width:200px;">'+    
               '<option value="" selected disabled>Select On A/C of</option>'+
               '<option value="rent" selected="Selected">Rent</option>'+
               '<option value="dtcm">DTCM</option>'+
               '<option value="security_deposit_recived">Security Deposit Received</option>'+
               '<option value="security_deposit_refund">Security Deposit Refund</option>'+
               '<option value="balance_rent_amount">Balance Rent Amount</option>'+
               '<option value="miscellaneous">Miscellaneous</option>'+
               '<option value="miscellaneous_charges">Miscellaneous Charges</option>'+
               '</select></td>' +
               '<td><input name="particular[]" style="width:150px;"  type="text" class="form-control" /></td>' +
               '<td></td>' +
               '<td></td>' +
               '<td><input name="amount[]"  type="nummber" Placeholder="Amount" class="form-control" style="width:100px;"/></td>' +
               
               '<td><select class="form-control" name="vat_tax[]" id="vat_tax" style="width:100px;">'+    
               ' <option value="" selected disabled>Select Vat Type</option>'+
               '<option value="Vat Included">Vat Included</option>'+
               '<option value="Vat Not Included">Vat Not Included</option>'+
               ' <option value="No Vat">No Vat</option>'+
               '</select></td>' +
               
               '<td> <input name="net_amount[]"  type="number" Placeholder="Net Amount" class="form-control" style="width:100px;"/></td>'+
               '<td id="vat_amount">  <input name="vat_amount[]"  type="number" Placeholder="Vat Amount" class="form-control" style="width:100px;"/></td>'+
               '<td id="vat_amount_empty" style="display:none;"></td>'+
               '<td id="with_vat_amount">  <input name="with_vat_amount[]"  type="number" Placeholder="With Vat" class="form-control" style="width:100px;"/></td>'+
               '<td><input name="credit_to_owner[]"  type="checkbox" Placeholder="With Vat" class="form-control" style="width:100px;"/></td>'+
               
               '<td><input class="security btn btn-primary" type="button" value="Delete" /></td>'+
           +
           '</tr>';
       $('#test-body1').append(new_row);
       row1++;
       return false;
   });
   
   // Remove criterion
   $(document).on("click", ".security", function () {
       //  alert("deleting row#"+row);
       if(row1>1) {
           $(this).closest('tr').remove();
           row1--;
       }
       return false;
   });
   
   
   
   // Add row
   var row2=1;
   $(document).on("click", "#other-row", function () {
       var new_row = '<tr id="row' + row2 + '">'
           +
           '<td>' +
           '<input name="other_serial_no[]" type="text" class="form-control" /></td>' +
           '<td><input name="other_particular[]" type="text"  class="form-control" /></td>' +
           '<td><input name="other_amount[]" type="number"  class="form-control" /></td>' +
           '<td><input name="other_vat[]" type="number" class="form-control" /></td>' +
           '<td><input class="other-row btn btn-primary" type="button" value="Delete" /></td>'
           +
           '</tr>';
       $('#test-body2').append(new_row);
       row1++;
       return false;
   });
   
   // Remove criterion
   $(document).on("click", ".other-row", function () {
       //  alert("deleting row#"+row);
       if(row1>1) {
           $(this).closest('tr').remove();
           row1--;
       }
       return false;
   });
   
   $(document).ready(function(){
       $('#on_ac_of').on('change', function() {
           if ( this.value == 'rent')
               //.....................^.......
   
           {
               $("#date_from").show();
               $("#date_from_empty").hide();
               $("#date_to").show();
               $("#date_to_empty").hide();
               
           }
           else
           {
               $("#date_from").hide();
               $("#date_from_empty").show();
               $("#date_to").hide();
               $("#date_to_empty").show();
               
           }
       });
   
   });
   
      $(document).ready(function(){
       $('#vat_tax').on('change', function() {
           if ( this.value == 'no_vat')
               //.....................^.......
   
           {
              
               
                $("#with_vat_amount").hide();
               $("#with_vat_amount_empty").show();
               $("#vat_amount").hide();
               $("#vat_amount_empty").show();
               
           }
           else
           {
                $("#with_vat_amount").show();
               $("#with_vat_amount_empty").hide();
               $("#vat_amount").show();
               $("#vat_amount_empty").hide();
               
           }
       });
   
   });
   
   
       $(document).on("click", "#internaluse-row", function () { 
       
      
       
       var new_row = '<tr id="row' + row1 + '">'
           +
               '<td><input name="serial_no[]" readonly   type="text" class="form-control" style="width:40px;"/>'+
                '</td>'+
                 '<td>'+
                      '<select class="form-control" name="accounts_head[]">'+
                      '<option  value="" selected disabled >Select One</option>'+
                           '<option  value="Commission">Commission</option>'+
                           '<option  value="Management Fee">Management Fee</option>'+
                                              '</select>'+
                '</td>'+
                 '<td>'+
                   '<input type="text" class="form-control" name="particulars_internal_use[]" placeholder="Particulars">'+
                 '</td>'+
                 '<td>'+
                   '<input type="text" class="form-control" name="party[]" placeholder="Party">'+
                 '</td>'+
                  '<td id="date_from">'+
                  '<select class="form-control" name="terms[]">'+
                  '<option value="" selected disabled>Select One</option>'+
                    '<option value="Fixed Amount">Fixed Amount</option>'+
                     '<option value="Percentage">Percentage</option>'+
                                           '</select>'+
                                          '</td>'+
                                          '<td >'+
                '<input type="number" class="form-control" name="percentage_fixed_amount[]" placeholder="Enter Percentage or Enter Fixed Amount">'+
                                          '</td>'+
                                          '<td >'+
                        '<input type="number" class="form-control" name="rent_amount[]" placeholder="Rent Amount">'+
                                          '</td>'+
                                          '<td >'+
                                    '<input type="number" class="form-control" name="commision_amount[]" placeholder="Commission Amount">'+
                                          '</td>'+
                                          '<td>'+
                                        '<select class="form-control" name="vat_tax_type[]" >'+    
                                              '<option value="" selected disabled>Select Vat Type</option>'+
                                              '<option value="Vat Included">Vat Included</option>'+
                                              '<option value="Vat Not Included">Vat Not Included</option>'+
                                              '<option value="No Vat">No Vat</option>'+
                                           '</select>'+
                                          '</td>'+
                                          '<td>'+
                                            '<input type="email" class="form-control" name="email[]" placeholder="Email">'+
                                          '</td>'+
                                          '<td>'+
                                            '<input type="number" class="form-control" name="vat1[]" placeholder="0">'+
                                          '</td>'+
                                          '<td id="vat_amount">'+
                                             '<input type="number" class="form-control" name="net_amount1[]" placeholder="0">'+
                                          '</td>'+
                                          '<td>'+
                                               '<input type="number" class="form-control" name="vat_amount1[]" placeholder="0">'+
                                          '</td>'+
                                          '<td>'+
                                               '<input type="number" class="form-control" name="with_vat_amount1[]" placeholder="0">'+
                                          '</td>'+
                                          '<td >'+
                                             '<input type="checkbox" class="form-control" name="charge_to_owner1[]" placeholder="0">'+
                                          '</td>'+
                                          '<td>'+
                                             '<input class="security btn btn-primary" type="button" value="Delete" />'+
                                          '</td>'+
           '</tr>';
       $('#internaluse-body').append(new_row);
       row1++;
       return false;
   });
   
   
   
   
   
   
   
   
   
   
   
       
       
       
   function assign_date_in()
   {
   
   var a =$('#check_in_date').val();
   $('.check_date_security').val(a);
   }
   
  function assign_date_out(){
      var a =$('#check_out_date').val();
  $('.check_issue_security').val(a);
  }
   
//   function assign_vat_tax(value)
//   {
//   //   <option value="Vat Included">Vat Included</option>
//   //         <option value="Vat Not Included">Vat Not Included</option>
//   //         <option value="No Vat">No Vat</option>
//   var amount= $('#party_name_security').val();
//   if(amount=="" || amount==null){
//       alert("Please Enter the Amount");
//       ('#party_name_security').focus();
//       return false;
//   }
//   if(value=="Vat Included"){
//       var net_amount = (amount*5)/100;
//       var vat_amount = amount-net_amount;
//      }   
//   }
   
</script>
@endsection
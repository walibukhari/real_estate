@extends('layouts.app', [
'class' => '',
'elementActive' => 'voucher_form'
])
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="content">
   @if (session('status'))
   <div class="alert alert-success" role="alert">
      {{ session('status') }}
   </div>
   @endif
   @if (session('password_status'))
   <div class="alert alert-success" role="alert">
      {{ session('password_status') }}
   </div>
   @endif
   @if (Session::has('error'))
   <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{!! Session('error') !!}</strong>
   </div>
   @endif
   @if ($errors->any())
   <div class="alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
   @endif
   <style type="text/css">
      .filelabel {
      width: 100%;
      border: 2px dashed grey;
      border-radius: 5px;
      display: block;
      padding: 5px;
      transition: border 300ms ease;
      cursor: pointer;
      text-align: center;
      margin: 0;
      }
      .filelabel i {
      display: block;
      font-size: 30px;
      padding-bottom: 5px;
      }
      .filelabel i,
      .filelabel .title {
      color: grey;
      transition: 200ms color;
      }
      .filelabel:hover {
      border: 2px solid #1665c4;
      }
      .filelabel:hover i,
      .filelabel:hover .title {
      color: #1665c4;
      }
      #FileInput{
      display:none;
      }
   </style>
   <div class="row">
      <div class="col-12">
         <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-2">
               <!--<button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Create New</button>-->
            </div>
         </div>
         <div class="material-card card">
            <div class="card-body">
               <h4 class="card-title">Receipt Voucher List</h4>
               <h6 class="card-subtitle">
               </h6>
               <br>
               <form class="form" method="POST" action="{{url('/Admin/Save_vouchers')}}" enctype="multipart/form-data">
                  @csrf
                  <div class="container">
                     <div class="row">
                        <div class="col-md-12">
                           <label class="text-primary">Select Type</label>
                           <input type="hidden" name="check" value="1"/>
                           <select class="form-control" name="type"  id="nature" required onchange="maintenance_type(this.value)">
                              <option value="" selected disabled>Select Type</option>
                              <option id="Office" value="Office">Office</option>
                              <option value="Apartment">Apartment</option>
                           </select>
                        </div>
                    </div><br>





  <!--office Start here-->
              <div id="box" >
               <div class="row clearfix">
                <div class="col-md-12">
                  <table class="table table-bordered table-hover" id="customers">
                    <thead>
                      <tr>
                        <th class="text-center"> # </th>
                        <th class="text-center"> Expense Head </th>
                        <th class="text-center"> Description </th>
                        <th class="text-center"> Quantity </th>
                        <th class="text-center"> Rate </th>
                        <th class="text-center"> Total </th>
                        <th class="text-center"> VAT </th>
                        <th class="text-center"> With VAT </th>
                        <th class="text-center"> Total with VAT </th>
                        <th class="text-center">Remove</th>

                      </tr>
                    </thead>
                    <tbody>
                      <tr id='addr0'>
                        <td>1</td>
                        <td>
                            <select class="form-control" name="office_expense_head[]" required >
                              <option value="" selected disabled>Select Expense Head</option>
                              <option value="Office">Office</option>
                              <option value="Personal">Personal</option>
                           </select>
                        </td>
                        <td>
                            <textarea class="form-control" name="office_description[]"> </textarea>
                        </td>

                        <td><input type="number" name='office_qty[]' placeholder='Quantity' class="form-control liter_sold"/>


                        </td>
                        <td><input type="number" name='office_rate[]' placeholder='Rate' class="form-control sale_price" /></td>
                        <td><input type="number" name='office_total[]' placeholder='0.00' class="form-control amount" readonly/></td>
                        <td>
                            <select class="form-control onchangeVat" name="office_vat_type[]"  required >
                              <option value="" selected disabled>Select Vat</option>
                              <option value="YES">YES</option>
                              <option value="NO">NO</option>
                           </select>
                        </td>
                        <td>
                            <input type="number" name='office_with_vat[]' placeholder='0.00' class="form-control amount_with_vat" readonly/>
                        </td>
                        <td>
                            <input type="number" name='office_total_vat[]' placeholder='0.00' class="form-control amount_total_vat" readonly/>
                        </td>

                        <td>
                        <button type="button" class="deletebtn btn btn-danger btn-sm"  title="Remove row">Remove</button>
                    </td>
                      </tr>
                      <tr id='addr1'></tr>
                    </tbody>
                  </table>
                </div>
              </div>
               <div class="row clearfix">
                <div class="col-md-12">
                  <input type="button" id="add_row" class="btn btn-default pull-left" value="Add Row">

                </div>
              </div>
               <div class="row clearfix" style="margin-top:20px">
                <div class="col-md-8"></div>
                <div class="pull-right col-md-4">
                  <table class="table table-bordered table-hover" id="customers_total">
                    <tbody>
                      <tr>
                        <th class="text-center">Sub Total</th>
                        <td class="text-center"><input type="number" name='sub_total' placeholder='0.00' class="form-control" id="sub_total" readonly/></td>
                      </tr>

                    </tbody>
                  </table>

                </div>
              </div>
                <button class="btn btn-started">Save</button>
              </div>
              </form>
  <!--office End here-->


  <!--Appartment Start here-->
                    <form class="form" method="POST" action="{{url('/Admin/Save_vouchers')}}" enctype="multipart/form-data">
                  @csrf
                     <div id="apart">
                     <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary">Project</label>
                           <input type="hidden" name="check" value="0"/>
                           <select class="form-control" name="project_id" id="selectProject" required onchange="selectproject(this.value)">
                              <option value="" selected disabled>Select project</option>
                              @foreach($projects as $pro)
                              <option value="{{$pro->id}}">{{$pro->name}}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="col-md-6">
                           <label class="text-primary"> Building</label>
                           <select class="form-control" name="building_id" required id="selectBuilding" onchange="selectbuilding(this.value)">
                           </select>
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary"> Floor</label>
                           <select class="form-control" name="floor_id" required id="selectFloor" onchange="selectfloor(this.value)">
                           </select>
                        </div>
                        <div class="col-md-6">
                           <label class="text-primary"> Flat</label>
                           <select class="form-control" name="flat" id="selectFlat" required onchange="selectflat(this.value)" >
                           </select>
                        </div>
                     </div>
                     <br>
                     <div class="row">
                         <div class="col-md-6">
                             <label class="text-primary">Owner Information</label>
                             <div class="container" style="border: 1px solid grey;">
                             <br>
                                <div class="row">
                                    <div class="col-md-3"><lable class="text-primary">Name</lable></div>
                                    <div class="col-md-9">
                                        <select class="form-control" name="owner_id" required>
                                            <option value="" selected disabled> Select Owner</option>
                                             @foreach($owner as $owners)
                                                <option value="{{$owners->id}}">{{$owners->name}}</option>
                                             @endforeach
                                        </select>
                                    </div>
                                </div> <br>

                                <div class="row">
                                    <div class="col-md-3"><lable class="text-primary">Contact No.</lable></div>
                                    <div class="col-md-9">
                                        <input type="number" class="form-control" required name="owner_contact" />
                                    </div>
                                </div> <br>

                                <div class="row">
                                    <div class="col-md-3"><lable class="text-primary">Email</lable></div>
                                    <div class="col-md-9">
                                        <input type="email" class="form-control" required name="owner_email" />
                                    </div>
                                </div>
                                <br>
                             </div>
                         </div>


                           <div class="col-md-6">
                             <label class="text-primary">Tenant Information</label>
                             <div class="container" style="border: 1px solid grey;">
                             <br>
                                <div class="row">
                                    <div class="col-md-3"><lable class="text-primary">Name</lable></div>
                                    <div class="col-md-9">
                                        <select class="form-control" name="tenant_id" required>
                                            <option value="" selected disabled> Select Tenant</option>
                                             @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                             @endforeach
                                        </select>
                                    </div>
                                </div> <br>

                                <div class="row">
                                    <div class="col-md-3"><lable class="text-primary">Contact No.</lable></div>
                                    <div class="col-md-9">
                                        <input type="number" required class="form-control" name="tenant_contact" />
                                    </div>
                                </div> <br>

                                <div class="row">
                                    <div class="col-md-3"><lable class="text-primary">Email</lable></div>
                                    <div class="col-md-9">
                                        <input type="email" required class="form-control" name="tenant_email" />
                                    </div>
                                </div>
                                <br>
                             </div>
                         </div>
                     </div>
                     <br>


                      <div id="box2" >
               <div class="row clearfix">
                <div class="col-md-12">
                  <table class="table table-bordered table-hover" id="customers2">
                    <thead>
                      <tr>
                        <th class="text-center"> # </th>
                        <th class="text-center"> Expense Head </th>
                        <th class="text-center"> Description </th>
                        <th class="text-center"> Quantity </th>
                        <th class="text-center"> Rate </th>
                        <th class="text-center"> Total </th>
                        <th class="text-center"> VAT </th>
                        <th class="text-center"> With VAT </th>
                        <th class="text-center"> Total with VAT </th>
                        <th class="text-center">Remove</th>

                      </tr>
                    </thead>
                    <tbody>
                      <tr id='addrr0'>
                        <td>1</td>
                        <td>
                            <select class="form-control" name="apart_expense_head[]"  required >
                              <option value="" selected disabled>Select Expense Head</option>
                              <option value="Office">Office</option>
                              <option value="Personal">Personal</option>
                           </select>
                        </td>
                        <td>
                            <textarea class="form-control" name="apart_description[]"> </textarea>
                        </td>
                        <td><input type="number" name='apart_qty[]' placeholder='Quantity' class="form-control liter_sold"/></td>
                        <td><input type="number" name='apart_rate[]' placeholder='Rate' class="form-control sale_price" /></td>
                        <td><input type="number" name='apart_total[]' placeholder='0.00' class="form-control amount" readonly/></td>
                        <td>
                            <select class="form-control" name="apart_vat_type[]"  required >
                              <option value="" selected disabled>Select Vat</option>
                              <option value="YES">YES</option>
                              <option value="NO">NO</option>
                           </select>
                        </td>
                        <td>
                            <input type="number" name='apart_with_vat[]' placeholder='0.00' class="form-control amount" readonly/>
                        </td>
                        <td>
                            <input type="number" name='apart_vat_total[]' placeholder='0.00' class="form-control amount" readonly/>
                        </td>

                        <td>
                        <button type="button" class="deletebtn btn btn-danger btn-sm" title="Remove row">Remove</button>
                    </td>
                      </tr>
                      <tr id='addrr1'></tr>
                    </tbody>
                  </table>
                </div>
              </div>
               <div class="row clearfix">
                <div class="col-md-12">
                  <input type="button" id="add_row1" class="btn btn-default pull-left" value="Add Row">

                </div>
              </div>
              <br>
                 <!--Attachment Area Started         -->
                <div id="attach" >
               <div class="row clearfix">
                <div class="col-md-12">
                  <table class="table table-bordered table-hover" id="att">
                    <thead>
                      <tr>
                        <th class="text-center"> # </th>
                        <th class="text-center"> Attachment </th>
                        <th class="text-center"> Memo </th>
                        <th class="text-center">Remove</th>

                      </tr>
                    </thead>
                    <tbody>
                      <tr id='addrrr0'>
                        <td>1</td>
                        <td>
                          <input type="file" name="attachment[]" class="form-control"/>
                        </td>
                        <td>
                            <textarea class="form-control" name="memo[]"> </textarea>
                        </td>
                        <td>
                        <button type="button" class="deletebtn btn btn-danger btn-sm" title="Remove row">Remove</button>
                       </td>
                      </tr>
                      <tr id='addrrr1'></tr>
                    </tbody>
                  </table>
                </div>
              </div>
               <div class="row clearfix">
                <div class="col-md-12">
                  <input type="button" id="add_row2" class="btn btn-default pull-left" value="Add Row">
                </div>
              </div>
              </div>

              <!--Attachment Area Ended-->


               <div class="row clearfix" style="margin-top:20px">
                <div class="col-md-8"></div>
                <div class="pull-right col-md-4">
                  <table class="table table-bordered table-hover" id="customers_total">
                    <tbody>
                      <tr>
                        <th class="text-center">Sub Total</th>
                        <td class="text-center"><input type="number" name='sub_total' placeholder='0.00' class="form-control" id="sub_total" readonly/></td>
                      </tr>

                    </tbody>
                  </table>
                </div>
              </div>
              </div>
               <button class="btn btn-started">Save</button>
                 </div>

<!--Appartment End here-->








                     <br>
                  </div>
                  <br>

                  <div class="container">
                      <div class="row">
                        <div class="text-center mt-4 col-md-2">

                        </div>
                      </div>
                  </div>

            </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript">



  $(document).ready(function(){
      $("#apart").hide();
       $("#box").hide();
    var i=1;
    $("#add_row").click(function(){
        b=i-1;
        $('#addr'+i).html($('#addr'+b).html()).find('td:first-child').html(i+1);
        $('#customers').append('<tr id="addr'+(i+1)+'"></tr>');
        i++;
    });
    $("#delete_row").click(function(){
      if(i>1){
    $("#addr"+(i-1)).html('');
    i--;
    }
    calc();
  });

  $('#customers tbody').on('keyup change',function(){
    calc();
  });
  $('#tax').on('keyup change',function(){
    calc_total();
  });
});

function calc()
{
  $('#customers tbody tr').each(function(i, element) {
    var html = $(this).html();
    if(html!='')
    {
      var liter_sold = $(this).find('.liter_sold').val();
      var sale_price = $(this).find('.sale_price').val();
      console.log(liter_sold);
      console.log(sale_price);
      $(this).find('.amount').val(liter_sold*sale_price);
      let totalValue = liter_sold*sale_price;
      let vatType = $(this).find('.onchangeVat').val();
      if(vatType == 'YES') {
          console.log('vatType YES');
          let vatTax = (5/100)*totalValue;
          console.log(vatTax.toFixed(2));
          console.log(totalValue);
          let totalVat = parseInt(totalValue) + parseInt(vatTax.toFixed(4));
          $(this).find('.amount_with_vat').val(vatTax.toFixed(2));
          $(this).find('.amount_total_vat').val(totalVat);
      } else {
          $(this).find('.amount_total_vat').val(totalValue);
          $(this).find('.amount_with_vat').val(totalValue);
      }
      calc_total();
    }
    });
}

function calc_total()
{
    console.log('calculate total');
  amount=0;
  $('.amount_total_vat').each(function() {
        amount += parseInt($(this).val());
    });
  $('#sub_total').val(amount.toFixed(2));
  tax_sum=amount/100*$('#tax').val();
  $('#tax_amount').val(tax_sum.toFixed(2));
  $('#total_amount').val((tax_sum+amount).toFixed(2));
}

</script>

<script>

$(document).on('click', 'button.deletebtn', function () {
     $(this).closest('tr').remove();
     return false;
 });

  $(document).ready(function(){
      $("#apart").hide();
      $("#box").hide();
      var i2=1;
    $("#add_row1").click(function()
    {
            b2=i2-1;
             $('#addrr'+i2).html($('#addrr'+b2).html()).find('td:first-child').html(i2+1);
             $('#customers2').append('<tr id="addrr'+(i2+1)+'"></tr>');
            i2++;
    });

    $("#delete_row1").click(function(){
    if(i2>1){
        $("#addrr"+(i2-1)).html('');
        i2--;
    }
    calc1();
  });

  $('#customers2 tbody').on('keyup change',function(){
    calc1();
  });
  $('#tax1').on('keyup change',function(){
    calc_total1();
  });
});

function calc1()
{
  $('#customers2 tbody tr').each(function(i2, element) {
    var html = $(this).html();
    if(html!='')
    {
      var liter_sold = $(this).find('.liter_sold').val();
      var sale_price = $(this).find('.sale_price').val();
      $(this).find('.amount').val(liter_sold*sale_price);

      calc_total1();
    }
    });
}

function calc_total1()
{
  amount=0;
  $('.amount').each(function() {
        amount += parseInt($(this).val());
    });
  $('#sub_total').val(amount.toFixed(2));
  tax_sum=amount/100*$('#tax').val();
  $('#tax_amount').val(tax_sum.toFixed(2));
  $('#total_amount').val((tax_sum+amount).toFixed(2));
}

</script>
<script type="text/javascript">





   let url = '/get/buildings';
   let url1 = '/get/floors';
   let url2 = '/get/flats';
   let url3 = '/get/flats/assign/lanlord';

   function selectproject(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_buildings')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, pro_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectBuilding').html(msg);
   });
   }


      function selectbuilding(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_floors')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, building_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectFloor').html(msg);
   });
   }


    function selectfloor(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_flats')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, building_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectFlat').html(msg);
   });
   }



        function selectflat(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_lanlords')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, flat_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#LanLord').val(msg);
   });
   }


   $(document).on("change" , "#no_installments" , function () {
       $('#rowTable').html('');
       let no_installments = $(this).val();
       let totalAmount = $('#totalAmount').val();
       console.log('value');
       console.log(no_installments);
       console.log(totalAmount);
       let totalA = totalAmount/no_installments;
       for (var i = 1; i <= no_installments; i++) {
           console.log('count iiiii');
           console.log(i);
           let Tblhtml = '<tr>' +
               '<td><input type="number" name="no_installment[]" value="'+ i +'" /></td>' +
               '<td><input type="number" name="amount[]" value="'+ totalA +'" /></td>' +
               '<td><input type="text" name="particular[]" value="" /></td>' +
               '<td><select name="pay_type" style="width:150px;" class="form-control">' +
                   '<option> select type </otpion>' +
                   '<option value="cash"> Cash </otpion>' +
                   '<option value="check"> Check </otpion>' +
               '</select></td>' +
               '<td><input type="number" name="check_no[]" /></td>' +
               '<td><input type="date" name="check_date[]" /></td>' +
               '<td><input type="text" name="check_issue[]" /></td>' +
               '<td><input type="text" name="party_name[]" /></td>' +
               '<td>' +
                   '<select id="selectOpt" name="selectOpt[]">' +
                       '<option>select option</option>' +
                       '<option>company</option>' +
                       '<option>landlord</option>' +
                   '</select>' +
               '</td>' +
               '</tr>';
           $('#rowTable').append(Tblhtml);
       }
       $('#rowTableDisplay').show();

   });


</script>
<script type="text/javascript">
   // Add row
   var row=1;
   $(document).on("click", "#add-row", function () {
       var new_row = '<tr id="row' + row + '"><td><input name="attachment[]" type="file" class="form-control" /></td><td><input name="comments[]" type="text" placeholder="Comment" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';

       $('#test-body').append(new_row);
       row++;
       return false;
   });

   // Remove criterion
   $(document).on("click", ".delete-row", function () {
       //  alert("deleting row#"+row);
       if(row>1) {
           $(this).closest('tr').remove();
           row--;
       }
       return false;
   });

   // Add row
   var row1=1;
   $(document).on("click", "#security-row", function () {



       var new_row = '<tr id="row' + row1 + '">'
           +
               '<td><input name="serial_no[]"  value="1" type="text" class="form-control" style="width:40px;"/>'+
               '</td>'+
                '<td>'+
                     '<select class="form-control" name="on_ac_of[]" style="width:200px;">'+
                      '<option  selected  disabled value="">Select One</option>'+
                      '<option  value="Empower">Empower</option>'+
                      '<option    value="Emicool">Emicool</option>'+
                      '<option  value="Dewa">Dewa</option>'+
                      '<option  value="Du">Du</option>'+
                      '<option  value="Etisalat">Etisalat</option>'+
                      '<option  value="My Ista">My Ista</option>'+
                      '<option  value="Service Charges">Service Charges</option>'+
                      '<option  value="New Access Card">New Access Card</option>'+
                      '<option  value="Lootha Gas">Lootha Gas</option>'+
                      '<option  value="Al-Fanar Gas">Al-Fanar Gas</option>'+
                      '<option  value="Aqua Cool">Aqua Cool</option>'+
                     '</select>'+
                                         '</td>'+
                                          '<td>'+
                                             '<input name="particular[]"  style="width:150px;" type="text" placeholder="Particular" class="form-control" />'+
                                          '</td>'+
                                          '<td>'+
                                             '<input name="ref_no[]"  id="" type="text" placeholder="Ref No."  class="form-control " style="width:150px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input name="amount[]" placeholder="Amount"  type="number" class="form-control " style="width:150px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input name="from[]"  type="date" class="form-control " style="width:150px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input name="to[]"  type="date" class="form-control " style="width:150px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                            '<input name="due_date[]"  type="date" class="form-control " style="width:150px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input name="next_billing_date[]"  type="date" class="form-control " style="width:150px;"/>'+
                                          '</td>'+
                                          '<td >'+
                                             '<input name="frequency[]"  type="text" Placeholder="Frequency" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                           '<td >'+
                                             '<input name="charge_to_owner[]"  type="text" Placeholder="Charge to Owner" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                           '<td>'+
                                             '<input name="equal_to_owner[]"  type="text" Placeholder="Equal to Owner" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                           '<td>'+
                                             '<input name="utility_attchment[]"  type="file"  class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                           '<td>'+
                                             '<input name="charge_to_tenant[]"  type="text" Placeholder="Charge to Tenant" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                           '<td>'+
                                             '<input name="email_to_owner[]"  type="text" Placeholder="Email to Owner" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                           '<td>'+
                                             '<input name="charge_to_company[]"  type="text" Placeholder="Charge to Company" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input name="company_to_head[]""  type="checkbox" Placeholder="With Vat" class="form-control" style="width:100px;"/>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input class="security btn btn-primary" type="button" value="Delete" />'+
                                          '</td>'+
           '</tr>';
       $('#test-body1').append(new_row);
       row1++;
       return false;
   });

   // Remove criterion
   $(document).on("click", ".security", function () {
       //  alert("deleting row#"+row);
       if(row1>1) {
           $(this).closest('tr').remove();
           row1--;
       }
       return false;
   });



   // Add row
   var row2=1;
   $(document).on("click", "#other-row", function () {
       var new_row = '<tr id="row' + row2 + '">'
           +
           '<td>' +
           '<input name="other_serial_no[]" type="text" class="form-control" /></td>' +
           '<td><input name="other_particular[]" type="text"  class="form-control" /></td>' +
           '<td><input name="other_amount[]" type="number"  class="form-control" /></td>' +
           '<td><input name="other_vat[]" type="number" class="form-control" /></td>' +
           '<td><input class="other-row btn btn-primary" type="button" value="Delete" /></td>'
           +
           '</tr>';
       $('#test-body2').append(new_row);
       row1++;
       return false;
   });

   // Remove criterion
   $(document).on("click", ".other-row", function () {
       //  alert("deleting row#"+row);
       if(row1>1) {
           $(this).closest('tr').remove();
           row1--;
       }
       return false;
   });

   $(document).ready(function(){
       $("#apart").hide();
        $("#box").hide();
       $('#on_ac_of').on('change', function() {
           if ( this.value == 'rent')
               //.....................^.......

           {
               $("#date_from").show();
               $("#date_from_empty").hide();
               $("#date_to").show();
               $("#date_to_empty").hide();

           }
           else
           {
               $("#date_from").hide();
               $("#date_from_empty").show();
               $("#date_to").hide();
               $("#date_to_empty").show();

           }
       });

   });

      $(document).ready(function(){
          $("#apart").hide();
           $("#box").hide();
       $('#vat_tax').on('change', function() {
           if ( this.value == 'no_vat')
               //.....................^.......

           {


                $("#with_vat_amount").hide();
               $("#with_vat_amount_empty").show();
               $("#vat_amount").hide();
               $("#vat_amount_empty").show();

           }
           else
           {
                $("#with_vat_amount").show();
               $("#with_vat_amount_empty").hide();
               $("#vat_amount").show();
               $("#vat_amount_empty").hide();

           }
       });

   });


      $(document).on("click", "#internaluse-row", function () {



       var new_row = '<tr id="row' + row1 + '">'
           +
               '<td><input name="serial_no[]" readonly   type="text" class="form-control" style="width:40px;"/>'+
                '</td>'+
                 '<td>'+
                      '<select class="form-control" name="accounts_head[]">'+
                      '<option  value="" selected disabled >Select One</option>'+
                           '<option  value="Commission">Commission</option>'+
                           '<option  value="Management Fee">Management Fee</option>'+
                                              '</select>'+
                '</td>'+
                 '<td>'+
                   '<input type="text" class="form-control" name="particulars_internal_use[]" placeholder="Particulars">'+
                 '</td>'+
                 '<td>'+
                   '<input type="text" class="form-control" name="party[]" placeholder="Party">'+
                 '</td>'+
                  '<td id="date_from">'+
                  '<select class="form-control" name="terms[]">'+
                  '<option value="" selected disabled>Select One</option>'+
                    '<option value="Fixed Amount">Fixed Amount</option>'+
                     '<option value="Percentage">Percentage</option>'+
                                           '</select>'+
                                          '</td>'+
                                          '<td >'+
                '<input type="number" class="form-control" name="percentage_fixed_amount[]" placeholder="Enter Percentage or Enter Fixed Amount">'+
                                          '</td>'+
                                          '<td >'+
                        '<input type="number" class="form-control" name="rent_amount[]" placeholder="Rent Amount">'+
                                          '</td>'+
                                          '<td >'+
                                    '<input type="number" class="form-control" name="commision_amount[]" placeholder="Commission Amount">'+
                                          '</td>'+
                                          '<td>'+
                                        '<select class="form-control" name="vat_tax_type[]" >'+
                                              '<option value="" selected disabled>Select Vat Type</option>'+
                                              '<option value="Vat Included">Vat Included</option>'+
                                              '<option value="Vat Not Included">Vat Not Included</option>'+
                                              '<option value="No Vat">No Vat</option>'+
                                           '</select>'+
                                          '</td>'+
                                          '<td>'+
                                            '<input type="email" class="form-control" name="email[]" placeholder="Email">'+
                                          '</td>'+
                                          '<td>'+
                                            '<input type="number" class="form-control" name="vat1[]" placeholder="VAT Amount">'+
                                          '</td>'+
                                          '<td id="vat_amount">'+
                                             '<input type="number" class="form-control" name="net_amount1[]" placeholder="Net Amount">'+
                                          '</td>'+
                                          '<td>'+
                                               '<input type="number" class="form-control" name="vat_amount1[]" placeholder=" VAT Amount">'+
                                          '</td>'+

                                          '<td>'+
                                               '<input type="number" class="form-control" name="with_vat_amount1[]" placeholder="Without VAT Amount">'+
                                          '</td>'+
                                           '<td >'+
                                             '<input type="checkbox" class="form-control" name="charge_to_owner[]">'+
                                          '</td>'+
                                          '<td>'+
                                             '<input class="security btn btn-primary" type="button" value="Delete" />'+
                                          '</td>'+
           '</tr>';
       $('#internaluse-body').append(new_row);
       row1++;
       return false;
   });









   function maintenance_type(value)
   {
       if(value=="Office")
       {
           $("#box").show();
           $("#apart").hide();
       }
       else if(value=="Apartment")
       {
          $("#apart").show();
          $("#box").hide();
       }

   }




  $(document).ready(function(){
      $("#apart").hide();
       $("#box").hide();
    var i3=1;
    $("#add_row2").click(function(){
       var b3=i3-1;
        $('#addrrr'+i3).html($('#addrrr'+b3).html()).find('td:first-child').html(i3+1);
        $('#att').append('<tr id="addrrr'+(i3+1)+'"></tr>');
        i3++;
    });
    $("#delete_row").click(function(){
      if(i3>1){
    $("#addrrr"+(i3-1)).html('');
    i3--;
    }
  });
});









</script>
@endsection

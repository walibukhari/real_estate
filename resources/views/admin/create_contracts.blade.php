@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'add_contracts'
])

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('error') !!}</strong>
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <style type="text/css">
            .filelabel {
                width: 100%;
                border: 2px dashed grey;
                border-radius: 5px;
                display: block;
                padding: 5px;
                transition: border 300ms ease;
                cursor: pointer;
                text-align: center;
                margin: 0;
            }
            .filelabel i {
                display: block;
                font-size: 30px;
                padding-bottom: 5px;
            }
            .filelabel i,
            .filelabel .title {
                color: grey;
                transition: 200ms color;
            }
            .filelabel:hover {
                border: 2px solid #1665c4;
            }
            .filelabel:hover i,
            .filelabel:hover .title {
                color: #1665c4;
            }
            #FileInput{
                display:none;
            }
        </style>
        <div class="row">
            <div class="col-12">

                <div class="material-card card">
                    <div class="card-body">

                        <h4 class="card-title">Long Term Tenancy  Contract</h4>
                        <h6 class="card-subtitle">
                        </h6><br>
                        <form class="form" method="POST" action="{{url('/Admin/Save_Contracts')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-6">
                                                <label class="text-primary" >Select Project</label>
                                            </div>

                                            <div class="col-md-6">
                                                <a target="_blank" href="{{url('/Add_Project')}}" style="text-decoration:none;" class="pull-right bg-success p-1 text-white">Add New</a>
                                            </div>

                                        </div>

                                        <select class="form-control" name="project" id="selectProject" required onchange="selectproject(this.value)">
                                                <option value="" selected disabled>Select project</option>
                                            @foreach($projects as $pro)
                                                <option value="{{$pro->id}}">{{$pro->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-6">
                                               <label class="text-primary">Select Building</label>
                                            </div>

                                            <div class="col-md-6">
                                                <a target="_blank" href="{{url('/Add_Building')}}" style="text-decoration:none;" class="pull-right bg-success p-1 text-white">Add New</a>
                                            </div>

                                        </div>

                                       <select class="form-control"  name="building" id="selectBuilding" onchange="selectbuilding(this.value)">
                                        </select>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-6">
                                         <div class="row">

                                            <div class="col-md-6">
                                               <label class="text-primary">Select Floor</label>
                                            </div>

                                            <div class="col-md-6">
                                                <a target="_blank" href="{{url('/Add_Floor')}}" style="text-decoration:none;" class="pull-right bg-success p-1 text-white">Add New</a>
                                            </div>

                                        </div>

                                        <select class="form-control"  name="floor" id="selectFloor" onchange="selectfloor(this.value)">
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                         <div class="row">

                                            <div class="col-md-6">
                                               <label class="text-primary">Select Flat</label>
                                            </div>

                                            <div class="col-md-6">
                                                <a target="_blank" href="{{url('/Add_Flat')}}" style="text-decoration:none;" class="pull-right bg-success p-1 text-white">Add New</a>
                                            </div>

                                        </div>

                                        <select class="form-control"  name="flat" id="selectFlat" onchange="selectflat(this.value)" >
                                        </select>
                                    </div>
                                </div><br>

                                 <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Location</label>
                                        <input type="" class="form-control" name="location" id="location" readonly/>

                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-primary">Permisses No.<small>(DEWA)</small></label>
                                        <input type="" class="form-control" name="permisses_no" id="permisses_no" readonly/>
                                    </div>
                                </div><br>


                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Plot No.</label>
                                        <input type="" class="form-control" name="plot_no" id="plot_no" readonly/>
                                    </div>

                                     <div class="col-md-6">
                                        <label class="text-primary">Property Size</label>
                                        <input type="" class="form-control" name="property_size" id="property_size" readonly/>
                                    </div>

                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Property Type</label>
                                        <input type="" class="form-control" name="" id="property_type" readonly/>
                                        <input type="hidden" class="form-control" name="property_type" id="property_type_id" readonly/>
                                    </div>


                                </div>
                                <br>



                                <hr>

                                <div class="row">
                                    <div class="col-md-6">
                                         <div class="row">

                                            <div class="col-md-6">
                                               <label class="text-primary">Owner Name</label>
                                            </div>

                                            <div class="col-md-6">
                                                <a target="_blank" href="{{url('/Add_Landlords')}}" style="text-decoration:none;" class="pull-right bg-success p-1 text-white">Add New</a>
                                            </div>

                                        </div>

                                        <input type="text" readonly id="LanLord" name="" class="form-control" />
                                        <input type="hidden" id="LanLord_id" name="lanlord_name" class="form-control" />
                                    </div>
                                    <div class="col-md-6">
                                         <div class="row">

                                            <div class="col-md-6">
                                               <label class="text-primary">Select Tenant</label>
                                            </div>

                                            <div class="col-md-6">
                                                <a target="_blank"  href="{{url('/Add_tenants')}}" style="text-decoration:none;" class="pull-right bg-success p-1 text-white">Add New</a>
                                            </div>

                                        </div>

                                        <select class="form-control" required name="tenant" >
                                            <option value="" selected disabled>Select Tenant</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Contract Start</label>
                                        <input type="date" class="form-control"  name="contract_start" placeholder="Flat Security">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-primary">Contract End</label>
                                        <input type="date" class="form-control" name="contract_end" placeholder="Flat Security">
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Total Contract Amount</label>
                                        <input type="number" id="totalAmount" name="totalAmount" class="form-control" placeholder="Total Contract Amount">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-primary">No of Installments </label>
                                        <input type="number" id="no_installments"  name="no_installments" class="form-control" placeholder="No of Installments">
                                    </div>
                                </div><br>

                                <div class="row" id="rowTableDisplay" style="display: none;">
                                    <div class="table-responsive">
                                        <table class="table table-borderless">
                                            <thead>
                                            <tr>
                                                <th>Installment No</th>
                                                <th>Installment Amount</th>
                                                <th>Particular</th>
                                                <th>Select Type</th>
                                                <th>Check No</th>
                                                <th>Check Date</th>
                                                <th>Check Issue From Bank</th>
                                                <th>Check Party Name</th>
                                                <th>Check Deposit</th>
                                            </tr>
                                            </thead>
                                            <tbody id="rowTable">

                                            </tbody>
                                        </table>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="text-primary">Managemnet Fee</label>
                                        <input class="form-control" value="" readonly id="management_fee" name="managment_fee" />
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Terms and conditions</label>
                                        <textarea class="form-control"  name="terms_conditions" placeholder="Terms and conditions"></textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-primary">Renew Terms and conditions</label>
                                        <textarea class="form-control" name="renew_terms_conditions" placeholder="Renew Terms and conditions"></textarea>
                                    </div>
                                </div><br>

{{--                                <div class="row">--}}
{{--                                    <div class="col-md-12">--}}
{{--                                        <label class="text-primary">Security Deposit<small>(Refundable)</small></label>--}}
{{--                                        <input type="number" class="form-control" name="security_deposite" placeholder="Security Deposit">--}}
{{--                                    </div>--}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="text-primary">Security Deposit<small>(Refundable)</small></label>
                                            <div style="border: 1px solid grey;">

                                                <table id="test-table" class="table table-condensed">
                                                    <thead>
                                                    <tr>
                                                        <th>Sr #</th>
                                                        <th>Security Deposit Amount</th>
                                                        <th>Check No</th>
                                                        <th>Check Date</th>
                                                        <th>Check Issue</th>
                                                        <th>Party Name</th>
                                                        <th>Check Deposit</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="test-body1">
                                                    <tr id="row0">
                                                        <td>
                                                            <input name='serial_no[]'  readonly value="1" type='text' class='form-control' />
                                                        </td>
                                                        <td>
                                                            <input name='security_deposit_amount[]'   value="" type='number' class="form-control" />
                                                        </td>
                                                        <td>
                                                            <input name='check_no_security[]'  type='text' class='form-control' />
                                                        </td>
                                                        <td>
                                                            <input name='check_date_security[]'  type='date' class='form-control' />
                                                        </td>
                                                        <td>
                                                            <input name='check_issue_security[]'  type='text' class='form-control' />
                                                        </td>
                                                        <td>
                                                            <input name='party_name_security[]'  type='text' class='form-control' />
                                                        </td>
                                                        <td>
                                                            <input name='check_deposite_security[]'  type='text' class='form-control' />
                                                        </td>
                                                        <td>
                                                            <input class='security btn btn-primary' type='button' value='Delete' />
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="text-primary">Other Name</label>
                                        <div style="border: 1px solid grey;">
                                            <center>
                                                <input id='other-row' class='btn btn-primary' type='button' value='Add More' /></center>
                                            <table id="test-table2" class="table table-condensed">
                                                <thead>
                                                <tr>
                                                    <th>Sr #</th>
                                                    <th>Particular</th>
                                                    <th>Amount</th>
                                                    <th>Vat</th>
                                                </tr>
                                                </thead>
                                                <tbody id="test-body2">
                                                <tr id="row0">
                                                    <td>
                                                        <input name='other_serial_no[]'  type='text' class='form-control' />
                                                    </td>
                                                    <td>
                                                        <input name='other_particular[]'   type='text' class='form-control' />
                                                    </td>
                                                    <td>
                                                        <input name='other_amount[]'  type='number' class='form-control' />
                                                    </td>
                                                    <td>
                                                        <input name='other_vat[]'  type='number' class='form-control' />
                                                    </td>
                                                    <td>
                                                        <input class="security btn btn-primary" type="button" value="Delete" />
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <br>

                             <div class="row">
                        <div class="col-md-12">
                           <label class="text-primary">For Internal Use</small></label>
                           <div style="border: 1px solid grey;">
                              <center>
                                 <!--<input type="button" id="security-row" class="btn btn-primary"  value="Add More" />-->
                                 <input id='internaluse-row' class='btn btn-primary' type='button' value='Add More' />
                              </center>
                              <div class="table-responsive">
                                 <table id="complex_header" class="table table-striped table-bordered display"
                                    style="width:100%">
                                    <thead>
                                       <tr>
                                          <th>Sr #</th>
                                          <th>Accounts Head</th>
                                          <th>Particulars</th>
                                          <th>Party</th>
                                          <th>Terms</th>
                                          <th>Percentage/Fixed Amount</th>
                                          <th>Rent Amount</th>
                                          <th>
                                             <center>Amount</center>
                                          </th>
                                          <th>Commission Terms</th>
                                          <th>Email</th>
                                          <th>
                                             <center>VAT</center>
                                          </th>
                                          <th>
                                             <center>Net Amount</center>
                                          </th>
                                          <th>
                                             <center>VAT Amount</center>
                                          </th>
                                          <th>
                                             <center>With VAT Amount</center>
                                          </th>
                                          <th>
                                             <center>charge to owner or no</center>
                                          </th>
                                          <center><th>-<th></center>
                                       </tr>
                                    </thead>
                                    <tbody id="internaluse-body">
                                       <tr id="row0">
                                          <td>
                                             <input name='serial_no[]' readonly  value="1" type="text" class="form-control" style="width:40px;"/>
                                          </td>
                                          <td>
                                              <select class="form-control" name="accounts_head[]">
                                                  <option selected value="Commission">Commission</option>
                                              </select>
                                          </td>
                                          <td>
                                            <input type="text" class="form-control" style="width:100px;" name="particulars_internal_use[]" placeholder="Particulars">
                                          </td>
                                          <td>
                                             <input type="text" class="form-control" style="width:100px;" name="party[]" placeholder="Party">
                                          </td>
                                          <td >
                                            <select class="form-control" name="terms[]" style="width:100px;">
                                                <option value="" selected disabled >Select One</option>
                                              <option value="Fixed Amount">Fixed Amount</option>
                                              <option value="Percentage">Percentage</option>
                                           </select>
                                          </td>
                                          <td ><input type="number" class="form-control" name="percentage_fixed_amount[]" placeholder="Enter Percentage or Enter Fixed Amount">
                                          </td>
                                          <td >
                                             <input type="number" class="form-control" style="width:100px;" name="rent_amount[]" placeholder="Rent Amount">
                                          </td>
                                          <td >
                                              <input type="number" class="form-control" style="width:100px;" name="commision_amount[]" placeholder="Commission Amount">
                                          </td>
                                          <td>
                                            <select class="form-control" style="width:100px;" name="vat_tax_type[]" >
                                              <option value="" selected disabled>Select Vat Type</option>
                                              <option value="Vat Included">Vat Included</option>
                                              <option value="Vat Not Included">Vat Not Included</option>
                                              <option value="No Vat">No Vat</option>
                                           </select>
                                          </td>
                                          <td>
                                             <input type="email" class="form-control" style="width:100px;" name="email[]" placeholder="Email">
                                          </td>
                                          <td>
                                            <input type="number" class="form-control" style="width:100px;"   name="vat1[]"  placeholder="0">
                                          </td>
                                          <td id="vat_amount">
                                             <input type="number" class="form-control" style="width:100px;" name="net_amount1[]" placeholder="0">
                                          </td>
                                          <td>
                                               <input type="number" class="form-control" style="width:100px;" name="vat_amount1[]" placeholder="0">
                                          </td>
                                           <td>
                                               <input type="number" class="form-control" style="width:100px;" name="with_vat_amount1[]" placeholder="0">
                                          </td>
                                          <td >
                                             <input type="checkbox" class="form-control" style="width:100px;" name="charge_to_owner1[]" >
                                          </td>

                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>

                                        <tr id="row0">
                                          <td>
                                             <input name='serial_no[]'  value='2' readonly type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                              <select class="form-control" name="accounts_head[]">
                                                  <option selected value="Management Fee">Management Fee</option>
                                              </select>
                                          </td>
                                          <td>
                                            <input type="text" class="form-control" name="particulars_internal_use[]" placeholder="Particular">
                                          </td>
                                          <td>
                                             <input type="text" class="form-control" name="party[]" placeholder="Party">
                                          </td>
                                          <td id="date_from">
                                            <select class="form-control" name="terms[]">
                                                  <option value="" selected disabled>Select One</option>
                                              <option value="Fixed Amount">Fixed Amount</option>
                                              <option value="Percentage">Percentage</option>
                                           </select>
                                          </td>
                                          <td ><input type="number" class="form-control" name="percentage_fixed_amount[]" placeholder="Enter Percentage or Enter Fixed Amount">
                                          </td>
                                          <td >
                                             <input type="number" class="form-control" name="rent_amount[]" placeholder="Rent Amount">
                                          </td>
                                          <td >
                                              <input type="number" class="form-control" name="commision_amount[]" placeholder="Commission Amount">
                                          </td>
                                          <td>
                                            <select class="form-control" name="vat_tax_type[]" >
                                              <option value="" selected disabled>Select Vat Type</option>
                                              <option value="Vat Included">Vat Included</option>
                                              <option value="Vat Not Included">Vat Not Included</option>
                                              <option value="No Vat">No Vat</option>
                                           </select>
                                          </td>
                                          <td>
                                             <input type="email" class="form-control" name="email[]" placeholder="Email">
                                          </td>
                                          <td>
                                            <input type="number" class="form-control"   name="vat1[]"  placeholder="0">
                                          </td>
                                          <td id="vat_amount">
                                             <input type="number" class="form-control" name="net_amount1[]" placeholder="0">
                                          </td>
                                          <td>
                                               <input type="number" class="form-control" name="vat_amount1[]" placeholder="0">
                                          </td>
                                           <td>
                                               <input type="number" class="form-control" name="with_vat_amount1[]" placeholder="0">
                                          </td>
                                          <td >
                                             <input type="checkbox" class="form-control" checked name="charge_to_owner1[]" >
                                          </td>

                                          <td>
                                             <input class='security btn btn-primary' type='button' value='Delete' />
                                          </td>
                                       </tr>

                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>

                     <br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Manual written performa of contract and Cheques scaned copy</label>
                                        <input type="file" class="form-control" name="scanned_doc">
                                    </div>
                                </div><br><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="border: 1px solid grey;">
                                            <center>
                                                <input id='add-row' class='btn btn-primary' type='button' value='Add More' /></center>
                                            <table id="test-table" class="table table-condensed">
                                                <thead>
                                                <tr>
                                                    <th>Attachments</th>
                                                    <th>Comment</th>
                                                </tr>
                                                </thead>
                                                <tbody id="test-body">
                                                <tr id="row0">
                                                    <td>
                                                        <input name='from_value[]'  type='file' class='form-control' />
                                                    </td>
                                                    <td>
                                                        <input name='to_value[]'  placeholder="Comment" type='text' class='form-control input-md' />
                                                    </td>
                                                    <td>
                                                        <input class='delete-row btn btn-primary' type='button' value='Delete' />
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                                <div class="text-center mt-4">
                                    <button class="btn btn-started"> Save</button>
                                </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">

        $(document).on("click", "#internaluse-row", function () {



        var new_row = '<tr id="row' + row1 + '">'
           +
               '<td><input name="serial_no[]" readonly   type="text" class="form-control" style="width:40px;"/>'+
                '</td>'+
                 '<td>'+
                      '<select class="form-control" name="accounts_head[]">'+
                      '<option  value="" selected disabled >Select One</option>'+
                           '<option  value="Commission">Commission</option>'+
                           '<option  value="Management Fee">Management Fee</option>'+
                                              '</select>'+
                '</td>'+
                 '<td>'+
                   '<input type="text" class="form-control" name="particulars_internal_use[]" placeholder="Particulars">'+
                 '</td>'+
                 '<td>'+
                   '<input type="text" class="form-control" name="party[]" placeholder="Party">'+
                 '</td>'+
                  '<td id="date_from">'+
                  '<select class="form-control" name="terms[]">'+
                  '<option value="" selected disabled>Select One</option>'+
                    '<option value="Fixed Amount">Fixed Amount</option>'+
                     '<option value="Percentage">Percentage</option>'+
                                           '</select>'+
                                          '</td>'+
                                          '<td >'+
                '<input type="number" class="form-control" name="percentage_fixed_amount[]" placeholder="Enter Percentage or Enter Fixed Amount">'+
                                          '</td>'+
                                          '<td >'+
                        '<input type="number" class="form-control" name="rent_amount[]" placeholder="Rent Amount">'+
                                          '</td>'+
                                          '<td >'+
                                    '<input type="number" class="form-control" name="commision_amount[]" placeholder="Commission Amount">'+
                                          '</td>'+
                                          '<td>'+
                                        '<select class="form-control" name="vat_tax_type[]" >'+
                                              '<option value="" selected disabled>Select Vat Type</option>'+
                                              '<option value="Vat Included">Vat Included</option>'+
                                              '<option value="Vat Not Included">Vat Not Included</option>'+
                                              '<option value="No Vat">No Vat</option>'+
                                           '</select>'+
                                          '</td>'+
                                          '<td>'+
                                            '<input type="email" class="form-control" name="email[]" placeholder="Email">'+
                                          '</td>'+
                                          '<td>'+
                                            '<input type="number" class="form-control" name="vat1[]" placeholder="0">'+
                                          '</td>'+
                                          '<td id="vat_amount">'+
                                             '<input type="number" class="form-control" name="net_amount1[]" placeholder="0">'+
                                          '</td>'+
                                          '<td>'+
                                               '<input type="number" class="form-control" name="vat_amount1[]" placeholder="0">'+
                                          '</td>'+
                                          '<td>'+
                                               '<input type="number" class="form-control" name="with_vat_amount1[]" placeholder="0">'+
                                          '</td>'+
                                          '<td >'+
                                             '<input type="checkbox" class="form-control" name="charge_to_owner1[]" placeholder="0">'+
                                          '</td>'+
                                          '<td>'+
                                             '<input class="security btn btn-primary" type="button" value="Delete" />'+
                                          '</td>'+
           '</tr>';
       $('#internaluse-body').append(new_row);
       row1++;
       return false;
   });



        function selectproject(value)
        {
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var request    = $.ajax({
        url: "{{url('/get_buildings')}}",
        method: "post",
        data: {_token: CSRF_TOKEN, pro_id:value},
        dataType: "html"
      });
      request.done(function( msg ) {
        //alert(msg);
        $('#selectBuilding').html(msg);
      });
        }


           function selectbuilding(value)
        {
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var request    = $.ajax({
        url: "{{url('/get_floors_detail')}}",
        method: "post",
        data: {_token: CSRF_TOKEN, building_id:value},
        dataType: "html"
      });
      request.done(function( msg ) {
          var res = msg.split("**");

        $('#selectFloor').html(res[0]);
        $('#location').val(res[1]);
        $('#permisses_no').val(res[2]);
        $('#plot_no').val(res[3]);

      });
        }


         function selectfloor(value)
        {
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var request    = $.ajax({
        url: "{{url('/get_flats')}}",
        method: "post",
        data: {_token: CSRF_TOKEN, building_id:value},
        dataType: "html"
      });
      request.done(function( msg ) {
        //alert(msg);
        $('#selectFlat').html(msg);
      });
        }



             function selectflat(value)
        {
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var request    = $.ajax({
        url: "{{url('/get_lanlords_detail')}}",
        method: "post",
        data: {_token: CSRF_TOKEN, flat_id:value},
        dataType: "html"
      });
      request.done(function( msg ) {
        //alert(msg);
        var res = msg.split("**");
        $('#LanLord_id').val(res[0]);
        $('#LanLord').val(res[1]);
        $('#property_size').val(res[2]);
        $('#property_type_id').val(res[3]);
        $('#property_type').val(res[4]);
        $('#management_fee').val(res[5]);
      });
        }


        $(document).on("change" , "#no_installments" , function () {
            $('#rowTable').html('');
            let no_installments = $(this).val();
            let totalAmount = $('#totalAmount').val();
            console.log('value');
            console.log(no_installments);
            console.log(totalAmount);
            let totalA = totalAmount/no_installments;
            for (var i = 1; i <= no_installments; i++) {
                console.log('count iiiii');
                console.log(i);
                let Tblhtml = '<tr>' +
                    '<td><input type="number"  name="no_installment[]" value="'+ i +'" /></td>' +
                    '<td><input type="text"  name="amount[]" value="'+ totalA +'" /></td>' +
                    '<td><input type="text"  name="particular[]" value="" /></td>' +
                    '<td><select  name="pay_type[]" style="width:150px;" class="form-control">' +
                        '<option value="" selected disabled> select type </otpion>' +
                        '<option value="Cash"> Cash </otpion>' +
                        '<option value="Check"> Check </otpion>' +
                    '</select></td>' +
                    '<td><input  type="number" name="check_no[]" /></td>' +
                    '<td><input  type="date" name="check_date[]" /></td>' +
                    '<td><input  type="text" name="check_issue[]" /></td>' +
                    '<td><input  type="text" name="party_name[]" /></td>' +
                    '<td>' +
                        '<select  id="selectOpt" name="selectOpt[]" class="form-control">' +
                            '<option value="" selected disabled>select option</option>' +
                            '<option>company</option>' +
                            '<option>landlord</option>' +
                        '</select>' +
                    '</td>' +
                    '</tr>';
                $('#rowTable').append(Tblhtml);
            }
            $('#rowTableDisplay').show();

        });


    </script>
    <script type="text/javascript">
        // Add row
        var row=1;
        $(document).on("click", "#add-row", function () {
            var new_row = '<tr id="row' + row + '"><td><input name="from_value[]" type="file" class="form-control" /></td><td><input name="to_value[]" type="text" placeholder="Comment" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';

            $('#test-body').append(new_row);
            row++;
            return false;
        });

        // Remove criterion
        $(document).on("click", ".delete-row", function () {
            //  alert("deleting row#"+row);
            if(row>1) {
                $(this).closest('tr').remove();
                row--;
            }
            return false;
        });

        // Add row
        var row1=1;
        $(document).on("click", "#security-row", function () {
            var new_row = '<tr id="row' + row1 + '">'
                +
                    '<td>' +
                    '<input name="serial_no[]" type="text" class="form-control" /></td>' +
                    '<td><input name="check_no_security[]" type="text"  class="form-control" /></td>' +
                    '<td><input name="check_date_security[]" type="date"  class="form-control" /></td>' +
                    '<td><input name="check_issue_security[]" type="text" class="form-control" /></td>' +
                    '<td><input name="party_name_security[]" type="text" class="form-control" /></td>' +
                    '<td><input name="check_deposite_security[]" type="text" class="form-control" /></td>' +
                    '<td><input class="security btn btn-primary" type="button" value="Delete" /></td>'
                +
                '</tr>';
            $('#test-body1').append(new_row);
            row1++;
            return false;
        });

        // Remove criterion
        $(document).on("click", ".security", function () {
            //  alert("deleting row#"+row);
            if(row1>1) {
                $(this).closest('tr').remove();
                row1--;
            }
            return false;
        });



        // Add row
        var row2=1;
        $(document).on("click", "#other-row", function () {
            var new_row = '<tr id="row' + row2 + '">'
                +
                '<td>' +
                '<input name="other_serial_no[]" type="text" class="form-control" /></td>' +
                '<td><input name="other_particular[]" type="text"  class="form-control" /></td>' +
                '<td><input name="other_amount[]" type="number"  class="form-control" /></td>' +
                '<td><input name="other_vat[]" type="number" class="form-control" /></td>' +
                '<td><input class="other-row btn btn-primary" type="button" value="Delete" /></td>'
                +
                '</tr>';
            $('#test-body2').append(new_row);
            row1++;
            return false;
        });

        // Remove criterion
        $(document).on("click", ".other-row", function () {
            //  alert("deleting row#"+row);
            if(row1>1) {
                $(this).closest('tr').remove();
                row1--;
            }
            return false;
        });

    </script>

@endsection

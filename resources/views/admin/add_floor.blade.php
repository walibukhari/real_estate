@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'add_floor'
])

@section('content')
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
           @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('error') !!}</strong>
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
        <style type="text/css">
            .filelabel {
    width: 100%;
    border: 2px dashed grey;
    border-radius: 5px;
    display: block;
    padding: 5px;
    transition: border 300ms ease;
    cursor: pointer;
    text-align: center;
    margin: 0;
}
.filelabel i {
    display: block;
    font-size: 30px;
    padding-bottom: 5px;
}
.filelabel i,
.filelabel .title {
  color: grey;
  transition: 200ms color;
}
.filelabel:hover {
  border: 2px solid #1665c4;
}
.filelabel:hover i,
.filelabel:hover .title {
  color: #1665c4;
}
#FileInput{
    display:none;
}
        </style>
                <div class="row">
  <div class="col-12">    
  <div class="row">
      <div class="col-md-3">
          
      </div>
       <div class="col-md-3">
          
      </div>
       <div class="col-md-4">
          
      </div>
       <div class="col-md-2">
            <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Create New</button>
      </div>
  </div>     
    <div class="material-card card">
      <div class="card-body">

        <h4 class="card-title">Floors List</h4>
        <h6 class="card-subtitle">
        </h6><br>
        <div class="table-responsive">
          <table id="complex_header" class="table table-striped table-bordered display"
          style="width:100%">
          <thead>

            <tr>
                     <th>#</th>
                     <th>Building</th>
                     <th>Floor Type</th>
                     <th>Name</th>
                     <th>No of Flats</th>
                     <th>Actions</th>
                    </tr>
                    
          </thead>
          <tbody>
            @foreach($floorlist as $floorlists)
          <tr>
            <td>{{$counter++}}</td>               
            <td>{{$floorlists->Building_name->name}}</td>
            <td>{{$floorlists->Floor_type_name->name}}</td>
            <td>{{$floorlists->name}}</td>
            <td>{{$floorlists->no_of_flats}}</td>
            <td>
              <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editModal{{$floorlists->id}}">Eidt </button>
             <!-- <a href="{{url('/delete_floorlists')}}/{{$floorlists->id}}" onclick="return confirm('Are you sure?')" class="btn btn-danger">Delete</a> -->
           </td>
         </tr>
         <div class="modal fade" id="editModal{{$floorlists->id}}"  role="dialog" >
    <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Building Facilities</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
                   <div class="p-5">
                  <form action="{{url('Admin/edit_floorslist')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                   <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">Name</label>
                        <input type="hidden" name="floor_list_id" value="{{$floorlists->id}}">
                        <input type="text"  class="form-control"  name="name" value="{{$floorlists->name}}" required="">
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">No. of Flats</label>
                        <input type="text" value="{{$floorlists->no_of_flats}}" class="form-control"  name="no_of_flats" value="" required="">
                       </div>
                  </div>
                  <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">Building</label>
                        <select class="form-control" name="building_id" required="">
                        <option value="" selected disabled>Select Building</option>
                        @foreach($buildinglist as $buildinglists)
                        <option @if($floorlists->building_id==$buildinglists->id) selected @endif value="{{$buildinglists->id}}">{{$buildinglists->name}}-{{$buildinglists->status}}</option>
                        @endforeach
                                                </select>
                       </div>
                        <div class="col-md-6">
                        <label class="text-primary">Floor Type</label>
                        <select class="form-control" name="floor_type_id" required="">

                        <option value="" selected="" disabled="">Select Floor Type</option>
                        @foreach($floortype as $floortypes)
                        <option @if($floorlists->floor_type_id==$floortypes->id) selected @endif value="{{$floortypes->id}}">{{$floortypes->name}}</option>
                        @endforeach                        </select>
                       </div>
                  </div> 
                <button  type="submit" name="createNewFloor" class="btn btn-primary btn-user btn-block">
                 Create
               </button>
              </form>
           </div>
        
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
        @endforeach
          
      </tbody>
      <tfoot>
              <tr>
                     <th>#</th>
                     <th>Building</th>
                     <th>Floor Type</th>
                     <th>Name</th>
                     <th>No of Flats</th>
                     <th>Actions</th>
                    </tr>
      </tfoot>
     </table>
    </div>
   </div>
  </div>
 </div>
</div>
  <div class="modal fade" id="myModal"  role="dialog" >
    <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">New Floor Registration Form</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
                   <div class="p-5">
                   <form action="{{url('Admin/save_floors')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        
                         <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">Building</label>
                        <select class="form-control" name="building_id" required="">
                        <option value="" selected disabled>Select Building</option>
                        @foreach($buildinglist as $buildinglists)
                        <option value="{{$buildinglists->id}}">{{$buildinglists->name}}-{{$buildinglists->status}}</option>
                        @endforeach
                                                </select>
                       </div>
                        <div class="col-md-6">
                        <label class="text-primary">Floor Type</label>
                        <select class="form-control" name="floor_type_id" required="">

                        <option value="" selected="" disabled="">Select Floor Type</option>
                        @foreach($floortype as $floortypes)
                        <option value="{{$floortypes->id}}">{{$floortypes->name}}</option>
                        @endforeach                        </select>
                       </div>
                  </div>
                  <br>
                   <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">Number/Name of the Floor</label>
                        <input type="text"  class="form-control"  name="name" value="" required="">
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">No. of Flats</label>
                        <input type="text"  class="form-control"  name="no_of_flats" value="" required="">
                       </div>
                  </div>
                  
                <button  type="submit" name="createNewFloor" class="btn btn-primary btn-user btn-block">
                 Create
               </button>
              </form>
           </div>
        
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>


@endsection

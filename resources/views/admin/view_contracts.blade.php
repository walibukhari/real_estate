<!DOCTYPE html>
<html>
<head>
  <title>Print</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
.col_six{

    width:100%;
    border-bottom: 2px dashed black;
    border-bottom-color:rgb(0,0,120);
}

.customCol12{
    display:flex;
    align-items:center;
}
.col_six12{
    width: 55%;
    border-bottom: 2px dashed black;
    border-bottom-color: rgb(0,0,120);
    position:relative;
    left:30px;
    font-size: 14px;
}
.customFontSize{
    font-size:25px;
    color:rgb(0,0,120);
        display: flex;
    align-items: center;
    justify-content: center;
        font-weight: bold;
}
.setCustomBorder{
   color: rgb(0,0,120);
    border: 2px solid rgb(0,0,120);
    border-radius: 20px;
}
.text-primary{
    color:rgb(0,0,120) !important;
        font-weight: 500;
}
.customSetLine{
       display: flex;
    align-items: center;
}
.custum-label{
    width:124px;
    font-size:17px;
}
.custum-label2{
    width:150px;
    font-size:17px;
}
.custum-label3{
    width:140px;
    font-size:17px;
}
.custum-label4{
    width:158px;
    font-size:17px;
}
.custum-label5{
    width:165px;
    font-size:17px;
}
.custum-label01{
    width:83px;
    font-size:17px;
}
.custum-label011{
    width:76px;
    font-size:17px;
}
.custum-label51{
    width:234px;
    font-size:17px;
}
.custum-label52{
    width:202px;
    font-size:17px;
}
.custum-label0111{
    width:52px;
    font-size:17px;
}
.custum-label321{
    width:303px;
    font-size:17px;
}

/*------------------terms and condidtions---------------------*/

.customHeaderTermCondition{
    background: rgb(0,0,120);
    padding: 20px;
    border-radius: 15px;
    margin-bottom: 15px;
}
.customSH2{
    font-size: 22px;
    font-weight: bold;
    color: #fff;
    margin: 0px;
}
.customTCP1{
    display:flex;
    align-items: center;
}
.customP1{
   padding: 4px 11px;
    display: flex;
    align-items: center;
    justify-content: center;
    border: 5px solid #efefef;
    border-radius: 20px;
    color: rgb(0,0,120) !important;
    font-weight:bold;
}
.customP12{
        margin-left: 5px;
    color: rgb(0,0,120) !important;
}
.customRowTop{
    margin-top:15px;
    margin-bottom:25px;
}
.customP1Bolit{
    background: rgb(0,0,120);
    color: rgb(0,0,120);
    padding: 6px;
    border-radius: 21px;
}
.customP123{
     width:100%;
    border-bottom: 2px dashed black;
    border-bottom-color:rgb(0,0,120);
}
.customTCP12{
    display:flex;
}
</style>
<body>
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <br>
                        
                        <div class="container">
                            <div class="row" >
                                <div class="col-md-6" >
                                    <img src="{{asset('/assets/images/images.jpg')}}"  height="60" width="180"/> 
                                </div>
                                <div class="col-md-6">
                                    <img src="{{asset('/assets/images/logo.png')}}" style="float:right" height="65" width="205"/>        
                                </div>
                            </div>
                            
                                <div class="row setCustomBorder">
                                    <div class="col-md-4">
                                        <div class="col-md-12 customCol12" style="position:relative;top:8px;">
                                            <label class="text-primary">Date:</label>
                                            <p  class="col_six12">{{date('Y-m-d')}}</p>
                                        </div>
                                        <div class="col-md-12 customCol12"> 
                                             <label class="text-primary">No.</label>
                                            <p  class="col_six12">1234431</p>
                                        </div>
                                    </div>
                                    <div class="col-md-5 customFontSize">
                                        <center><h2 class="customFontSize">TENANCY CONTRACT</h2></center>
                                    </div>
                                    <div class="col-md-3">
                                        
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12 customSetLine">
                                      <label class="custum-label text-primary">Owner Name</label> 
                                       @if($contract->Landlord_Name)
                                       <p  class="col_six">{{$contract->Landlord_Name->name}}</p> 
                                       <!--<label class="custum-label text-primary">اسم المالك</label>-->
                                       @else
                                       <br>
                                       <!--<p  class="col_six"></p>  <label class="custum-label text-primary">اسم المالك</label> -->
                                       @endif
                                    </div>
                                </div><br>
                                
                                  <div class="row">
                                    <div class="col-md-12 customSetLine">
                                      <label class="custum-label2 text-primary">Landlord Name</label> 
                                        @if($contract->Landlord_Name)
                                       <p  class="col_six">{{$contract->Landlord_Name->name}}</p>
                                       @else
                                       <br>
                                       <p  class="col_six"></p>
                                       @endif
                                    </div>
                                </div><br>
                                
                                  <div class="row">
                                    <div class="col-md-12 customSetLine">
                                         <label class="custum-label text-primary">Tenant Name</label> 
                                      @if($contract->Tenant_Name)
                                       <p  class="col_six">{{$contract->Tenant_Name->name}}</p>
                                       <!--<label class="custum-label text-primary">اسم المستأجر</label> -->
                                       @else
                                       <br>
                                       <p  class="col_six"></p>
                                           <!--<label class="custum-label text-primary">اسم المستأجر</label> -->
                                       @endif
                                    </div>
                                </div><br>
                                
                                
                              
                                
                                
                                
                                <div class="row">
                                               <div class="col-md-6 customSetLine">
                                                <label class="custum-label3 text-primary">Tenant Email</label> 
                                       @if($contract->Tenant_Name)
                                       <p  class="col_six">{{$contract->Tenant_Name->email}}</p>
                                       <!--<label class="custum-label text-primary">البريد الإلكتروني للمستأجر</label> -->
                                       @else
                                       <br>
                                       <p  class="col_six"></p>
                                       <!--<label class="custum-label text-primary">البريد الإلكتروني للمستأجر</label> -->
                                       @endif
                                                </div> 
                                               <div class="col-md-6 customSetLine">
                                                   <label class="custum-label4 text-primary">Landlord Email</label> 
                                                    @if($contract->Landlord_Name)
                                       <p  class="col_six">{{$contract->Landlord_Name->email}}</p>
                                       @else
                                       <br>
                                       <p  class="col_six"></p>
                                       @endif
                                                </div> 
                                  
                                    
                                </div><br>
                                
                                
                                
                                  <div class="row">
                                               <div class="col-md-6 customSetLine">
                                                    <label class="custum-label3 text-primary">Tenant Phone</label> 
                                    @if($contract->Tenant_Name)
                                       <p  class="col_six">{{$contract->Tenant_Name->phone}}</p>
                                       <!--<label class="custum-label3 text-primary">هاتف المستأجر</label> -->
                                       @else
                                       <br>
                                       <p  class="col_six"></p>
                                       <!--<label class="custum-label3 text-primary">هاتف المستأجر</label> -->
                                       @endif
                                                </div> 
                                               <div class="col-md-6 customSetLine">
                                                    <label class="custum-label5 text-primary">Landlord Phone</label> 
                                      @if($contract->Landlord_Name)
                                       <p  class="col_six">{{$contract->Landlord_Name->phone}}</p>
                                       @else
                                       <br>
                                       <p  class="col_six"></p>
                                       @endif
                                                </div> 
                                    
                                </div><br>
                                
                                
                                    <div class="row">
                                               <div class="col-md-6 customSetLine">
                                                   <label class="custum-label4 text-primary">Building Name</label> 
                                                 @if($contract->Building_Name)
                                                   <p  class="col_six">{{$contract->Building_Name->name}}</p>
                                                   <!--<label class="custum-label3 text-primary">اسم المبنى</label> -->
                                                   @else
                                                   <br>
                                                   <p  class="col_six"></p>
                                                   <!--<label class="custum-label3 text-primary">اسم المبنى</label> -->
                                                   @endif
                                                   
                                                </div> 
                                                
                                               <div class="col-md-6 customSetLine">
                                                   <label class="custum-label01 text-primary">Location</label> 
                                                   <p  class="col_six">{{$contract->location}}</p>
                                                   <!--<label class="custum-label4 text-primary">موقعك</label>-->
                                                </div> 
                                             </div><br>
                                
                                  <div class="row">
                                     <div class="col-md-4 customSetLine">
                                          <label class="custum-label5 text-primary">Property Size</label> 
                                         @if($contract->property_size)
                                        <p  class="col_six">{{$contract->property_size}}</p>
                                        <!--<label class="custum-label5 text-primary">Property Size</label>-->
                                        @else
                                        <br>
                                        <p  class="col_six">{{$contract->property_size}}</p>
                                        <!--<label class="custum-label5 text-primary">Property Size</label>-->
                                        @endif
                                       
                                    </div>
                                     <div class="col-md-4 customSetLine">
                                         <label class="custum-label5 text-primary">Property Type</label> 
                                         @if($contract->Flat_type_name)
                                        <p  class="col_six">{{$contract->Flat_type_name->name}}</p>
                                        @else
                                        <br>
                                        <p  class="col_six"></p>
                                        @endif
                                       
                                    </div>
                                     <div class="col-md-4 customSetLine">
                                         <label class="custum-label5 text-primary">Property No.</label> 
                                        <p  class="col_six">{{$contract->flat_id}}</p>
                                       
                                    </div>
                                </div><br>
                                
                                
                                <div class="row">
                                     <div class="col-md-6 customSetLine">
                                          <label class="custum-label51 text-primary">Permisses No<small>(DEWA)</small></label> 
                                        @if($contract->permisses_no)
                                        <p  class="col_six">{{$contract->permisses_no}}</p>
                                        @else
                                        <br>
                                        <p  class="col_six">{{$contract->permisses_no}}</p>
                                        @endif
                                       
                                    </div>
                                    <div class="col-md-6 customSetLine">
                                         <label class="custum-label011 text-primary">Plot No.</label> 
                                        @if($contract->plot_no)
                                        <p  class="col_six">{{$contract->plot_no}}</p>
                                        @else
                                        <br>
                                        <p  class="col_six">{{$contract->plot_no}}</p>
                                        @endif
                                       
                                    </div>
                                </div><br>
                                    
                                
                                
                                
                                  <div class="row">
                                     <div class="col-md-6 customSetLine">
                                        <label class="custum-label52 text-primary">Contract Period <small>To</small></label> 
                                        <p  class="col_six">{{$contract->contract_from}}</p>
                                       <!--<label class="custum-label0111 text-primary">إلى</label> -->
                                    </div>
                                    <div class="col-md-6 customSetLine">
                                        <label class="custum-label0111 text-primary">From</label> 
                                        <p  class="col_six">{{$contract->contract_to}}</p>
                                        <!--<label class="custum-label0111 text-primary">فترة العقد من</label> -->
                                        
                                       
                                    </div>
                                </div><br>
                                
                                
                                 <div class="row">
                                     <div class="col-md-12 customSetLine">
                                         <label class="custum-label text-primary">Annual Rent</label>
                                         @if($contract->contract_amount)
                                          <p  class="col_six">{{$contract->contract_amount}}</p>
                                          <!--<label class="custum-label text-primary">الإيجار السنوي</label>-->
                                          @else
                                          <br>
                                          <p  class="col_six">{{$contract->contract_amount}}</p>
                                          <!--<label class="custum-label text-primary">الإيجار السنوي</label>-->
                                          @endif
                                       
                                    </div>
                                </div><br>
                                
                                  <div class="row">
                                     <div class="col-md-12 customSetLine">
                                         <label class="custum-label3 text-primary">Contract Value </label>
                                           @if($contract->contract_amount)
                                          <p  class="col_six">{{$contract->contract_amount}}</p>
                                          <!--<label class="custum-label3 text-primary">قيمة العقد</label>-->
                                          @else
                                          <br>
                                          <p  class="col_six">{{$contract->contract_amount}}</p>
                                          <!--<label class="custum-label3 text-primary">قيمة العقد </label>-->
                                          @endif
                                       
                                    </div>
                                </div><br>
                                
                                 <div class="row">
                                     <div class="col-md-6 customSetLine">
                                         <label class="custum-label321 text-primary">Security Deposit Amount </label> 
                                         @if($contract->Deposit_Amount)
                                          <p  class="col_six">{{isset($contract->Deposit_Amount->security_deposit_amount) ? $contract->Deposit_Amount->security_deposit_amount : 0}}</p>
                                          <label class="custum-label3 text-primary">مبلغ التأمين </label>
                                          @else
                                          <br>
                                          <p  class="col_six">0</p>
                                          <!--<label class="custum-label3 text-primary">مبلغ التأمين </label>-->
                                          @endif
                                       
                                    </div>
                                     <div class="col-md-6 customSetLine">
                                         <label class="custum-label52 text-primary">Mode of Payment </label> 
                                           @if($contract->installments_no)
                                          <p  class="col_six">{{$contract->installments_no}}</p>
                                          @else
                                          <br>
                                          <p  class="col_six">{{$contract->installments_no}}</p>
                                          @endif
                                       
                                    </div>
                                </div><br>
                                
                                
                                
                                


                                <div class="text-center mt-4">
                                    <!--<button class="btn btn-started"> Save</button>-->
                                </div>
                    </div>
                        <!--  terms and conditions -->
                        <div class="container">
                            <div class="row customHeaderTermCondition">
                                <h2 class="customSH2">Terms & Conditions:</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1">1</p>
                                    <p class="customP12">
                                        The tenant has inspected the premises and agreed to lease the unit on it s current condition. 
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1">2</p>
                                    <p class="customP12">
                                        Tenant undertakes to use the premises for designated purpose,
                                        tenant has no rights to transfer or relinquish the tenancy contract either with or
                                        without counterpart to any without landlord written approval Also tenan
                                        is not allowed to sublease the premises or any part thereof to third party
                                        in  whole or in part unless it is legally permitted.
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1">3</p>
                                    <p class="customP12">
                                       The tenant undertakes not t o  make any amendments, modifications or
                                       addendums to the premises subject of the contract without obt aining the
                                       landlord written approval, tenant shall be liable for any damages or f ailure due to that. 
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1">4</p>
                                    <p class="customP12">
                                        The tenant shall be responsible for payment of all electricity,  water, 
                                         cooling and gas charges resulting of occupying leased unit unless other condition agreed in written. 
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1">5</p>
                                    <p class="customP12">
                                       The tenant  must pay the rent amount in  the m anner and dates agreed
                                       with the landlord.
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1">6</p>
                                    <p class="customP12">
                                       The Tenant fully undertakes  to comply with all  the regulations and
                                       instructions related to the management ofthe property and the use of the
                                       premises and of common areas such (parking, swimming pools, 
                                       gymnasium, etc...).
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1">7</p>
                                    <p class="customP12">
                                       Tenancy contract parties declare all mentioned emails addresses and
                                       phone numbers are correct, all formal and legal notifications will be sent
                                       to those addresses in case of dispute between parties.
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1">8</p>
                                    <p class="customP12">
                                       The  Landlord undertakes  to enable  the  t enant of the  full use of t he
                                       premises including its facilities (Swimming pool, gym, parking lot, et c)
                                       and do th e  regular  maintenance as intended unless other condition
                                       agreed in written, and not to do any act that would detract from t he premises benefit
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1">9</p>
                                    <p class="customP12">
                                       By signing this agreement from the first party, the "Landlord' hereby 
                                       confirms and undertakes that he is the current owner of the property or
                                       his legal representative under legal power of attorney duly entitled by the
                                       com petent authorities.
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row customRowTop">
                                <div class="col-md-6 text-center">
                                    <p class="customP12 text-center">Tenant Signature</p>    
                                    <div class="col-md-12 customCol12" style="position:relative;top:8px;    display: flex;
    justify-content: center;">
                                        <label class="text-primary">Date:</label>
                                        <p  class="col_six12"style="text-align:left;">{{date('Y-m-d')}}</p>
                                    </div>
                                </div>
                                <div class="col-md-6 text-center">
                                    <p class="customP12 text-center">Landlord Signature</p>
                                    <div class="col-md-12 customCol12" style="position:relative;top:8px;    display: flex;
    justify-content: center;">
                                        <label class="text-primary">Date:</label>
                                        <p  class="col_six12" style="text-align:left;">{{date('Y-m-d')}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- know your rights -->
                        <div class="container">
                            <div class="row customHeaderTermCondition">
                                <h2 class="customSH2">Know your rights: </h2>
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1Bolit"></p>
                                    <p class="customP12">
                                       You may visit Rental Dispute Center website www.rdc.gov.ae and use Smart Judge service in case of any rental dispute between parties. 
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1Bolit"></p>
                                    <p class="customP12">
                                       Law No 26 of 2007 regulating relationship betw een landlords and tenants. 
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1Bolit"></p>
                                    <p class="customP12">
                                       Law No 33 of 2008 amending law 26 of year 2007.
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1Bolit"></p>
                                    <p class="customP12">
                                       Law No 43 of 2013 determining rent increases for properties. 
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                        </div>
                        <br>
                        <!-- Attachments for EJARI registration -->
                        <div class="container">
                            <div class="row customHeaderTermCondition">
                                <h2 class="customSH2">Attachments for EJARI registration:</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1">1</p>
                                    <p class="customP12">
                                       Original unified tenancy contract.
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1">2</p>
                                    <p class="customP12">
                                       Copy of Emirates ID or passport for tenant (individuals) Or trade license 2 for tenant (companies).
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP1">3</p>
                                    <p class="customP12">
                                       Original Emirates ID of applicant or representative card by DNRD.
                                    </p>
                                </div>
                                <!--<div class="col-md-6">-->
                                    
                                <!--</div>-->
                            </div>
                        </div>
                         <br>
                        <!-- Additionals Terms -->
                        <div class="container">
                            <div class="row customHeaderTermCondition">
                                <h2 class="customSH2">Additional Terms: </h2>
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP12">
                                    <p class="customP1">1</p>
                                    <p class="customP123">
                                         <textarea class="form-control customP12" style="border-style:none;overflow:hidden;"  rows="2" ></textarea>
                                    </p>
                                   
                                    <p class="customP1">1</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP12">
                                    <p class="customP1">2</p>
                                    <p class="customP123">
                                    </p>
                                    <p class="customP1">2</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP12">
                                    <p class="customP1">3</p>
                                    <p class="customP123">
                                    </p>
                                    <p class="customP1">3</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP12">
                                    <p class="customP1">4</p>
                                    <p class="customP123">
                                    </p>
                                    <p class="customP1">4</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP12">
                                    <p class="customP1">5</p>
                                    <p class="customP123">
                                    </p>
                                    <p class="customP1">5</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP12">
                                    <p class="customP1">6</p>
                                    <p class="customP123">
                                    </p>
                                    <p class="customP1">6</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP12">
                                    <p class="customP1">7</p>
                                    <p class="customP123">
                                    </p>
                                    <p class="customP1">7</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 customTCP12">
                                    <p class="customP1">8</p>
                                    <p class="customP123">
                                    </p>
                                    <p class="customP1">8</p>
                                </div>
                            </div>
                        </div>
                         <br>
                        <!-- Note -->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP12">
                                        <b>Note:</b> You may add an addendum to this tenancy contract  in case you have additional t erms while it needs to  be signed  by all  parties.
                                    </p>
                                </div>
                            </div>
                            <div class="row customRowTop">
                                <div class="col-md-6 text-center">
                                    <p class="customP12 text-center">Tenant Signature</p>    
                                    <div class="col-md-12 customCol12" style="position:relative;top:8px;    display: flex;
    justify-content: center;">
                                        <label class="text-primary">Date:</label>
                                        <p  class="col_six12"style="text-align:left;">{{date('Y-m-d')}}</p>
                                    </div>
                                </div>
                                <div class="col-md-6 text-center">
                                    <p class="customP12 text-center">Landlord Signature</p>
                                    <div class="col-md-12 customCol12" style="position:relative;top:8px;    display: flex;
    justify-content: center;">
                                        <label class="text-primary">Date:</label>
                                        <p  class="col_six12" style="text-align:left;">{{date('Y-m-d')}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <br>
                        <!-- Note -->
                        <div class="container">
                            <div class="row setCustomBorder">
                                <div class="col-md-12 customTCP1">
                                    <p class="customP12" style="    height: 55px;
    display: flex;
    align-items: center;
    margin-bottom: 0px;
    width:100%;
       justify-content: center;
    ">
                                        Tel: 8004488 F ax: +9714 222 2251 P.O .Box  1166, Dubai, U .A.E
                                    </p>
                                </div>
                            </div>
                        </div>
                        <br>
                       <!--<center> <h3>Add Terms & Conditions</h3></center>-->
                       <!-- <textarea class="form-control" style="border:none">cdjfasdiofioashdfioashid;ofhas;iodfasiodvnas;oi nasod has8odyv'oasdjcasodhvoashdvi;asvdasd</textarea>-->
                    </div>
                </div>
            </div>
        </div>
</body>
</html>

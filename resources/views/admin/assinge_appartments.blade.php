@extends('layouts.app', [
'class' => '',
'elementActive' => 'add_landlords'
])
@section('content')
<div class="content">
   @if (session('status'))
   <div class="alert alert-success" role="alert">
      {{ session('status') }}
   </div>
   @endif
   @if (session('password_status'))
   <div class="alert alert-success" role="alert">
      {{ session('password_status') }}
   </div>
   @endif
   @if (Session::has('error'))
   <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{!! Session('error') !!}</strong>
   </div>
   @endif
   @if ($errors->any())
   <div class="alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
   @endif
   <style type="text/css">
      .filelabel {
      width: 100%;
      border: 2px dashed grey;
      border-radius: 5px;
      display: block;
      padding: 5px;
      transition: border 300ms ease;
      cursor: pointer;
      text-align: center;
      margin: 0;
      }
      .filelabel i {
      display: block;
      font-size: 30px;
      padding-bottom: 5px;
      }
      .filelabel i,
      .filelabel .title {
      color: grey;
      transition: 200ms color;
      }
      .filelabel:hover {
      border: 2px solid #1665c4;
      }
      .filelabel:hover i,
      .filelabel:hover .title {
      color: #1665c4;
      }
      #FileInput{
      display:none;
      }

   </style>
   <div class="row">
      <div class="col-12">
         <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-4">
            </div>
         </div>
         <div class="material-card card">
            <div class="card-body">
               <h4 class="card-title">Landlords  List</h4>
               <h6 class="card-subtitle">
               </h6>
               <br>
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{session()->get('message')}}
            </div>
        @endif
        <div class="table-responsive">
            <div class="row">
                <div class="col-md-12 text-right" style="padding-right: 20px;">
                    <button class="btn btn-success"  data-toggle="modal" data-target="#myModal">Create</button>
                </div>
            </div>
          <table id="complex_header" class="table table-striped table-bordered display"
          style="width:100%">
          <thead>

            <tr>
              <th>Sr#</th>
              <th>Username</th>
              <th>Building Name</th>
              <th>Flat Unit Code</th>
              <th>Type</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody id="tbodayData">
                @foreach($getAssignUser aS $data)
                    <tr>
                        <td>{{$data->id}}</td>
                        <td>{{$data->user->name}}</td>
                        <td>{{$data->building->name}}</td>
                        <td>{{$data->flat_id}}</td>
                        <td>{{$data->user_type}}</td>
                        <td><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myEditModal-{{$data->id}}">Edit</button></td>
                    </tr>


                    <div class="modal fade" id="myEditModal-{{$data->id}}"  role="dialog" >
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Update Status</h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="p-5">
                                        <form class="form" method="POST" action="{{route('Admin_Update_Status')}}">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}" />
                                            <div class="container">
                                                <div class="row">
                                                    <label>Select Status</label>
                                                    <select class="form-control" name="status">
                                                        <option>Select Status</option>
                                                        @if($data->status == \App\Segregation::ACTIVE)
                                                            <option value="1" selected>Active</option>
                                                            <option value="2">De-Active</option>
                                                        @else
                                                            <option value="1">Active</option>
                                                            <option value="2" selected>De-Active</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="text-center mt-4">
                                                <button type="submit" class="btn btn-started"> Update</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
      <tfoot>
        <tr>
            <th>Sr#</th>
            <th>Username</th>
            <th>Building Name</th>
            <th>Flat Unit Code</th>
            <th>Status</th>
            <th>Actions</th>
            </tr>
      </tfoot>
     </table>
    </div>

            </div>

         </div>
      </div>
   </div>
       <div class="modal fade" id="myModal"  role="dialog" >
           <div class="modal-dialog modal-lg" >
               <div class="modal-content">
                   <div class="modal-header">
                       <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                       <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">×</span>
                       </button>
                   </div>
                   <div class="modal-body">
                       <div class="p-5">
                           <form class="form">
                               @csrf
                               <div class="container">
                                   <div class="row">
                                       <div class="col-md-4">
                                           <label class="text-primary">Select Project</label>
                                           <select class="form-control" id="project_id">
                                               @foreach($projects as $pro)
                                                   <option value="{{$pro->id}}">{{$pro->name}}</option>
                                               @endforeach
                                           </select>
                                       </div>
                                       <div class="col-md-4">
                                           <label class="text-primary">Select Building</label>
                                           <select class="form-control" id="building_id">
                                               @foreach($buildings as $build)
                                                   <option value="{{$build->id}}">{{$build->name}}</option>
                                               @endforeach
                                           </select>
                                       </div>
                                       <div class="col-md-4">
                                           <label class="text-primary">Select Flat</label>
                                           <select class="form-control" id="flat_id">
                                               @foreach($flat as $fl)
                                                   <option value="{{$fl->unit_code}}">{{$fl->unit_code}}</option>
                                               @endforeach
                                           </select>
                                       </div>
                                   </div>
                                   <br>
                                   <div class="row">
                                       <div class="col-md-6">
                                           <label class="text-primary">Select Types</label>
                                           <select class="form-control" id="userType" onchange="onChangeFunction()">
                                               <option>Select Type</option>
                                               <option value="Tenant">Tenant</option>
                                               <option value="Landlord">Landlord</option>
                                           </select>
                                       </div>
                                       <div class="col-md-6" id="dis" style="display: none;">
                                           <label class="text-primary">Select Users</label>
                                           <select class="form-control" id="userData">
                                           </select>
                                       </div>
                                   </div>
                               </div>
                               <div class="text-center mt-4">
                                   <button type="button" class="btn btn-started" onclick="updateFunction()"> Save</button>
                               </div>
                           </form>
                       </div>
                   </div>
                   <div class="modal-footer">
                       <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                   </div>
               </div>
           </div>
       </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    function onChangeFunction() {
        let userType = $('#userType').val();
        console.log('building_id,project_id,flat_id');
        console.log(userType);
        $('#userData').append('');
        $('#userData').html('');
        $.ajax({
            url:'/get/users/'+userType,
            method:'GET',
            success: function (response){
                console.log('response');
                console.log(response);
                response.data.map((data) => {
                    console.log('data');
                    console.log(data);
                    let html = '<option value="'+ data.id +'">'+ data.name +'</option>'
                    $('#userData').append(html);
                });
                $('#dis').show();
            },
            error: function (error) {
                console.log('error');
                console.log(error);
            }
        })

    }

    function updateFunction(){
        let project_id = $('#project_id').val();
        let building_id = $('#building_id').val();
        let flat_id = $('#flat_id').val();
        let userType = $('#userType').val();
        let userData = $('#userData').val();
        let data = {
            '_token':'{{csrf_token()}}',
            'project_id':project_id,
            'building_id':building_id,
            'flat_id':flat_id,
            'userType':userType,
            'userData':userData,
        }
        console.log('data');
        console.log(data);
        $.ajax({
            url:'/Admin/save_assign_flat',
            method:'POST',
            data:data,
            success: function (response){
                console.log('response');
                console.log(response);
                window.location.reload();
            },
            error: function (error) {
                console.log('error');
                console.log(error);
            }
        })
    }
</script>
@endsection

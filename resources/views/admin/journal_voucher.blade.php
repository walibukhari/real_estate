@extends('layouts.app', [
'class' => '',
'elementActive' => 'journal_voucher'
])
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="content">
   @if (session('status'))
   <div class="alert alert-success" role="alert">
      {{ session('status') }}
   </div>
   @endif
   @if (session('password_status'))
   <div class="alert alert-success" role="alert">
      {{ session('password_status') }}
   </div>
   @endif
   @if (Session::has('error'))
   <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{!! Session('error') !!}</strong>
   </div>
   @endif
   @if ($errors->any())
   <div class="alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
   @endif
   <style type="text/css">
      .filelabel {
      width: 100%;
      border: 2px dashed grey;
      border-radius: 5px;
      display: block;
      padding: 5px;
      transition: border 300ms ease;
      cursor: pointer;
      text-align: center;
      margin: 0;
      }
      .filelabel i {
      display: block;
      font-size: 30px;
      padding-bottom: 5px;
      }
      .filelabel i,
      .filelabel .title {
      color: grey;
      transition: 200ms color;
      }
      .filelabel:hover {
      border: 2px solid #1665c4;
      }
      .filelabel:hover i,
      .filelabel:hover .title {
      color: #1665c4;
      }
      #FileInput{
      display:none;
      }
   </style>
   <div class="row">
      <div class="col-12">
         <div class="material-card card">
            <div class="card-body">
               <h4 class="card-title">Journal Voucher Entry</h4>
               <h6 class="card-subtitle">
               </h6>
               <br>
               <form class="form" method="POST" action="#" enctype="multipart/form-data">
                  @csrf
                  <div class="container">
                      
                       <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary">Journal Date </label>
                           <input type="date" class="form-control" value="{{date('Y-m-d')}}" name="day"/>
                        </div>
                        
                        <div class="col-md-6">
                           <label class="text-primary">Journal Voucher No.</label>
                           <input type="text" class="form-control" name="jvoucher_no" placeholder="e.g 35263" />
                        </div>
                     </div>
                     
                     <br>
                    <hr>
                   <br>
                     
                     <div class="row">
                        <div class="col-md-12">
                           <label class="text-primary">For Journal Voucher</small></label>
                           <div style="border: 1px solid grey;">
                              <center>
                                 <input id='internaluse-row' class='btn btn-primary' type='button' value='Add More' />
                              </center>
                              <div class="table-responsive">
                                 <table id="complex_header" class="table table-striped table-bordered display"
                                    style="width:100%">
                                    <thead>
                                       <tr>
                                          <th>Sr #</th>
                                          <th><center>Accounts</center> </th>
                                          <th><center>Debits</center></th>
                                          <th><center>Credits</center></th>
                                          <th><center>Particular</center></th>
                                          <th><center>Charge to Party</center></th>
                                          <th><center>Project</center></th>
                                          <th><center>Building</center></th>
                                          <th><center>Floor</center></th>
                                          <th><center>Unit Code</center></th>
                                          <th></th>
                                       </tr>
                                    </thead>
                                    <tbody id="internaluse-body">
                                       <tr id="row0">
                                          <td>
                                             <input name="serial_no[]" readonly  value="1" type='text' class='form-control' style="width:40px;"/>
                                          </td>
                                          <td>
                                             <select class="form-control" name="accounts_head[]">
                                                  <option  value="" selected disabled>Select One</option>
                                                  <option  value="Commission">Commission</option>
                                                  <option value="Management Fee">Management Fee</option>
                                             </select>
                                          </td>
                                          <td>
                                            <input type="number" class="form-control" name="debits[]" placeholder="0">
                                          </td>
                                          <td>
                                             <input type="number" class="form-control" name="credits[]" placeholder="0">
                                          </td>
                                          <td>
                                           <input type="text" name="particular[]" class="form-control" placeholder="Enter Particular Name"/>
                                          </td>
                                          <td>
                                             <input type="text" name="charge_to[]" class="form-control" placeholder="Enter  Name"/>
                                          </td>
                            <td>
                             <select class="form-control" name="project" id="selectProject" onchange="selectproject(this.value)">
                              <option value="" selected disabled>Select project</option>
                              @foreach($project as $pro)
                              <option value="{{$pro->id}}">{{$pro->name}}</option>
                              @endforeach
                             </select>
                            </td>
                            <td>
                               <select class="form-control" name="building" id="selectBuilding" data-Id="1" onchange="selectbuilding(this.value)"></select>
                            </td>
                            <td>
                              <select class="form-control" name="floor" id="selectFloor" onchange="selectfloor(this.value)"></select>
                            </td>
                            <td>
                             <select class="form-control" name="flat" id="selectFlat" onchange="selectflat(this.value)" >
                             </select>
                            </td>
                            <td>
                             <input class='security btn btn-primary' type='button' value='Delete' />
                            </td>
                        </tr>
                    </tbody>
                </table>
              </div>
           </div>
        </div>
     </div>

                     <br>
                   
                  <!--</div>-->
                  <br>
                  <div class="row">
                     <div class="col-md-12">
                         <label class="text-primary">Attachments</small></label>
                        <div style="border: 1px solid grey;">
                           <center>
                              <input id='add-row' class='btn btn-primary' type='button' value='Add More' />
                           </center>
                           <table id="test-table" class="table table-condensed">
                              <thead>
                                 <tr>
                                    <th>Attachments</th>
                                    <th>Comment</th>
                                 </tr>
                              </thead>
                              <tbody id="test-body">
                                 <tr id="row0">
                                    <td>
                                       <input name='attachment[]'  type='file' class='form-control' />
                                    </td>
                                    <td>
                                       <input name='comments[]'  placeholder="Comment" type='text' class='form-control input-md' />
                                    </td>
                                    <td>
                                       <input class='delete-row btn btn-primary' type='button' value='Delete' />
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  </div>
                  <div class="text-center mt-4">
                     <button class="btn btn-started"> Save</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript">

   function selectproject(value)
   {
          
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
       
      url: "{{url('/get_buildings')}}",
      method: "post",
      data: {_token: CSRF_TOKEN, pro_id:value},
      dataType: "html"
    });
    
   request.done(function( msg ) {
       //alert(msg);
      $('#selectBuilding').html(msg);
     });
   }
   
   
      function selectbuilding(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_floors')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, building_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectFloor').html(msg);
   });
   }
   
   
    function selectfloor(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_flats')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, building_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectFlat').html(msg);
   });
   }
   
   
   
        function selectflat(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_lanlords')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, flat_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#LanLord').val(msg);
   });
   }
   
   
   $(document).on("change" , "#no_installments" , function () {
       $('#rowTable').html('');
       let no_installments = $(this).val();
       let totalAmount = $('#totalAmount').val();
       console.log('value');
       console.log(no_installments);
       console.log(totalAmount);
       let totalA = totalAmount/no_installments;
       for (var i = 1; i <= no_installments; i++) {
           console.log('count iiiii');
           console.log(i);
           let Tblhtml = '<tr>' +
               '<td><input type="number" name="no_installment[]" value="'+ i +'" /></td>' +
               '<td><input type="number" name="amount[]" value="'+ totalA +'" /></td>' +
               '<td><input type="text" name="particular[]" value="" /></td>' +
               '<td><select name="pay_type" style="width:150px;" class="form-control">' +
                   '<option> select type </otpion>' +
                   '<option value="cash"> Cash </otpion>' +
                   '<option value="check"> Check </otpion>' +
               '</select></td>' +
               '<td><input type="number" name="check_no[]" /></td>' +
               '<td><input type="date" name="check_date[]" /></td>' +
               '<td><input type="text" name="check_issue[]" /></td>' +
               '<td><input type="text" name="party_name[]" /></td>' +
               '<td>' +
                   '<select id="selectOpt" name="selectOpt[]">' +
                       '<option>select option</option>' +
                       '<option>company</option>' +
                       '<option>landlord</option>' +
                   '</select>' +
               '</td>' +
               '</tr>';
           $('#rowTable').append(Tblhtml);
       }
       $('#rowTableDisplay').show();
   
   });
   
   
</script>
<script type="text/javascript">
   // Add row
   var row=1;
   $(document).on("click", "#add-row", function () {
       var new_row = '<tr id="row' + row + '"><td><input name="attachment[]" type="file" class="form-control" /></td><td><input name="comments[]" type="text" placeholder="Comment" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';
   
       $('#test-body').append(new_row);
       row++;
       return false;
   });
   
   // Remove criterion
   $(document).on("click", ".delete-row", function () {
       //  alert("deleting row#"+row);
       if(row>1) {
           $(this).closest('tr').remove();
           row--;
       }
       return false;
   });
   

   // Remove criterion
   $(document).on("click", ".security", function () {
       //  alert("deleting row#"+row);
       if(row1>1) {
           $(this).closest('tr').remove();
           row1--;
       }
       return false;
   });
   
  
// add more for the the journal voucher
  
    var row1=1;
    var inc =1;
       $(document).on("click", "#internaluse-row", function () { 
        inc++;
        var new_row = '<tr id="row' + row1 + '">'+
               '<td><input name="serial_no[]" readonly value="'+inc+'"  type="text" class="form-control" style="width:40px;"/>'+
                '</td>'+
                 '<td>'+
                      '<select class="form-control" name="accounts_head[]">'+
                      '<option  value="" selected disabled >Select One</option>'+
                           '<option  value="Commission">Commission</option>'+
                           '<option  value="Management Fee">Management Fee</option>'+
                                              '</select>'+
                '</td>'+
                 '<td>'+
                   '<input type="number" class="form-control" name="debits[]" placeholder="0">'+
                 '</td>'+
                 '<td>'+
                   '<input type="number" class="form-control" name="credits[]" placeholder="0">'+
                 '</td>'+
                  '<td id="date_from">'+
                  '<input type="text" name="particular[]" class="form-control" placeholder="Enter Particular Name"/>'+
                                          '</td>'+
                                          '<td >'+
                '<input type="text" name="charge_to[]" class="form-control" placeholder="Enter  Name"/>'+
                                          '</td>'+
                                          '<td >'+
                        '<select class="form-control" name="project" id="selectProject" onchange="selectproject(this.value)">'+
                              '<option value="" selected disabled>Select project</option>'+
                             '@foreach($project as $pro)'+
                              '<option value="{{$pro->id}}">{{$pro->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                                          '</td>'+
                                          '<td >'+
                                    '<select class="form-control" name="building" id="selectBuilding" onchange="selectbuilding(this.value)">'+
                                               '</select>'+
                                          '</td>'+
                                          '<td>'+
                                        '<select class="form-control" name="floor" id="selectFloor" onchange="selectfloor(this.value)">'+
                                            '</select>'+
                                          '</td>'+
                                          '<td>'+
                                            '<select class="form-control" name="flat" id="selectFlat" onchange="selectflat(this.value)" >'+
                                             '</select>'+
                                          '</td>'+
                                          '<td>'+
                                             '<input class="security btn btn-primary" type="button" value="Delete" />'+
                                          '</td>'+
           '</tr>';
       $('#internaluse-body').append(new_row);
       row1++;
       return false;
   });
</script>
@endsection
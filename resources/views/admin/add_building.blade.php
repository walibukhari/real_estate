@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'add_building'
])

@section('content')
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
          @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('error') !!}</strong>
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
        <style type="text/css">
            .filelabel {
    width: 100%;
    border: 2px dashed grey;
    border-radius: 5px;
    display: block;
    padding: 5px;
    transition: border 300ms ease;
    cursor: pointer;
    text-align: center;
    margin: 0;
}
.filelabel i {
    display: block;
    font-size: 30px;
    padding-bottom: 5px;
}
.filelabel i,
.filelabel .title {
  color: grey;
  transition: 200ms color;
}
.filelabel:hover {
  border: 2px solid #1665c4;
}
.filelabel:hover i,
.filelabel:hover .title {
  color: #1665c4;
}
#FileInput{
    display:none;
}
        </style>
            <div class="row">
  <div class="col-12">    
  <div class="row">
      <div class="col-md-3">
          
      </div>
       <div class="col-md-3">
          
      </div>
       <div class="col-md-4">
          
      </div>
       <div class="col-md-2">
            <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Create New</button>
      </div>
  </div>     
    <div class="material-card card">
      <div class="card-body">

        <h4 class="card-title">Buildings List</h4>
        <h6 class="card-subtitle">
        </h6><br>
        <div class="table-responsive">
          <table id="complex_header" class="table table-striped table-bordered display"
          style="width:100%">
          <thead>

            <tr>
              <th>Sr#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Status</th>
              <!--<th>Variations</th>-->
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
               @foreach($buildinglist as $buildinglists)
          <tr>
           
              <td>{{$counter++}}</td>
             
              <td>{{$buildinglists->name}}</td>
              
              <td>Project: @if($buildinglists->Project_name){{$buildinglists->Project_name->name}}@endif<br>    
               Manager: @if($buildinglists->Manager_name){{$buildinglists->Manager_name->name}}@endif<br> Location: {{$buildinglists->address}}</td>
              <td>{{$buildinglists->status}}</td>
             <td>
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editModal{{$buildinglists->id}}">Eidt </button>
                   <div class="modal fade" id="editModal{{$buildinglists->id}}" role="dialog" >
    <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Building</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
                   <div class="p-5">
                    <form action="{{url('Admin/edit_building')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                   <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">Manager of the Buildings</label>
                          <input type="hidden" name="buildinglists_id" value="{{$buildinglists->id}}">
                        <select class="form-control" name="manager_id" required="">
                        <option value="" selected disabled>Select Manager</option> 
                            @foreach($g_manager as $g_managers)
                           <option @if($buildinglists->manager_id==$g_managers->id) selected @endif value="{{$g_managers->id}}">{{$g_managers->name}}</option>
                           @endforeach                         </select>
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">Project</label>
                         <select class="form-control"  required="" name="project_name">
                                                    <option value="" selected disabled>Select Project Name</option>
                                                    @foreach($project_name as $project_names)
                                                    <option @if($buildinglists->project_id==$project_names->id) selected @endif value="{{$project_names->id}}">{{$project_names->name}}</option>
                                                    @endforeach
                                                </select>
                       </div>
                  </div>
                  <div class="form-group row">
                       <div class="col-md-12">
                        <label class="text-primary">Name of the Building</label>
                        <input type="text"  class="form-control"  name="name" value="{{$buildinglists->name}}" required="">
                       </div>
                   </div> 
                  <div class="form-group row">
                       <div class="col-md-12">
                        <label class="text-primary">Address of the Building</label>
                        <input type="text"  class="form-control"  name="address" value="{{$buildinglists->address}}" required="">
                       </div>
                  </div>
                       
                  <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">No. of Floors</label>
                        <input type="text"  class="form-control"  name="no_floors" value="{{$buildinglists->no_floors}}" required="">
                       </div>
                        <div class="col-md-6">
                        <label class="text-primary">No. of Flats</label>
                        <input type="text"  class="form-control"  name="no_flats" value="{{$buildinglists->no_flats}}" required="">
                       </div>
                   </div>
                   <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">Building Type</label>
                        <select class="form-control" name="type_id" required="">
                        <option value="" selected disabled>Select Building Type</option>
                        @foreach($buildingtype as $buildingtypes)
                        <option @if($buildinglists->type_id==$buildingtypes->id) selected @endif value="{{$buildingtypes->id}}">{{$buildingtypes->name}}</option>  
                        @endforeach
                        </select>
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">Parking</label>
                        <select class="form-control" name="parking" required="">
                            <option value="">Select Parking Status</option>
                            
                            @if($buildinglists->parking=="Yes")
                            <option selected value="Yes">Yes</option>
                            <option value="No">No</option>
                            @elseif($buildinglists->parking=="No")
                             <option selected value="No">No</option>
                            <option value="Yes">Yes</option>

                        @endif
                            
                        </select>
                       </div>
                   </div>
                   
                     <div class=" row">
                        <div class="col-md-6">
                        <label class="text-primary">Permisses No</label>
                        <input type="text"  class="form-control"  name="permisses_no" value="{{$buildinglists->permisses_no}}" required="">
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">Plot No</label>
                        <input type="text"  class="form-control"  name="plot_no" value="{{$buildinglists->plot_no}}" required="">
                       </div>
                      
                   </div>  
                   
                   <div class=" row">
                        <div class="col-md-6">
                        <label class="text-primary">Location</label>
                        <input type="text"  class="form-control"  name="location" value="{{$buildinglists->location}}" required="">
                       </div>
                       
                       <div class="col-md-6">
                        <label class="text-primary">Building Image</label>
                        <input type="file"  class="form-control" onclick="remove_img()" name="image" value="{{$buildinglists->img_link}}" >
                        <input type="hidden" name="old_img" value="{{$buildinglists->img_link}}">
                       </div>
                    
                   </div>    
                   
                   <br>
                                    
                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="border: 1px solid grey;">
                                            <center>
                                                <input id='add-row2' class='btn btn-primary' type='button' value='Add More' /></center>
                                            <table id="test-table" class="table table-condensed">
                                                <thead>
                                                <tr>
                                                    <th>Attachments</th>
                                                  
                                                </tr>
                                                </thead>
                                                <tbody id="test-body2">
                                                <tr id="row2">
                                                    <td>
                                                        <input name='building_attachments[]'  type='file' class='form-control' />
                                                    </td>
                                                    <td>
                                                        <input name='comments[]'  placeholder="Comment" type='text' class='form-control input-md' />
                                                    </td>
                                                    <td>
                                                        <input class='delete-row2 btn btn-primary' type='button' value='Delete' />
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                    <hr>
                  <div class="form-group row">
                    <label class="text-primary">Building Facilities</label>
                    <table id="fac_fieds_tbl2{{$buildinglists->id}}" class="table table-bordered">
                        <thead>
                            <tr class="text-center">
                                <td colspan="2">Select</td>
                            </tr>
                        </thead>
                          <tfoot>
                              @if($buildinglists->Building_facilitie)
                             
                        @foreach($buildinglists->Building_facilitie as $r)
                      <tr>
                        <td>
                          <select id="fac_type_id" name="facilitie_type[]" required="" class="form-control">
                          @foreach($buildingfaciltie as $buildingfacilties) 
                          <option @if($r->building_fac_id == $buildingfacilties->id) selected @endif value="{{$buildingfacilties->id}}">{{$buildingfacilties->name}}</option>
                          @endforeach
                     </select></td><td class="text-center">
                       <button type="button" class="btn btn-danger remove-fac-row2">X</button>
                          </td>
                      </tr>
                      @endforeach
                       @endif
                    <center><tr><td colspan="2">  <button type="button" class="btn btn-primary add-more-fac2" id="add-more-fac2{{$buildinglists->id}}" value="{{$buildinglists->id}}" onclick="addmore(this.value)"><b>+ Add More</b></button></td></tr></center>
                    </tfoot>
                        <!--<tfoot>-->
                        <!--    <tr class="text-center">-->
                        <!--        <td colspan="2"><button type="button" class="btn btn-primary add-more-fac2" ><b>+ Add More</b></button></td>-->
                        <!--    </tr>-->
                        <!--</tfoot>-->
                          <!-- Coming form ajax-->
                    </table>
                   </div>
                   
                <button  type="submit" name="createNewBuilding" class="btn btn-primary btn-user btn-block">
                 Update
               </button>
              </form>
           </div>
        
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
              </td>
          </tr>
        
          @endforeach
      </tbody>
      <tfoot>
         <tr>
             <th>Sr#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Status</th>
           
              <th>Actions</th>
            </tr>
      </tfoot>
     </table>
    </div>
   </div>
  </div>
 </div>
</div>
 <div class="modal fade" id="myModal" role="dialog" >
    <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">New Building Registration Form</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
                   <div class="p-5">
                    <form action="{{url('Admin/save_building')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                   <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">Manager of the Buildings</label>
                        <select class="form-control" name="manager_id" required="">
                        <option value="" selected disabled>Select Manager</option> 
                            @foreach($g_manager as $g_managers)
                           <option value="{{$g_managers->id}}">{{$g_managers->name}}</option>
                           @endforeach                         </select>
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">Project</label>
                         <select class="form-control" required="" name="project_name">
                                                    <option value="" selected disabled>Select Project Name</option>
                                                    @foreach($project_name as $project_names)
                                                    <option value="{{$project_names->id}}">{{$project_names->name}}</option>
                                                    @endforeach
                                                </select>
                       </div>
                  </div>
                  <div class="form-group row">
                       <div class="col-md-12">
                        <label class="text-primary">Name o the Building</label>
                        <input type="text"  class="form-control"  name="name" value="" required="">
                       </div>
                   </div> 
                  <div class="form-group row">
                       <div class="col-md-12">
                        <label class="text-primary">Address of the Building</label>
                        <input type="text"  class="form-control"  name="address" value="" required="">
                       </div>
                  </div>
                       
                  <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">No. of Floors</label>
                        <input type="text"  class="form-control"  name="no_floors" value="" required="">
                       </div>
                        <div class="col-md-6">
                        <label class="text-primary">No. of Flats</label>
                        <input type="text"  class="form-control"  name="no_flats" value="" required="">
                       </div>
                   </div>
                   <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">Building Type</label>
                        <select class="form-control" name="type_id" required="">
                        <option value="" selected disabled>Select Building Type</option>
                        @foreach($buildingtype as $buildingtypes)
                        <option value="{{$buildingtypes->id}}">{{$buildingtypes->name}}</option>  
                        @endforeach
                        </select>
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">Parking</label>
                        <select class="form-control" name="parking" required="">
                            <option value="">Select Parking Status</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                       </div>
                   </div>
                   
                    <div class=" row">
                        <div class="col-md-6">
                        <label class="text-primary">Permisses No</label>
                        <input type="text"  class="form-control"  name="permisses_no" value="" required="">
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">Plot No</label>
                        <input type="text"  class="form-control"  name="plot_no" value="" required="">
                       </div>
                      
                   </div>    
                   
                   <div class=" row">
                        <div class="col-md-6">
                        <label class="text-primary">Location</label>
                        <input type="text"  class="form-control"  name="location" value="" required="">
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">Building Image</label>
                        <input type="file"  class="form-control"  name="image" value="" required="">
                       </div>
                      
                   </div>    
                   
                   <br>
                                    
                                    <div class="row">
                                    <div class="col-md-12">
                                        <div style="border: 1px solid grey;">
                                            <center>
                                                <input id='add-row' class='btn btn-primary' type='button' value='Add More' /></center>
                                            <table id="test-table" class="table table-condensed">
                                                <thead>
                                                <tr>
                                                    <th>Attachments</th>
                                                  
                                                </tr>
                                                </thead>
                                                <tbody id="test-body">
                                                <tr id="row0">
                                                    <td>
                                                        <input name='building_attachments[]'  type='file' class='form-control' />
                                                    </td>
                                                    <td>
                                                        <input name='comments[]'  placeholder="Comment" type='text' class='form-control input-md' />
                                                    </td>
                                                    <td>
                                                        <input class='delete-row btn btn-primary' type='button' value='Delete' />
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                    <hr>
                  <div class="form-group row">
                    <label class="text-primary">Building Facilities</label>
                    <table id="fac_fieds_tbl" class="table table-bordered">
                        <thead>
                            <tr class="text-center">
                                <td colspan="2">Select</td>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr class="text-center">
                                <td colspan="2"><button type="button" class="btn btn-primary add-more-fac" ><b>+ Add More</b></button></td>
                            </tr>
                        </tfoot>
                          <!-- Coming form ajax-->
                    </table>
                   </div>
                   
                <button  type="submit" name="createNewBuilding" class="btn btn-primary btn-user btn-block">
                 Create
               </button>
              </form>
           </div>
        
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>

    </div>
    
    <input type="hidden" name="rem_img" value="1" id="rem_img">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
    $("#FileInput").on('change',function (e) {
            var labelVal = $(".title").text();
            var oldfileName = $(this).val();
                fileName = e.target.value.split( '\\' ).pop();

                if (oldfileName == fileName) {return false;}
                var extension = fileName.split('.').pop();

            if ($.inArray(extension,['jpg','jpeg','png']) >= 0) {
                $(".filelabel i").removeClass().addClass('fa fa-file-image-o');
                $(".filelabel i, .filelabel .title").css({'color':'#208440'});
                $(".filelabel").css({'border':' 2px solid #208440'});
            }
            else if(extension == 'pdf'){
                $(".filelabel i").removeClass().addClass('fa fa-file-pdf-o');
                $(".filelabel i, .filelabel .title").css({'color':'red'});
                $(".filelabel").css({'border':' 2px solid red'});

            }
  else if(extension == 'doc' || extension == 'docx'){
            $(".filelabel i").removeClass().addClass('fa fa-file-word-o');
            $(".filelabel i, .filelabel .title").css({'color':'#2388df'});
            $(".filelabel").css({'border':' 2px solid #2388df'});
        }
            else{
                $(".filelabel i").removeClass().addClass('fa fa-file-o');
                $(".filelabel i, .filelabel .title").css({'color':'black'});
                $(".filelabel").css({'border':' 2px solid black'});
            }

            if(fileName ){
                if (fileName.length > 10){
                    $(".filelabel .title").text(fileName.slice(0,4)+'...'+extension);
                }
                else{
                    $(".filelabel .title").text(fileName);
                }
            }
            else{
                $(".filelabel .title").text(labelVal);
            }
        });
</script>
 <script type="">
    $(document).ready(function() {
     //add new amount field
        add_fac_row();
    $(document).on('click', '.add-more-fac', function(){
        add_fac_row();
     }); 
    
     //function to add new row from amount
    function add_fac_row(){
         var html = '';
         html +='<tr>';
         html +='<td>';
         html +='<select id="fac_type_id" name="fac_type_id[]"  class="form-control">';
         html +='<option value="" selected disabled>Select Facilities</option> @foreach($buildingfaciltie as $buildingfacilties) <option value="{{$buildingfacilties->id}}">{{$buildingfacilties->name}}</option> @endforeach';
         html +='</select>';
         html +='</td>';
         html +='<td class="text-center">';
         html +='<button type="button" class="btn btn-danger remove-fac-row" >X</button>';
         html +='</td>';
         html +='</tr>';
         $('#fac_fieds_tbl').append(html);
    }
    
    
     
     //remove amount row
     $(document).on('click', '.remove-fac-row', function(){
     $(this).closest('tr').remove();
        });
        
         $(document).on('click', '.remove-fac-row2', function(){
     $(this).closest('tr').remove();
        });
   });
   
   
   
   
   
      var row=1;
        $(document).on("click", "#add-row", function () {
            var new_row = '<tr id="row' + row + '"><td><input name="building_attachments[]" type="file" class="form-control" /></td><td><input name="comments[]" placeholder="Comment" type="text" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';

            $('#test-body').append(new_row);
            row++;
            return false;
        });
        
        //for the edit section
        //   $(document).on("click", "#add-row2", function () {
        //     var new_row = '<tr id="row' + row + '"><td><input name="building_attachments[]" type="file" class="form-control" /></td><td><input name="comments[]" placeholder="Comment" type="text" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';

        //     $('#test-body2').append(new_row);
        //     row++;
        //     return false;
        // });

        // Remove criterion
        $(document).on("click", ".delete-row", function () {
            //  alert("deleting row#"+row);
            if(row>1) {
                $(this).closest('tr').remove();
                row--;
            }
            return false;
        });
        
         function addmore(value)
   {
        var html = '';
       html +='<tr>';
       html +='<td>';
       html +='<select id="fac_type_id" name="facilitie_type[]" required="" class="form-control">';
       html +='<option value="" selected disabled>Select Facilities</option> @foreach($buildingfaciltie as $buildingfacilties) <option value="{{$buildingfacilties->id}}">{{$buildingfacilties->name}}</option> @endforeach';
       html +='</select>';
       html +='</td>';
       html +='<td class="text-center">';
       html +='<button type="button" class="btn btn-danger remove-fac-row2" >X</button>';
       html +='</td>';
       html +='</tr>';
       $('#fac_fieds_tbl2'+value).append(html);  
   }
        
        //for edit section
        //  $(document).on("click", ".delete-row2", function () {
        //     //  alert("deleting row#"+row);
        //     if(row>1) {
        //         $(this).closest('tr').remove();
        //         row--;
        //     }
        //     return false;
        // });
</script>
 <script type="">
        function remove_img()
    {
        $('#rem_img').val("0");
    }
 </script>
@endsection

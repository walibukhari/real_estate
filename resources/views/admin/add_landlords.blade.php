@extends('layouts.app', [
'class' => '',
'elementActive' => 'add_landlords'
])
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('error') !!}</strong>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <style type="text/css">
            .filelabel {
                width: 100%;
                border: 2px dashed grey;
                border-radius: 5px;
                display: block;
                padding: 5px;
                transition: border 300ms ease;
                cursor: pointer;
                text-align: center;
                margin: 0;
            }
            .filelabel i {
                display: block;
                font-size: 30px;
                padding-bottom: 5px;
            }
            .filelabel i,
            .filelabel .title {
                color: grey;
                transition: 200ms color;
            }
            .filelabel:hover {
                border: 2px solid #1665c4;
            }
            .filelabel:hover i,
            .filelabel:hover .title {
                color: #1665c4;
            }
            #FileInput{
                display:none;
            }

        </style>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Create New</button>
                    </div>
                </div>
                <div class="material-card card">
                    <div class="card-body">
                        <h4 class="card-title">Owner's  List</h4>
                        <h6 class="card-subtitle">
                        </h6>
                        <br>

                        <div class="table-responsive">
                            <table id="complex_header" class="table table-striped table-bordered display"
                                   style="width:100%">
                                <thead>

                                <tr>
                                    <th>Sr#</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>View Profile</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Landlord as $landlords)
                                    <tr>
                                        <td>{{$landlords->id}}</td>
                                        <td>{{$landlords->title}} {{$landlords->name}}</td>
                                        <td>{{$landlords->phone}}</td>
                                        <td>{{$landlords->email}}</td>
                                        <td>{{$landlords->status}}</td>
                                        <td><button class="btn btn-info btn-sm">View Profile</button></td>
                            <td>
                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal{{$landlords->id}}">Edit</button>

                                <!-- edit modal start here -->
                                <div class="modal fade" id="myModal{{$landlords->id}}"  role="dialog" >
                                    <div class="modal-dialog modal-lg" >
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="p-5">
                                                    <form class="form" method="POST" action="{{url('/Admin/save_landlord')}}">
                                                        @csrf


                        <div class="container">
                            <div class="row">
                                <input type="hidden" name="update_id" value="{{$landlords->id}}">
                                <div class="col-md-4 ">
                                    <label class="text-primary">Title</label>
                                    <select class="form-control"  name="title">
                                        <option value="" >Select One</option>
                                        @if($landlords->title=='Mr')
                                            <option selected value="Mr">Mr</option>
                                            <option value="Mrs">Mrs</option>
                                        @endif
                                        @if($landlords->title=='Mrs')
                                            <option  value="Mr">Mr</option>
                                            <option selected value="Mrs">Mrs</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-4{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="text-primary">Name</label>
                                    <div class="input-group-prepend">
                                    </div>
                                    <input name="name" type="text" class="form-control" placeholder="Name" value="{{$landlords->name}}"  >
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                              <strong>{{ $errors->first('name') }}</strong>
                              </span>
                                                                                @endif
                                                                            </div>
            <div class="col-md-4 ">
                <label class="text-primary">Name in Arabic</label>
                <input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_name_in_arabic}}@endif" name="first_per_name_in_arabic" class="form-control " placeholder="Name in Arabic"
                       aria-label="Name">
                                                                            </div>
                                                                        </div>
                                                                        <br>
    <div class="row">
        <div class="col-md-4{{ $errors->has('email') ? ' has-danger' : '' }}">
            <label class="text-primary">Email</label>
            <div class="input-group-prepend">
                <!--    <span class="input-group-text">
                   <i class="nc-icon nc-email-85"></i>
                   </span> -->
            </div>
            <input name="email" type="email" class="form-control" placeholder="Email"  value="@if($landlords->Profiles){{$landlords ->email}}@endif">
            @if ($errors->has('email'))
                <span class="invalid-feedback" style="display: block;" role="alert">
                              <strong>{{ $errors->first('email') }}</strong>
                              </span>
                                                                                @endif
                                                                            </div>
        <div class="col-md-4{{ $errors->has('password') ? ' has-danger' : '' }}">
            <label class="text-primary">Password</label>
            <div class="input-group-prepend">
                <!--  <span class="input-group-text">
                   <i class="nc-icon nc-key-25"></i>
                   </span> -->
            </div>
            <input name="password" type="password" value="{{$landlords->password}}" class="form-control" placeholder="Password" >
            @if ($errors->has('password'))
                <span class="invalid-feedback" style="display: block;" role="alert">
                              <strong>{{ $errors->first('password') }}</strong>
                              </span>
                                                                                @endif
                                                                            </div>
    <div class="col-md-4">
        <label class="text-primary">Confirm Password</label>
        <div class="input-group-prepend">
        </div>
        <input name="password_confirmation" type="password" class="form-control" placeholder="Password confirmation" >
        @if ($errors->has('password_confirmation'))
            <span class="invalid-feedback" style="display: block;" role="alert">
                          <strong>{{ $errors->first('password_confirmation') }}</strong>
                          </span>
                                                                            @endif
                                                                        </div>




                                                                        </div><br>

                                                                        <div class="row">
                <div class="col-md-4">
                    <label class="text-primary">Nationality</label>
                    <select class="form-control"  name="first_per_nationality" >
                        <option value="" >Select One</option>
                        <option value="1">Pakistan</option>
                        <option value="3">Iran</option>
                        <option value="2">India</option>
                        <option value="5">China</option>
                    </select>
                                                                            </div>
                <div class=" col-md-4 ">
                    <label class="text-primary">Nature</label>
                    <select class="form-control"  id="Nature1">
                        <option>Select Nature</option>
                        <option>Natural Person</option>
                        <option id="company1" value="company">Company</option>
                    </select>
                                                                            </div>
                      
                                                                        </div><br>

                        <div id="box1" style="display: ;">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="text-primary">Company Trade License Number</label>
                                    <input type="number" class="form-control" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_trade_license_no}}@endif" placeholder="Company Trade License Number" name="first_per_trade_license_no">
                                </div>
                                <div class=" col-md-4 ">
                                    <label class="text-primary">licening authority</label>
                                    <input type="number" class="form-control" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_license_authority}}@endif" placeholder="licening authority" name="first_per_license_authority">
                                </div>
                                <div class="col-md-4">
                                    <label class="text-primary">VAT Reg Number</label>
                                    <input type="number" class="form-control"  value="@if($landlords->Profiles){{$landlords->Profiles->first_per_reg_no}}@endif" name="first_per_reg_no" placeholder="VAT Reg Number" class="form-control">
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="text-primary">VAT Emirate</label>
                                    <input type="number" class="form-control" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_vat_emirates}}@endif"  placeholder="VAT Emirate" name="first_per_vat_emirates">
                                </div>
                                <div class=" col-md-4 ">
                                    <label class="text-primary">VAT GCC</label>
                                    <input type="number" class="form-control" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_vat_gcc}}@endif" placeholder="VAT GCC" name="first_per_vat_gcc">
                                </div>
                            </div><br>
                                                                        </div>





                    <div class="row">
                        <div class="col-md-6">
                            <label class="text-primary">Tel. Phone</label>
                            <input type="number" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_phone}}@endif"  placeholder="123" class="form-control" name="first_per_phone">
                        </div>
                        
                        

                    </div><br>

                    <div class="row">
                        
                        <div class="col-md-6">
                            <label class="text-primary">Contact Number 1</label>
                            <input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_contact1}}@endif" class="form-control" placeholder="Contact Number" name="first_per_contact1">
                        </div>
                        <div class="col-md-6">
                            <label class="text-primary">Contact Number 2</label>
                            <input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_contact2}}@endif" class="form-control" placeholder="Contact Number" name="first_per_contact2">
                        </div>
                     

                    </div><br>

                    <div class="row">
                        <div class="col-md-6">
                            <label class="text-primary">Location</label>
                            <textarea   name="first_per_location" placeholder="Location" class="form-control">@if($landlords->Profiles){{$landlords->Profiles->first_per_location}}@endif</textarea>
                        </div>
                        <div class=" col-md-6 ">
                            <label class="text-primary">PO box</label>
                            <input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_pobox}}@endif" name="first_per_pobox" class="form-control " placeholder="PO box"
                                   aria-label="Name">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-primary">Passport Number</label>
                            <input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_passport_no}}@endif" name="first_per_passport_no" placeholder="Passport" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label class="text-primary">Expiry Date</label>
                            <input type="date" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_passport_expiry_date}}@endif" name="first_per_passport_expiry_date" class="form-control">
                        </div>

                    </div><br>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-primary">Visa number</label>
                            <input type="number"  value="@if($landlords->Profiles){{$landlords->Profiles->first_per_visa_no}}@endif" name="first_per_visa_no" placeholder="Visa number" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label class="text-primary">Expiry Date</label>
                            <input type="date" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_visa_no_expiry_date}}@endif" placeholder="Expiry Date" class="form-control" name="first_per_visa_no_expiry_date">
                        </div>
                        <div class="col-md-4">
                            <label class="text-primary">Remarks</label>
                            <input type="text" name="first_per_remarks" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_remarks}}@endif" placeholder="Remarks" class="form-control" >
                        </div>
                    </div><br>

                   
                    <div class="form-group row">
                        <label class="text-primary">Attachements option</label>
                        <table id="fac_fieds_tbl" class="table table-bordered">
                            <thead>
                            <tr class="text-center">
                                <td colspan="2">Attach Document</td>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr class="text-center">

                                <td colspan="2" ><button type="button" class="btn btn-primary add-more-fac" ><b>+ Add More</b></button></td>

                            </tr>
                            </tfoot>
                            <!-- Coming form ajax-->
                        </table>
                    </div>
                    <hr>
                    <h4 class="text-primary">Second Person Contact Details</h4>
                    <div class="row">
                        <br>
                        <div class="col-md-4">
                            <label class="text-primary">Name</label>
                            <input type="text"  value="@if($landlords->Profiles){{$landlords->Profiles->second_per_name}}@endif"    name="second_per_name" placeholder="Name" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label class="text-primary">Name in Arabic</label>
                            <input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->second_per_name_in_arabic}}@endif"    name="second_per_name_in_arabic" placeholder="Name in Arabic" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label class="text-primary">Designation</label>
                            <input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->second_per_designation}}@endif"  name="second_per_designation" class="form-control" placeholder="">
                        </div>

                    </div><br>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-primary">Phone</label>
                            <input type="number" value="@if($landlords->Profiles){{$landlords->Profiles->second_per_phone}}@endif"  name="second_per_phone"  class="form-control" placeholder="Enter phone number...">
                        </div>
                        <div class="col-md-4">
                            <label class="text-primary">Mobile</label>
                            <input type="number" value="@if($landlords->Profiles){{$landlords->Profiles->second_per_mobile}}@endif" name="second_per_mobile"  class="form-control" placeholder="Enter Mobile number...">
                        </div>

                    </div>
                    <hr>
                    <div class="row">

                        <!--<div class="col-md-4">-->
                        <!--    <label class="text-primary">Card No</label>-->
                        <!--    <input type="number"  value="@if($landlords->Profiles){{$landlords->Profiles->second_per_card_no}}@endif" placeholder="e.g 45325" name="second_per_card_no"  class="form-control">-->
                        <!--</div>-->
                        <!--<div class="col-md-4">-->
                        <!--    <label class="text-primary">ID No.</label>-->
                        <!--    <input type="text" placeholder="e.g. 36302-xxxxxxx-x" value="@if($landlords->Profiles){{$landlords->Profiles->second_per_id_no}}@endif" name="second_per_id_no"  class="form-control">-->
                        <!--</div>-->
                        <div class="col-md-4">
                            <label class="text-primary">Nationality</label>
                            <input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->second_per_nationality}}@endif" placeholder="" name="second_per_nationality"  class="form-control">
                        </div>
                    </div><br>
                    <div class="row">

                        <div class="col-md-4">
                            <label class="text-primary">VAT Country</label>
                            <select class="form-control" value="@if($landlords->Profiles){{$landlords->Profiles->second_per_vat_country}}@endif" name="second_per_vat_country" >
                                <option value="" >Select One</option>
                                <option value="testvatcountry">Test Vat Country</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="text-primary">Profession</label>
                            <input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->second_per_profession}}@endif" name="second_per_profession" placeholder="Profession" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label class="text-primary">Address 1</label>
                            <input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->second_per_address1}}@endif" name="second_per_address1" placeholder="Enter address" class="form-control">
                        </div>
                    </div><br>
                    <div class="row">

                        <div class="col-md-4">
                            <label class="text-primary">Address 2</label>
                            <input type="text"  value="@if($landlords->Profiles){{$landlords->Profiles->second_per_address2}}@endif" name="second_per_address2" placeholder="Enter address" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label class="text-primary">VAT Reg No</label>
                            <input type="text"  value="@if($landlords->Profiles){{$landlords->Profiles->second_per_vat_reg_no}}@endif" class="form-control" name="second_per_vat_reg_no" placeholder="Enter VAT reg no">
                        </div>
                        <div class="col-md-4">
                            <label class="text-primary">VAT Emirates</label>
                            <select class="form-control" name="second_per_vat_emirates" >
                                <option value="" >Select One</option>
                                <option value="2">hello</option>
                            </select>
                        </div>
                    </div><br>
                    <div class="row">

                        <div class="col-md-4">
                            <label class="text-primary">Remarks</label>
                            <input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->second_per_remarks}}@endif" name="second_per_remarks"  class="form-control" placeholder="">
                        </div>
                        <!--<div class=" col-md-6 ">-->
                        <!--    <label class="text-primary">Any Other Ref No</label>-->
                        <!--    <input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_ref_no}}@endif" name="first_per_ref_no" class="form-control " placeholder="Ref no"-->
                        <!--           aria-label="Name">-->
                        <!--</div>-->
                    </div>
                                                                        <br>




                                                                    </div>
                                                                    <div class="text-center mt-4">
                                                                        <button class="btn btn-started"> Update</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- edit modal end here -->

                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Sr#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
     

        <!-- create new modal start -->
        <div class="modal fade " id="myModal"  role="dialog" >
            <div class="modal-dialog modal-lg" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New Owner Registration From</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="p-5">
                            <form class="form" method="POST" action="{{url('/Admin/save_landlord')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="container">
                                    <div class="row">
                                        
                                        <div class=" col-md-6 ">
                                            <label class="text-primary">Owner Category</label>
                                            <select class="form-control" id="Nature" name="owner_category">
                                                <option value="" selected disabled>Select Owner Category</option>
                                                <option value="Natural Person">Natural Person</option>
                                                <option id="company" value="company">Company</option>
                                            </select>
                                        </div>
                                        
                                        
                                            <div class="col-md-6" >
                                            <div id="box2" >
                                                <label class="text-primary">Title</label>
                                                <select class="form-control"  name="title">
                                                    <option value="" >Select Title</option>
                                                    <option value="Mr">Mr.</option>
                                                    <option value="Mrs">Mrs.</option>
                                                    <option value="Ms.">Ms.</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        
                                        
                            
                                        
                                        
                                        
                                        
                                    </div>
                                    <br>
                                    <div class="row">
                                        
                                          <div class="col-md-6{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <label class="text-primary">Name</label>
                                            <div class="input-group-prepend">
                                            </div>
                                            <input name="name" type="text" class="form-control" placeholder="Name" value="{{ old('name') }}" required >
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" style="display: block;" role="alert">
                              <strong>{{ $errors->first('name') }}</strong>
                              </span>
                                            @endif
                                        </div>
                                        
                                        <div class="col-md-6 ">
                                            <label class="text-primary">Name in Arabic</label>
                                            <input type="text" value="{{ old('first_per_name_in_arabic') }}" name="first_per_name_in_arabic" class="form-control " placeholder="Name in Arabic"
                                                   aria-label="Name">
                                        </div>
                                        
                                        
                                        
                                        
                                       




                                    </div><br>
                                    
                                    <div class="row">
                                        <div class="col-md-6{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <label class="text-primary">Email</label>
                                            <div class="input-group-prepend">
                                                <!--    <span class="input-group-text">
                                                   <i class="nc-icon nc-email-85"></i>
                                                   </span> -->
                                            </div>
                                            <input name="email" type="email" class="form-control" placeholder="Email" required value="{{ old('email') }}">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" style="display: block;" role="alert">
                              <strong>{{ $errors->first('email') }}</strong>
                              </span>
                                            @endif
                                        </div>
                                        
                                        <div class="col-md-6{{ $errors->has('password') ? ' has-danger' : '' }}">
                                            <label class="text-primary">Password</label>
                                            <div class="input-group-prepend">
                                                <!--  <span class="input-group-text">
                                                   <i class="nc-icon nc-key-25"></i>
                                                   </span> -->
                                            </div>
                                            <input name="password" type="password" class="form-control" placeholder="Password" required>
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" style="display: block;" role="alert">
                              <strong>{{ $errors->first('password') }}</strong>
                              </span>
                                            @endif
                                        </div>
                                        
                                    </div><br>
                                    
                                    
                                    <div class="row">
                                         <div class="col-md-6">
                                            <label class="text-primary">Confirm Password</label>
                                            <div class="input-group-prepend">
                                            </div>
                                            <input name="password_confirmation" type="password" class="form-control" placeholder="Password confirmation" required>
                                            @if ($errors->has('password_confirmation'))
                                                <span class="invalid-feedback" style="display: block;" role="alert">
                              <strong>{{ $errors->first('password_confirmation') }}</strong>
                              </span>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            <label class="text-primary">Nationality</label>
                                            <select class="form-control"  name="first_per_nationality" >
                                                <option value="" >Select One</option>
                                                <option value="1">Pakistan</option>
                                                <option value="3">Iran</option>
                                                <option value="2">India</option>
                                                <option value="5">China</option>
                                            </select>
                                        </div>
                                    </div> <br>

                                    <div id="box3" >
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="text-primary">Passport Number</label>
                                            <input type="text" value="{{ old('first_per_passport_no') }}" name="first_per_passport_no" placeholder="Passport" class="form-control">
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-primary">Expiry Date</label>
                                            <input type="date" name="first_per_passport_expiry_date" class="form-control">
                                        </div>
                                        
                                         <div class="col-md-4">
                                            <label class="text-primary">Passport Attachment</label>
                                            <input type="file" name="passport_attachment" class="form-control">
                                        </div>

                                    </div><br>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="text-primary">Visa number</label>
                                            <input type="number" name="first_per_visa_no" placeholder="Visa number" class="form-control">
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-primary">Expiry Date</label>
                                            <input type="date" value="{{ old('first_per_visa_no_expiry_date') }}" placeholder="Expiry Date" class="form-control" name="first_per_visa_no_expiry_date">
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-primary">Visa Attachment</label>
                                            <input type="file" name="visa_attachment" class="form-control">
                                        </div>
                                    </div><br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="text-primary">Emirates ID No.</label>
                                            <input type="number" value="{{ old('emirates_id_no') }}" placeholder="" class="form-control" name="emirates_id_no">
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-primary">Emirates Expiry Date</label>
                                            <input type="date" name="emirates_expiry_date" value="{{ old('emirates_expiry_date') }}" placeholder="Emirates Expiry Date" class="form-control" >
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-primary">Passport Attachment</label>
                                            <input type="file" name="emirates_visa_attachment" class="form-control">
                                        </div>
                                    </div><br>

                                    <div class="row">
                                        
                                        
                                    <!-- <div  class="col-md-4">
                              <label class="text-primary">Location</label>
                              <textarea  value="{{ old('location') }}" name="location" placeholder="Location" class="form-control"></textarea>
                           </div> -->
                                    </div><br>

                                    <div id="box" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="text-primary">Company Trade License Number</label>
                                                <input type="number" class="form-control" placeholder="Company Trade License Number" name="first_per_trade_license_no">
                                            </div>
                                            <div class=" col-md-4 ">
                                                <label class="text-primary">licening authority</label>
                                                <input type="number" class="form-control" placeholder="licening authority" name="first_per_license_authority">
                                            </div>
                                            <div class="col-md-4">
                                                <label class="text-primary">VAT Reg Number</label>
                                                <input type="number" class="form-control"  value="" name="first_per_reg_no" placeholder="VAT Reg Number" class="form-control">
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="text-primary">VAT Emirate</label>
                                                <input type="number" class="form-control" placeholder="VAT Emirate" name="first_per_vat_emirates">
                                            </div>
                                            <div class=" col-md-4 ">
                                                <label class="text-primary">VAT GCC</label>
                                                <input type="number" class="form-control" placeholder="VAT GCC" name="first_per_vat_gcc">
                                            </div>
                                        </div><br>
                                    </div>





                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="text-primary">Mobile Number</label>
                                            <input type="number" value="{{ old('first_per_phone') }}"  placeholder="123" class="form-control" name="first_per_phone">
                                        </div>
                                        <!-- <div class=" col-md-6 ">-->
                                        <!--    <label class="text-primary">Ref No</label>-->
                                        <!--    <input type="text" value="{{ old('first_per_ref_no') }}" name="first_per_ref_no" class="form-control " placeholder="Ref no"-->
                                        <!--           aria-label="Name">-->
                                        <!--</div>-->
                                         <div class="col-md-6">
                                            <label class="text-primary">Contact Number 2</label>
                                            <input type="text" value="{{ old('first_per_contact1') }}" class="form-control" placeholder="Contact Number" name="first_per_contact1">
                                        </div>
                                    </div><br>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="text-primary">Address 1</label>
                                            <input type="text" value="{{ old('second_per_address1') }}" name="second_per_address1" placeholder="Enter address" class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="text-primary">Address 2</label>
                                            <input type="text"  value="{{ old('second_per_address2') }}" name="second_per_address2" placeholder="Enter address" class="form-control">
                                        </div>
                                    </div><br>

                                    <div class="row">
                                        <div class=" col-md-6 ">
                                            <label class="text-primary">PO box</label>
                                            <input type="text" value="{{ old('first_per_pobox') }}" name="first_per_pobox" class="form-control " placeholder="PO box"
                                                   aria-label="Name">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="text-primary">Remarks</label>
                                            <input type="text" name="first_per_remarks" value="{{ old('first_per_remarks') }}" placeholder="Remarks" class="form-control" >
                                        </div>
                                    </div><br>
                                    

                                    <div class="row">
                                        <div class=" col-md-6 ">
                                            <label class="text-primary">Vat Country</label>
                                            <!--<input type="text" value="@if($landlords->Profiles){{$landlords->Profiles->first_per_ref_no}}@endif" name="first_per_ref_no" class="form-control " placeholder="Ref no"-->
                                            <!--       aria-label="Name">-->
                                            <select class="form-control" name="second_per_vat_emirates" >
                                                <option value="" >Select Vat Country</option>
                                                <option value="2">GCC UAE</option>
                                                <option value="2">GCC Non UAE</option>
                                                <option value="2">Non GCC</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="text-primary">VAT Emirates</label>
                                            <select class="form-control" name="second_per_vat_emirates" >
                                                <option value="" >Select VAT Emirates</option>
                                                <option value="2">Abu Dhabi</option>
                                                <option value="2">Ajman</option>
                                                <option value="2">Dubai</option>
                                                <option value="2">Fujairah</option>
                                                <option value="2">Ras Al Khaima</option>
                                                <option value="2">Sharjah</option>
                                                <option value="2">Umm Al Quwain</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="text-primary">Profession</label>
                                            <input type="text" value="{{ old('second_per_profession') }}" name="second_per_profession" placeholder="Profession" class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="text-primary">VAT Reg No (TRN)</label>
                                            <input type="text"  value="{{ old('second_per_vat_reg_no') }}" class="form-control" name="second_per_vat_reg_no" placeholder="Enter VAT reg no">
                                        </div>
                                        
                                    </div><br>
                                    
                                    <div class="row">
                                          <div class="col-md-6">
                                            <label class="text-primary">Any Other Ref. no.</label>
                                            <input type="number" value="{{ old('other_ref_no') }}" name="other_ref_no" placeholder="Any Other Ref. no." class="form-control">

                                        </div>
                                        <div class="col-md-6">
                                            <label class="text-primary">Website <small>Optional</small></label>
                                            <input type="text" value="{{ old('other_ref_no') }}" name="website" placeholder="Website" class="form-control">
                                        </div>
                                    </div>
                                    <br>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="text-primary">Location</label>
                                            <textarea  value="{{ old('first_per_location') }}" name="first_per_location" placeholder="Location" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <br>
                                    
                                    
                                    <div class="row">
                                    <div class="col-md-12">
                                        <div style="border: 1px solid grey;">
                                            <center>
                                                <input id='add-row' class='btn btn-primary' type='button' value='Add More' /></center>
                                            <table id="test-table" class="table table-condensed">
                                                <thead>
                                                <tr>
                                                    <th>Attachments</th>
                                                    <th>Comment</th>
                                                </tr>
                                                </thead>
                                                <tbody id="test-body">
                                                <tr id="row0">
                                                    <td>
                                                        <input name='tenants_attachments[]'  type='file' class='form-control' />
                                                    </td>
                                                    <td>
                                                        <input name='comments[]'  placeholder="Comment" type='text' class='form-control input-md' />
                                                    </td>
                                                    <td>
                                                        <input class='delete-row btn btn-primary' type='button' value='Delete' />
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                    <hr>
                                    <h4 class="text-primary"> Second Person Contact Details</h4>
                                    <hr>
                                    <div class="row">
                                        <br>
                                        <div class="col-md-6">
                                            <label class="text-primary">Name</label>
                                            <input type="text" value="{{ old('second_per_name') }}"    name="second_per_name" placeholder="Name" class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="text-primary">Relationship with Landlord</label>
                                            <input type="text" value="{{ old('realationship_with_landlord') }}"  name="realationship_with_landlord" class="form-control" placeholder="Relationship with Landlord">
                                        </div>
                                    </div><br>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="text-primary">Phone</label>
                                            <input type="number" value="{{ old('second_per_phone') }}"  name="second_per_phone"  class="form-control" placeholder="Enter phone number...">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="text-primary">Mobile</label>
                                            <input type="number" value="{{ old('second_per_mobile') }}" name="second_per_mobile"  class="form-control" placeholder="Enter Mobile number...">
                                        </div>

                                    </div><br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="text-primary">Email</label>
                                            <input type="email" value="{{ old('second_per_email') }}"  name="second_per_email"  class="form-control" placeholder="Email">
                                        </div>
                                    </div><br>
                                    <hr>
                                    
                                   <hr>
                                  
                                    <h4 class="text-primary">Opening Balance</h4>
                                    
                                     <div id="detail">
                                    <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Select Project</label>
                                        <select class="form-control" name="project[]" id="selectProject" onchange="selectproject(this.value)">
                                                <option value="" selected disabled>Select project</option>
                                            @foreach($projects as $pro)
                                                <option value="{{$pro->id}}">{{$pro->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-primary">Select Building</label>
                                        <select class="form-control" name="building[]" id="selectBuilding" onchange="selectbuilding(this.value)">
                                        </select>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Select Floor</label>
                                        <select class="form-control" name="floor[]" id="selectFloor" onchange="selectfloor(this.value)">
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-primary">Select Apartment No.</label>
                                        <select class="form-control" name="apartment_no[]" id="selectFlat" >
                                        </select>
                                    </div>
                                </div><br>
                                
                                    <div class="row">
                                        <br>
                                        <div class="col-md-6">
                                            <label class="text-primary">Contract Amount</label>
                                            <input type="number" value="{{ old('contract_amount') }}" onkeyup="con_amount()" id="contract_amount"  name="contract_amount[]" placeholder="" class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="text-primary">Cash Discount</label>
                                            <input type="number" value="0"  id="cash_discount"  onkeyup="rent_amount()"  name="cash_discount[]" placeholder="" class="form-control">
                                        </div>
                                    </div><br>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="text-primary">Net Rent Amount</label>
                                            <input type="number" value="{{ old('net_rent_amount') }}" readonly id="net_rent_amount" name="net_rent_amount[]" class="form-control" placeholder="">
                                        </div>
                                        <div class="col-md-6">
                                              <label class="text-primary">Amount Paid</label>
                                            <input type="number" value="{{ old('amount_received') }}" onkeyup="final_amount()" id="amount_received"  name="amount_received[]" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6">
                                              <label class="text-primary">Outstanding Amount</label>
                                            <input type="number" value="{{ old('outstanding_amount') }}" readonly id="outstanding_amount"  name="outstanding_amount[]" placeholder="" class="form-control">
                                        </div>
                                        
                                    </div>
                                    </div>
                                    
                                    <div class="row">
    <div class="col-md-8">
        <span id="addmore" class="pull-right btn btn-primary" onclick="addmore()">Add More <i class="fa fa-plus"></i></span>
    </div>
  </div>
                                </div>
                                <div class="text-center mt-4">
                                    <button class="btn btn-started"> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- create new modal end -->
    </div>
    
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
     <script type="text/javascript">
        let url = '/get/buildings';
        let url1 = '/get/floors';
        let url2 = '/get/flats';
        let url3 = '/get/flats/assign/lanlord';
        
        function selectproject(value)
        {
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var request    = $.ajax({
        url: "{{url('/get_buildings')}}",
        method: "post",
        data: {_token: CSRF_TOKEN, pro_id:value},
        dataType: "html"
      });
      request.done(function( msg ) {
        //alert(msg);
        $('#selectBuilding').html(msg);
      });
        }
        
        
        // $(document).on("change" , "#selectProject" , function () {
        //     let value = $(this).val();
        //     console.log('value');
        //     console.log(value);
        //     $('#selectBuilding').html('');
        //     $.ajax({
        //         url:url+'/'+value,
        //         Type:'GET',
        //         success: function (response) {
        //             if(response.status == true) {
        //                 let html = '<option>Select Building</option>';
        //                 $('#selectBuilding').append(html);
        //                 response.data.map((d) => {
        //                     html = '<option value="'+ d.id +'">'+ d.name +'</option>';
        //                     $('#selectBuilding').append(html);
        //                 });
        //             }
        //         },
        //         error: function (error) {
        //             console.log(error);
        //             console.log(error);
        //         }
        //     })
        // });

          function selectbuilding(value)
        {
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var request    = $.ajax({
        url: "{{url('/get_floors')}}",
        method: "post",
        data: {_token: CSRF_TOKEN, building_id:value},
        dataType: "html"
      });
      request.done(function( msg ) {
        //alert(msg);
        $('#selectFloor').html(msg);
      });
        }

        // $(document).on("change" , "#selectBuilding" , function () {
        //     let value = $(this).val();
        //     console.log('value');
        //     console.log(value);
        //     $('#selectFloor').html('');
        //     $.ajax({
        //         url:url1+'/'+value,
        //         Type:'GET',
        //         success: function (response) {
        //             if(response.status == true) {
        //                 let html = '<option>Select Floor</option>';
        //                 $('#selectFloor').append(html);
        //                 response.data.map((d) => {
        //                     html = '<option value="'+ d.building_id +'">'+ d.name +'</option>';
        //                     $('#selectFloor').append(html);
        //                 });
        //             }
        //         },
        //         error: function (error) {
        //             console.log(error);
        //             console.log(error);
        //         }
        //     })
        // });

     function selectfloor(value)
        {
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var request    = $.ajax({
        url: "{{url('/get_flats')}}",
        method: "post",
        data: {_token: CSRF_TOKEN, building_id:value},
        dataType: "html"
      });
      request.done(function( msg ) {
        //alert(msg);
        $('#selectFlat').html(msg);
      });
        }



        // $(document).on("change" , "#selectFloor" , function () {
        //     let value = $(this).val();
        //     console.log('value');
        //     console.log(value);
        //     $('#selectFlat').html('');
        //     $.ajax({
        //         url:url2+'/'+value,
        //         Type:'GET',
        //         success: function (response) {
        //             if(response.status == true) {
        //                 let html = '<option>Select Flat</option>';
        //                 $('#selectFlat').append(html);
        //                 response.data.map((d) => {
        //                     html = '<option value="'+ d.unit_code +'">'+ d.unit_code +'</option>';
        //                     $('#selectFlat').append(html);
        //                 });
        //             }
        //         },
        //         error: function (error) {
        //             console.log(error);
        //             console.log(error);
        //         }
        //     })
        // });

        $(document).on("change" , "#selectFlat" , function () {
            let value = $(this).val();
            console.log('value');
            console.log(value);
            $('#LanLord').html('');
            $.ajax({
                url:url3+'/'+value,
                Type:'GET',
                success: function (response) {
                    console.log(response.data);
                    console.log('response.data');
                        if (response.status == true) {
                            if(response.data != null) {
                                $('#LanLord').val(response.data.user.name);
                                $('#managment_fee').val(response.data.profile.managment_fee);
                            }
                        }
                },
                error: function (error) {
                    console.log(error);
                    console.log(error);
                }
            })
        });


        $(document).on("change" , "#no_installments" , function () {
            $('#rowTable').html('');
            let no_installments = $(this).val();
            let totalAmount = $('#totalAmount').val();
            console.log('value');
            console.log(no_installments);
            console.log(totalAmount);
            let totalA = totalAmount/no_installments;
            for (var i = 1; i <= no_installments; i++) {
                console.log('count iiiii');
                console.log(i);
                let Tblhtml = '<tr>' +
                    '<td><input type="number" name="no_installment[]" value="'+ i +'" /></td>' +
                    '<td><input type="number" name="amount[]" value="'+ totalA +'" /></td>' +
                    '<td><input type="text" name="particular[]" value="" /></td>' +
                    '<td><select name="pay_type" style="width:150px;" class="form-control">' +
                        '<option> select type </otpion>' +
                        '<option value="cash"> Cash </otpion>' +
                        '<option value="check"> Check </otpion>' +
                    '</select></td>' +
                    '<td><input type="number" name="check_no[]" /></td>' +
                    '<td><input type="date" name="check_date[]" /></td>' +
                    '<td><input type="text" name="check_issue[]" /></td>' +
                    '<td><input type="text" name="party_name[]" /></td>' +
                    '<td>' +
                        '<select id="selectOpt" name="selectOpt[]">' +
                            '<option>select option</option>' +
                            '<option>company</option>' +
                            '<option>landlord</option>' +
                        '</select>' +
                    '</td>' +
                    '</tr>';
                $('#rowTable').append(Tblhtml);
            }
            $('#rowTableDisplay').show();

        });


    </script>
    <script type="text/javascript">
        // Add row
        var row=1;
        $(document).on("click", "#add-row", function () {
            var new_row = '<tr id="row' + row + '"><td><input name="tenants_attachments[]" type="file" class="form-control" /></td><td><input name="comments[]" type="text" placeholder="Comment" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';

            $('#test-body').append(new_row);
            row++;
            return false;
        });

        // Remove criterion
        $(document).on("click", ".delete-row", function () {
            //  alert("deleting row#"+row);
            if(row>1) {
                $(this).closest('tr').remove();
                row--;
            }
            return false;
        });

        // Add row
        var row1=1;
        $(document).on("click", "#security-row", function () {
            var new_row = '<tr id="row' + row1 + '">'
                +
                    '<td>' +
                    '<input name="serial_no[]" type="text" class="form-control" /></td>' +
                    '<td><input name="check_no_security[]" type="text"  class="form-control" /></td>' +
                    '<td><input name="check_date_security[]" type="date"  class="form-control" /></td>' +
                    '<td><input name="check_issue_security[]" type="text" class="form-control" /></td>' +
                    '<td><input name="party_name_security[]" type="text" class="form-control" /></td>' +
                    '<td><input name="check_deposite_security[]" type="text" class="form-control" /></td>' +
                    '<td><input class="security btn btn-primary" type="button" value="Delete" /></td>'
                +
                '</tr>';
            $('#test-body1').append(new_row);
            row1++;
            return false;
        });

        // Remove criterion
        $(document).on("click", ".security", function () {
            //  alert("deleting row#"+row);
            if(row1>1) {
                $(this).closest('tr').remove();
                row1--;
            }
            return false;
        });



        // Add row
        var row2=1;
        $(document).on("click", "#other-row", function () {
            var new_row = '<tr id="row' + row2 + '">'
                +
                '<td>' +
                '<input name="other_serial_no[]" type="text" class="form-control" /></td>' +
                '<td><input name="other_particular[]" type="text"  class="form-control" /></td>' +
                '<td><input name="other_amount[]" type="number"  class="form-control" /></td>' +
                '<td><input name="other_vat[]" type="number" class="form-control" /></td>' +
                '<td><input class="other-row btn btn-primary" type="button" value="Delete" /></td>'
                +
                '</tr>';
            $('#test-body2').append(new_row);
            row1++;
            return false;
        });

        // Remove criterion
        $(document).on("click", ".other-row", function () {
            //  alert("deleting row#"+row);
            if(row1>1) {
                $(this).closest('tr').remove();
                row1--;
            }
            return false;
        });

    </script>
    
    <script type="text/javascript">
        $(document).ready(function() { 
            //add new amount field
            add_fac_row();
            //   add_fac_row2();
            $(document).on('click', '.add-more-fac', function(){
                add_fac_row();
            });

            $(document).on('click', '.add-more-fac2', function(){
                add_fac_row2();
            });
            //function to add new row from amount
            function add_fac_row(){
                var html = '';
                html +='<tr>';
                html +='<td>';
                html +='<input type="file" name="acc_no_cr" placeholder="Attachements option" class="form-control">';

                html +='</td>';
                html +='<td class="text-center">';
                html +='<button type="button" class="btn btn-danger remove-fac-row" >X</button>';
                html +='</td>';
                html +='</tr>';
                $('#fac_fieds_tbl').append(html);
            }



            function add_fac_row2(){
                var html = '';
                html +='<tr>';
                html +='<td>';
                html +='<input type="file" name="acc_no_cr" placeholder="Attachements option" class="form-control">';

                html +='</td>';
                html +='<td class="text-center">';
                html +='<button type="button" class="btn btn-danger remove-fac-row2" >X</button>';
                html +='</td>';
                html +='</tr>';
                $('#fac_fieds_tbl2').append(html);
            }




            //remove amount row
            $(document).on('click', '.remove-fac-row', function(){
                $(this).closest('tr').remove();
            });
            $(document).on('click', '.remove-fac-row2', function(){
                $(this).closest('tr').remove();
            });
        });
    </script>
    <script type="text/javascript">
        $("#FileInput").on('change',function (e) {
            var labelVal = $(".title").text();
            var oldfileName = $(this).val();
            fileName = e.target.value.split( '\\' ).pop();

            if (oldfileName == fileName) {return false;}
            var extension = fileName.split('.').pop();

            if ($.inArray(extension,['jpg','jpeg','png']) >= 0) {
                $(".filelabel i").removeClass().addClass('fa fa-file-image-o');
                $(".filelabel i, .filelabel .title").css({'color':'#208440'});
                $(".filelabel").css({'border':' 2px solid #208440'});
            }
            else if(extension == 'pdf'){
                $(".filelabel i").removeClass().addClass('fa fa-file-pdf-o');
                $(".filelabel i, .filelabel .title").css({'color':'red'});
                $(".filelabel").css({'border':' 2px solid red'});

            }
            else if(extension == 'doc' || extension == 'docx'){
                $(".filelabel i").removeClass().addClass('fa fa-file-word-o');
                $(".filelabel i, .filelabel .title").css({'color':'#2388df'});
                $(".filelabel").css({'border':' 2px solid #2388df'});
            }
            else{
                $(".filelabel i").removeClass().addClass('fa fa-file-o');
                $(".filelabel i, .filelabel .title").css({'color':'black'});
                $(".filelabel").css({'border':' 2px solid black'});
            }

            if(fileName ){
                if (fileName.length > 10){
                    $(".filelabel .title").text(fileName.slice(0,4)+'...'+extension);
                }
                else{
                    $(".filelabel .title").text(fileName);
                }
            }
            else{
                $(".filelabel .title").text(labelVal);
            }
        });
    </script>


    <script>
        $(document).ready(function(){
            $('#Nature').on('change', function() {
                if ( this.value == 'company')
                    //.....................^.......

                {
                    $("#box").show();
                    $("#box2").hide();
                    $("#box3").hide();
                }
                else
                {
                    $("#box").hide();
                    $("#box2").show();
                    $("#box3").show();
                }
            });

        });
        
        $(document).ready(function(){
            // alert("f");
            $('#Nature1').on('change', function() {alert("de");
                if ( this.value == 'company')
                    //.....................^.......

                {
                    $("#box1").show();
                }
                else
                {
                    $("#box1").hide();
                }
            });
            
            
            


        });
        
        
        



        function con_amount()
        {
            var cash   = $('#contract_amount').val(); 
            var dis    = $('#cash_discount').val();
            var result = cash-dis;
             $('#net_rent_amount').val(result);
            // if(dis>0){
                
            // }
        }

        function rent_amount()
        {
            var cash   = $('#contract_amount').val();
            var dis    = $('#cash_discount').val();
            var result = cash-dis;
            if(result<0){
                alert("Discount Amount Should Be Less Than Contract Amount");
                $('#cash_discount').focus();
            }
            $('#net_rent_amount').val(result);
        }
    
        function final_amount()
        {
            var net_amount =$('#net_rent_amount').val();
            var rcv_cash =$('#amount_received').val();
            var result = net_amount-rcv_cash;
            $('#outstanding_amount').val(result);
        }
        
           function addmore()
    {
       
       var row="";
       row +='<br><div class="row warning-element">'+
                '<div class="col-md-6">'+
                '<label class="text-primary">Select Project</label>'+
                                        '<select class="form-control" name="project[]" id="selectProject" onchange="selectproject(this.value)">'+
                                                '<option value="" selected disabled>Select project</option>@foreach($projects as $pro)<option value="{{$pro->id}}">{{$pro->name}}</option>@endforeach'+
                                        '</select>'+
                                    '</div>'+
                     '<div class="col-md-6"><label class="text-primary">Select Building</label><select class="form-control" name="building[]" id="selectBuilding" onchange="selectbuilding(this.value)"></select></div>'+
               ' </div>'+
               '<br><div class="row"><div class="col-md-6"><label class="text-primary">Select Floor</label><select class="form-control" name="floor[]" id="selectFloor" onchange="selectfloor(this.value)"></select></div>'+
                                    '<div class="col-md-6">'+
                                        '<label class="text-primary">Select Flat</label>'+
                                        '<select class="form-control" name="apartment_no[]" id="selectFlat" >'+
                                        '</select>'+
                                    '</div>'+
                                '</div><br>'+
                                '<div class="row"><br><div class="col-md-6">'+
                                            '<label class="text-primary">Contract Amount</label>'+
                                            '<input type="number" value="{{ old('contract_amount') }}" onkeyup="con_amount()" id="contract_amount"  name="contract_amount[]" placeholder="" class="form-control">'+
                                        '</div>'+
                                        '<div class="col-md-6">'+
                                            '<label class="text-primary">Cash Discount</label>'+
                                            '<input type="number" value="0"  id="cash_discount"  onkeyup="rent_amount()"  name="cash_discount[]" placeholder="" class="form-control">'+
                                        '</div>'+
                                    '</div><br>'+
                                    '<div class="row">'+
                                        '<div class="col-md-6">'+
                                            '<label class="text-primary">Net Rent Amount</label>'+
                                            '<input type="number" value="{{ old('net_rent_amount') }}" readonly id="net_rent_amount" name="net_rent_amount[]" class="form-control" placeholder="">'+
                                        '</div>'+
                                        '<div class="col-md-6">'+
                                              '<label class="text-primary">Amount Paid</label>'+
                                            '<input type="number" value="{{ old('amount_received') }}" onkeyup="final_amount()" id="amount_received"  name="amount_received[]" placeholder="" class="form-control">'+
                                        '</div>'+
                                   '</div>'+
                                    
                                    '<br><div class="row">'+
                                        '<div class="col-md-6">'+
                                              '<label class="text-primary">Outstanding Amount</label>'+
                                            '<input type="number" value="{{ old('outstanding_amount') }}" readonly id="outstanding_amount"  name="outstanding_amount[]" placeholder="" class="form-control">'+
                                        '</div>'+
                                    '</div>'+
                                    '</div><br>';
       $('#detail').append(row);
    //     $(document).on('click', '.remove-row', function(){
    //  $(this).closest('div').remove();
    //     });
    }

    </script>
@endsection

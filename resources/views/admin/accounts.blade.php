@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'warehouse_list'
])

@section('content')
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
                 @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('error') !!}</strong>
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
<style type="text/css">
            .filelabel {
    width: 100%;
    border: 2px dashed grey;
    border-radius: 5px;
    display: block;
    padding: 5px;
    transition: border 300ms ease;
    cursor: pointer;
    text-align: center;
    margin: 0;
}
.filelabel i {
    display: block;
    font-size: 30px;
    padding-bottom: 5px;
}
.filelabel i,
.filelabel .title {
  color: grey;
  transition: 200ms color;
}
.filelabel:hover {
  border: 2px solid #1665c4;
}
.filelabel:hover i,
.filelabel:hover .title {
  color: #1665c4;
}
#FileInput{
    display:none;
}
</style>
        <div class="row">
  <div class="col-12">
  <div class="row">
      <div class="col-md-3">

      </div>
       <div class="col-md-3">

      </div>
       <div class="col-md-4">

      </div>
       <div class="col-md-2">
            <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Add New</button>
      </div>
  </div>
    <div class="material-card card">
      <div class="card-body">

        <h4 class="card-title"> List</h4>
        <h6 class="card-subtitle">
        </h6><br>
        <div class="table-responsive">
          <table id="complex_header" class="table table-striped table-bordered display"
          style="width:100%">
          <thead>

            <tr>
              <th>Sr#</th>
              <th>Account Name</th>
              <th>Account Type</th>
              <th>Asset & Liabilities</th>
              <th>Description</th>
              <th>Type</th>
              <th>Actions</th>
            </tr>
          </thead>
           <tbody>
               @foreach($account as $accounts)
               <tr>
                   <td>{{$counter++}}</td>
                   <td>{{$accounts->account_name}}</td>
                   <td>{{$accounts->account_type}}</td>
                   <td>{{$accounts->asset_liabilitie}}</td>
                   <td>{{$accounts->description}}</td>
                   <td>{{$accounts->type}}</td>
                   <td>
                       <!--<a class="btn btn-primary"></a>-->
                   </td>
               </tr>
               @endforeach
           </tbody>

      <tfoot>
        <tr>
           <th>Sr#</th>
              <th>Account Name</th>
              <th>Account Type</th>
              <th>Asset & Liabilities</th>
              <th>Description</th>
              <th>Type</th>
              <th>Actions</th>
            </tr>
      </tfoot>
     </table>
    </div>
   </div>
  </div>
 </div>
</div>
  <div class="modal fade" id="myModal" role="dialog" >
    <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Accounts</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">

                   <div class="p-2">
  <center><h3><b>Add New Account</b></h3>
            
           </center>
                   <form class="user" action="{{url('/Admin/save_account')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                   <div class="form-group row">
                        <div class="col-md-2">
                       <label class="text-primary">Account Type</label>
                       <br>
                              
                              
                    </div>
                    
                     <div class="col-md-2">
                     <input type="radio" class="" onclick="dd()" checked id="male"  name="gender" value="male">
                               <label for="male">Income</label><br>
                       
                    </div>
                      <div class="col-md-5">
                     <input type="radio"  onclick="dd()" id="female" name="gender" value="female">
                               <label for="female">Expense</label>
                               <input type="hidden" name="account_type" id="years">
                       
                    </div>
                    
                    </div>
                    
                    
                      <div class="form-group row">
                       <div class="col-md-2">
                            <label class="text-primary">Asset & Liabilities</label>
                       </div>   
                       <div class="col-md-10">
                        
                        <select class="form-control" name="asset_liabilitie">
                            <option value="" selected disabled>Select One</option>
                            <option value="Fixed Asset (major purchase)">Fixed Asset (major purchase)</option>
                            <option value="Bank">Bank</option>
                            <option value="Loan">Loan</option>
                            <option value="Credit Card">Credit Card</option>
                            <option value="Equity">Equity</option>
                        </select>
                       </div>
                  </div>
                  
                  
                  
                  
                  <div class="form-group row">
                       <div class="col-md-2">
                       <label class="text-primary">Other Account Types</label>
                       </div>
                       <div class="col-md-1">
                        <input type="checkbox" value=""  name="other_account_select" id="other_account_select">
                       </div>
                        <div class="col-md-9">
                         
                        <select class="form-control" style="display:none" name="other_account_type" id="show_accounts">
                            <option value="" selected disabled>Select One</option>
                            <option value="Accounts Receivable">Accounts Receivable</option>
                            <option value="Other Current Asset">Other Current Asset</option>
                            <option value="Other Asset">Other Asset</option>
                            <option value="Accounts Payable">Accounts Payable</option>
                            <option value="Other Current Liability">Other Current Liability</option>
                            <option value="Long Term Liability">Long Term Liability</option>
                            <option value="Cost of Goods Sold">Cost of Goods Sold</option>
                            <option value="Other Income">Other Income</option>
                            <option value="Other Expense">Other Expense</option>
                        </select>
                       </div>

                  </div>
                  
                  
                  <hr>
                 <div class="form-group row">
                     <div class="col-md-2">
                         <label class="text-primary">Account Name</label>
                        
                       </div>
                       <div class="col-md-10">
                        
                        <input type="text" placeholder="Account Name" class="form-control" name="account_name" required="" value="{{old('account_name')}}">
                       </div>
                  </div>
                  
                   <div class="form-group row">
                     <div class="col-md-2">
                         <label class="text-primary">Description</label>
                        
                       </div>
                       <div class="col-md-10">
                        
                        <textarea name="description" class="form-control"></textarea>
                       </div>
                  </div>
                  
                  
                        <div class="form-group row">
                       <div class="col-md-2">
                       <label class="text-primary">Sub-Account of</label>
                       </div>
                       <div class="col-md-1">
                        <input type="checkbox" value=""  name="subaccount" id="subaccount">
                       </div>
                        <div class="col-md-9">
                         
                        <select class="form-control" style="display:none" name="sub_account_type" id="sub_account_type">
                            <option value="" selected disabled>Select One</option>
                            @foreach($account as $accounts)
                            <option value="{{$accounts->id}}">{{$accounts->account_name}}</option>
                            @endforeach
                        </select>
                       </div>

                  </div>
                  
                  
                  
                  
                    <div class="form-group row">
                     <div class="col-md-2">
                         <label class="text-primary">Note</label>
                        
                       </div>
                       <div class="col-md-10">
                        
                        <input type="text" placeholder="Note" class="form-control" name="note" required="" value="{{old('note')}}">
                       </div>
                  </div>
                  
                   <div class="form-group row">
                     <div class="col-md-2">
                         <label class="text-primary">Vat Code</label>
                        
                       </div>
                       <div class="col-md-10">
                        
                        <select class="form-control" name="vat_code" >
                                              <option value="" selected disabled>Select Vat Type</option>
                                              <option value="Vat Included">Vat Included</option>
                                              <option value="Vat Not Included">Vat Not Included</option>
                                              <option value="No Vat">No Vat</option>
                                           </select>
                       </div>
                  </div>
    
               
<br>
                <button  type="submit" name="createNewProject" class="btn btn-primary btn-user btn-block">
                 Create
               </button>
              </form>
           </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<input type="hidden" name="rem_photo" value="1" id="rem_photo">


<!-- script area  -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    $("#FileInput").on('change',function (e) {
            var labelVal = $(".title").text();
            var oldfileName = $(this).val();
                fileName = e.target.value.split( '\\' ).pop();

                if (oldfileName == fileName) {return false;}
                var extension = fileName.split('.').pop();

            if ($.inArray(extension,['jpg','jpeg','png']) >= 0) {
                $(".filelabel i").removeClass().addClass('fa fa-file-image-o');
                $(".filelabel i, .filelabel .title").css({'color':'#208440'});
                $(".filelabel").css({'border':' 2px solid #208440'});
            }
            else if(extension == 'pdf'){
                $(".filelabel i").removeClass().addClass('fa fa-file-pdf-o');
                $(".filelabel i, .filelabel .title").css({'color':'red'});
                $(".filelabel").css({'border':' 2px solid red'});

            }
  else if(extension == 'doc' || extension == 'docx'){
            $(".filelabel i").removeClass().addClass('fa fa-file-word-o');
            $(".filelabel i, .filelabel .title").css({'color':'#2388df'});
            $(".filelabel").css({'border':' 2px solid #2388df'});
        }
            else{
                $(".filelabel i").removeClass().addClass('fa fa-file-o');
                $(".filelabel i, .filelabel .title").css({'color':'black'});
                $(".filelabel").css({'border':' 2px solid black'});
            }

            if(fileName ){
                if (fileName.length > 10){
                    $(".filelabel .title").text(fileName.slice(0,4)+'...'+extension);
                }
                else{
                    $(".filelabel .title").text(fileName);
                }
            }
            else{
                $(".filelabel .title").text(labelVal);
            }
        });
</script>
 <script type="">

$('#other_account_select').click(function() {
    $("#show_accounts").toggle(this.checked);
});

$('#subaccount').click(function() {
    $("#sub_account_type").toggle(this.checked);
});




     if(document.getElementById('male').checked) {
    gender="1";
    $('#years').val(gender);
}
else if(document.getElementById('female').checked) {
 gender="2";
 $('#years').val(gender);
}

function dd(){
    if(document.getElementById('male').checked) {
    gender="1";
    $('#years').val(gender);
  }
  else if(document.getElementById('female').checked) {
 gender="2";
 $('#years').val(gender);
}
}

 </script>
@endsection

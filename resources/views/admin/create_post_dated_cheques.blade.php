@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'create_post_dated_cheeques'
])

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('error') !!}</strong>
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <style type="text/css">
            .filelabel {
                width: 100%;
                border: 2px dashed grey;
                border-radius: 5px;
                display: block;
                padding: 5px;
                transition: border 300ms ease;
                cursor: pointer;
                text-align: center;
                margin: 0;
            }
            .filelabel i {
                display: block;
                font-size: 30px;
                padding-bottom: 5px;
            }
            .filelabel i,
            .filelabel .title {
                color: grey;
                transition: 200ms color;
            }
            .filelabel:hover {
                border: 2px solid #1665c4;
            }
            .filelabel:hover i,
            .filelabel:hover .title {
                color: #1665c4;
            }
            #FileInput{
                display:none;
            }
        </style>
        <div class="row">
            <div class="col-12">

                <div class="material-card card">
                    <div class="card-body">

                        <center><h3 class="card-title">Create Post Dated Cheques</h3></center>
                        <div class="row">
                            <div class="col-md-1"></div>
                             <div class="col-md-2">
                               <input onclick="dd()" type="radio" name="rad" id="bank" />
                               <label><b>Bank Name</b></label>
                             </div>

                             <div class="col-md-2">
                               <input onclick="dd()" type="radio" name="rad" id="account" />
                               <label><b>Amount</b></label>
                             </div>

                             <div class="col-md-2">
                               <input onclick="dd()" type="radio" name="rad" id="voucher" />
                               <label><b>Voucher No.</b></label>
                             </div>

                             <div class="col-md-2">
                               <input onclick="dd()" type="radio" name="rad" id="cheque" />
                               <label><b>Cheque No.</b></label>
                               <input type="hidden" name="no_of_years" name="rad" id="val">
                             </div>

                               <div class="col-md-2">
                               <input onclick="dd()" type="radio" name="rad" id="all" />
                               <label><b>All</b></label>
                             </div>

                        </div>
                        <br>

                        <div class="row">
                           <div class="col-md-4">
                                <label class=""><b>Select Tenant</b></label>
                                <select class="form-control" name="tenant" id="tenant" >
                              <option value="" selected disabled>Select Tenant</option>
                              @foreach($tenant as $tenants)
                              <option value="{{$tenants->id}}">{{$tenants->name}}</option>
                              @endforeach
                           </select>
                           </div>

                           <div class="col-md-4">
                               <label class=""><b>Select Landlord</b></label>
                                <select class="form-control" name="landlord" id="landlord" >
                                     <option value="" selected disabled>Select Landlord</option>
                              @foreach($landlord as $landlords)
                              <option value="{{$landlords->id}}">{{$landlords->name}}</option>
                              @endforeach
                           </select>
                           </div>

                              <div class="col-md-4">
                               <label class=""><b>Select Status</b></label>
                                <select class="form-control" name="status_type" id="status_type" >
                                     <option value="" selected disabled>Select Status</option>
                                     <option value="Open" >Open</option>
                                     <option value="Cleared PDC" >Cleared PDC</option>
                                     <option value="Pending PDC Submitted To Landlord" >Pending PDC Submitted To Landlord</option>
                                     <option value="PDC Submitted To Landlord" >PDC Submitted To Landlord</option>
                                     <option value="Bounced PDC" >Bounced PDC</option>
                                     <option value="Re-Submitted PDC" >Re-Submitted PDC</option>
                                     <option value="Replacement PDC" >Replacement PDC</option>
                                     <option value="Bounced Cleared  PDC" >Bounced Cleared  PDC</option>
                                     <option value="Bounced Cheque" >Bounced Cheque</option>
                                     <option value="Replaced" >Replaced</option>
                                     <option value="Replaced By Cheque" >Replaced By Cheque</option>
                                     <option value="Hold" >Hold</option>
                                     <option value="Release" >Release</option>

                           </select>
                           </div>


                       </div>


                        <br>
                         <div class="row">
                           <div class="col-md-3">
                                <label class=""><b>Select Project</b></label>
                                <select class="form-control" name="project" id="selectProject" onchange="selectproject(this.value)">
                              <option value="" selected disabled>Select project</option>
                              @foreach($projects as $pro)
                              <option value="{{$pro->id}}">{{$pro->name}}</option>
                              @endforeach
                           </select>
                           </div>

                           <div class="col-md-3">
                               <label class=""><b>Select Building</b></label>
                                <select class="form-control" name="building" id="selectBuilding" onchange="selectbuilding(this.value)">
                           </select>
                           </div>

                           <div class="col-md-3">
                                <label class=""><b>Select Floor</b></label>
                                <select class="form-control" name="floor" id="selectFloor" onchange="selectfloor(this.value)">
                           </select>
                           </div>

                           <div class="col-md-3">
                               <label class=""><b>Select Flat</b></label>
                                <select class="form-control" name="flat" id="selectFlat" onchange="selectflat(this.value)" >
                           </select>
                           </div>

                       </div>
                       <br>


                       <br>

                       <div class="row">

                           <div class="col-md-3">
                               <label><b>Search</b></label>
                               <input type="text" class="form-control" name="search" id="search"/>
                           </div>

                           <div class="col-md-3">
                               <label><b>Start Date</b></label>
                               <input type="date" value="" class="form-control" name="day1" id="day1"/>
                           </div>

                           <div class="col-md-3">
                               <label><b>End Date</b></label>
                                <input type="date" value="" class="form-control" name="day2" id="day2"/>
                           </div>

                           <div class="col-md-3">
                               <button class="btn btn-success" onclick="filter()">Filter</button>
                           </div>

                       </div>




                </div>
            </div>
            <div class="table-responsive">
            <table id="complex_header" class="table table-striped table-bordered display"
                                    style="width:100%">
                                    <thead>
                                       <tr>
                                          <th>Sr #</th>
                                          <th>Installment No.</th>
                                          <th>Party Name</th>
                                          <th>Building Name</th>
                                          <th>Flat/Apartment No.</th>
                                          <th>Particular</th>
                                          <th>Cheque No</th>
                                          <th>Cheque Type</th>
                                          <th>Cheque Date</th>
                                          <th>Cheque Issue From Bank</th>
                                          <th>Amount</th>
                                          <th>Cheque Deposit</th>
                                          <th>Status</th>
                                          <!--<th>Type</th>-->
                                          <center><th>-</th></center>

                                       </tr>
                                    </thead>
                                   <tbody id="detail">

                                   </tbody>
                                 </table>
            </div>
        </div>
    </div>



     <button type="button" style="display:none" class="btn btn-info " id="replace_btn" data-toggle="modal" data-target="#myModal"> Add New</button>

    <div class="modal fade" id="myModal" role="dialog" >
    <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Replacement PDC</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">

                   <div class="p-5">
  <center><h3><b>Replacement PDC</b></h3>

           </center>
                   <form class="user" action="{{url('/Admin/save_replacement_pdc')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                   <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">INSTALLMENT NO </label>

                        <input type="number" placeholder="Installment No." class="form-control" value="{{old('installment_no')}}"  name="installment_no" required>
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">INSTALLMENT AMOUNT></label>
                        <input type="number" placeholder="Installment Amount" class="form-control" value="{{old('amount')}}"  name="amount" required>
                       </div>
                  </div>
                  <div class="form-group row">
                       <div class="col-md-6">
                       <label class="text-primary">PARTICULAR</label>
                        <input type="text" placeholder="Particular"  class="form-control" value="{{old('particular')}}"  name="particular" >
                       </div>
                        <div class="col-md-6">
                         <label class="text-primary">SELECT TYPE</label>
                         <select class="form-control" name="pay_type">
                             <option value="" selected disabled>Select Type</option>
                             <option value="Cash">Cash</option>
                             <option value="Check" >Check</option>
                         </select>
                       </div>

                  </div>
                  <div class=" row">
                     <div class="col-md-6">
                         <label class="text-primary">CHECK NO</label>
                        <input type="number" placeholder="0"  value="{{old('check_no')}}" readonly class="form-control" id="check_no"  name="check_no" required="">
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">CHECK DATE</label>
                        <input type=""   class="form-control" name="check_date" readonly id="c_date" >
                        <input type="hidden"   class="form-control" name="contract_id" readonly id="contract_id">
                        <input type="hidden"   class="form-control" name="installment" readonly id="installment">
                       </div>
                  </div>

                  <div class="form-group row">
                      <div class="col-md-6">
                        <label class="text-primary">CHECK ISSUE FROM BANK</label>
                        <input type="text"  value="{{old('check_issue')}}"  class="form-control"  name="check_issue" required="">
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">CHECK PARTY NAME</label>
                        <input type="text" value="{{old('party_name')}}"  class="form-control"  name="party_name" required="">
                       </div>
                  </div>

                  <div class="form-group row">
                      <div class="col-md-6">
                        <label class="text-primary">CHECK DEPOSITE</label>
                         <select class="form-control" name="deposit_to">
                             <option value="" selected disabled>Select One</option>
                             <option value="company">company</option>
                             <option value="landlord" >landlord</option>
                         </select>
                       </div>

                       <div class="col-md-6">
                           <label class="text-primary">Other Charges</label>
                           <input type="number" class="form-control" name="other_charges"/>
                       </div>

                  </div>





<br>
                <button  type="submit" name="createNewProject" class="btn btn-primary btn-user btn-block">
                 Create
               </button>
              </form>
           </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>


    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">



           function filter()
        {
        var start     = $('#day1').val();
        var end       = $('#day2').val();
        var search    = $('#search').val();
        var check     = $('#val').val();
        var projects  = $('#selectProject').val();
        var buildings = $('#selectBuilding').val();
        var floors    = $('#selectFloor').val();
        var flats     = $('#selectFlat').val();
        var tenants   = $('#tenant').val();
        var landlords = $('#landlord').val();
        var statuses  = $('#status_type').val();
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var request    = $.ajax({
        url: "{{url('/get_filter_data')}}",
        method: "post",
        data: {_token: CSRF_TOKEN, start:start,end:end,search:search,check:check,project:projects,building:buildings,floor:floors,flat:flats,tenant:tenants,landlord:landlords,status:statuses},
        dataType: "html"
      });
      request.done(function( msg ) {
        // alert(msg);
        $('#detail').html(msg);

         $('#day1').val("");
         $('#day2').val("");
         $('#search').val("");
         $('#val').val("");
         $('#selectProject').val("");
         $('#selectBuilding').val("");
         $('#selectFloor').val("");
         $('#selectFlat').val("");
         $('#tenant').val("");
         $('#landlord').val("");
         $('#status_type').val("");
      });
        }




//       if(document.getElementById('bank').checked) {
//     gender="1";
//     $('#val').val(gender);
// }
// else if(document.getElementById('account').checked) {
//  gender="2";
//  $('#val').val(gender);
// }
// else if(document.getElementById('voucher').checked) {
//  gender="2";
//  $('#val').val(gender);
// }
// else if(document.getElementById('cheque').checked) {
//  gender="2";
//  $('#val').val(gender);
// }

function dd(){
    if(document.getElementById('bank').checked) {
    gender="1";
    $('#val').val(gender);
  }
  else if(document.getElementById('account').checked) {
 gender="2";
 $('#val').val(gender);
}
  else if(document.getElementById('voucher').checked) {
 gender="3";
 $('#val').val(gender);
}
  else if(document.getElementById('cheque').checked) {
 gender="4";
 $('#val').val(gender);
}
 else if(document.getElementById('all').checked) {
 gender="5";
 $('#val').val(gender);
}
}


 function selectproject(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_buildings')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, pro_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectBuilding').html(msg);
   });
   }


      function selectbuilding(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_floors')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, building_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectFloor').html(msg);
   });
   }


    function selectfloor(value)
   {
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var request    = $.ajax({
   url: "{{url('/get_flats')}}",
   method: "post",
   data: {_token: CSRF_TOKEN, building_id:value},
   dataType: "html"
   });
   request.done(function( msg ) {
   //alert(msg);
   $('#selectFlat').html(msg);
   });
   }


     function status_change(id,val)
     {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var request    = $.ajax({
                url: "{{url('/change_status')}}",
                method: "post",
                data: {_token: CSRF_TOKEN, installment_id:id,check_val:val},
                dataType: "html"
                });
                request.done(function( msg ) {
                    location.reload();
                });
     }

     function replacement(id)
     {
         var res = id.split("*");
         $('#installment').val(res[2]);
         $('#c_date').val(res[1]);
         $('#contract_id').val(res[0]);
         $('#check_no').val(res[3]);
         $('#replace_btn').click();
     }

    </script>

@endsection

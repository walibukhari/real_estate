@extends('layouts.app', [
'class' => '',
'elementActive' => 'supplier'
])
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="content">
   @if (session('status'))
   <div class="alert alert-success" role="alert">
      {{ session('status') }}
   </div>
   @endif
   @if (session('password_status'))
   <div class="alert alert-success" role="alert">
      {{ session('password_status') }}
   </div>
   @endif
   @if (Session::has('error'))
   <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{!! Session('error') !!}</strong>
   </div>
   @endif
   @if ($errors->any())
   <div class="alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
   @endif
   <style type="text/css">
      .filelabel {
      width: 100%;
      border: 2px dashed grey;
      border-radius: 5px;
      display: block;
      padding: 5px;
      transition: border 300ms ease;
      cursor: pointer;
      text-align: center;
      margin: 0;
      }
      .filelabel i {
      display: block;
      font-size: 30px;
      padding-bottom: 5px;
      }
      .filelabel i,
      .filelabel .title {
      color: grey;
      transition: 200ms color;
      }
      .filelabel:hover {
      border: 2px solid #1665c4;
      }
      .filelabel:hover i,
      .filelabel:hover .title {
      color: #1665c4;
      }
      #FileInput{
      display:none;
      }
   </style>
   <div class="row">
      <div class="col-12">
         <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-2">
               <!--<button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Create New</button>-->
            </div>
         </div>
         <div class="material-card card">
            <div class="card-body">
               <h4 class="card-title">Supplier/Party</h4>
               <h6 class="card-subtitle">
               </h6>
               <br>
               <form class="form" method="POST" action="{{url('/Admin/Save_supplier')}}" enctype="multipart/form-data">
                  @csrf
                  <div class="container">
                     <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary">Name</label>
                           <input type="text" class="form-control" name="name" placeholder="Supplier Name" />
                        </div>
                        <div class="col-md-6">
                           <label class="text-primary"> Address</label>
                           <input type="text" class="form-control" name="address" placeholder="Enter Address" />
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-md-6">
                           <label class="text-primary"> Contact No.1</label>
                           <input type="number" class="form-control" name="contact1" placeholder="Contact no.1" />
                        </div>
                        <div class="col-md-6">
                           <label class="text-primary"> Contact No.2</label>
                            <input type="number" class="form-control" name="contact2" placeholder="Contact no.1" />
                        </div>
                     </div>
                     <br>
                     <div class="row">
                         <div class="col-md-6">
                             <label class="text-primary">Email 1</label>
                            <input type="email" class="form-control" name="email1" placeholder="Enter Email Address" />
                        </div>
                        <div class="col-md-6">
                             <label class="text-primary">Email 2</label>
                            <input type="email" class="form-control" name="email2" placeholder="Enter Email Address" />
                        </div>
                    </div> 
                    
                    <div class="row">
                         <div class="col-md-6">
                             <label class="text-primary">Po Box </label>
                            <input type="text" class="form-control" name="pobox"  />
                        </div>
                        <div class="col-md-6">
                             <label class="text-primary">Trade License Copy</label>
                            <input type="file" class="form-control" name="license_copy" />
                        </div>
                    </div> 
                    <div class="row">
                         <div class="col-md-6">
                             <label class="text-primary">TRN </label>
                            <input type="text" class="form-control" name="trn" />
                        </div>
                      
                    </div> 
                                
                               
                             
                         </div>
                         
                         
                           
                     </div>
                     
                   


                  <br>
                  
                  <div class="container">
                      <div class="row">
                        <div class="text-center mt-4 col-md-2">
                         <button class="btn btn-started">Save</button>
                        </div>
                        
                      </div>
                  </div>
                  
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

@endsection
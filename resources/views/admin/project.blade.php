@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'add_project'
])

@section('content')
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
                 @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('error') !!}</strong>
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
        <style type="text/css">
            .filelabel {
    width: 100%;
    border: 2px dashed grey;
    border-radius: 5px;
    display: block;
    padding: 5px;
    transition: border 300ms ease;
    cursor: pointer;
    text-align: center;
    margin: 0;
}
.filelabel i {
    display: block;
    font-size: 30px;
    padding-bottom: 5px;
}
.filelabel i,
.filelabel .title {
  color: grey;
  transition: 200ms color;
}
.filelabel:hover {
  border: 2px solid #1665c4;
}
.filelabel:hover i,
.filelabel:hover .title {
  color: #1665c4;
}
#FileInput{
    display:none;
}
        </style>
        <div class="row">
  <div class="col-12">
  <div class="row">
      <div class="col-md-3">

      </div>
       <div class="col-md-3">

      </div>
       <div class="col-md-4">

      </div>
       <div class="col-md-2">
            <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Add Project</button>
      </div>
  </div>
    <div class="material-card card">
      <div class="card-body">

        <h4 class="card-title">Projects List</h4>
        <h6 class="card-subtitle">
        </h6><br>
        <div class="table-responsive">
          <table id="complex_header" class="table table-striped table-bordered display"
          style="width:100%">
          <thead>

            <tr>
              <th>Sr#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
             @foreach($project as $projects)
            <tr>
                <td>{{$counter++}}</td>
                <td>{{$projects->name}}</td>
                <td>Start Date: {{$projects->s_date}} <br>Last Date: {{$projects->e_date}} <br>Location: {{$projects->location}}</td>
                <td>{{$projects->status}}</td>
                <td>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editModal{{$projects->id}}">Eidt </button>
                   <!--  <a href="{{url('/delete_project')}}/{{$projects->id}}" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-xs">Delete</a> -->
                </td>
            </tr>
                   <div class="modal fade" id="editModal{{$projects->id}}"  role="dialog" >
    <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Floor Types</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
                   <div class="p-5">
                     <form class="user" action="{{url('Admin/edit_project')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                   <div class="form-group row">
                       <div class="col-md-12">
                        <label class="text-primary">Name</label>
                        <input type="hidden" value="{{$projects->id}}" name="project_id">
                        <input type="text"  class="form-control" value="{{$projects->name}}"  name="name" required="">
                       </div>
                  </div>
                  <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">Start Data</label>
                        <input type="date" value="{{$projects->s_date}}"  class="form-control"  name="sdate"  required="">
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">End Data</label>
                        <input type="date"  class="form-control" value="{{$projects->e_date}}"   name="edate" value="" required="">
                       </div>
                  </div>
                  <div class="form-group row">
                       <div class="col-md-12">
                        <label class="text-primary">Location</label>
                        <input type="text" value="{{$projects->location}}"  class="form-control"  name="location" value="" required="">
                       </div>
                  </div>
                  <div class=" row">
                       <div class="col-md-6">
                        <label class="text-primary">Manager</label>
                        <select class="form-control" name="manager_id" required="">
                           <option value="" selected disabled>Select Manager</option>
                           @foreach($g_manager as $g_managers)
                           <option @if($g_managers->id==$projects->manager_id) selected @endif value="{{$g_managers->id}}">{{$g_managers->name}}</option>
                           @endforeach                   </select>
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">Files</label>
                       <input type="file"   name="document"  onclick="remove_doc()" value="{{isset($projects->Projectfile) ? $projects->Projectfile->file_link : ''}}" >
                       <input type="hidden" name="old_doc" value="{{isset($projects->Projectfile) ? $projects->Projectfile->file_link : ''}}">
                       </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                       <div class="form-group">
                    <label class="text-primary">Status</label>
                   <select class="form-control" name="status">
                      <option value="" disabled selected>Select Status</option>
                      @if($projects->status=='Pending')
                      <option selected value="Pending">Pending</option>
                      <option  value="Approved">Approved</option>
                      <option  value="Disapproved">Disapproved</option>
                      @endif
                      @if($projects->status=='Approved')
                      <option selected value="Approved">Approved</option>
                      <option  value="Disapproved">Disapproved</option>
                      <option  value="Pending">Pending</option>

                      @endif
                        @if($projects->status=='Disapproved')
                      <option selected value="Disapproved">Disapproved</option>
                      <option  value="Pending">Pending</option>
                      <option  value="Approved">Approved</option>
                      @endif
                   </select>
                    </div>
                    </div>
                  </div>

                <button  type="submit" name="createNewProject" class="btn btn-primary btn-user btn-block">
                 Update
               </button>
              </form>
           </div>

        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
            @endforeach
      </tbody>
      <tfoot>
        <tr>
              <th>Sr#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
      </tfoot>
     </table>
    </div>
   </div>
  </div>
 </div>
</div>
                      <div class="modal fade" id="myModal" role="dialog" >
    <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create New</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
                   <div class="p-5">
                   <form class="user" action="{{url('Admin/Add_project')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                   <div class="form-group row">
                       <div class="col-md-12">
                        <label class="text-primary">Name</label>
                        <input type="text"  class="form-control"  name="name" required="">
                       </div>
                  </div>
                  <div class="form-group row">
                       <div class="col-md-6">
                        <label class="text-primary">Start Data</label>
                        <input type="date"  class="form-control"  name="sdate" value="" required="">
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">End Data</label>
                        <input type="date"  class="form-control"  name="edate" value="" required="">
                       </div>
                  </div>
                  <div class="form-group row">
                       <div class="col-md-12">
                        <label class="text-primary">Location</label>
                        <input type="text"  class="form-control"  name="location" value="" required="">
                       </div>
                  </div>
                  <div class=" row">
                       <div class="col-md-6">
                        <label class="text-primary">Manager</label>
                        <select class="form-control" name="manager_id" required="">
                           <option value="" disabled selected>Select Manager</option>
                            @foreach($g_manager as $g_managers)
                           <option value="{{$g_managers->id}}">{{$g_managers->name}}</option>
                           @endforeach                               </select>
                       </div>
                       <div class="col-md-6">
                        <label class="text-primary">Files</label>
                       <input type="file"   name="document"    required="">
                       </div>
                  </div>

                <button  type="submit" name="createNewProject" class="btn btn-primary btn-user btn-block">
                 Create
               </button>
              </form>
           </div>

        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>






    </div>
     <input type="hidden" name="rem_doc" value="1" id="rem_doc">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
    $("#FileInput").on('change',function (e) {
            var labelVal = $(".title").text();
            var oldfileName = $(this).val();
                fileName = e.target.value.split( '\\' ).pop();

                if (oldfileName == fileName) {return false;}
                var extension = fileName.split('.').pop();

            if ($.inArray(extension,['jpg','jpeg','png']) >= 0) {
                $(".filelabel i").removeClass().addClass('fa fa-file-image-o');
                $(".filelabel i, .filelabel .title").css({'color':'#208440'});
                $(".filelabel").css({'border':' 2px solid #208440'});
            }
            else if(extension == 'pdf'){
                $(".filelabel i").removeClass().addClass('fa fa-file-pdf-o');
                $(".filelabel i, .filelabel .title").css({'color':'red'});
                $(".filelabel").css({'border':' 2px solid red'});

            }
  else if(extension == 'doc' || extension == 'docx'){
            $(".filelabel i").removeClass().addClass('fa fa-file-word-o');
            $(".filelabel i, .filelabel .title").css({'color':'#2388df'});
            $(".filelabel").css({'border':' 2px solid #2388df'});
        }
            else{
                $(".filelabel i").removeClass().addClass('fa fa-file-o');
                $(".filelabel i, .filelabel .title").css({'color':'black'});
                $(".filelabel").css({'border':' 2px solid black'});
            }

            if(fileName ){
                if (fileName.length > 10){
                    $(".filelabel .title").text(fileName.slice(0,4)+'...'+extension);
                }
                else{
                    $(".filelabel .title").text(fileName);
                }
            }
            else{
                $(".filelabel .title").text(labelVal);
            }
        });
</script>
 <script type="">
        function remove_doc()
    {
        $('#rem_doc').val("0");
    }
 </script>
@endsection

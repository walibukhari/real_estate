@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'nature_of_maintenance'
])

@section('content')
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
             @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('error') !!}</strong>
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
        <style type="text/css">
            .filelabel {
    width: 100%;
    border: 2px dashed grey;
    border-radius: 5px;
    display: block;
    padding: 5px;
    transition: border 300ms ease;
    cursor: pointer;
    text-align: center;
    margin: 0;
}
.filelabel i {
    display: block;
    font-size: 30px;
    padding-bottom: 5px;
}
.filelabel i,
.filelabel .title {
  color: grey;
  transition: 200ms color;
}
.filelabel:hover {
  border: 2px solid #1665c4;
}
.filelabel:hover i,
.filelabel:hover .title {
  color: #1665c4;
}
#FileInput{
    display:none;
}
        </style>
            <div class="row">
  <div class="col-12">    
  <div class="row">
      <div class="col-md-3">
          
      </div>
       <div class="col-md-3">
          
      </div>
       <div class="col-md-4">
          
      </div>
       <div class="col-md-2">
            <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Create New</button>
      </div>
  </div>     
    <div class="material-card card">
      <div class="card-body">

        <h4 class="card-title">Questions  List</h4>
        <h6 class="card-subtitle">
        </h6><br>
        <div class="table-responsive">
          <table id="complex_header" class="table table-striped table-bordered display"
          style="width:100%">
          <thead>

            <tr>
              <th>Sr#</th>
              <th>Maintenance Head Name</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </thead>
         <tbody>
             @foreach($question as $questions)
             <tr>
                 <td>{{$counter++}}</td>
                 <td>@if($questions->MaintenanceHeadName){{$questions->MaintenanceHeadName->name}}@endif</td>
                 <td>{{$questions->question}}</td>
                 <td>
                     <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editModal{{$questions->id}}">Edit </button>
                 </td>
             </tr>
             @endforeach
         </tbody>
      <tfoot>
       <tr>
              <th>Sr#</th>
              <th>Maintenance Head Name</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
      </tfoot>
     </table>
    </div>
   </div>
  </div>
 </div>
</div>
  <div class="modal fade" id="myModal"  role="dialog" >
    <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create New</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
                   <div class="p-5">
                  <form action="{{url('Admin/save_questions')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                         <input type="hidden"  class="form-control"  name="check" value="0" >
                         <div class="row">
                             <div class="col-md-12">
                                 <label class="text-primary">Select Maintenance Head</label>
                                  <select class="form-control" name="heads" required>
                        <option value="" selected disabled>Select One</option>
                        @foreach($head as $heads)
                        <option value="{{$heads->id}}" >{{$heads->name}}</option>
                        @endforeach
                        
                    </select>
                </div>
                         </div><br>
                 <div class="row">
                     <div class="col-md-12">
                        <div style="border: 1px solid grey;">
                           <center>
                              <input id='add-row' class='btn btn-primary' type='button' value='Add More' />
                           </center>
                           <table id="test-table" class="table table-condensed">
                              <thead>
                                 <tr>
                                  <th>Sr#</th>
                                    <th>Question</th>
                                    <th>Answers</th>
                                 </tr>
                              </thead>
                              <tbody id="test-body">
                                 <tr id="row0">
                                    <td><input type="number" value="1"  style="width:50px" class="form-control" readonly /></td>
                                    <td>
                                       <textarea name="question[]"  required class="form-control ">
                                       </textarea>
                                    </td>
                                     <td>
                                             <div class="row">
                     <div class="col-md-12">
                        <div style="border: 1px solid grey;">
                           <center>
                              <input id='add-row1' class='btn btn-primary' type='button' value='Add More' />
                           </center>
                           <table id="test-table" class="table table-condensed">
                              <thead>
                                 <tr>
                                  
                                  
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody id="test-body1">
                                 <tr id="row0">
                                    
                                  
                                     <td>
                                         <textarea  class="form-control" name="answer[]"></textarea>
                                     </td>
                                    <td>
                                       <input class='delete-row1 btn btn-primary' type='button' value='Delete' />
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                                     </td>
                                    <td>
                                       <input class='delete-row btn btn-primary' type='button' value='Delete' />
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  
                  
                    <br>
                <button  type="submit" name="createBuidlingType" class="btn btn-primary btn-user btn-block">
                 Create
               </button>
              </form>
           </div>
        
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>

    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
    $("#FileInput").on('change',function (e) {
            var labelVal = $(".title").text();
            var oldfileName = $(this).val();
                fileName = e.target.value.split( '\\' ).pop();

                if (oldfileName == fileName) {return false;}
                var extension = fileName.split('.').pop();

            if ($.inArray(extension,['jpg','jpeg','png']) >= 0) {
                $(".filelabel i").removeClass().addClass('fa fa-file-image-o');
                $(".filelabel i, .filelabel .title").css({'color':'#208440'});
                $(".filelabel").css({'border':' 2px solid #208440'});
            }
            else if(extension == 'pdf'){
                $(".filelabel i").removeClass().addClass('fa fa-file-pdf-o');
                $(".filelabel i, .filelabel .title").css({'color':'red'});
                $(".filelabel").css({'border':' 2px solid red'});

            }
  else if(extension == 'doc' || extension == 'docx'){
            $(".filelabel i").removeClass().addClass('fa fa-file-word-o');
            $(".filelabel i, .filelabel .title").css({'color':'#2388df'});
            $(".filelabel").css({'border':' 2px solid #2388df'});
        }
            else{
                $(".filelabel i").removeClass().addClass('fa fa-file-o');
                $(".filelabel i, .filelabel .title").css({'color':'black'});
                $(".filelabel").css({'border':' 2px solid black'});
            }

            if(fileName ){
                if (fileName.length > 10){
                    $(".filelabel .title").text(fileName.slice(0,4)+'...'+extension);
                }
                else{
                    $(".filelabel .title").text(fileName);
                }
            }
            else{
                $(".filelabel .title").text(labelVal);
            }
        });
        
        
        
            var row=1;
   $(document).on("click", "#add-row", function () {
       var new_row = '<tr id="row' + row + '"><td><input type="number" style="width:50px" class="form-control" value="0" readonly /></td><td><textarea name="question[]" required  class="form-control" ></textarea></td><td>'+
                                             '<div class="row">'+
                     '<div class="col-md-12">'+
                        '<div style="border: 1px solid grey;">'+
                           '<center>'+
                              '<input id="add-row1" class="btn btn-primary" type="button" value="Add More" />'+
                           '</center>'+
                           '<table id="test-table" class="table table-condensed">'+
                              '<thead>'+
                                 '<tr>'+
                                    '<th></th>'+
                                 '</tr>'+
                              '</thead>'+
                             '<tbody id="test-body1">'+
                                 '<tr id="row0">'+
                                     '<td>'+
                                         '<textarea  class="form-control" name="answer[]"></textarea>'+
                                     '</td>'+
                                    '<td>'+
                                       '<input class="delete-row1 btn btn-primary" type="button" value="Delete" />'+
                                    '</td>'+
                                 '</tr>'+
                              '</tbody>'+
                           '</table>'+
                        '</div>'+
                     '</div>'+
                  '</div>'+
                '</td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';
   
       $('#test-body').append(new_row);
       row++;
       return false;
   });
   
   
   
   
   $(document).on("click", "#add-row1", function () {
       var new_row = '<tr id="row' + row + '"><td><textarea  class="form-control" name="answer[]"></textarea></td><td><input class="delete-row1 btn btn-primary" type="button" value="Delete" /></td></tr>';
   
       $('#test-body1').append(new_row);
       row++;
       return false;
   });
   
   
        
     // Remove criterion
   $(document).on("click", ".delete-row", function () {
       //  alert("deleting row#"+row);
       if(row>1) {
           $(this).closest('tr').remove();
           row--;
       }
       return false;
   });  
   
   
   
   
    $(document).on("click", ".delete-row1", function () {
       //  alert("deleting row#"+row);
       if(row>1) {
           $(this).closest('tr').remove();
           row--;
       }
       return false;
   });  
        
</script>

@endsection

@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'admin'
])

@section('content')
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
          @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('error') !!}</strong>
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
        <style type="text/css">
            .filelabel {
    width: 100%;
    border: 2px dashed grey;
    border-radius: 5px;
    display: block;
    padding: 5px;
    transition: border 300ms ease;
    cursor: pointer;
    text-align: center;
    margin: 0;
}
.filelabel i {
    display: block;
    font-size: 30px;
    padding-bottom: 5px;
}
.filelabel i,
.filelabel .title {
  color: grey;
  transition: 200ms color;
}
.filelabel:hover {
  border: 2px solid #1665c4;
}
.filelabel:hover i,
.filelabel:hover .title {
  color: #1665c4;
}
#FileInput{
    display:none;
}
        </style>
            <div class="row">
  <div class="col-12">    
  <div class="row">
      <div class="col-md-3">
          
      </div>
       <div class="col-md-3">
          
      </div>
       <div class="col-md-4">
          
      </div>
       <div class="col-md-2">
            <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Create New</button>
      </div>
  </div>     
    <div class="material-card card">
      <div class="card-body">

        <h4 class="card-title">Users List</h4>
        <h6 class="card-subtitle">
        </h6><br>
        <div class="table-responsive">
          <table id="complex_header" class="table table-striped table-bordered display"
          style="width:100%">
          <thead>

            <tr>
              <th>Sr#</th>
             <!--  <th>Project Name</th>
              <th>Manager Name</th> -->
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Role</th>
              <!-- <th>Image</th>
              <th>No. of Floors</th>
              <th>No. of Flats</th>
              <th>Flat Type</th>
              <th>Parking</th> -->
              <th>Status</th>
              <th>Operations</th>
            </tr>
          </thead>
          <tbody>
          <tr>
            @foreach($admin as $admins)
              <td>{{$counter++}}</td>
             
              <td>{{$admins->name}}</td>
              <td>{{$admins->phone}}</td>
             <td>{{$admins->email}}</td>
             <td>{{$admins->role}}</td>
              <td>{{$admins->status}}</td>
              <td>
                  <a href="#" class="btn btn-danger">Delete</a>
              </td>
          </tr>
          @endforeach
      </tbody>
      <tfoot>
         <tr>
              <th>Sr#</th>
             <!--  <th>Project Name</th>
              <th>Manager Name</th> -->
              <th>Building Name</th>
              <th>Address</th>
             <!--  <th>Image</th>
              <th>No. of Floors</th>
              <th>No. of Flats</th>
              <th>Flat Type</th>
              <th>Parking</th> -->
              <th>Status</th>
              <th>Operations</th>
            </tr>
      </tfoot>
     </table>
    </div>
   </div>
  </div>
 </div>
</div>
 <div class="modal fade" id="myModal"  role="dialog" >
    <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create New</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
                     <div class="p-5">
                    <form action="{{url('Admin/save_admins')}}" method="POST" enctype="multipart/form-data">
                      @csrf
                    <div class="form-group">
                    <label class="text-primary">Role</label>
                    <select class="form-control" required="" name="role">
                        <option value="" selected disabled>Select Role</option>
                        <option value="Super Admin">Super Admin</option>
                        <option value="Admin">Admin</option>
                        <option value="General Manager">General Manager</option>
                        <option value="Agent">Agent</option>
                        <option value="Owner">Owner</option>
                        <option value="Tenant">Tenant</option>
                        <option value="Accountant">Accountant</option>
                        
                    </select>
                    </div>    
                   <div class="form-group">
                    <label class="text-primary">Name</label>
                    <input type="text"  class="form-control"  name="name" value="" required="">
                    </div>
                   <div class="form-group">
                    <label class="text-primary">Phone</label>
                    <input type="text" class="form-control"  name="phone" required="">
                    </div>
                    <div class="form-group">
                    <label class="text-primary">Email</label>
                    <input type="email" class="form-control"  name="email" required="">
                    </div>
                     <div class="form-group">
                    <label class="text-primary">Password</label>
                    <input type="password" minlength="8" class="form-control"  name="pass" required="">
                    </div>
                <button  type="submit" name="createNewUser" class="btn btn-primary btn-user btn-block">
                 Create
               </button>
              </form>
           </div>
        
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>



    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
    $("#FileInput").on('change',function (e) {
            var labelVal = $(".title").text();
            var oldfileName = $(this).val();
                fileName = e.target.value.split( '\\' ).pop();

                if (oldfileName == fileName) {return false;}
                var extension = fileName.split('.').pop();

            if ($.inArray(extension,['jpg','jpeg','png']) >= 0) {
                $(".filelabel i").removeClass().addClass('fa fa-file-image-o');
                $(".filelabel i, .filelabel .title").css({'color':'#208440'});
                $(".filelabel").css({'border':' 2px solid #208440'});
            }
            else if(extension == 'pdf'){
                $(".filelabel i").removeClass().addClass('fa fa-file-pdf-o');
                $(".filelabel i, .filelabel .title").css({'color':'red'});
                $(".filelabel").css({'border':' 2px solid red'});

            }
  else if(extension == 'doc' || extension == 'docx'){
            $(".filelabel i").removeClass().addClass('fa fa-file-word-o');
            $(".filelabel i, .filelabel .title").css({'color':'#2388df'});
            $(".filelabel").css({'border':' 2px solid #2388df'});
        }
            else{
                $(".filelabel i").removeClass().addClass('fa fa-file-o');
                $(".filelabel i, .filelabel .title").css({'color':'black'});
                $(".filelabel").css({'border':' 2px solid black'});
            }

            if(fileName ){
                if (fileName.length > 10){
                    $(".filelabel .title").text(fileName.slice(0,4)+'...'+extension);
                }
                else{
                    $(".filelabel .title").text(fileName);
                }
            }
            else{
                $(".filelabel .title").text(labelVal);
            }
        });
</script>

@endsection

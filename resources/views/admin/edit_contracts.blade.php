@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'add_building_facilties'
])

@section('content')
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('error') !!}</strong>
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <style type="text/css">
            .filelabel {
                width: 100%;
                border: 2px dashed grey;
                border-radius: 5px;
                display: block;
                padding: 5px;
                transition: border 300ms ease;
                cursor: pointer;
                text-align: center;
                margin: 0;
            }
            .filelabel i {
                display: block;
                font-size: 30px;
                padding-bottom: 5px;
            }
            .filelabel i,
            .filelabel .title {
                color: grey;
                transition: 200ms color;
            }
            .filelabel:hover {
                border: 2px solid #1665c4;
            }
            .filelabel:hover i,
            .filelabel:hover .title {
                color: #1665c4;
            }
            #FileInput{
                display:none;
            }
        </style>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Create New</button>
                    </div>
                </div>
                <div class="material-card card">
                    <div class="card-body">

                        <h4 class="card-title">Contracts  List</h4>
                        <h6 class="card-subtitle">
                        </h6><br>
                        <form class="form" method="POST" action="{{url('/Admin/Save_Contracts')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="text-primary">Select Project</label>
                                        <select class="form-control" name="project" id="selectProject">
                                                <option>Select project</option>
                                            @foreach($projects as $pro)
                                                <option value="{{$pro->id}}" @if($pro->id == (int)$contract->project) selected @endif>{{$pro->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="text-primary">Select Building</label>
                                        <select class="form-control" name="building" id="selectBuilding">
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="text-primary">Select Floor</label>
                                        <select class="form-control" name="floor" id="selectFloor">
                                        </select>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Select Flat</label>
                                        <select class="form-control" name="flat" id="selectFlat">
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-primary">Select Tenant</label>
                                        <select class="form-control" name="tanent">
                                            <option>Select Tenant</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="text-primary">Security Deposite<small>(Refundable)</small></label>
                                        <input type="number" class="form-control" name="security_deposite" placeholder="Security Deposite">
                                    </div>

                                </div><br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Contract Start</label>
                                        <input type="date" class="form-control" name="contract_start" placeholder="Flat Security">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-primary">Contract End</label>
                                        <input type="date" class="form-control" name="contract_end" placeholder="Flat Security">
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Total Contract Amount</label>
                                        <input type="number" id="totalAmount" name="totalAmount" class="form-control" placeholder="Total Contract Amount">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-primary">No of Installments </label>
                                        <input type="number" id="no_installments" name="no_installments" class="form-control" placeholder="No of Installments">
                                    </div>
                                </div><br>

                                <div class="row" id="rowTableDisplay" style="display: none;">
                                    <div class="table-responsive">
                                        <table class="table table-borderless">
                                            <thead>
                                            <tr>
                                                <th>Installment No</th>
                                                <th>Particular Installment Amount</th>
                                                <th>Check No</th>
                                                <th>Check Date</th>
                                                <th>Check Issue From Bank</th>
                                                <th>Party Name</th>
                                            </tr>
                                            </thead>
                                            <tbody id="rowTable">

                                            </tbody>
                                        </table>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Terms and conditions</label>
                                        <textarea class="form-control" name="terms_conditions" placeholder="Terms and conditions"></textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-primary">Renew Terms and conditions</label>
                                        <textarea class="form-control" name="renew_terms_conditions" placeholder="Renew Terms and conditions"></textarea>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-primary">Manual written performa of contract and Cheques scaned copy</label>
                                        <input type="file" class="form-control" name="scanned_doc">
                                    </div>
                                </div><br><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="border: 1px solid grey;">
                                            <center>
                                                <input id='add-row' class='btn btn-primary' type='button' value='Add More' /></center>
                                            <table id="test-table" class="table table-condensed">
                                                <thead>
                                                <tr>
                                                    <th>Attachments</th>
                                                    <th>Comment</th>
                                                </tr>
                                                </thead>
                                                <tbody id="test-body">
                                                <tr id="row0">
                                                    <td>
                                                        <input name='from_value[]'  type='file' class='form-control' />
                                                    </td>
                                                    <td>
                                                        <input name='to_value[]'  placeholder="Comment" type='text' class='form-control input-md' />
                                                    </td>
                                                    <td>
                                                        <input class='delete-row btn btn-primary' type='button' value='Delete' />
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                                <div class="text-center mt-4">
                                    <button class="btn btn-started"> Save</button>
                                </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        let url = '/get/buildings';
        let url1 = '/get/floors';
        let url2 = '/get/flats';

       $(document).ready(function (){
            $(function(){
                $('#selectProject').trigger('change'); //This event will fire the change event.
                $('#selectProject').change(function(){
                    var data= $(this).val();
                    alert(data);
                });
            });
       });



        $(document).on("change" , "#selectProject" , function () {
            let value = $(this).val();
            console.log('value');
            console.log(value);
            $('#selectBuilding').html('');
            $.ajax({
                url:url+'/'+value,
                Type:'GET',
                success: function (response) {
                    if(response.status == true) {
                        let html = '<option>Select Building</option>';
                        $('#selectBuilding').append(html);
                        response.data.map((d) => {
                            html = '<option value="'+ d.id +'">'+ d.name +'</option>';
                            $('#selectBuilding').append(html);
                        });
                    }
                },
                error: function (error) {
                    console.log(error);
                    console.log(error);
                }
            })
        });

        $(document).on("change" , "#selectBuilding" , function () {
            let value = $(this).val();
            console.log('value');
            console.log(value);
            $('#selectFloor').html('');
            $.ajax({
                url:url1+'/'+value,
                Type:'GET',
                success: function (response) {
                    if(response.status == true) {
                        let html = '<option>Select Floor</option>';
                        $('#selectFloor').append(html);
                        response.data.map((d) => {
                            html = '<option value="'+ d.building_id +'">'+ d.name +'</option>';
                            $('#selectFloor').append(html);
                        });
                    }
                },
                error: function (error) {
                    console.log(error);
                    console.log(error);
                }
            })
        });

        $(document).on("change" , "#selectFloor" , function () {
            let value = $(this).val();
            console.log('value');
            console.log(value);
            $('#selectFlat').html('');
            $.ajax({
                url:url2+'/'+value,
                Type:'GET',
                success: function (response) {
                    if(response.status == true) {
                        let html = '<option>Select Flat</option>';
                        $('#selectFlat').append(html);
                        response.data.map((d) => {
                            html = '<option value="'+ d.building_id +'">'+ d.unit_code +'</option>';
                            $('#selectFlat').append(html);
                        });
                    }
                },
                error: function (error) {
                    console.log(error);
                    console.log(error);
                }
            })
        });


        $(document).on("change" , "#no_installments" , function () {
            $('#rowTable').html('');
            let no_installments = $(this).val();
            let totalAmount = $('#totalAmount').val();
            console.log('value');
            console.log(no_installments);
            console.log(totalAmount);
            let totalA = totalAmount/no_installments;
            for (var i = 1; i <= no_installments; i++) {
                console.log('count iiiii');
                console.log(i);
                let Tblhtml = '<tr>' +
                    '<td><input type="number" name="no_installment[]" value="'+ i +'" /></td>' +
                    '<td><input type="number" name="amount[]" value="'+ totalA +'" /></td>' +
                    '<td><input type="number" name="check_no[]" /></td>' +
                    '<td><input type="date" name="check_date[]" /></td>' +
                    '<td><input type="text" name="check_issue[]" /></td>' +
                    '<td><input type="text" name="party_name[]" /></td>' +
                    '<td>' +
                        '<select id="selectOpt" name="selectOpt[]">' +
                            '<option>select option</option>' +
                            '<option>company</option>' +
                            '<option>landlord</option>' +
                        '</select>' +
                    '</td>' +
                    '</tr>';
                $('#rowTable').append(Tblhtml);
            }
            $('#rowTableDisplay').show();

        });


    </script>
    <script type="text/javascript">
        // Add row
        var row=1;
        $(document).on("click", "#add-row", function () {
            var new_row = '<tr id="row' + row + '"><td><input name="from_value[]" type="file" class="form-control" /></td><td><input name="to_value[]" type="text" placeholder="Comment" class="form-control" /></td><td><input class="delete-row btn btn-primary" type="button" value="Delete" /></td></tr>';

            $('#test-body').append(new_row);
            row++;
            return false;
        });

        // Remove criterion
        $(document).on("click", ".delete-row", function () {
            //  alert("deleting row#"+row);
            if(row>1) {
                $(this).closest('tr').remove();
                row--;
            }
            return false;
        });

    </script>

@endsection

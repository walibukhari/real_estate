<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <title>Login</title>


    <style>
        .main-section {
            background-image: url(https://image.freepik.com/free-vector/abstract-dark-halftone-background-design_1017-15505.jpg);
            background-repeat: no-repeat;
            width: 100%;
            height: 100vh;
            background-size: 100% 100% !important;
        }

        .input-style {
            height: 42px;
            border-left: 0 !important;
            padding-left: 0 !important;

        }

        .icon-style {
            height: 42px;
            border-right: 0 !important;
        }

        .input-style:focus {
            outline: none;
            box-shadow: none;
            border-color: #ced4da;
        }

        .btn-started {
            background-color: #50bcd9;
            padding: 8px 18px;
            color: white;
            border: 1px solid #50bcd9;
            border-radius: 25px;
            font-size: 0.9em;
            font-weight: bold;
            text-transform: uppercase;
        }
        @media (max-width: 780px){
            .main-section {
            background-image: url(https://image.freepik.com/free-vector/abstract-dark-halftone-background-design_1017-15505.jpg);
            background-repeat: no-repeat;
            width: 100%;
            height: auto;
            background-size: 100% 100% !important;
        }
        }
    </style>
</head>


<body class=" main-section">
    <section class="mt-5">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-md-2">
                    
                </div>
                <div class="col-md-4">
                    <div class="card mb-5" style="border-radius: 10px;">
                        <div class="card-body">
                            <div class="row justify-content-end
                            " >
                                <div class="col-5 pr-0">
                                    <h6 class="mt-1" style="font-size: 0.8em;" id="admint">Switch to Admin</h6>
                                    <h6 class="mt-1" style="font-size: 0.8em; display: none;" id="usert" >Switch to User</h6>

                                </div>
                                <div class="col-2 pl-0"><span class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="customSwitches">
                                    <label class="custom-control-label" for="customSwitches"></label>
                                </span></div>
                            </div>
                            <div id="user">
                                <h4 class="text-center mt-4">Register </h4>
                                <div class="text-center mt-4">
                                    <a href="#"><img src="https://www.flaticon.com/svg/static/icons/svg/179/179319.svg"
                                            class="img-fluid" width="40"></a>
                                    <a href="#" class="ml-2"><img
                                            src="https://www.flaticon.com/svg/static/icons/svg/179/179326.svg"
                                            class="img-fluid" width="40"> </a>

                                    <a href="#" class="ml-2"><img
                                            src="https://www.flaticon.com/svg/static/icons/svg/179/179342.svg"
                                            class="img-fluid" width="40"> </a>
                                </div>
                                <h6 class="text-muted text-center mt-2">or be classical</h6>
                                <form>
                                    <div class="input-group mb-3 mt-4">
                                        <div class="input-group-prepend ">
                                            <span class="input-group-text bg-white icon-style" id="basic-name"><i
                                                    class="far fa-user "></i></span>
                                        </div>
                                        <input type="text" class="form-control input-style" placeholder="Name"
                                            aria-label="Name">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend ">
                                            <span class="input-group-text bg-white icon-style" id="basic-email"><i
                                                    class="far fa-envelope mt-1"></i></span>
                                        </div>
                                        <input type="email" class="form-control input-style" placeholder="Email"
                                            aria-label="Username">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend ">
                                            <span class="input-group-text bg-white icon-style" id="basic-pass"><i
                                                    class="fas fa-key mt-1"></i></span>
                                        </div>
                                        <input type="password" class="form-control input-style" placeholder="Password"
                                            aria-label="Password">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend ">
                                            <span class="input-group-text bg-white icon-style" id="basic-Cpass"><i
                                                    class="fas fa-key mt-1"></i></span>
                                        </div>
                                        <input type="password" class="form-control input-style" placeholder="Confirm Password"
                                            aria-label="Password">
                                    </div>
                                    <div class="form-check ml-1">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck1"
                                            style="margin-top: 0.4rem;">
                                        <label class="form-check-label" for="exampleCheck1">I agree the <a
                                                href="#">terms and conditions</a> </label>
                                    </div>
                                    <div class="text-center mt-4">
                                        <button class="btn btn-started"> Get Started</button>
                                    </div>
                                </form>
                            </div>
                            <div id="admin" style="display: none;">
                                <h4 class="text-center mt-4"> Admin Register </h4>
                                <div class="text-center mt-4">
                                    <a href="#"><img src="https://www.flaticon.com/svg/static/icons/svg/179/179319.svg"
                                            class="img-fluid" width="40"></a>
                                    <a href="#" class="ml-2"><img
                                            src="https://www.flaticon.com/svg/static/icons/svg/179/179326.svg"
                                            class="img-fluid" width="40"> </a>

                                    <a href="#" class="ml-2"><img
                                            src="https://www.flaticon.com/svg/static/icons/svg/179/179342.svg"
                                            class="img-fluid" width="40"> </a>
                                </div>
                                <h6 class="text-muted text-center mt-2">or be classical</h6>
                                <form>
                                    <div class="input-group mb-3 mt-4">
                                        <div class="input-group-prepend ">
                                            <span class="input-group-text bg-white icon-style" id="basic-name"><i
                                                    class="far fa-user "></i></span>
                                        </div>
                                        <input type="text" class="form-control input-style" placeholder="Username"
                                            aria-label="Name">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend ">
                                            <span class="input-group-text bg-white icon-style" id="basic-email"><i
                                                    class="far fa-envelope mt-1"></i></span>
                                        </div>
                                        <input type="email" class="form-control input-style" placeholder="Email"
                                            aria-label="Username">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend ">
                                            <span class="input-group-text bg-white icon-style" id="basic-pass"><i
                                                    class="fas fa-key mt-1"></i></span>
                                        </div>
                                        <input type="password" class="form-control input-style" placeholder="Password"
                                            aria-label="Password">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend ">
                                            <span class="input-group-text bg-white icon-style" id="basic-Cpass"><i
                                                    class="fas fa-key mt-1"></i></span>
                                        </div>
                                        <input type="password" class="form-control input-style" placeholder="Password"
                                            aria-label="Password">
                                    </div>
                                    <div class="form-check ml-1">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck1"
                                            style="margin-top: 0.4rem;">
                                        <label class="form-check-label" for="exampleCheck1">I agree the <a
                                                href="#">terms and conditions</a> </label>
                                    </div>
                                    <div class="text-center mt-4">
                                        <button class="btn btn-started"> Get Started</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
            $('#customSwitches').click(function () {
                if ($(this).prop("checked") == true) {
                    document.getElementById('user').style.display = 'none';
                    document.getElementById('admin').style.display = 'block';
                    document.getElementById('admint').style.display = 'none';
                    document.getElementById('usert').style.display = 'block';



                }
                else if ($(this).prop("checked") == false) {
                    document.getElementById('user').style.display = 'block';
                    document.getElementById('admin').style.display = 'none'; 
                    document.getElementById('admint').style.display = 'block';
                    document.getElementById('usert').style.display = 'none';               }
            });
        });
    </script>


</body>

</html>
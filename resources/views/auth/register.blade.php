<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <title>Login</title>


    <style>
        .main-section {
            background-image: url(https://image.freepik.com/free-vector/abstract-dark-halftone-background-design_1017-15505.jpg);
            background-repeat: no-repeat;
            width: 100%;
         /*   height: 100vh;*/
            background-repeat: all;
            background-size: 100% 100% !important;
        }
        div.ex3 {
 /* background-color: lightblue;*/
  /*//width: 110px;*/
height: 59%;
overflow: auto;
}

        .input-style {
            height: 42px;
            border-left: 0 !important;
            padding-left: 0 !important;

        }

        .icon-style {
            height: 42px;
            border-right: 0 !important;
        }

        .input-style:focus {
            outline: none;
            box-shadow: none;
            border-color: #ced4da;
        }

        .btn-started {
            background-color: #50bcd9;
            padding: 8px 18px;
            color: white;
            border: 1px solid #50bcd9;
            border-radius: 25px;
            font-size: 0.9em;
            font-weight: bold;
            text-transform: uppercase;
        }
        @media (max-width: 780px){
            .main-section {
            background-image: url(https://image.freepik.com/free-vector/abstract-dark-halftone-background-design_1017-15505.jpg);
            background-repeat: no-repeat;
            width: 100%;
            height: auto;
            background-size: 100% 100% !important;
        }
        }
    </style>
</head>


<body class=" main-section">
    <section class="mt-5">
        <div class="container">
            <div class="row justify-content-center">
                
                <div class="col-md-12">
                    <div class="card mb-5 " style="border-radius: 10px;width: 100%">
                        <div class="card-body">
                            <div class="row justify-content-end
                            " >
                                <div class="col-5 pr-0">
                                    <h6 class="mt-1" style="font-size: 0.8em;" id="admint">Switch to Landlord</h6>
                                    <h6 class="mt-1" style="font-size: 0.8em; display: none;" id="usert" >Switch to Tenants</h6>

                                </div>
                                <div class="col-2 pl-0"><span class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="customSwitches">
                                    <label class="custom-control-label" for="customSwitches"></label>
                                </span></div>
                            </div>
                            <div id="user">
                                <h4 class="text-center mt-4">Tenants Register </h4>
                                <div class="text-center mt-4">
                                    <a href="#"><img src="https://www.flaticon.com/svg/static/icons/svg/179/179319.svg"
                                            class="img-fluid" width="40"></a>
                                    <a href="#" class="ml-2"><img
                                            src="https://www.flaticon.com/svg/static/icons/svg/179/179326.svg"
                                            class="img-fluid" width="40"> </a>

                                    <a href="#" class="ml-2"><img
                                            src="https://www.flaticon.com/svg/static/icons/svg/179/179342.svg"
                                            class="img-fluid" width="40"> </a>
                                </div>
                                <h6 class="text-muted text-center mt-2">or be classical</h6>
                                <form class="form" method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="container">
                                    <div class="row">
                                             <div class="col-md-4 ">
                                        <label class="text-primary">Initial Occupation Date</label>    
                                      <input type="date" value="{{ old('initial_occupation_date') }}"  name="initial_occupation_date" class="form-control">
                                    </div>
                                           <div class="col-md-4 ">
                                        <label class="text-primary">Title</label>    
                                        <select class="form-control"  name="title">
                                            <option value=""  >Select One</option>
                                            <option value="Mr">Mr</option>
                                            <option value="Mrs">Mrs</option>
                                        </select>
                                    </div> 
                                      <div class=" col-md-4 ">
                                        <label class="text-primary">Ref No</label>
                                        <input type="text" name="ref_no" class="form-control " value="{{ old('ref_no') }}" placeholder="Ref no"
                                            aria-label="Name">
                                            
                                    </div>
                                     
                                   
                                  
                                    </div>

                                     <div class="row">
                                        <div class="col-md-4{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <label class="text-primary">Name</label>
                                    <div class="input-group-prepend">
                                   <!--      <span class="input-group-text">
                                            <i class="nc-icon nc-single-02"></i>
                                        </span> -->
                                    </div>
                                    <input name="name" type="text" class="form-control" placeholder="Name" value="{{ old('name') }}" required >
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                            <div class="col-md-4 ">
                                             <label class="text-primary">Name in Arabic</label>
                                        <input type="text" value="{{ old('name_in_arabic') }}" name="name_in_arabic" class="form-control " placeholder="Name in Arabic"
                                            aria-label="Name">
                                    </div>
                                     <div class="col-md-4{{ $errors->has('email') ? ' has-danger' : '' }}">

                                        <label class="text-primary">Email</label>
                                    <div class="input-group-prepend">
                                     <!--    <span class="input-group-text">
                                            <i class="nc-icon nc-email-85"></i>
                                        </span> -->
                                    </div>
                                    <input name="email" type="email" class="form-control" placeholder="Email" required value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>


                                     
                                    
                                     
                                         
                                    </div>

                                    <div class="row">
                                          <div class="col-md-4{{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label class="text-primary">Password</label>
                                    <div class="input-group-prepend">
                                       <!--  <span class="input-group-text">
                                            <i class="nc-icon nc-key-25"></i>
                                        </span> -->
                                    </div>
                                    <input name="password" type="password" class="form-control" placeholder="Password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                         <div class="col-md-4">
                                            <label class="text-primary">Confirm Password</label>
                                    <div class="input-group-prepend">
                                      <!--   <span class="input-group-text">
                                            <i class="nc-icon nc-key-25"></i>
                                        </span> -->
                                    </div>
                                    <input name="password_confirmation" type="password" class="form-control" placeholder="Password confirmation" required>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                         <div class=" col-md-4 ">
                                       <label class="text-primary">Company</label>
                                        <input type="text" value="{{ old('company') }}" name="company" class="form-control " placeholder="Company"
                                            aria-label="Name">
                                            <input type="hidden" name="contact_per_remarks" value="">
                                            <input type="hidden" name="contact_per_mobile" value="">
                                            <input type="hidden" name="contact_per_phone" value="">
                                            <input type="hidden" name="is_show" value="0">
                                            <input type="hidden" name="contact_per_name" value="">
                                            <input type="hidden" name="acc_no_db" value="">
                                            <input type="hidden" name="acc_no_cr" value="">
                                    </div>
                                      
                                       
                                      
                                        
                                    </div>
                                     <div class="row">
                                          <div class="col-md-4">
                                            <label class="text-primary">Nationality</label>
                                           <select class="form-control" name="nationality" >
                                               <option value="" >Select One</option>
                                               <option value="1">Pakistani</option>
                                               <option value="0">Foriegn</option>
                                           </select>
                                        </div>
                                        <div class="col-md-4">
                                          <label class="text-primary">Visa No</label>  
                                          <input type="text" value="{{ old('visa_no') }}" name="visa_no" class="form-control" placeholder="Visa No">
                                        </div>
                                        <div class="col-md-4">
                                           <label class="text-primary">Location</label> 
                                           <input type="text" value="{{ old('location') }}"  name="location" placeholder="Location" class="form-control">
                                        </div>
                                       
                                      
                                       
                                    </div>
                                     <div class="row">
                                          <div class="col-md-4">
                                           <label class="text-primary">Phone</label> 
                                           <input type="number" value="{{ old('phone') }}" placeholder="123" class="form-control" name="phone">
                                        </div>
                                          <div class="col-md-4">
                                           <label class="text-primary">Card No</label> 
                                           <input type="number" value="{{ old('card_no') }}" placeholder="123" class="form-control" name="card_no">
                                        </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">PO box</label> 
                                           <input type="text" value="{{ old('po_box') }}" class="form-control" placeholder="PO Box" name="po_box">
                                        </div>
                                      
                                      
                                       
                                    </div>
                                    <div class="row">
                                         <div class="col-md-4">
                                           <label class="text-primary">Designation</label>
                                           <input type="text" value="{{ old('designation') }}" placeholder="Designation" name="designation" class="form-control">
                                       </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">Profession</label>
                                           <input type="text"  value="{{ old('profession') }}" name="profession" placeholder="Profession" class="form-control">
                                       </div>
                                            <div class="col-md-4">
                                          <label class="text-primary">Passport No</label>  
                                         <input type="text" value="{{ old('passport_no') }}" name="passport_no" class="form-control" placeholder="e.g 57279">
                                        </div>
                                       
                                     
                                     
                                    </div>

                                    <div class="row">
                                         <div class="col-md-4">
                                           <label class="text-primary">VAT Country</label>
                                         <select class="form-control" name="vat_country" >
                                             <option value="" >Select One</option>
                                             <option value="testvatcountry">Test Vat Country</option>
                                         </select>
                                       </div>
                                          <div class="col-md-4">
                                           <label class="text-primary">VAT Emirates</label>
                                          <select class="form-control" name="vat_emirates" >
                                             <option value="">Select One</option>
                                             <option value="2">hello</option>
                                         </select>
                                       </div>
                                          <div class="col-md-4">
                                           <label class="text-primary">Passport Expiry Date</label>
                                           <input type="date"  value="{{ old('pass_expiry_date') }}" name="pass_expiry_date"  class="form-control">
                                       </div>
                                        
                                       
                                    </div>


                                    <div class="row">
                                         <div class="col-md-4">
                                           <label class="text-primary">Visa Expiry Date</label>
                                           <input type="date" value="{{ old('visa_expiry_date') }}" name="visa_expiry_date"  class="form-control">
                                       </div>
                                        <div class="col-md-4">
                                           <label class="text-primary">Country</label>
                                            <select class="form-control"  name="country" >
                                             <option value="" >Select One</option>
                                             <option value="1">Pakistan</option>
                                             <option value="3">Iran</option>
                                             <option value="2">India</option>
                                             <option value="5">China</option>
                                         </select>
                                       </div>
                                           <div class="col-md-4">
                                           <label class="text-primary">City</label>
                                            <select class="form-control" name="city" >
                                             <option value="">Select One</option>
                                             <option value="2">Multan</option>
                                             <option value="1">Lahore</option>
                                             <option value="5">Karachi</option>
                                         </select>
                                       </div>
                                        
                                      
                                    </div>


                                       <div class="row">
                                         <div class="col-md-4">
                                           <label class="text-primary">Mobile</label>
                                           <input type="number" value="{{ old('mobile') }}" placeholder="e.g 0300" name="mobile"  class="form-control">
                                       </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">Fax</label>
                                            <input type="text" name="fax" value="{{ old('fax') }}" placeholder="Fax No" class="form-control">
                                       </div>
                                           <div class="col-md-4">
                                           <label class="text-primary">Trade License Expiry Date</label>
                                             <input type="date" name="trade_license_expiry_date" value="{{ old('trade_license_expiry_date') }}"  class="form-control">
                                       </div>
                                         
                                         
                                      
                                    </div>
                                    <div class="row">
                                         <div class="col-md-4">
                                           <label class="text-primary">Trade License No</label> 
                                           <input type="number" value="{{ old('trade_license_no') }}" placeholder="123" class="form-control" name="trade_license_no">
                                        </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">ID No.</label>
                                           <input type="text" value="{{ old('id_no') }}" placeholder="e.g. 36302-xxxxxxx-x" name="id_no"  class="form-control">
                                       </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">Address 1</label>
                                            <input type="text" value="{{ old('address1') }}" name="address1" placeholder="Enter address" class="form-control">
                                       </div>
                                        
                                      
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                          <label class="text-primary">Address 2</label>
                                            <input type="text"  value="{{ old('address2') }}" name="address2" placeholder="Enter address" class="form-control">
                                        </div>
                                         <div class="col-md-4">
                                            <label class="text-primary">VAT Reg No</label>
                                            <input type="hidden" name="type" value="tenants">
                                            <input type="text"  value="{{ old('vat_reg_no') }}" class="form-control" name="vat_reg_no" placeholder="Enter VAT reg no">
                                        </div> 
                                    </div><br>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <a  class="btn btn-primary">Find</a> 
                                        </div>
                                         <div class="col-md-1">
                                           <a  class="btn btn-primary">New</a> 
                                        </div>
                                         <div class="col-md-1">
                                            <a  class="btn btn-primary">Edit</a> 
                                        </div>
                                         <div class="col-md-1">
                                          <a  class="btn btn-primary">Save</a> 
                                        </div>
                                         <div class="col-md-1">
                                             <a  class="btn btn-primary">Delete</a> 
                                        </div>
                                           <div class="col-md-1">
                                             <a  class="btn btn-primary">Cancel</a> 
                                        </div>
                                         <div class="col-md-1">
                                           <a  class="btn btn-primary">Quit</a> 
                                        </div>
                                         <div class="col-md-2">
                                             <a  class="btn btn-primary">Attch Files</a> 
                                        </div>
                                    </div>
                                </div>
                                
                                      
                                       
                                    
                         <div class="row">
                             <div class="col-md-4">
                                  <div class="form-check text-left">
                                    <label class="form-check-label">
                                        <input class="form-check-input" name="agree_terms_and_conditions" type="checkbox">
                                        <span class="form-check-sign"></span>
                                            {{ __('I agree to the') }}
                                        <a href="#something">{{ __('terms and conditions') }}</a>.
                                    </label>
                                    @if ($errors->has('agree_terms_and_conditions'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('agree_terms_and_conditions') }}</strong>
                                        </span>
                                    @endif
                                </div>
                             </div>
                         </div>
                                    <div class="text-center mt-4">
                                        <button class="btn btn-started"> Get Started</button>
                                    </div>
                                </form>
                            </div>
                            <div id="admin" style="display: none;">
                                <h4 class="text-center mt-4"> Landlord Register </h4>
                                <div class="text-center mt-4">
                                    <a href="#"><img src="https://www.flaticon.com/svg/static/icons/svg/179/179319.svg"
                                            class="img-fluid" width="40"></a>
                                    <a href="#" class="ml-2"><img
                                            src="https://www.flaticon.com/svg/static/icons/svg/179/179326.svg"
                                            class="img-fluid" width="40"> </a>

                                    <a href="#" class="ml-2"><img
                                            src="https://www.flaticon.com/svg/static/icons/svg/179/179342.svg"
                                            class="img-fluid" width="40"> </a>
                                </div>
                                <h6 class="text-muted text-center mt-2">or be classical</h6>
                                  <form class="form" method="POST" action="{{ route('register') }}">
                                @csrf


                                         <div class="container">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="text-primary">Acc No(Debitor)</label>
                                                    <input type="number" name="acc_no_db" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                   <label class="text-primary">Acc No(Creditor)</label>
                                                    <input type="number" name="acc_no_cr" class="form-control">  
                                                </div>
                                                  <div class="col-md-4 ">
                                        <label class="text-primary">Title</label>    
                                        <select class="form-control"  name="title">
                                            <option value="" >Select One</option>
                                            <option value="Mr">Mr</option>
                                            <option value="Mrs">Mrs</option>
                                        </select>
                                    </div> 
                                            </div>
                                    <div class="row">
                                        
                                         
                                      <div class=" col-md-4 ">
                                        <label class="text-primary">Ref No</label>
                                        <input type="text" value="{{ old('ref_no') }}" name="ref_no" class="form-control " placeholder="Ref no"
                                            aria-label="Name">
                                            
                                    </div>
                                      <div class="col-md-4">
                                           <label class="text-primary">Booking Number</label> 
                                           <input type="text" name="booking_no" value="{{ old('booking_no') }}" placeholder="e.g 123" class="form-control" name="card_no">
                                        </div>
                                
                                    <div class="col-md-4">
                                           <label class="text-primary">Phone</label> 
                                           <input type="number" value="{{ old('phone') }}"  placeholder="123" class="form-control" name="phone">
                                        </div>  
                                  
                                    </div>

                                     <div class="row">
                                           <div class="col-md-4{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <label class="text-primary">Name</label>
                                    <div class="input-group-prepend">
                                   <!--      <span class="input-group-text">
                                            <i class="nc-icon nc-single-02"></i>
                                        </span> -->
                                    </div>
                                    <input name="name" type="text" class="form-control" placeholder="Name" value="{{ old('name') }}" required >
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                            <div class="col-md-4 ">
                                             <label class="text-primary">Name in Arabic</label>
                                        <input type="text" value="{{ old('name_in_arabic') }}" name="name_in_arabic" class="form-control " placeholder="Name in Arabic"
                                            aria-label="Name">
                                    </div>
                                         <div class="col-md-4{{ $errors->has('email') ? ' has-danger' : '' }}">

                                        <label class="text-primary">Email</label>
                                    <div class="input-group-prepend">
                                     <!--    <span class="input-group-text">
                                            <i class="nc-icon nc-email-85"></i>
                                        </span> -->
                                    </div>
                                    <input name="email" type="email" class="form-control" placeholder="Email" required value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                               
                                     
                                         
                                    </div>
                                    
                                    <div class="row">
                                         <div class="col-md-4{{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label class="text-primary">Password</label>
                                    <div class="input-group-prepend">
                                       <!--  <span class="input-group-text">
                                            <i class="nc-icon nc-key-25"></i>
                                        </span> -->
                                    </div>
                                    <input name="password" type="password" class="form-control" placeholder="Password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                       
                                            <div class="col-md-4">
                                            <label class="text-primary">Confirm Password</label>
                                    <div class="input-group-prepend">
                                      <!--   <span class="input-group-text">
                                            <i class="nc-icon nc-key-25"></i>
                                        </span> -->
                                    </div>
                                    <input name="password_confirmation" type="password" class="form-control" placeholder="Password confirmation" required>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                 <div class=" col-md-4 ">
                                       <label class="text-primary">Company</label>
                                        <input type="text" value="{{ old('company') }}" name="company" class="form-control " placeholder="Copmpany"
                                            aria-label="Name">
                                    </div>
                                    </div>
                                     <div class="row">
                                        <div class="col-md-4">
                                           <label class="text-primary">Location</label> 
                                           <input type="text" value="{{ old('location') }}" name="location" placeholder="Location" class="form-control">
                                        </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">PO box</label> 
                                           <input type="text" value="{{ old('po_box') }}" class="form-control" placeholder="PO Box" name="po_box">
                                        </div>
                                          <div class="col-md-4">
                                           <label class="text-primary">Fax</label>
                                            <input type="text" value="{{ old('fax') }}" name="fax" placeholder="Fax No" class="form-control">
                                       </div>
                                       
                                    </div>
                                     <div class="row">
                                          <div class="col-md-4">
                                           <label class="text-primary">Country</label>
                                            <select class="form-control"  name="country" >
                                             <option value="" >Select One</option>
                                             <option value="1">Pakistan</option>
                                             <option value="3">Iran</option>
                                             <option value="2">India</option>
                                             <option value="5">China</option>
                                         </select>
                                       </div>
                                        <div class="col-md-4">
                                           <label class="text-primary">City</label>
                                            <select class="form-control" name="city" >
                                             <option value="" >Select One</option>
                                             <option value="2">Multan</option>
                                             <option value="1">Lahore</option>
                                             <option value="5">Karachi</option>
                                         </select>
                                       </div>
                                                                           
                                    </div>
                                    <hr>
                                    <h4 class="text-primary">Contact Person Details</h4>
                                    <div class="row">
                                        <br>
                                          <div class="col-md-4">
                                           <label class="text-primary">Name</label>
                                           <input type="text" value="{{ old('contact_per_name') }}"    name="contact_per_name" placeholder="Name" class="form-control">
                                       </div>
                                            <div class="col-md-4">
                                          <label class="text-primary">Designation</label>  
                                         <input type="text" value="{{ old('designation') }}"  name="designation" class="form-control" placeholder="">
                                        </div>
                                        <div class="col-md-4">
                                           <label class="text-primary">Phone</label>
                                       <input type="number" value="{{ old('contact_per_phone') }}"  name="contact_per_phone"  class="form-control" placeholder="Enter phone number...">
                                       </div>
                                     
                                     
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                           <label class="text-primary">Mobile</label>
                                       <input type="number" value="{{ old('contact_per_mobile') }}" name="contact_per_mobile"  class="form-control" placeholder="Enter Mobile number...">
                                       </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">Remarks</label>
                                       <input type="text" value="{{ old('contact_per_remarks') }}" name="contact_per_remarks"  class="form-control" placeholder="">
                                       </div>
                                       
                                    </div>
                                    <hr>

                                    <div class="row">
                                          <div class="col-md-4">
                                           <label class="text-primary">Card No</label>
                                           <input type="number"  value="{{ old('card_no') }}" placeholder="e.g 45325" name="card_no"  class="form-control">
                                       </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">ID No.</label>
                                           <input type="text" placeholder="e.g. 36302-xxxxxxx-x" value="{{ old('id_no') }}" name="id_no"  class="form-control">
                                       </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">Designation</label>
                                           <input type="text" value="{{ old('designation') }}" placeholder="" name="designation"  class="form-control">
                                       </div>
                                    </div>


                                       <div class="row">
                                        <div class="col-md-4">
                                           <label class="text-primary">Nationality</label>
                                           <input type="text" value="{{ old('nationality') }}" placeholder="" name="nationality"  class="form-control">
                                       </div>
                                       <div class="col-md-4">
                                           <label class="text-primary">VAT Country</label>
                                         <select class="form-control" name="vat_country" >
                                             <option value="" >Select One</option>
                                             <option value="testvatcountry">Test Vat Country</option>
                                         </select>
                                       </div>
                                       <div class="col-md-4">
                                           <label class="text-primary">Profession</label>
                                           <input type="text" value="{{ old('profession') }}" name="profession" placeholder="Profession" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                           <div class="col-md-4">
                                           <label class="text-primary">Address 1</label>
                                            <input type="text" value="{{ old('address1') }}" name="address1" placeholder="Enter address" class="form-control">
                                       </div>
                                        <div class="col-md-4">
                                          <label class="text-primary">Address 2</label>
                                            <input type="text"  value="{{ old('address2') }}" name="address2" placeholder="Enter address" class="form-control">
                                        </div>
                                        <div class="col-md-4">

                                            <label class="text-primary">VAT Reg No</label>
                                            <input type="hidden" name="type" value="landlord">
                                            <input type="hidden" name="visa_no" value="">
                                            <input type="hidden" name="passport_no" value="">
                                            <input type="hidden" name="pass_expiry_date" value="">
                                            <input type="hidden" name="visa_expiry_date" value="">
                                            <input type="hidden" name="trade_license_no" value="">
                                            <input type="hidden" name="mobile" value="">
                                            <input type="hidden" name="trade_license_expiry_date" value="">
                                            <input type="hidden" name="initial_occupation_date" value="">
                                            <input type="hidden" name="is_show" value="0">
                                            <input type="text"  value="{{ old('vat_reg_no') }}" class="form-control" name="vat_reg_no" placeholder="Enter VAT reg no">
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">

                                           <label class="text-primary">VAT Emirates</label>
                                          <select class="form-control" name="vat_emirates" >
                                             <option value="" >Select One</option>
                                             <option value="2">hello</option>
                                         </select>
                                       </div>
                                    </div>
                                    <br>
                                
                                      <div class="row">
                                        <div class="col-md-1">
                                            <a  class="btn btn-primary">Find</a> 
                                        </div>
                                         <div class="col-md-1">
                                           <a  class="btn btn-primary">Add</a> 
                                        </div>
                                         <div class="col-md-1">
                                            <a  class="btn btn-primary">Edit</a> 
                                        </div>
                                         <div class="col-md-1">
                                          <a  class="btn btn-primary">Update</a> 
                                        </div>
                                         <div class="col-md-1">
                                             <a  class="btn btn-primary">Cancel</a> 
                                        </div>
                                           <div class="col-md-1">
                                             <a  class="btn btn-primary">Delete</a> 
                                        </div>
                                         <div class="col-md-1">
                                           <a  class="btn btn-primary">Close</a> 
                                        </div>
                                         <div class="col-md-2">
                                             <a  class="btn btn-primary">Attachment</a> 
                                        </div>
                                    </div>
                                     <div class="row">
                                       
                                      
                                        
                                    </div>
                                </div>
                                
                            <div class="row">
                             <div class="col-md-4">
                                  <div class="form-check text-left">
                                    <label class="form-check-label">
                                        <input class="form-check-input" name="agree_terms_and_conditions" type="checkbox">
                                        <span class="form-check-sign"></span>
                                            {{ __('I agree to the') }}
                                        <a href="#something">{{ __('terms and conditions') }}</a>.
                                    </label>
                                    @if ($errors->has('agree_terms_and_conditions'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('agree_terms_and_conditions') }}</strong>
                                        </span>
                                    @endif
                                </div>
                             </div>
                         </div>
                                    <div class="text-center mt-4">
                                        <button class="btn btn-started"> Get Started</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
            $('#customSwitches').click(function () {
                if ($(this).prop("checked") == true) {
                    document.getElementById('user').style.display = 'none';
                    document.getElementById('admin').style.display = 'block';
                    document.getElementById('admint').style.display = 'none';
                    document.getElementById('usert').style.display = 'block';



                }
                else if ($(this).prop("checked") == false) {
                    document.getElementById('user').style.display = 'block';
                    document.getElementById('admin').style.display = 'none'; 
                    document.getElementById('admint').style.display = 'block';
                    document.getElementById('usert').style.display = 'none';               }
            });
        });
    </script>


</body>

</html>
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/Add_Project', 'AdminController@Project_Register')->name('Project')->middleware('auth');
Route::get('/get/buildings/{id}', 'AdminController@getBuildings')->name('getBuildings')->middleware('auth');
Route::get('/get/floors/{id}', 'AdminController@getFloors')->name('getFloors')->middleware('auth');
Route::get('/get/flats/{id}', 'AdminController@getFlat')->name('getFlat')->middleware('auth');
Route::post('/Admin/Add_project', 'AdminController@Add_project');
// Route::get('/delete_project/{ID}','AdminController@delete_project');
Route::post('/Admin/edit_project', 'AdminController@edit_project');

Route::get('/Add_Building', 'AdminController@add_building')->name('Add_Building')->middleware('auth');
Route::get('/Add_Landlords', 'AdminController@Add_Landlords')->name('Add.Landlords')->middleware('auth');
Route::get('/Add_Assign_APPART', 'AdminController@Add_Assign_APPART')->name('Add.Assign_APPART')->middleware('auth');


Route::get('/Add_tenants', 'AdminController@Add_tenants')->name('Add.tenants')->middleware('auth');
Route::post('Admin/save_assign_flat', 'AdminController@saveData')->name('saveData')->middleware('auth');
Route::post('Admin/Update_Status', 'AdminController@updateStatus')->name('Admin_Update_Status')->middleware('auth');
Route::get('/get/users/{type}', 'AdminController@getUser')->name('get.users')->middleware('auth');


Route::get('/Nature_maintenance', 'AdminController@Nature_maintenance')->name('Nature.maintenance')->middleware('auth');
Route::post('Admin/save_maintenance_head', 'AdminController@save_maintenance_head')->name('maintenance.head')->middleware('auth');

Route::get('/Voucher_Detail', 'AdminController@voucher_detail')->name('Voucher_Detail')->middleware('auth');
Route::get('/Create_Voucher_Detail', 'AdminController@create_voucher_detail')->name('Create_Voucher_Detail')->middleware('auth');
Route::get('/Receipt_Voucher', 'AdminController@create_receipt_voucher_detail')->name('Create_Voucher_Detail')->middleware('auth');

Route::get('/Questions', 'AdminController@Questions')->name('Questions')->middleware('auth');
Route::post('Admin/save_questions', 'AdminController@save_questions')->name('save.questions')->middleware('auth');

Route::post('Admin/Save_vouchers', 'AdminController@Save_vouchers')->name('Save.vouchers')->middleware('auth');



Route::get('/Add_building_facilties', 'AdminController@Add_building_facilties')->name('Add_building_facilties')->middleware('auth');

Route::get('/Add_building_types', 'AdminController@Add_building_types')->name('Add_Building_types')->middleware('auth');
// Route::get('/delete_building_types/{ID}','AdminController@delete_building_types');
Route::post('/Admin/edit_building_types', 'AdminController@edit_building_types');

Route::post('/Admin/save_building', 'AdminController@save_building');

Route::post('/Admin/save_floors', 'AdminController@save_floors');

Route::post('/Admin/save_building_types', 'AdminController@save_building_types');
Route::post('/Admin/save_account', 'AdminController@save_account');

Route::post('/Admin/save_building_facilties', 'AdminController@save_building_facilties');
// Route::get('/delete_flat_types/{ID}','AdminController@delete_flat_types');
Route::post('/Admin/edit_flat_types', 'AdminController@edit_flat_types');

Route::get('/Add_flat_types', 'AdminController@Add_flat_types')->name('flat.types')->middleware('auth');
Route::post('/Admin/save_flat_types', 'AdminController@save_flat_types');
// Route::get('/delete_building_facilties/{ID}','AdminController@delete_building_facilties');
Route::post('/Admin/edit_building_facilties', 'AdminController@edit_building_facilties');
Route::post('/Admin/edit_flats', 'AdminController@edit_flats');
Route::post('/Admin/edit_building', 'AdminController@edit_building');



Route::get('/Add_flat_pro_types', 'AdminController@Add_flat_pro_types')->name('flat.protypes')->middleware('auth');
Route::post('/Admin/save_flatpro_types', 'AdminController@save_flatpro_types');
// Route::get('/delete_flat_pro_types/{ID}','AdminController@delete_flat_pro_types');
Route::post('/Admin/edit_flat_pro_types', 'AdminController@edit_flat_pro_types');

Route::get('/Add_flat_facilties', 'AdminController@Add_flat_facilties')->name('flat.facilties')->middleware('auth');



Route::get('/New_page', 'AdminController@New_page')->name('New.page')->middleware('auth');
Route::get('/Owner/Report', 'AdminController@Owner_report')->name('admin.owner_report')->middleware('auth');
Route::get('/Owner/Flats/{owner_id}', 'AdminController@Owner_Flats')->name('admin.owner_flats')->middleware('auth');
Route::get('/Report/', 'AdminController@Owner_Flats')->name('admin.report')->middleware('auth');
Route::post('/Post/Owner/Report', 'AdminController@Owner_Post')->name('admin.owner_post')->middleware('auth');

// for the accounts
Route::get('/Add_Account', 'AdminController@Add_Account')->name('Admin.Add_Account')->middleware('auth');



Route::post('/Admin/save_flat_facilties', 'AdminController@save_flat_facilties');
// Route::get('/delete_flat_facilities/{ID}','AdminController@delete_flat_facilities');
Route::post('/Admin/edit_flat_facilities', 'AdminController@edit_flat_facilities');

Route::get('/Add_floor_types', 'AdminController@Add_floor_types')->name('floor.types')->middleware('auth');
Route::post('/Admin/save_floor_types', 'AdminController@save_floor_types');
// Route::get('/delete_floor_type/{ID}','AdminController@delete_floor_type');
Route::post('/Admin/edit_floor_types', 'AdminController@edit_floor_types');



Route::get('/Add_admin', 'AdminController@Add_admin')->name('add.admin')->middleware('auth');
Route::post('/Admin/save_admins', 'AdminController@save_admins');

Route::post('/Admin/save_landlord', 'AdminController@save_landlord')->middleware('auth');
Route::post('/Admin/save_tenants', 'AdminController@save_tenants')->middleware('auth');

Route::get('/Add_Supplier', 'AdminController@Add_Supplier')->name('supplier.name')->middleware('auth');
Route::get('/supplier_list', 'AdminController@supplier_list')->name('supplier.list')->middleware('auth');
Route::post('/Admin/Save_supplier', 'AdminController@Save_supplier');

Route::get('/Post_Dated_Cheques', 'AdminController@post_dated_cheques')->name('Post_Dated_Cheques')->middleware('auth');
Route::get('/Create_Post_Dated_Cheques', 'AdminController@create_post_dated_cheques')->name('Create_Post_Dated_Cheques')->middleware('auth');

Route::get('/Maintaince_List', 'AdminController@Maintaince_List')->name('Maintaince.List')->middleware('auth');
Route::get('/Maintenance_Form', 'AdminController@Maintenance_Form')->name('Maintenance.Form')->middleware('auth');
Route::post('/Admin/Save_maintenance', 'AdminController@Save_maintenance');

Route::get('/Add_Floor', 'AdminController@add_floor')->name('Add_Floor')->middleware('auth');
// Route::get('/delete_floorlists/{ID}','AdminController@delete_floorlists');
Route::post('/Admin/edit_floorslist', 'AdminController@edit_floorslist');

Route::get('/Add_Flat', 'AdminController@add_flat')->name('Add_Flat')->middleware('auth');
Route::post('/Admin/save_flats', 'AdminController@save_flats');
// Route::get('/delete_flat/{ID}','AdminController@delete_flat');

Route::get('/Add_Contracts', 'AdminController@add_contracts')->name('Add_Contracts')->middleware('auth');
Route::get('/Add_Short_Contracts', 'AdminController@add_short_contracts')->name('Add_Short_Contracts')->middleware('auth');
Route::get('/Sale_Purchase_Contract', 'AdminController@Sale_Purchase_Contract')->name('Sale_Purchase_Contract')->middleware('auth');
Route::get('/create_sale_purchase_contract', 'AdminController@create_sale_purchase_contract')->name('create_sale_purchase_contract')->middleware('auth');

Route::get('/Create_Contracts', 'AdminController@create_contract')->name('admin.create_new_contract')->middleware('auth');
Route::get('/Add_journal_voucher', 'AdminController@Add_journal_voucher')->name('admin.Add_journal_voucher')->middleware('auth');

Route::get('/Create_Short_Contracts', 'AdminController@create_short_contracts')->name('admin.create_short_contracts')->middleware('auth');

Route::get('/Utility_Management', 'AdminController@utility_management')->name('admin.Utility_Management')->middleware('auth');
Route::get('/Utility_Management_Show', 'AdminController@utility_management_show')->name('admin.Utility_Management_Show')->middleware('auth');


Route::post('/Admin/Save_Contracts', 'AdminController@save_contract')->name('admin.save_contract')->middleware('auth');

Route::post('/Admin/Save_Short_Contracts', 'AdminController@save_short_contracts')->name('admin.Save_Short_Contracts')->middleware('auth');



Route::post('/Admin/save_replacement_pdc', 'AdminController@save_replacement_pdc')->name('admin.save_replacement_pdc')->middleware('auth');

Route::post('/Admin/Save_Utility_management', 'AdminController@Save_Utility_management')->name('admin.SaveUtilitymanagement')->middleware('auth');

Route::get('/Admin/Edit_Contracts/{id}', 'AdminController@edit_contract')->name('admin.edit_contract')->middleware('auth');
Route::get('/Admin/view_Contracts/{id}', 'AdminController@view_contract')->name('admin.view_contract')->middleware('auth');


Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'PageController@index']);

});

Route::get('get/flats/assign/lanlord/{id}', 'AdminController@getLanlord')->name('admin.getLanlord')->middleware('auth');


Route::post('/get_buildings', 'AdminController@get_buildings');
Route::post('/get_floors', 'AdminController@get_floors');


Route::post('/get_flats', 'AdminController@get_flats');
Route::post('/get_lanlords', 'AdminController@get_lanlords');


Route::post('/get_floors_detail', 'AdminController@get_floors_detail');
Route::post('/get_lanlords_detail', 'AdminController@get_lanlords_detail');
Route::post('/get_filter_data', 'AdminController@get_filter_data');
Route::post('/change_status', 'AdminController@change_status');
Route::post('/get_management_fee', 'AdminController@get_management_fee');






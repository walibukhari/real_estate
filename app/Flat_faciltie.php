<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flat_faciltie extends Model
{
  public function Facilitie_name() {
        return $this->hasOne(Faciltie_of_flat::class,'id', 'flat_fac_id' );
    }
}

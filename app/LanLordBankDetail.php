<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LanLordBankDetail extends Model
{
    protected $fillable = [
        'lanlord_id','name','branch','address','title','account_number','iban_number','swift_number'
    ];
}

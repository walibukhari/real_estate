<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building_list extends Model
{
  public function Buildingfile() {
        return $this->hasOne(Building_file::class, 'building_id');
    }
     public function Project_name() {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }
     public function Building_type() {
        return $this->hasOne(Building_type::class, 'id', 'type_id');
    }
       public function Manager_name() {
        return $this->hasOne(User::class, 'id', 'manager_id');
    }

     public function Building_facilitie() {
        return $this->hasMany(Building_faciltie::class,  'building_id');
    }

}

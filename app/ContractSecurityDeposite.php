<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractSecurityDeposite extends Model
{
    protected $fillable = [
        'contract_id','serial_no','check_no','check_date','check_issue','party_name','check_deposite','security_deposit_amount'
    ];
}

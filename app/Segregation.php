<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Segregation extends Model
{
    const ACTIVE = 1;
    const DE_ACTIVE = 2;

    protected $fillable = [
         'building_id','project_id','flat_id','user_id','user_type','status'
    ];
    public static function createSegregation($data){
        return self::create([
            'building_id' => $data['building_id'],
            'project_id' => $data['project_id'],
            'flat_id' => $data['flat_id'],
            'user_id' => $data['userData'],
            'user_type' => $data['userType']
        ]);
    }

    public function project(){
        return $this->hasOne(Project::class,'id','project_id');
    }

    public function building(){
        return $this->hasOne(Building_list::class,'id','building_id');
    }
    public function flat(){
        return $this->hasOne(Flat_list::class,'unit_code','flat_id');
    }
    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
    public function profile(){
        return $this->hasOne(Profile::class,'user_id','user_id');
    }
}

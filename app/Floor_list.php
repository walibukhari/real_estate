<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Floor_list extends Model
{
   public function Building_name() {
        return $this->hasOne(Building_list::class, 'id', 'building_id');
    }
    public function Floor_type_name() {
        return $this->hasOne(Floor_type::class, 'id', 'floor_type_id');
    }
}

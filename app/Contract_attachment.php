<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract_attachment extends Model
{
    protected $fillable = [
      'contract_id','attachment','comment'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
   public function Projectfile() {
        return $this->hasOne(Project_file::class, 'project_id');
    }
}

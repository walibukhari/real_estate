<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstallmentCheck extends Model
{
    protected $fillable = [
      'contract_id','installment_no','amount','check_no','check_date','check_issue','party_name','drop_down_opt'
    ];
    
    public function Contract_data() {
        return $this->hasOne(Contract::class, 'id','contract_id');
    }
    
}

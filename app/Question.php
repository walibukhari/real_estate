<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function MaintenanceHeadName() {
        return $this->hasOne(Maintenance_head::class, 'id','head_id');
    }
}

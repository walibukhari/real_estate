<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    //

    public function owner_report(){
        return $this->hasOne(OwnerReport::class,'contract_id','id');
    }
    public function installment_check(){
        return $this->hasMany(InstallmentCheck::class,'contract_id','id');
    }
    public function attachments(){
        return $this->hasMany(Contract_attachment::class,'contract_id','id');
    }
    public function Flat_type_name() {
        return $this->hasOne(Flat_type::class, 'id', 'flat_pro_type');
    }
    public function Flat_lists() {
        return $this->hasMany(Flat_list::class, 'unit_code', 'flat_id');
    }
    public function Building_Name() {
        return $this->hasOne(Building_list::class, 'id', 'building_id');
    }
     public function Tenant_Name() {
        return $this->hasOne(User::class, 'id','tenant');
    }
    public function Landlord_Name() {
        return $this->hasOne(User::class, 'id','lanlord_name');
    }
    public function Deposit_Amount() {
        return $this->hasOne(ContractSecurityDeposite::class, 'contract_id','id');
    }
}

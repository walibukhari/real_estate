<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractOtherName extends Model
{
    protected $fillable = [
        'sr_no','particular','amount','vat'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnerReport extends Model
{
    protected $fillable = [
      'contract_id','owner_id','user_id','debit','credit'
    ];

    public function owner() {
        return $this->hasOne(User::class,'id','owner_id');
    }
    public function Flat_lists() {
        return $this->hasOne(Flat_list::class,'owner_id','owner_id');
    }
}

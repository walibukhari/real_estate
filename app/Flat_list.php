<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flat_list extends Model
{
  public function Building_name() {
        return $this->hasOne(Building_list::class, 'id', 'building_id');
    }
    public function Flat_facilitie() {
        return $this->hasMany(Flat_faciltie::class,  'flat_id');
    }
    public function Flat_Attachment() {
        return $this->hasMany(Flat_attachment::class,  'flat_id');
    }

    public function Flat_type_name() {
        return $this->hasOne(Flat_type::class, 'id', 'flat_type_id');
    }

    public function Landlord_name() {
        return $this->hasOne(User::class, 'id', 'owner_id');
    }
    public function owner_report() {
        return $this->hasOne(OwnerReport::class, 'owner_id', 'owner_id');
    }
}

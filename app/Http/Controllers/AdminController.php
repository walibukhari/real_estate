<?php

namespace App\Http\Controllers;

use App\Contract;
use App\Contract_attachment;
use App\ContractOtherName;
use App\ContractSecurityDeposite;
use App\InstallmentCheck;
use App\Maintenance_detail;
use App\LanLordBankDetail;
use App\Account;
use App\OwnerReport;
use App\Segregation;
use App\Utility_attachment;
use App\Apart_voucher_attachment;
use App\Long_contract_internal;
use App\Apart_voucher_detail;
use App\Office_voucher_detail;
use App\Maintenance_head;
use App\Utility_management;
use App\Maintenance_form;
use App\Voucher;
use App\Question;
use App\Utility_management_external;
use App\Utility_management_internal;
use App\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Project;
use App\Short_contract;
use App\Short_contract_attachment;
use App\Short_contract_external;
use App\Short_contract_internal;
use App\Flat_attachment;
use App\Answer;
use App\Building;
use App\Building_type;
use App\Floor;
use App\Tenants_detail;
use App\Building_file;
use App\Landlord_tenants_detail;
use App\Building_list;
use App\Building_of_faciltie;
use App\Flat_faciltie;
use App\User;
use App\Flat_list;
use Hash;
use App\Building_faciltie;
use App\Flat;
use App\Flat_pro_type;
use App\Tenants_attachment;
use App\Flat_type;
use App\Floor_list;
use App\Faciltie_of_flat;
use App\Project_file;
use App\Profile;
use App\Floor_type;
use Auth;
use File;
use DB;
class AdminController extends Controller
{
    public function Project_Register()
    {
        $data['counter'] = 1;
        $data['g_manager'] = User::where('role','General Manager')->get();
        $data['project'] = Project::all();
    	return view('admin.project',$data);
    }

    public function getBuildings($id){
        $building = Building_list::where('project_id','=',$id)->get();
        return collect([
            'status' => true,
            'data' => $building
        ]);
    }

    public function getFloors($id){
        $floor = Floor_list::where('building_id','=',$id)->get();
        return collect([
            'status' => true,
            'data' => $floor
        ]);
    }

    public function getFlat($id){
        $flat = Flat_list::where('building_id','=',$id)->get();
        return collect([
            'status' => true,
            'data' => $flat
        ]);
    }

    public function Add_project(request $request)
    {

        $user_id                   = Auth::user()->id;
    	$addproject                = new Project();
    	$addproject->name          = $request->name;
    	$addproject->s_date        = $request->sdate;
    	$addproject->e_date        = $request->edate;
    	$addproject->location      = $request->location;
    	$addproject->created_by_id = $user_id;
    	$addproject->manager_id    = $request->manager_id;
    	$addproject->save();
        $master                    = $addproject->id;

         $project_file             = new Project_file();
         $project_file->project_id = $master;
        if ($request->hasFile('document'))
        {
            request()->validate([

               'document' =>  'required|mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
            ]);
            if($request->file('document')->isValid()) {

                $file = $request->file('document');
                $name =  $file->getClientOriginalName();
                $request->file('document')->move("public/files/images/", $name);
            }
            $documentPath = 'public/files/images/'.$name;
        }
         $project_file->file_link = $documentPath;
         $project_file->save();

    	return back()->withStatus(__('project registered successfully.'));
    }



    public function Add_Building()
    {
        $data['buildingfaciltie'] = Building_of_faciltie::all();
        // dd($data['buildingfaciltie']);
        $data['counter']          = 1;
        $data['buildinglist']     = Building_list::all();
        $data['buildingtype']     = Building_type::all();
        $data['project_name']     = Project::all();
        $data['g_manager'] = User::where('role','General Manager')->get();
    	return view('admin.add_building',$data);
    }

    public function add_floor()
    {
       $data['floorlist']   = Floor_list::all();
       $data['counter']      = 1;
       $data['floortype']    = Floor_type::all();
       $data['buildinglist'] = Building_list::all();
    	return view('admin.add_floor',$data);
    }

    public function save_building(Request $request)
    {
                 if ($request->hasFile('image'))
        {
            request()->validate([

                'image' => 'required|image|mimes:jpg,jpeg,png',
            ]);
            if($request->file('image')->isValid()) {

                $file = $request->file('image');
                $name =  $file->getClientOriginalName();
                $request->file('image')->move("public/files/images/", $name);
            }
            $imagePath = 'public/files/images/'.$name;
        }
        $building                   = new Building_list();
        $building->name             = $request->name;
        $building->project_id       = $request->project_name;
        $building->manager_id       = $request->manager_id;
        $building->address          = $request->address;
        $building->no_floors        = $request->no_floors;
        $building->no_flats         = $request->no_flats;
        $building->type_id          = $request->type_id;
        $building->parking          = $request->parking;
        $building->location          = $request->location;
        $building->plot_no          = $request->plot_no;
        $building->permisses_no     = $request->permisses_no;
        $building->img_link         = $imagePath;
        $building->save();
        $master                     = $building->id;

        $buildingfile               = new Building_file();
        $buildingfile->building_id  = $master;

        //       if ($request->hasFile('document'))
        // {
        //     request()->validate([

        //       'document' =>  'required|mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
        //     ]);


        //     if($request->file('document')->isValid()) {

        //         $file = $request->file('document');
        //         $name =  $file->getClientOriginalName();
        //         $request->file('document')->move("public/files/images/", $name);
        //     }
        //     $documentPath = 'public/files/images/'.$name;
        // }
        // $buildingfile->file_link = $documentPath;
        // $buildingfile->save();


        if(request('building_attachments'))
        {
                foreach (request('building_attachments') as $key => $value) {
                $buildingfile               = new Building_file();
                $buildingfile->comment      = $request->comments[$key];
                $buildingfile->building_id = $master;
                $file3 = $request->file('building_attachments')[$key];
                $name3 =  $file3->getClientOriginalName();
                $request->file('building_attachments')[$key]->move("public/files/attachments/", $name3);
                $file = 'public/files/attachments/'.$name3;
                $buildingfile->file_link =$file;
                $buildingfile->save();
                }
        }






/// new table insertion building facilties
        if($request->fac_type_id){
             foreach ($request->fac_type_id as $key => $value) {
       $building_faciltie                  = new Building_faciltie();
       $building_faciltie->building_id     = $master;
       $building_faciltie->building_fac_id = $request->fac_type_id[$key];
       $building_faciltie->save();
        }
        }



        return back()->withStatus(__('Building registered successfully.'));
   // }
}

    public function add_flat()
    {
        $data['counter']          = 1;
        $data['flatlist']         = Flat_list::all();
        $data['faciltie_of_flat'] = Faciltie_of_flat::all();
        $data['flat_pro_type']    = Flat_pro_type::all();
        $data['flat_type']        = Flat_type::all();
        $data['buildinglist']     = Building_list::all();
        $data['floor_list']     = Floor_list::all();
        $data['landlored']     = User::where('role',"Landlord")->get();
        return view('admin.add_flat',$data);
    }

    public function save_floors(Request $request)
    {
        $floor                   = new Floor_list();
        $floor->building_id      = $request->building_id;
        $floor->name             = $request->name;
        $floor->floor_type_id    = $request->floor_type_id;
        $floor->no_of_flats      = $request->no_of_flats;
        $floor->save();
        return back()->withStatus(__('Floor registered successfully.'));
    }

    public function save_flats(Request $request)
    {
         request()->validate([
                   'facilitie_type' =>  'required',
                ]);

        $flats                      = new Flat_list();
        $flats->flat_type_id        = $request->flat_type_id;
        $flats->flat_pro_type_id    = $request->flat_pro_type_id;
        $flats->building_id         = $request->building_id;
        $flats->unit_code           = $request->unit_code;
        $flats->rent_range          = $request->rent_range;
        $flats->management_fee_type = $request->management_fee_type;
        $flats->management_fee      = $request->management_fee;
        $flats->owner_id            = $request->owner_id;
        $flats->floor_id            = $request->floor_id;
        $flats->property_size       = $request->property_size;
        // $flats->apartment_no        = $request->apartment_no;



        $flats->save();
        $master   = $flats->id;

        if ($request->facilitie_type) {
             foreach ($request->facilitie_type as $key => $value) {
        $flat_faciltie              = new Flat_faciltie();
        $flat_faciltie->flat_id     = $master;
        $flat_faciltie->flat_fac_id = $request->facilitie_type[$key];
        $flat_faciltie->save();
        }
    }



                        if($request->flat_attachments){
        //       $validator = request()->validate([
        //     'flat_attachments' => 'mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
        // ]);
        //dd($request->tenants_attachments);
                 foreach (request('flat_attachments') as $key => $value) {
                $attachments = new Flat_attachment();
                $attachments->flat_id = $master;
                $attachments->comment = $request->comments[$key];

                $file3 = $request->file('flat_attachments')[$key];
                $name3 =  $file3->getClientOriginalName();
                $request->file('flat_attachments')[$key]->move("public/files/attachments/", $name3);
                $file = 'public/files/attachments/'.$name3;

                $attachments->attachment =$file;
                $attachments->save();
                }



                }


        return back()->withStatus(__('Flat registered successfully.'));
    }

    public function Add_building_types()
    {
        $data['counter'] =1;
        $data['building_type'] = Building_type::all();
        return view('admin.add_building_types',$data);
    }

    public function save_building_types(Request $request)
    {
        $building_type = new Building_type();
        $building_type->name = $request->name;
        $building_type->description = $request->note;
        $building_type->save();
        return back()->withStatus(__('Building Type registered successfully.'));
    }

    public function Add_building_facilties()
    {
         $data['counter'] =1;
        $data['building_faciltie'] = Building_of_faciltie::all();
        return view('admin.add_building_facilties',$data);
    }

     public function save_building_facilties(Request $request)
    {
        $building_faciltie              = new Building_of_faciltie();
        $building_faciltie->name        = $request->name;
        $building_faciltie->description = $request->note;
        $building_faciltie->save();
        return back()->withStatus(__('Building Faciltie registered successfully.'));
    }

    public function Add_flat_types()
    {
        $data['counter'] =1;
        $data['flattype'] = Flat_type::all();
        return view('admin.add_flat_types',$data);
    }


        public function save_flat_types(Request $request)
    {
        $flat_type              = new Flat_type();
        $flat_type->name        = $request->name;
        $flat_type->description = $request->note;
        $flat_type->save();
        return back()->withStatus(__('Flat Type save successfully.'));
    }
     public function Add_flat_pro_types()
    {
        $data['counter'] =1;
        $data['flat_pro_type'] = Flat_pro_type::all();
        return view('admin.add_flatpro_types',$data);
    }


        public function save_flatpro_types(Request $request)
    {
        $flat_pro_type              = new Flat_pro_type();
        $flat_pro_type->name        = $request->name;
        $flat_pro_type->description = $request->note;
        $flat_pro_type->save();
        return back()->withStatus(__('Flat Property type save successfully.'));
    }

     public function Add_flat_facilties()
    {
        $data['counter'] =1;
        $data['flat_faciltie'] = Faciltie_of_flat::all();
        return view('admin.add_flat_facilties',$data);
    }


        public function save_flat_facilties(Request $request)
    {
        $flat_facilties              = new Faciltie_of_flat();
        $flat_facilties->name        = $request->name;
        $flat_facilties->description = $request->note;
        $flat_facilties->save();
        return back()->withStatus(__('Flat Facilties save successfully.'));
    }


      public function Add_floor_types()
    {
        $data['counter'] =1;
        $data['floor_type'] = Floor_type::all();
        return view('admin.add_floor_types',$data);
    }


        public function save_floor_types(Request $request)
    {
        $floor_types              = new Floor_type();
        $floor_types->name        = $request->name;
        $floor_types->description = $request->note;
        $floor_types->save();
        return back()->withStatus(__('Floor type save successfully.'));
    }

      public function Add_admin()
    {
        $data['counter'] =1;
        $data['admin'] = User::all();
        return view('admin.admin',$data);
    }


         public function save_admins(Request $request)
    {

        request()->validate([
            'email'       => ['required', 'unique:admins'],
        ]);

        $admin              = new User();
        $admin->name        = $request->name;
        $admin->email       = $request->email;
        $admin->password    = $request->name;
        $admin->role        = $request->role;
        $admin->phone       = $request->phone;
        $admin->is_show     = 1;
        $admin->password    = Hash::make($request->pass);
        $admin->save();
        return back()->withStatus(__('User Added successfully.'));
    }


    public function edit_floor_types(Request $request)
    {
        $update        = Floor_type::where('id', $request->floor_type_id)->first();
        $update->name        = $request->name;
        $update->description = $request->note;
        $update->status      = $request->status;
        $update->save();
        return back()->withStatus(__('Floor type update successfully.'));
    }


    public function edit_flat_facilities(Request $request)
    {
        $update        = Faciltie_of_flat::where('id', $request->flat_facilitie_id)->first();
        $update->name        = $request->name;
        $update->description = $request->note;
        $update->status      = $request->status;
        $update->save();
        return back()->withStatus(__('Flat Facilities update successfully.'));
    }


    public function edit_flat_pro_types(Request $request)
    {
        $update        = Flat_pro_type::where('id', $request->flat_pro_types_id)->first();
        $update->name        = $request->name;
        $update->description = $request->note;
        $update->status      = $request->status;
        $update->save();
        return back()->withStatus(__('Flat Property type update successfully.'));
    }


    public function edit_flat_types(Request $request)
    {
        $update        = Flat_type::where('id', $request->flattypes_id)->first();
        // dd($update);
        $update->name        = $request->name;
        $update->description = $request->note;
        $update->status      = $request->status;
        $update->save();
        return back()->withStatus(__('Flat Property type update successfully.'));
    }


    public function edit_project(Request $request)
    {

        if ($request->rem_doc==1) {
            $doc= $request->rem_doc;
        }
        else{
            if ($request->hasFile('document'))
            {
                request()->validate([
                   'document' =>  'required|mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
                ]);
                if($request->file('document')->isValid()) {
                    $file = $request->file('document');
                    $name =  $file->getClientOriginalName();
                    $request->file('document')->move("public/files/images/",$name);
                }
            }

            if($request->document == NULL)
            {
                $doc = $request->old_doc;
            }
            else{
                $doc = "public/files/images/".$name."";
                if ($request->old_doc!="" && file_exists($request->old_doc)) {
                    unlink($request->old_doc);
                }
            }
        }




        $update        = Project::where('id', $request->project_id)->first();
        $update->name          = $request->name;
        $update->s_date        = $request->sdate;
        $update->e_date        = $request->edate;
        $update->location      = $request->location;
        $update->manager_id    = $request->manager_id;
        $update->status        = $request->status;
        $update->save();

        //update the project file table
        $update_project_file = Project_file::where('project_id', $request->project_id)->first();

         $update_project_file->file_link = $doc;
         $update_project_file->save();
           return back()->withStatus(__('Project updated successfully.'));
    }


    public function edit_building_facilties(Request $request)
    {
        $update        = Building_of_faciltie::where('id', $request->building_facilties_id)->first();
        $update->name        = $request->name;
        $update->description = $request->note;
        $update->status      = $request->status;
        $update->save();
        return back()->withStatus(__('Building Facilities updated successfully.'));
    }


    public function edit_building_types(Request $request)
    {
        $update        = Building_type::where('id', $request->building_types_id)->first();
        $update->name        = $request->name;
        $update->description = $request->note;
        $update->status      = $request->status;
        $update->save();
        return back()->withStatus(__('Building Facilities updated successfully.'));
    }


    public function edit_floorslist(Request $request)
    {
        $update        = Floor_list::where('id', $request->floor_list_id)->first();
        $update->name          = $request->name;
        $update->no_of_flats   = $request->no_of_flats;
        $update->building_id   = $request->building_id;
        $update->floor_type_id = $request->floor_type_id;
        $update->save();
        return back()->withStatus(__('Floor List updated successfully.'));
    }


    public function edit_flats(Request $request)
    {
       // dd($request->facilitie_type);exit();
       $update  = Flat_list::where('id', $request->flatlists_id)->first();
        $update->flat_type_id        = $request->flat_type_id;
        $update->flat_pro_type_id    = $request->flat_pro_type_id;
        $update->building_id         = $request->building_id;
        $update->unit_code           = $request->unit_code;
        $update->rent_range          = $request->rent_range;
        $update->management_fee_type = $request->management_fee_type;
        $update->management_fee      = $request->management_fee;
        $update->owner_id            = $request->owner_id;
        $update->floor_id            = $request->floor_id;
        $update->status              = $request->status;
        $update->property_size       = $request->property_size;
        $update->save();


        // $update->flat_type_id     = $request->flat_type_id;
        // $update->flat_pro_type_id = $request->flat_pro_type_id;
        // $update->building_id      = $request->building_id;
        // $update->unit_code        = $request->unit_code;
        // $update->rent_range       = $request->rent_range;



Flat_faciltie::where('flat_id', $request->flatlists_id)->delete();


        if ($request->facilitie_type) {
        foreach ($request->facilitie_type as $key => $value) {
        $flat_faciltie              = new Flat_faciltie();
        $flat_faciltie->flat_id     =  $request->flatlists_id;
        $flat_faciltie->flat_fac_id = $request->facilitie_type[$key];
        $flat_faciltie->save();
        }
    }

     return back()->withStatus(__('Flat List updated successfully.'));
    }



            public function edit_building(Request $request)
    {


             if ($request->rem_img==1) {
            $img= $request->old_img;
        }
        else{
            if ($request->hasFile('image'))
            {
                request()->validate([
                   'image' => 'required|image|mimes:jpg,jpeg,png',
                ]);
                if($request->file('image')->isValid()) {
                    $file = $request->file('image');
                    $name =  $file->getClientOriginalName();
                    $request->file('image')->move("public/files/images/",$name);
                }
            }

            if($request->image == NULL)
            {
                $img = $request->old_img;
            }
            else{
                $img = "public/files/images/".$name."";
                if ($request->old_img!="" && file_exists($request->old_img)) {
                    unlink($request->old_img);
                }
            }
        }

        $update  = Building_list::where('id', $request->buildinglists_id)->first();
        $update->name             = $request->name;
        $update->project_id       = $request->project_name;
        $update->manager_id       = $request->manager_id;
        $update->address          = $request->address;
        $update->no_floors        = $request->no_floors;
        $update->no_flats         = $request->no_flats;
        $update->type_id          = $request->type_id;
        $update->parking          = $request->parking;
        $update->location         = $request->location;
        $update->plot_no          = $request->plot_no;
        $update->permisses_no     = $request->permisses_no;
        $update->img_link         = $img;
        $update->save();



Building_faciltie::where('building_id', $request->buildinglists_id)->delete();


        if ($request->facilitie_type) {

             foreach ($request->facilitie_type as $key => $value) {
        $building_faciltie = new Building_faciltie();
        $building_faciltie->building_id =  $request->buildinglists_id;
        $building_faciltie->building_fac_id = $request->facilitie_type[$key];
        $building_faciltie->save();
        }
    }

     return back()->withStatus(__('Building List updated successfully.'));
    }






















    public function getUser($type){
        $users = User::where('role','=',$type)->get();
        return collect([
           'status' => true,
           'data' => $users
        ]);
    }

    public function saveData(Request  $request){
        Segregation::createSegregation($request->all());
        return collect([
            'status' => true,
            'message' => 'data save successfully'
        ]);
    }

    public function Add_tenants()
    {
        $data['Landlord'] = User::where('role',"Tenant")->get();
        $data['projects'] = Project::all();
        $data['users'] = User::where('role','=','Tenant')->get();


        return view('admin.add_tenants',$data);
    }
    public function Add_Landlords()
    {
        $data['Landlord'] = User::where('role',"Landlord")->get();
        $data['projects'] = Project::all();
        $data['users'] = User::where('role','=','Landlord')->get();
        return view('admin.add_landlords',$data);
    }

    public function updateStatus(Request  $request){
        Segregation::where('id','=',$request->id)->update([
           'status' => $request->status
        ]);
        return back()->with('message','status changes success');
    }

    public function Add_Assign_APPART()
    {
        $buildings = Building_list::all();
        $projects = Project::all();
        $flat = Flat_list::all();
        $users = User::where('role','!=','Super Admin')->get();
        $getAssignUser =  Segregation::with('project','building','flat','user')->get();
        return view('admin.assinge_appartments',[
            'buildings' => $buildings,
            'projects' => $projects,
            'flat' => $flat,
            'users_types' => $users,
            'getAssignUser' => $getAssignUser
        ]);
    }

    public function save_landlord(Request $request)
    {
        $user_exist = User::where('email',$request->email)->first();



         if ($request->update_id) {
             $user_data          =  User::where('id',$request->update_id)->first();
            $user_data->title    = $request->title;
            $user_data->name     = $request->name;
            $user_data->email    = $request->email;
            $user_data->password = $request->password;
            $user_data->role     = 'Landlord';
            $user_data->save();
            $id_user             = $user_data->id;

            $profile_exists = Profile::where('user_id',$id_user)->first();
            if($profile_exists)
            {


            $profile                          =  Profile::where('user_id',$id_user)->first();

                //first person detail
                if($request->fee_type == 'percentage') {
                    $profile->percentage = $request->percentage;
                }
                if($request->fee_type == 'fixed') {
                    $profile->fixed = $request->fixed;
                }
                $profile->managment_fee = $request->management_fee;
            $profile->user_id                        = $id_user;
            $profile->first_per_name_in_arabic       = $request->first_per_name_in_arabic;
            $profile->first_per_ref_no               = $request->first_per_ref_no;
            $profile->first_per_nationality          = $request->first_per_nationality;
            $profile->first_per_trade_license_no     = $request->first_per_trade_license_no;
            $profile->first_per_license_authority    = $request->first_per_license_authority;
            $profile->first_per_reg_no               = $request->first_per_reg_no;
            $profile->first_per_vat_emirates         = $request->first_per_vat_emirates;
            $profile->first_per_vat_gcc              = $request->first_per_vat_gcc;
            $profile->first_per_phone                = $request->first_per_phone;
            $profile->first_per_contact1             = $request->first_per_contact1;
            $profile->first_per_contact2             = $request->first_per_contact2;
            $profile->first_per_country              = $request->first_per_country;
            $profile->first_per_city                 = $request->first_per_city;
            $profile->first_per_location             = $request->first_per_location;
            $profile->first_per_pobox                = $request->first_per_pobox;
            $profile->first_per_passport_no          = $request->first_per_passport_no;
            $profile->first_per_passport_expiry_date = $request->first_per_passport_expiry_date;
            $profile->first_per_visa_no              = $request->first_per_visa_no;
            $profile->first_per_visa_no_expiry_date  = $request->first_per_visa_no_expiry_date;
            $profile->first_per_remarks              = $request->first_per_remarks;

            //second person detail
            $profile->second_per_name                = $request->second_per_name;
            $profile->second_per_name_in_arabic      = $request->second_per_name_in_arabic;
            $profile->second_per_designation         = $request->second_per_designation;
            $profile->second_per_phone               = $request->second_per_phone;
            $profile->second_per_mobile              = $request->second_per_mobile;
            $profile->second_per_card_no             = $request->second_per_card_no;
            $profile->second_per_id_no               = $request->second_per_id_no;
            $profile->second_per_vat_country         = $request->second_per_vat_country;
            $profile->second_per_nationality         = $request->second_per_nationality;
            $profile->second_per_profession          = $request->second_per_profession;
            $profile->second_per_address1            = $request->second_per_address1;
            $profile->second_per_address2            = $request->second_per_address2;
            $profile->second_per_vat_reg_no          = $request->second_per_vat_reg_no;
            $profile->second_per_vat_emirates        = $request->second_per_vat_emirates;
            $profile->second_per_remarks             = $request->second_per_remarks;
            $profile->save();
            return back()->withStatus(__('Landlord Updated successfully.'));
            }
            else
            {
            $profile                          = new Profile();
                //first person detail
                if($request->fee_type == 'percentage') {
                    $profile->percentage = $request->percentage;
                }
                if($request->fee_type == 'fixed') {
                    $profile->fixed = $request->fixed;
                }
                $profile->managment_fee = $request->management_fee;
            $profile->user_id                        = $id_user;
            $profile->first_per_name_in_arabic       = $request->first_per_name_in_arabic;
            $profile->first_per_ref_no               = $request->first_per_ref_no;
            $profile->first_per_nationality          = $request->first_per_nationality;
            $profile->first_per_trade_license_no     = $request->first_per_trade_license_no;
            $profile->first_per_license_authority    = $request->first_per_license_authority;
            $profile->first_per_reg_no               = $request->first_per_reg_no;
            $profile->first_per_vat_emirates         = $request->first_per_vat_emirates;
            $profile->first_per_vat_gcc              = $request->first_per_vat_gcc;
            $profile->first_per_phone                = $request->first_per_phone;
            $profile->first_per_contact1             = $request->first_per_contact1;
            $profile->first_per_contact2             = $request->first_per_contact2;
            $profile->first_per_country              = $request->first_per_country;
            $profile->first_per_city                 = $request->first_per_city;
            $profile->first_per_location             = $request->first_per_location;
            $profile->first_per_pobox                = $request->first_per_pobox;
            $profile->first_per_passport_no          = $request->first_per_passport_no;
            $profile->first_per_passport_expiry_date = $request->first_per_passport_expiry_date;
            $profile->first_per_visa_no              = $request->first_per_visa_no;
            $profile->first_per_visa_no_expiry_date  = $request->first_per_visa_no_expiry_date;
            $profile->first_per_remarks              = $request->first_per_remarks;

            //second person detail
            $profile->second_per_name                = $request->second_per_name;
            $profile->second_per_name_in_arabic      = $request->second_per_name_in_arabic;
            $profile->second_per_designation         = $request->second_per_designation;
            $profile->second_per_phone               = $request->second_per_phone;
            $profile->second_per_mobile              = $request->second_per_mobile;
            $profile->second_per_card_no             = $request->second_per_card_no;
            $profile->second_per_id_no               = $request->second_per_id_no;
            $profile->second_per_vat_country         = $request->second_per_vat_country;
            $profile->second_per_nationality         = $request->second_per_nationality;
            $profile->second_per_profession          = $request->second_per_profession;
            $profile->second_per_address1            = $request->second_per_address1;
            $profile->second_per_address2            = $request->second_per_address2;
            $profile->second_per_vat_reg_no          = $request->second_per_vat_reg_no;
            $profile->second_per_vat_emirates        = $request->second_per_vat_emirates;
            $profile->second_per_remarks             = $request->second_per_remarks;
            $profile->save();

            $this->createLanLordUpdateBankDetails($id_user,$request);



                return back()->withStatus(__('Landlord Updated successfully.'));
            }
         }

         else{

             if($user_exist)
        {

        return back()->withStatus(__('Email already exists.'));
        }
        else
        {
            $user_data           = new User();
            $user_data->title    = $request->title;
            $user_data->name     = $request->name;
            $user_data->email    = $request->email;
            $user_data->password = $request->password;
            $user_data->role     = 'Landlord';
            $user_data->phone                = $request->first_per_phone;
            $user_data->save();
            $id_user             = $user_data->id;

                $profile                                 = new Profile();
                //first person detail
                $profile->user_id                        = $id_user;
                $profile->owner_category                 = $request->owner_category;
                $profile->first_per_name_in_arabic       = $request->first_per_name_in_arabic;
                $profile->first_per_ref_no               = $request->first_per_ref_no;
                $profile->first_per_nationality          = $request->first_per_nationality;
                $profile->first_per_trade_license_no     = $request->first_per_trade_license_no;
                $profile->first_per_license_authority    = $request->first_per_license_authority;
                $profile->first_per_reg_no               = $request->first_per_reg_no;
                $profile->first_per_vat_emirates         = $request->first_per_vat_emirates;
                $profile->first_per_vat_gcc              = $request->first_per_vat_gcc;
                $profile->first_per_phone                = $request->first_per_phone;
                $profile->first_per_contact1             = $request->first_per_contact1;
                $profile->first_per_contact2             = $request->first_per_contact2;
                $profile->first_per_country              = $request->first_per_country;
                $profile->first_per_city                 = $request->first_per_city;
                $profile->first_per_location             = $request->first_per_location;
                $profile->first_per_pobox                = $request->first_per_pobox;
                $profile->first_per_passport_no          = $request->first_per_passport_no;
                $profile->first_per_passport_expiry_date = $request->first_per_passport_expiry_date;
                $profile->first_per_visa_no              = $request->first_per_visa_no;
                $profile->first_per_visa_no_expiry_date  = $request->first_per_visa_no_expiry_date;
                $profile->first_per_remarks              = $request->first_per_remarks;
                $profile->website                        = $request->website;





     if ($request->hasFile('passport_attachment'))
    {
     // dd($request->passport_attachment);

    $validator = request()->validate([
            'passport_attachment' => 'mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
        ]);
        if($request->file('passport_attachment')->isValid()) {
            try {
                $file = $request->file('passport_attachment');
                $name =  $file->getClientOriginalName();
                $request->file('passport_attachment')->move("public/files/attachments/", $name);
                $pass_attach = "public/files/attachments/".$name."";
                 $profile->passport_attachment             = $pass_attach;
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
            }
        }
    }

      if ($request->hasFile('visa_attachment'))
    {
     // dd($request->passport_attachment);

    $validator = request()->validate([
            'visa_attachment' => 'mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
        ]);
        if($request->file('visa_attachment')->isValid()) {
            try {
                $file1 = $request->file('visa_attachment');
                $name1 =  $file1->getClientOriginalName();
                $request->file('visa_attachment')->move("public/files/attachments/", $name1);
                $visa_attach = "public/files/attachments/".$name1."";
                  $profile->visa_attachment             = $visa_attach;
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
            }
        }
    }


          if ($request->hasFile('emirates_visa_attachment'))
    {
     // dd($request->passport_attachment);

    $validator = request()->validate([
            'emirates_visa_attachment' => 'mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
        ]);
        if($request->file('emirates_visa_attachment')->isValid()) {
            try {
                $file2 = $request->file('emirates_visa_attachment');
                $name2 =  $file2->getClientOriginalName();
                $request->file('emirates_visa_attachment')->move("public/files/attachments/", $name2);
                $emirates_pasa_attach = "public/files/attachments/".$name2."";
                 $profile->emirates_visa_attachment             = $emirates_pasa_attach;
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
            }
        }
    }




                //second person detail
                $profile->second_per_name                = $request->second_per_name;
                $profile->second_per_name_in_arabic      = $request->second_per_name_in_arabic;
                $profile->relation_with_tenant          = $request->relation_with_tenant;
                $profile->second_per_email               = $request->second_per_phone;
                $profile->second_per_mobile              = $request->second_per_mobile;
                $profile->second_per_card_no             = $request->second_per_card_no;
                $profile->second_per_id_no               = $request->second_per_id_no;
                $profile->second_per_vat_country         = $request->second_per_vat_country;
                $profile->second_per_nationality         = $request->second_per_nationality;
                $profile->second_per_profession          = $request->second_per_profession;
                $profile->other_ref_no                   = $request->other_ref_no;

                $profile->second_per_address1            = $request->second_per_address1;
                $profile->second_per_address2            = $request->second_per_address2;
                $profile->second_per_vat_reg_no          = $request->second_per_vat_reg_no;
                $profile->second_per_vat_emirates        = $request->second_per_vat_emirates;
                $profile->second_per_remarks             = $request->second_per_remarks;
                $profile->emirates_expiry_date           = $request->emirates_expiry_date;
                $profile->emirates_id_no                 = $request->emirates_id_no;
                $profile->realationship_with_landlord    = $request->realationship_with_landlord;
                $profile->save();
                $profile_id = $profile->id;

                if($request->tenants_attachments){
        //       $validator = request()->validate([
        //     'tenants_attachments' => 'mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
        // ]);
      //  dd($request->tenants_attachments);
                 foreach (request('tenants_attachments') as $key => $value) {
                $attachments = new Tenants_attachment();
                $attachments->profiles_id = $profile_id;
                $attachments->comment = $request->comments[$key];

                $file3 = $request->file('tenants_attachments')[$key];
                $name3 =  $file3->getClientOriginalName();
                $request->file('tenants_attachments')[$key]->move("public/files/attachments/", $name3);
                $file = 'public/files/attachments/'.$name3;

                $attachments->attachment =$file;
                $attachments->save();
                }
            }



    if($request->apartment_no){
        foreach ($request->apartment_no as $key => $value) {
             $detail = new Landlord_tenants_detail();
               $detail->user_id = $id_user;
               $detail->profile_id = $profile_id;
                 $detail->contract_amount               = $request->contract_amount[$key];
                $detail->cash_discount                  = $request->cash_discount[$key];
                $detail->net_rent_amount                = $request->net_rent_amount[$key];
                $detail->amount_received                = $request->amount_received[$key];
                $detail->project                     = $request->project[$key];
                $detail->building                    = $request->building[$key];
                $detail->floor                          = $request->floor[$key];
                $detail->apartment_no                      = $request->apartment_no[$key];
                $detail->outstanding_amount             = $request->outstanding_amount[$key];
                $detail->save();

      }
    }























          //  $this->createLanLordBankDetails($id_user,$request);



            return back()->withStatus(__('Landlord registered successfully.'));
         }
        }

    }

    public function createLanLordUpdateBankDetails($id_user,$request){
        $arr = [];
        LanLordBankDetail::where('lanlord_id' , '=' , $id_user)->delte();
        foreach ($request['bank_name'] as $name) {
            $InstallmentCheck = LanLordBankDetail::create([
                'lanlord_id' => $id_user,
                'name' => $name,
            ]);
            array_push($arr,$InstallmentCheck->id);
        }
        foreach ($request['bank_title'] as $key => $title) {
            LanLordBankDetail::where('id','=',$arr[$key])->update([
                'title' => $title,
            ]);
        }
        foreach ($request['bank_branch'] as $key => $branch) {
            LanLordBankDetail::where('id','=',$arr[$key])->update([
                'branch' => $branch,
            ]);
        }
        foreach ($request['bank_address'] as $key => $address) {
            LanLordBankDetail::where('id','=',$arr[$key])->update([
                'address' => $address,
            ]);
        }
        foreach ($request['bank_account_number'] as $key => $account_number) {
            LanLordBankDetail::where('id','=',$arr[$key])->update([
                'account_number' => $account_number,
            ]);
        }
        foreach ($request['bank_iban_number'] as $key => $iban_number) {
            LanLordBankDetail::where('id','=',$arr[$key])->update([
                'iban_number' => $iban_number,
            ]);
        }
        foreach ($request['bank_swift_number'] as $key => $swift_number) {
            LanLordBankDetail::where('id','=',$arr[$key])->update([
                'swift_number' => $swift_number,
            ]);
        }
    }
    public function createLanLordBankDetails($id_user,$request){
        $arr = [];
        foreach ($request['bank_name'] as $name) {
            $InstallmentCheck = LanLordBankDetail::create([
                'lanlord_id' => $id_user,
                'name' => $name,
            ]);
            array_push($arr,$InstallmentCheck->id);
        }
        foreach ($request['bank_title'] as $key => $title) {
            LanLordBankDetail::where('id','=',$arr[$key])->update([
                'title' => $title,
            ]);
        }
        foreach ($request['bank_branch'] as $key => $branch) {
            LanLordBankDetail::where('id','=',$arr[$key])->update([
                'branch' => $branch,
            ]);
        }
        foreach ($request['bank_address'] as $key => $address) {
            LanLordBankDetail::where('id','=',$arr[$key])->update([
                'address' => $address,
            ]);
        }
        foreach ($request['bank_account_number'] as $key => $account_number) {
            LanLordBankDetail::where('id','=',$arr[$key])->update([
                'account_number' => $account_number,
            ]);
        }
        foreach ($request['bank_iban_number'] as $key => $iban_number) {
            LanLordBankDetail::where('id','=',$arr[$key])->update([
                'iban_number' => $iban_number,
            ]);
        }
        foreach ($request['bank_swift_number'] as $key => $swift_number) {
            LanLordBankDetail::where('id','=',$arr[$key])->update([
                'swift_number' => $swift_number,
            ]);
        }
    }

    public function save_tenants(Request $request) {
// dd("fdf");
        $user_exist = User::where('email',$request->email)->first();



        if ($request->update_id) {
            $user_data          =  User::where('id',$request->update_id)->first();
            $user_data->title    = $request->title;
            $user_data->name     = $request->name;
            $user_data->email    = $request->email;
            $user_data->password = $request->password;
            $user_data->role     = 'Tenant';
            $user_data->save();
            $id_user             = $user_data->id;

            $profile_exists = Profile::where('user_id',$id_user)->first();
            if($profile_exists)
            {


                $profile                          =  Profile::where('user_id',$id_user)->first();
                $profile->user_id                        = $id_user;
                $profile->first_per_name_in_arabic       = $request->first_per_name_in_arabic;
                $profile->first_per_ref_no               = $request->first_per_ref_no;
                $profile->first_per_nationality          = $request->first_per_nationality;
                $profile->first_per_trade_license_no     = $request->first_per_trade_license_no;
                $profile->first_per_license_authority    = $request->first_per_license_authority;
                $profile->first_per_reg_no               = $request->first_per_reg_no;
                $profile->first_per_vat_emirates         = $request->first_per_vat_emirates;
                $profile->first_per_vat_gcc              = $request->first_per_vat_gcc;
                $profile->first_per_phone                = $request->first_per_phone;
                $profile->first_per_contact1             = $request->first_per_contact1;
                $profile->first_per_contact2             = $request->first_per_contact2;
                $profile->first_per_country              = $request->first_per_country;
                $profile->first_per_city                 = $request->first_per_city;
                $profile->first_per_location             = $request->first_per_location;
                $profile->first_per_pobox                = $request->first_per_pobox;
                $profile->first_per_passport_no          = $request->first_per_passport_no;
                $profile->first_per_passport_expiry_date = $request->first_per_passport_expiry_date;
                $profile->first_per_visa_no              = $request->first_per_visa_no;
                $profile->first_per_visa_no_expiry_date  = $request->first_per_visa_no_expiry_date;
                $profile->first_per_remarks              = $request->first_per_remarks;

                //second person detail
                $profile->second_per_name                = $request->second_per_name;
                $profile->second_per_name_in_arabic      = $request->second_per_name_in_arabic;
                $profile->second_per_designation         = $request->second_per_designation;
                $profile->second_per_phone               = $request->second_per_phone;
                $profile->second_per_mobile              = $request->second_per_mobile;
                $profile->second_per_card_no             = $request->second_per_card_no;
                $profile->second_per_id_no               = $request->second_per_id_no;
                $profile->second_per_vat_country         = $request->second_per_vat_country;
                $profile->second_per_nationality         = $request->second_per_nationality;
                $profile->second_per_profession          = $request->second_per_profession;
                $profile->second_per_address1            = $request->second_per_address1;
                $profile->second_per_address2            = $request->second_per_address2;
                $profile->second_per_vat_reg_no          = $request->second_per_vat_reg_no;
                $profile->second_per_vat_emirates        = $request->second_per_vat_emirates;
                $profile->second_per_remarks             = $request->second_per_remarks;
                $profile->save();
                return back()->withStatus(__('Tenant Updated successfully.'));
            }
            else
            {
                $profile                          = new Profile();
                $profile->user_id                        = $id_user;
                $profile->first_per_name_in_arabic       = $request->first_per_name_in_arabic;
                $profile->first_per_ref_no               = $request->first_per_ref_no;
                $profile->first_per_nationality          = $request->first_per_nationality;
                $profile->first_per_trade_license_no     = $request->first_per_trade_license_no;
                $profile->first_per_license_authority    = $request->first_per_license_authority;
                $profile->first_per_reg_no               = $request->first_per_reg_no;
                $profile->first_per_vat_emirates         = $request->first_per_vat_emirates;
                $profile->first_per_vat_gcc              = $request->first_per_vat_gcc;
                $profile->first_per_phone                = $request->first_per_phone;
                $profile->first_per_contact1             = $request->first_per_contact1;
                $profile->first_per_contact2             = $request->first_per_contact2;
                $profile->first_per_country              = $request->first_per_country;
                $profile->first_per_city                 = $request->first_per_city;
                $profile->first_per_location             = $request->first_per_location;
                $profile->first_per_pobox                = $request->first_per_pobox;
                $profile->first_per_passport_no          = $request->first_per_passport_no;
                $profile->first_per_passport_expiry_date = $request->first_per_passport_expiry_date;
                $profile->first_per_visa_no              = $request->first_per_visa_no;
                $profile->first_per_visa_no_expiry_date  = $request->first_per_visa_no_expiry_date;
                $profile->first_per_remarks              = $request->first_per_remarks;

                //second person detail
                $profile->second_per_name                = $request->second_per_name;
                $profile->second_per_name_in_arabic      = $request->second_per_name_in_arabic;
                $profile->second_per_designation         = $request->second_per_designation;
                $profile->second_per_phone               = $request->second_per_phone;
                $profile->second_per_mobile              = $request->second_per_mobile;
                $profile->second_per_card_no             = $request->second_per_card_no;
                $profile->second_per_id_no               = $request->second_per_id_no;
                $profile->second_per_vat_country         = $request->second_per_vat_country;
                $profile->second_per_nationality         = $request->second_per_nationality;
                $profile->second_per_profession          = $request->second_per_profession;
                $profile->second_per_address1            = $request->second_per_address1;
                $profile->second_per_address2            = $request->second_per_address2;
                $profile->second_per_vat_reg_no          = $request->second_per_vat_reg_no;
                $profile->second_per_vat_emirates        = $request->second_per_vat_emirates;
                $profile->second_per_remarks             = $request->second_per_remarks;
                $profile->save();
                return back()->withStatus(__('Tenant Updated successfully.'));
            }
        }

        else{

            if($user_exist)
            {

                return back()->withStatus(__('Email already exists.'));
            }
            else
            {

                  // dd( $request->apartment_no);
                $user_data           = new User();
                $user_data->title    = $request->title;
                $user_data->name     = $request->name;
                $user_data->email    = $request->email;
                $user_data->password = $request->password;
                $user_data->phone                = $request->first_per_phone;
                $user_data->role     = 'Tenant';
                $user_data->save();
                $id_user             = $user_data->id;












                           $profile                                 = new Profile();
                //first person detail
                $profile->user_id                        = $id_user;
                $profile->owner_category                 = $request->owner_category;
                $profile->first_per_name_in_arabic       = $request->first_per_name_in_arabic;
                $profile->first_per_ref_no               = $request->first_per_ref_no;
                $profile->first_per_nationality          = $request->first_per_nationality;
                $profile->first_per_trade_license_no     = $request->first_per_trade_license_no;
                $profile->first_per_license_authority    = $request->first_per_license_authority;
                $profile->first_per_reg_no               = $request->first_per_reg_no;
                $profile->first_per_vat_emirates         = $request->first_per_vat_emirates;
                $profile->first_per_vat_gcc              = $request->first_per_vat_gcc;
                $profile->first_per_phone                = $request->first_per_phone;
                $profile->first_per_contact1             = $request->first_per_contact1;
                $profile->first_per_contact2             = $request->first_per_contact2;
                $profile->first_per_country              = $request->first_per_country;
                $profile->first_per_city                 = $request->first_per_city;
                $profile->first_per_location             = $request->first_per_location;
                $profile->first_per_pobox                = $request->first_per_pobox;
                $profile->first_per_passport_no          = $request->first_per_passport_no;
                $profile->first_per_passport_expiry_date = $request->first_per_passport_expiry_date;
                $profile->first_per_visa_no              = $request->first_per_visa_no;
                $profile->first_per_visa_no_expiry_date  = $request->first_per_visa_no_expiry_date;
                $profile->first_per_remarks              = $request->first_per_remarks;
                $profile->website                        = $request->website;





     if ($request->hasFile('passport_attachment'))
    {
     // dd($request->passport_attachment);

    $validator = request()->validate([
            'passport_attachment' => 'mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
        ]);
        if($request->file('passport_attachment')->isValid()) {
            try {
                $file = $request->file('passport_attachment');
                $name =  $file->getClientOriginalName();
                $request->file('passport_attachment')->move("public/files/attachments/", $name);
                $pass_attach = "public/files/attachments/".$name."";
                 $profile->passport_attachment             = $pass_attach;
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
            }
        }
    }

      if ($request->hasFile('visa_attachment'))
    {
     // dd($request->passport_attachment);

    $validator = request()->validate([
            'visa_attachment' => 'mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
        ]);
        if($request->file('visa_attachment')->isValid()) {
            try {
                $file1 = $request->file('visa_attachment');
                $name1 =  $file1->getClientOriginalName();
                $request->file('visa_attachment')->move("public/files/attachments/", $name1);
                $visa_attach = "public/files/attachments/".$name1."";
                  $profile->visa_attachment             = $visa_attach;
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
            }
        }
    }


          if ($request->hasFile('emirates_visa_attachment'))
    {
     // dd($request->passport_attachment);

    $validator = request()->validate([
            'emirates_visa_attachment' => 'mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
        ]);
        if($request->file('emirates_visa_attachment')->isValid()) {
            try {
                $file2 = $request->file('emirates_visa_attachment');
                $name2 =  $file2->getClientOriginalName();
                $request->file('emirates_visa_attachment')->move("public/files/attachments/", $name2);
                $emirates_pasa_attach = "public/files/attachments/".$name2."";
                 $profile->emirates_visa_attachment             = $emirates_pasa_attach;
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
            }
        }
    }




                //second person detail
                $profile->second_per_name                = $request->second_per_name;
                $profile->second_per_name_in_arabic      = $request->second_per_name_in_arabic;
                $profile->relation_with_tenant          = $request->relation_with_tenant;
                $profile->second_per_email               = $request->second_per_phone;
                $profile->second_per_mobile              = $request->second_per_mobile;
                $profile->second_per_card_no             = $request->second_per_card_no;
                $profile->second_per_id_no               = $request->second_per_id_no;
                $profile->second_per_vat_country         = $request->second_per_vat_country;
                $profile->second_per_nationality         = $request->second_per_nationality;
                $profile->second_per_profession          = $request->second_per_profession;
                $profile->other_ref_no                   = $request->other_ref_no;

                $profile->second_per_address1            = $request->second_per_address1;
                $profile->second_per_address2            = $request->second_per_address2;
                $profile->second_per_vat_reg_no          = $request->second_per_vat_reg_no;
                $profile->second_per_vat_emirates        = $request->second_per_vat_emirates;
                $profile->second_per_remarks             = $request->second_per_remarks;
                $profile->emirates_expiry_date           = $request->emirates_expiry_date;
                $profile->emirates_id_no                 = $request->emirates_id_no;
                $profile->realationship_with_landlord  = $request->realationship_with_tenants;
                $profile->save();
                $profile_id = $profile->id;

                if($request->tenants_attachments){
        //       $validator = request()->validate([
        //     'tenants_attachments' => 'mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
        // ]);
      //  dd($request->tenants_attachments);
                 foreach (request('tenants_attachments') as $key => $value) {
                $attachments = new Tenants_attachment();
                $attachments->profiles_id = $profile_id;
                $attachments->comment = $request->comments[$key];

                $file3 = $request->file('tenants_attachments')[$key];
                $name3 =  $file3->getClientOriginalName();
                $request->file('tenants_attachments')[$key]->move("public/files/attachments/", $name3);
                $file = 'public/files/attachments/'.$name3;

                $attachments->attachment =$file;
                $attachments->save();
                }
            }



    if($request->apartment_no){
        foreach ($request->apartment_no as $key => $value) {
             $detail = new Tenants_detail();
               $detail->user_id = $id_user;
               $detail->profile_id = $profile_id;
                $detail->contract_amount                = $request->contract_amount[$key];
                $detail->cash_discount                  = $request->cash_discount[$key];
                $detail->net_rent_amount                = $request->net_rent_amount[$key];
                $detail->amount_received                = $request->amount_received[$key];
                $detail->project                        = $request->project[$key];
                $detail->building                       = $request->building[$key];
                $detail->floor                          = $request->floor[$key];
                $detail->apartment_no                   = $request->apartment_no[$key];
                $detail->outstanding_amount             = $request->outstanding_amount[$key];
                $detail->save();

      }
    }



                return back()->withStatus(__('Tenant registered successfully.'));
            }
        }

    }

//    public function save_tenants(Request $request)
//    {
//         $user_exist = User::where('email',$request->email)->first();
//
//        if($user_exist)
//        {
//
//        return back()->withStatus(__('Email already exists.'));
//        }
//        else{
//
//       $user_data = new User();
//        $user_data->title = $request->title;
//        $user_data->name = $request->name;
//        $user_data->email = $request->email;
//        $user_data->password = $request->password;
//        $user_data->role = 'Tenant';
//        $user_data->save();
//        $id_user = $user_data->id;
//
//        $profile = new Profile();
//        $profile->user_id = $id_user;
//        $profile->name_in_arabic = $request->name_in_arabic;
//        $profile->phone = $request->phone;
//        $profile->ref_no = $request->ref_no;
//        $profile->location = $request->location;
//        $profile->country_id = $request->country_id;
//        $profile->city_id = $request->city_id;
//        $profile->company = $request->company;
//        $profile->po_box = $request->po_box;
//        $profile->fax = $request->fax;
//        $profile->acc_no_db = $request->acc_no_db;
//        $profile->acc_no_cr = $request->acc_no_cr;
//        $profile->booking_no = $request->booking_no;
//        $profile->contact_per_name = $request->contact_per_name;
//        $profile->contact_per_name_arabic = $request->contact_per_name_arabic;
//        $profile->designation = $request->designation;
//        $profile->contact_per_phone = $request->contact_per_phone;
//        $profile->contact_per_mobile = $request->contact_per_mobile;
//        $profile->card_no = $request->card_no;
//        $profile->id_no = $request->id_no;
//        $profile->contact_per_phone = $request->contact_per_phone;
//        $profile->nationality = $request->nationality;
//        $profile->vat_country = $request->vat_country;
//        $profile->profession = $request->profession;
//        $profile->address1 = $request->address1;
//        $profile->address2 = $request->address2;
//        $profile->vat_reg_no = $request->vat_reg_no;
//        $profile->vat_emirates = $request->vat_emirates;
//        $profile->contact_per_remarks = $request->contact_per_remarks;
//        $profile->save();
//        return back()->withStatus(__('Tenant registered successfully.'));
//      }
//    }


    public function add_contracts()
    {
        $contracts = Contract::all();
        return view('admin.add_contracts',[
            'contracts' => $contracts
        ]);
    }


     public function add_short_contracts()
    {
        $contracts = Contract::all();
        return view('admin.add_short_contracts',[
            'contracts' => $contracts
        ]);
    }


  public function Sale_Purchase_Contract()
    {
        $contracts = Contract::all();
        return view('admin.sale_purchase_contract',[
            'contracts' => $contracts
        ]);
    }

    public  function getLanlord($id){
        $data = Segregation::where('flat_id','=',$id)->with('user','profile')->first();
        return collect([
            'status' => true,
           'data' => $data
        ]);
    }

    public function create_contract()
    {
        $projects = Project::all();
        $users = User::where('role','=','Tenant')->get();
        return view('admin.create_contracts',[
            'projects' => $projects,
            'users' => $users
        ]);
    }

     public function create_short_contracts()
    {
        $projects = Project::all();
        $users = User::where('role','=','Tenant')->get();
        return view('admin.create_short_contracts',[
            'projects' => $projects,
            'users' => $users
        ]);
    }


      public function create_sale_purchase_contract()
    {
        $projects = Project::all();
        $users = User::where('role','=','Tenant')->get();
        return view('admin.create_sale_purchase_contract',[
            'projects' => $projects,
            'users' => $users
        ]);
    }


     public function utility_management()
    {
        $data['projects'] = Project::all();
        $data['users'] = User::where('role','=','Tenant')->get();
        $data['owner'] = User::where('role','=','Landlord')->get();
        return view('admin.utility_management',$data);
    }


      public function utility_management_show()
    {
        $contracts = Contract::all();
        return view('admin.utility_management_show',[
            'contracts' => $contracts
        ]);
    }

    public function save_short_contracts(Request $request)
    {
        // dd($request->credit_to_owner);
      $short                             = new  Short_contract();
      $short->user_id                    = Auth::user()->id;
      $short->project_id                 = $request->project;
      $short->building_id                = $request->building;
      $short->floor_id                   = $request->floor;
      $short->flat_id                    = $request->flat;
      $short->landlord_id                = $request->lanlord_name;
      $short->tenant_id                  = $request->tenant_name;
      $short->name                       = $request->name;
      $short->email                      = $request->email1;
      $short->contact_1                  = $request->contact1;
      $short->contact_2                  = $request->contact2;
      $short->check_in                   = $request->check_in_date;
      $short->check_out                  = $request->check_out_date;
      $short->number_of_person           = $request->num_of_person;
      $short->adults                     = $request->adults;
      $short->childrens                  = $request->childrens;
      $short->management_fee             = $request->managment_fee;
      $short->terms_and_condition        = $request->terms_conditions;
      $short->renew_terms_and_conditions = $request->renew_terms_conditions;
      $short->status                     = 0;
      $short->save();
      $short_contract_id                 = $short->id;

      if($request->on_ac_of){
          $ch = 1;
          $charge="";
          foreach ($request->on_ac_of as $key => $value) {
          $charge = $request->credit_to_owner[$key];
        $short_external = new Short_contract_external();
        $short_external->short_contract_id = $short_contract_id;
        $short_external->on_ac_of          = $request->on_ac_of[$key];
        $short_external->particular        = $request->particular[$key];
        if($ch<=2)
        {
        $short_external->from_date         = $request->check_date_security[$key];
        $short_external->to_date           = $request->check_issue_security[$key];
        $ch++;
        }

        $short_external->amount            = $request->amount[$key];
        $short_external->vat               = $request->vat_tax[$key];
        $short_external->net_amount        = $request->net_amount[$key];
        $short_external->vat_amount        = $request->vat_amount[$key];
        $short_external->with_vat_amount   = $request->with_vat_amount[$key];
        if($charge)
        {
           $short_external->credit_to_owner   = 1;
        }
        else{
           $short_external->credit_to_owner   = 0;
        }



        $short_external->status            = 0;
        $short_external->save();
      }
     }

     if($request->accounts_head)
     {
         foreach ($request->accounts_head as $key => $value) {
            $short_internal     = new Short_contract_internal();
            $short_internal->short_contract_id = $short_contract_id;
            $short_internal->account_head      = $request->accounts_head[$key];
            $short_internal->particulars       = $request->particulars_internal_use[$key];
            $short_internal->party             = $request->party[$key];
            $short_internal->terms             = $request->terms[$key];
            $short_internal->percentage_fixed            = $request->percentage_fixed_amount[$key];
            $short_internal->rent_amount                 = $request->rent_amount[$key];
            $short_internal->commision_amount            = $request->commision_amount[$key];
            $short_internal->commission_term             = $request->vat_tax_type[$key];
            $short_internal->email                       = $request->email[$key];
            $short_internal->vat                         = $request->vat1[$key];
            $short_internal->net_amount                  = $request->net_amount1[$key];
            $short_internal->vat_amount                  = $request->vat_amount1[$key];
            $short_internal->with_vat_amount             = $request->with_vat_amount1[$key];
            $short_internal->charge_to_owner             = 0;
            $short_internal->status                      = 0;
            $short_internal->save();
         }
     }

      if($request->attachment)
      {
          foreach ($request->attachment as $key => $value) {
	        $short_attachment = new Short_contract_attachment();
	        $short_attachment->short_contract_id = $short_contract_id;
	        $short_attachment->comment = $request->comments[$key];

	            $file3 = $request->file('attachment')[$key];
                $name3 =  $file3->getClientOriginalName();
                $request->file('attachment')[$key]->move("public/files/attachments/", $name3);
                $file = 'public/files/attachments/'.$name3;

                $short_attachment->attachment =$file;
                $short_attachment->save();
          }

      }












     //Sending sms Api
//      $contact_one = $request->contact1;
//      $contact_two = $request->contact2;
//      $message =urlencode("Dear customer,
// Your May bill for 0567549180 is available.

// Total amount to pay before 15 JUN 2020: AED 180.60 (VAT inclusive)

// To view and pay your bill, please click here https://www.etisalat.ae/en/mybill.jsp?bk=PGXJkqWlAHnj9FNhm25mYw%3D%3D

// Bill summary (AED):
// Plan, Add-ons & Devices: 150.00
// Out-of-plan usage charges: 5.80
// VAS & third-party services: 17.00
// VAT on taxable services: 7.80
// Balance from your last bill: 194.89
// Payments processed: -194.89

// Thank you for choosing etisalat");

//      $api = "http://customers.smsmarketing.ae/app/smsapi/index.php?key=5f7c6bbc246fb&campaign=6721&routeid=39&type=text&contacts={$contact_one}&senderid=786&msg={$message}";


// $get_result = file_get_contents($api);

     return redirect()->back();
    }



    public function Save_Utility_management(Request $request)
    {

      $utility                   = new Utility_management();
      $utility->user_id          = Auth::user()->id;
      $utility->project_id       = $request->project;
      $utility->building_id      = $request->building;
      $utility->floor_id         = $request->floor;
      $utility->flat_id          = $request->flat;
      $utility->owner_id         = $request->owner_id;
      $utility->tenant_id        = $request->tenant_id;
      $utility->owner_contact_no = $request->owner_contact_no;
      $utility->owner_email      = $request->owner_email;
      $utility->tenant_contact_no= $request->tenant_contact_no;
      $utility->tenant_email     = $request->tenant_email;
      $utility->status           = 0;
      $utility->save();
      $utility_management_id                 = $utility->id;




        if($request->attachment)
      {
          foreach ($request->attachment as $key => $value) {
	        $utility_attachment = new Utility_attachment();
	        $utility_attachment->utility_management_id = $utility_management_id;
	        $utility_attachment->comment = $request->comments[$key];

	            $file3 = $request->file('attachment')[$key];
                $name3 =  $file3->getClientOriginalName();
                $request->file('attachment')[$key]->move("public/files/attachments/", $name3);
                $file = 'public/files/attachments/'.$name3;

                $utility_attachment->attachment =$file;
                $utility_attachment->save();
          }

      }


       if($request->accounts_head)
     {
         foreach ($request->accounts_head as $key => $value) {
            $utility_internal                          = new Utility_management_internal();
            $utility_internal->utility_management_id   = $utility_management_id;
            $utility_internal->account_head            = $request->accounts_head[$key];
            $utility_internal->particulars       = $request->particulars_internal_use[$key];
            $utility_internal->party             = $request->party[$key];
            $utility_internal->terms                       = $request->terms[$key];
            $utility_internal->percentage_fixed            = $request->percentage_fixed_amount[$key];
            $utility_internal->rent_amount                 = $request->rent_amount[$key];
            $utility_internal->commision_amount            = $request->commision_amount[$key];
            $utility_internal->commission_term             = $request->vat_tax_type[$key];
            $utility_internal->email                       = $request->email[$key];
            $utility_internal->vat                         = $request->vat1[$key];
            $utility_internal->net_amount                  = $request->net_amount1[$key];
            $utility_internal->vat_amount                  = $request->vat_amount1[$key];
            $utility_internal->with_vat_amount             = $request->with_vat_amount1[$key];
            $utility_internal->charge_to_owner             = 0;
            $utility_internal->status                      = 0;
            $utility_internal->save();
         }
     }

      if($request->on_ac_of)
      {
          foreach ($request->on_ac_of as $key => $value) {
	        $utility_external                        = new Utility_management_external();
	        $utility_external->utility_management_id = $utility_management_id;
	        $utility_external->utility_type          = $request->on_ac_of[$key];
	        $utility_external->particular            = $request->particular[$key];
	        $utility_external->ref_no                = $request->ref_no[$key];
	        $utility_external->amount                = $request->amount[$key];
	        $utility_external->date_from             = $request->from[$key];
	        $utility_external->date_to               = $request->to[$key];
	        $utility_external->due_date              = $request->due_date[$key];
	        $utility_external->next_billing_date     = $request->next_billing_date[$key];
	        $utility_external->frequency             = $request->frequency[$key];
	        $utility_external->charge_to_owner       = $request->charge_to_owner[$key];
	        $utility_external->equal_to_owner        = $request->equal_to_owner[$key];
	        $utility_external->email_to_owner        = $request->email_to_owner[$key];
	        $utility_external->charge_to_company     = $request->charge_to_company[$key];
	        $utility_external->charge_to_tenant      = $request->charge_to_tenant[$key];
	        $utility_external->company_to_head       = 0;
	        $utility_external->status                = 0;

	   //       if($request->utility_attchment[$key])
    //   {
	   //          $file = $request->file('utility_attchment')[$key];
    //             $name =  $file->getClientOriginalName();
    //             $request->file('utility_attchment')[$key]->move("public/files/attachments/", $name);
    //             $file1 = 'public/files/attachments/'.$name;
    //             $utility_external->attachment =$file1;
    //   }





	        $utility_external->save();
          }
      }







       return back()->withStatus(__('Utility Management Saved successfully.'));
    }


    public function createOwnerReport($request,$amount,$contractId){
        OwnerReport::create([
            'contract_id' => $contractId,
            'owner_id' => $request->lanlord_name,
            'user_id' => $request->tenant,
            'debit' => $amount,
        ]);
    }


    public function save_contract(Request  $request){
        $contract                            = new Contract();
        $contract->project                   = $request->project;
        $contract->tenant                    = $request->tenant;
        $contract->building_id               = $request->building;
        $contract->floor_id                  = $request->floor;
        $contract->flat_id                   = $request->flat;
        $contract->lanlord_name              = $request->lanlord_name;
        $contract->contract_from             = $request->contract_start;
        $contract->contract_to               = $request->contract_end;
        $contract->contract_amount           = $request->totalAmount;
        $contract->installments_no           = $request->no_installments;
        $contract->terms_and_condition       = $request->terms_conditions;
        $contract->renew_terms_and_condition = $request->renew_terms_conditions;
        $contract->property_size             = $request->property_size;
        $contract->plot_no                   = $request->plot_no;
        $contract->permisses_no              = $request->permisses_no;
        $contract->location                  = $request->location;
        $contract->flat_pro_type             = $request->property_type;


        if($request->hasFile('scanned_doc')) {
            $path = 'uploads/scanned/docs';
            $name = $request->file('scanned_doc')->getClientOriginalName();
            $fileName = $path.'/'.$name;
            $request->file('scanned_doc')->move($path,$name);
        }
        $contract->scanned_doc = isset($fileName) ? $fileName : '--';
        $contract->status = 0;
        $contract->save();
        $long_contract_id = $contract->id;

          if($request->accounts_head)
     {
         foreach ($request->accounts_head as $key => $value) {
             if($value == 'Management Fee') {
                $this->createOwnerReport($request,$request->commision_amount[$key],$contract->id);
             }
            $long_internal                              = new Long_contract_internal();
            $long_internal->long_contract_id            = $long_contract_id;
            $long_internal->account_head                = $request->accounts_head[$key];
            $long_internal->particulars                 = $request->particulars_internal_use[$key];
            $long_internal->party                       = $request->party[$key];
            $long_internal->terms                       = $request->terms[$key];
            $long_internal->percentage_fixed            = $request->percentage_fixed_amount[$key];
            $long_internal->rent_amount                 = $request->rent_amount[$key];
            $long_internal->commision_amount            = $request->commision_amount[$key];
            $long_internal->commission_term             = $request->vat_tax_type[$key];
            $long_internal->email                       = $request->email[$key];
            $long_internal->vat                         = $request->vat1[$key];
            $long_internal->net_amount                  = $request->net_amount1[$key];
            $long_internal->vat_amount                  = $request->vat_amount1[$key];
            $long_internal->with_vat_amount             = $request->with_vat_amount1[$key];
            $long_internal->charge_to_owner             = 0;
            $long_internal->status                      = 0;
//            $long_internal->save();
         }
     }

        if($request->serial_no){
        $this->createSecurityDeposite($contract->id,$request->all());
        }

        if($request->other_serial_no){
        $this->createContractOtherName($contract->id,$request->all());
        }

        if($request->no_installment){
        $this->createInstallmentsCheck($contract->id,$request->all());
        }


        if($request->from_value){
        $this->createContractAttachments($contract->id,$request->all());
        }

        return redirect('Add_Contracts')->withStatus(__('Contract registered successfully.'));
    }

    public function createContractOtherName($contractId , $request){
        $arr = [];
        foreach ($request['other_serial_no'] as $serial_no) {
            $InstallmentCheck = ContractOtherName::create([
                'contract_id' => $contractId,
                'sr_no' => $serial_no,
            ]);
            array_push($arr,$InstallmentCheck->id);
        }
        foreach ($request['other_particular'] as $key => $other_particular) {
            ContractOtherName::where('id','=',$arr[$key])->update([
                'particular' => $other_particular,
            ]);
        }
        foreach ($request['other_amount'] as $key => $other_amount) {
            ContractOtherName::where('id','=',$arr[$key])->update([
                'amount' => $other_amount,
            ]);
        }
        foreach ($request['other_vat'] as $key => $other_vat) {
            ContractOtherName::where('id','=',$arr[$key])->update([
                'vat' => $other_vat,
            ]);
        }
    }

    public function createSecurityDeposite($contractId , $request){
        $arr = [];
        foreach ($request['serial_no'] as $serial_no) {
            $InstallmentCheck = ContractSecurityDeposite::create([
                'contract_id' => $contractId,
                'serial_no' => $serial_no,
            ]);
            array_push($arr,$InstallmentCheck->id);
        }


          foreach ($request['security_deposit_amount'] as $key => $security_deposit_amounts) {
            ContractSecurityDeposite::where('id','=',$arr[$key])->update([
                'security_deposit_amount' => $security_deposit_amounts,
            ]);
        }


        foreach ($request['check_no_security'] as $key => $check_no) {
            ContractSecurityDeposite::where('id','=',$arr[$key])->update([
                'check_no' => $check_no,
            ]);
        }
        foreach ($request['check_date_security'] as $key => $check_date_security) {
            ContractSecurityDeposite::where('id','=',$arr[$key])->update([
                'check_date' => $check_date_security,
            ]);
        }
        foreach ($request['check_issue_security'] as $key => $check_issue_security) {
            ContractSecurityDeposite::where('id','=',$arr[$key])->update([
                'check_issue' => $check_issue_security,
            ]);
        }
        foreach ($request['party_name_security'] as $key => $party_name_security) {
            ContractSecurityDeposite::where('id','=',$arr[$key])->update([
                'party_name' => $party_name_security,
            ]);
        }
        foreach ($request['check_deposite_security'] as $key => $check_deposite_security) {
            ContractSecurityDeposite::where('id','=',$arr[$key])->update([
                'check_deposite' => $check_deposite_security,
            ]);
        }
    }

    public function createInstallmentsCheck($contractId , $request){
        $arr = [];
        foreach ($request['no_installment'] as $no_installment) {
            $InstallmentCheck = InstallmentCheck::create([
                'contract_id' => $contractId,
                'installment_no' => $no_installment,
            ]);
            array_push($arr,$InstallmentCheck->id);
        }
        foreach ($request['amount'] as $key => $amount) {
            InstallmentCheck::where('id','=',$arr[$key])->update([
                'amount' => $amount,
            ]);
        }
        foreach ($request['check_no'] as $key => $check_no) {
            InstallmentCheck::where('id','=',$arr[$key])->update([
                'check_no' => $check_no,
            ]);
        }
        foreach ($request['check_date'] as $key => $check_date) {
            InstallmentCheck::where('id','=',$arr[$key])->update([
                'check_date' => $check_date,
            ]);
        }
        foreach ($request['check_issue'] as $key => $check_issue) {
            InstallmentCheck::where('id','=',$arr[$key])->update([
                'check_issue' => $check_issue,
            ]);
        }
        foreach ($request['party_name'] as $key => $party_name) {
            InstallmentCheck::where('id','=',$arr[$key])->update([
                'party_name' => $party_name,
            ]);
        }
        foreach ($request['selectOpt'] as $key => $selectOpt) {
            InstallmentCheck::where('id','=',$arr[$key])->update([
                'drop_down_opt' => $selectOpt,
            ]);
        }

         foreach ($request['particular'] as $key => $particulars) {
            InstallmentCheck::where('id','=',$arr[$key])->update([
                'particular' => $particulars,
            ]);
        }


          foreach ($request['pay_type'] as $key => $pay_types) {
            InstallmentCheck::where('id','=',$arr[$key])->update([
                'pay_type' => $pay_types,
            ]);
        }


    }
    public function createContractAttachments($contractId,$request){
        $arr = [];

        foreach ($request['to_value'] as $toValue) {
            $contractAttachment = Contract_attachment::create([
                'contract_id' => $contractId,
                'comment' => $toValue
            ]);
            array_push($arr,$contractAttachment->id);
        }




             foreach ($request['from_value'] as $key => $FromValue) {
            if($FromValue) {
                $name = $FromValue->getClientOriginalName();
                $path = 'uploads/file';
                $fileName = $path.'/'.$name;
                $FromValue->move($path,$name);
            }
            Contract_attachment::where('id','=',$arr[$key])->update([
                'attachment' => $fileName
            ]);
        }


    }

    public function edit_contract($id){
        $projects = Project::all();
        $users = User::where('role','=','Tenant')->get();
        $contract = Contract::where('id','=',$id)->with('installment_check','attachments')->first();
        return view('admin.edit_contracts',[
            'projects' => $projects,
            'users' => $users,
            'contract' => $contract
        ]);
    }


    public function view_contract($id){
        $data['contract'] = Contract::where('id','=',$id)->first();
        return view('admin.view_contracts',$data);
    }



    public function update_contract($id, Request $request){
        $contract = Contract::where('id','=',$id)->first();
        dd($contract);
        $contract->project = $request->project;
        $contract->tenant = $request->tenant;
        $contract->building_id = $request->building;
        $contract->floor_id = $request->floor;
        $contract->flat_id = $request->flat;
        $contract->flat_security = $request->security_deposite;
        $contract->contract_from = $request->contract_start;
        $contract->contract_to = $request->contract_end;
        $contract->contract_amount = $request->totalAmount;
        $contract->installments_no = $request->no_installments;
        $contract->terms_and_condition = $request->terms_conditions;
        $contract->renew_terms_and_condition = $request->renew_terms_conditions;
        if($request->hasFile('scanned_doc')) {
            $path = 'uploads/scanned/docs';
            $name = $request->file('scanned_doc')->getClientOriginalName();
            $fileName = $path.'/'.$name;
            $request->file('scanned_doc')->move($path,$name);
        }
        $contract->scanned_doc = isset($fileName) ? $fileName : '--';
        $contract->status = 0;
        $contract->save();

        if($request->no_installment){
        $this->createInstallmentsCheck($contract->id,$request->all());
        }

        if($request->from_value){
        $this->createContractAttachments($contract->id,$request->all());
        }

        return redirect('Add_Contracts')->withStatus(__('Tenant registered successfully.'));
    }

    public function get_buildings(Request $request)
    {
        //
        $building = Building_list::where('project_id',$request->pro_id)->get();
        // dd($building);
        //echo $request->pro_id;

         $op="";
    if ($building) {
        $op.="<option value='' selected disabled>Select One</option>";
      foreach ($building as  $value) {
        $op.="<option value='".$value->id."'>".$value->name."</option>";
      }
    }
    else {
      $op.="<option value=''>No Buildings  Found</option>";
    }
    echo $op;
    }

     public function get_floors(Request $request)
    {
        $floor = Floor_list::where('building_id',$request->building_id)->get();

         $op="";
    if ($floor) {
        $op.="<option value='' selected disabled>Select One</option>";
      foreach ($floor as  $value) {
        $op.="<option value='".$value->building_id."'>".$value->name."</option>";
      }
    }
         else {
           $op.="<option value=''>No Floors Found</option>";
         }
        echo $op;
    }



    //multiple returns
       public function get_floors_detail(Request $request)
    {
        $floor = Floor_list::where('building_id',$request->building_id)->get();
        $building = Building_list::where('id',$request->building_id)->first();
        // dd($building->l);

         $op="";
    if ($floor) {
        $op.="<option value='' selected disabled>Select One</option>";
      foreach ($floor as  $value) {
        $op.="<option value='".$value->building_id."'>".$value->name."</option>";
      }
    }
         else {
           $op.="<option value=''>No Floors Found</option>";
         }
        echo $op."**".$building->location."**".$building->permisses_no."**".$building->plot_no;
    }


        public function get_flats(Request $request)
    {
       $flat = Flat_list::where('building_id',$request->building_id)->get();

         $op="";
    if ($flat) {
        $op.="<option value='' selected disabled>Select One</option>";
      foreach ($flat as  $value) {
        $op.="<option value='".$value->unit_code."'>".$value->unit_code."</option>";
      }
    }
    else {
      $op.="<option value=''>No Flats Found</option>";
    }
    echo $op;
    }

    public function get_lanlords_detail(Request $request)
    {

     $result = Flat_list::where('unit_code',$request->flat_id)->first();
    //  dd($result->Flat_type_name->name);


         echo $result->owner_id."**".$result->Landlord_name->name."**".$result->property_size."**".$result->flat_type_id."**".$result->Flat_type_name->name."**".$result->management_fee;


    }

     public function get_management_fee(Request $request)
    {

     $result = Flat_list::where('unit_code',$request->flat_id)->first();
         echo $result->management_fee;


    }

        public function supplier_list()
        {
            $data['supplier'] = Supplier::all();
             return view('admin.supplier_list',$data);
        }

          public function Add_Supplier()
        {
            // $data['supplier'] = Supplier::all();
            return view('admin.supplier');
        }

         public function post_dated_cheques()
        {
            $data['supplier'] = Supplier::all();
             return view('admin.post_dated_cheques',$data);
        }


         public function create_post_dated_cheques()
        {
            // $data['supplier'] = Supplier::all();
        $projects = Project::all();
        $landlord = User::where('role','=','Landlord')->get();
        $tenant    = User::where('role','=','Tenant')->get();
        return view('admin.create_post_dated_cheques',[
            'projects' => $projects,
            'tenant' => $tenant,
            'landlord' => $landlord
        ]);


        }



        public function Save_supplier(Request $request)
        {
              $supplier               = new Supplier();
              $supplier->name         = $request->name;
              $supplier->address      = $request->address;
              $supplier->contact1     = $request->contact1;
              $supplier->contact2     = $request->contact2;
              $supplier->email1       = $request->email1;
              $supplier->email2       = $request->email2;
              $supplier->pobox        = $request->pobox;
            //   $supplier->license_copy = $request->license_copy;
              $supplier->trn          = $request->trn;



                  if ($request->hasFile('license_copy'))
    {
     // dd($request->passport_attachment);

    $validator = request()->validate([
            'license_copy' => 'mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
        ]);
        if($request->file('license_copy')->isValid()) {
            try {
                $file = $request->file('license_copy');
                $name =  $file->getClientOriginalName();
                $request->file('license_copy')->move("public/files/attachments/", $name);
                $pass_attach = "public/files/attachments/".$name."";
                 $supplier->license_copy             = $pass_attach;
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
            }
        }
    }
              $supplier->save();
             return back()->withStatus(__('Supplier Registered successfully.'));
        }

        public function Maintaince_List()
        {
            return view('admin.maintenance_list');
        }

      public function  Maintenance_Form()
      {
        $data['projects'] = Project::all();
        $data['users'] = User::where('role','=','Tenant')->get();
        $data['owner'] = User::where('role','=','Landlord')->get();
        $data['supplier'] = Supplier::all();
          return view('admin.maintenance_form',$data);
      }

      public function Nature_maintenance()
      {
          $data['counter'] = 1;
          $data['maintenance'] = Maintenance_head::all();
         return view('admin.nature_of_maintenance',$data);
      }

      public function save_maintenance_head(Request $request)
      {
          if($request->check==0)
          {
          $head         = new Maintenance_head();
          $head->name   = $request->name;
          $head->status = $request->status;
          $head->save();
         return back()->withStatus(__('Maintenance Head Saved successfully.'));
          }
          else if($request->check==1)
          {
          $update         = Maintenance_head::where('id',$request->head_id)->first();
          $update->name   = $request->name;
          $update->status = $request->status;
          $update->save();
         return back()->withStatus(__('Maintenance Head Updated successfully.'));
          }
      }

      public function Questions()
      {
          $data['head'] = Maintenance_head::all();
          $data['question'] = Question::all();
          $data['counter'] =1;
          return view('admin.questions',$data);
      }


      public function voucher_detail()
      {
          $data['head'] = Maintenance_head::all();
          $data['question'] = Question::all();
          $data['counter'] =1;
          return view('admin.voucher_detail',$data);
      }


      public function create_voucher_detail()
      {
          $data['projects'] = Project::all();
        $data['users'] = User::where('role','=','Tenant')->get();
        $data['owner'] = User::where('role','=','Landlord')->get();
        $data['supplier'] = Supplier::all();
          return view('admin.create_voucher_detail',$data);
      }

    public function create_receipt_voucher_detail()
    {
        $data['projects'] = Project::all();
        $data['users'] = User::where('role','=','Tenant')->get();
        $data['owner'] = User::where('role','=','Landlord')->get();
        $data['supplier'] = Supplier::all();
        return view('admin.create_receipt_voucher_detail',$data);
    }

      public function save_questions(Request $request)
      {
        //   dd($request);
          if($request->check==0)
          {
              $head = $request->heads;
            //   dd($head);
             foreach ($request->question as  $value) {
                $ques = new Question();
                $ques->head_id  = $head;
                $ques->question = $value;
                $ques->save();
                $question_id = $ques->id;
             }


             foreach ($request->answer as $key => $value) {
                $ans = new Answer();
                $ans->head_id = $head;
                $ans->question_id = $question_id;
                $ans->answer = $request->answer[$key];
                $ans->save();
            }
             return back()->withStatus(__('Questions Added  successfully.'));
          }

      }

      public function Save_maintenance(Request $request)
      {
          $form = new Maintenance_form();
          $form->project_id = $request->project_id;
          $form->building_id = $request->building_id;
          $form->floor_id = $request->floor_id;
          $form->flat = $request->flat;
          $form->owner_id = $request->owner_id;
          $form->owner_contact = $request->owner_contact;
          $form->owner_email = $request->owner_email;
          $form->tenant_id = $request->tenant_id;
          $form->tenant_contact = $request->tenant_contact;
          $form->tenant_email = $request->tenant_email;
          $form->save();
          $maintenance_form_id = $form->id;


          if($request->nature_of_maintenance){
              foreach ($request->nature_of_maintenance as $key => $value) {
                $detail = new Maintenance_detail();
                $detail->maintenance_form_id = $maintenance_form_id;
                $detail->nature_of_maintenance = $request->nature_of_maintenance[$key];
                $detail->supplier1 = $request->supplier1[$key];
                $detail->supplier2 = $request->supplier2[$key];
                $detail->supplier3 = $request->supplier3[$key];
                $detail->email     = $request->email[$key];
                $detail->quotation = $request->quotation[$key];
                $detail->charge_to = $request->charge_to[$key];
                $detail->save();
              }

          }
           return back()->withStatus(__('Maintenance Form  Added  successfully.'));
      }



      public function saveOwnerReport($id,$amount){
        OwnerReport::create([
            'contract_id' => $id,
            'debit' => $amount
        ]);
      }

        public function Save_vouchers(Request $request)
    {
        if($request->check==1){

        $voucher                = new Voucher();
        $voucher->voucher_type  = "Office";
        $voucher->save();
        $voucher_id = $voucher->id;

        if($request->office_expense_head){
            foreach ($request->office_expense_head as $key => $value) {
                $this->saveOwnerReport($voucher_id,$request->office_total_vat[$key]);
                $office                 = new Office_voucher_detail();
                $office->voucher_id     = $voucher_id;
                $office->expense_head   = $request->office_expense_head[$key];
                $office->description    = $request->office_description[$key];
                $office->qty            = $request->office_qty[$key];
                $office->rate           = $request->office_rate[$key];
                $office->total          = $request->office_total[$key];
                $office->vat            = $request->office_vat_type[$key];
                $office->with_vat       = $request->office_with_vat[$key];
                $office->total_with_vat = $request->office_total_vat[$key];
                $office->save();
            }
        }

        return back()->withStatus(__('Office Voucher  Saved  Successfully.'));
        }
        else if($request->check==0){

        $voucher                = new Voucher();
        $voucher->voucher_type  = "Apartment";
        $voucher->project_id    = $request->project_id;
        $voucher->building_id   = $request->building_id;
        $voucher->floor_id      = $request->floor_id;
        $voucher->flat          = $request->flat;
        $voucher->owner_id      = $request->owner_id;
        $voucher->owner_contact = $request->owner_contact;
        $voucher->owner_email = $request->owner_email;
        $voucher->tenant_id = $request->tenant_id;
        $voucher->tenant_contact = $request->tenant_contact;
        $voucher->tenant_email = $request->tenant_email;
        $voucher->save();
        $voucher_id = $voucher->id;


         if($request->apart_expense_head){
            foreach ($request->apart_expense_head as $key => $value) {
                $apart                 = new Apart_voucher_detail();
                $apart->voucher_id     = $voucher_id;
                $apart->expense_head   = $request->apart_expense_head[$key];
                $apart->description    = $request->apart_description[$key];
                $apart->qty            = $request->apart_qty[$key];
                $apart->rate           = $request->apart_rate[$key];
                $apart->total          = $request->apart_total[$key];
                $apart->vat            = $request->apart_vat_type[$key];
                $apart->with_vat       = $request->apart_with_vat[$key];
                $apart->total_with_vat = $request->apart_vat_total[$key];
                $apart->save();
            }
        }

         if(request('attachment'))
        {
                foreach (request('attachment') as $key => $value) {
                $attach               = new Apart_voucher_attachment();
                $attach->memo        = $request->memo[$key];
                $attach->voucher_id = $voucher_id;
                $file3 = $request->file('attachment')[$key];
                $name3 =  $file3->getClientOriginalName();
                $request->file('attachment')[$key]->move("public/files/attachments/", $name3);
                $file = 'public/files/attachments/'.$name3;
                $attach->attachment =$file;
                $attach->save();
                }
        }




        return back()->withStatus(__('Apartment Voucher  Saved  successfully.'));
        }

    }


    public function Add_journal_voucher()
    {
        $data['project'] = Project::all();
        return view('admin.journal_voucher',$data);
    }

    public function get_filter_data(Request $request)
    {
         $pro1     = "";
         $st       = $request->start;
         $end      = $request->end;
         $tbl      = "";
         $count    = 0;
         $pro      = $request->project;
         $build    = $request->building;
         $floor    = $request->floor;
         $flat     = $request->flat;
         $tenant   = $request->tenant;
         $landlord = $request->landlord;
         $status   = $request->status;


         if($pro!=null || $tenant!=null || $landlord!=null){
             if($pro!=null && $build!=null && $floor!=null && $flat!=null && $tenant!=null && $landlord!=null )
         {
            $pro_data = Contract::where('project',$pro)->where('building_id',$build)->where('floor_id',$floor)->where('flat_id',$flat)->where('lanlord_name',$landlord)->where('tenant',$tenant)->get();
         }
          else if($pro!=null && $build!=null && $floor!=null && $flat!=null && $tenant==null && $landlord!=null)
         {
            $pro_data = Contract::where('project',$pro)->where('building_id',$build)->where('floor_id',$floor)->where('flat_id',$flat)->where('lanlord_name',$landlord)->get();
         }
           else if($pro!=null && $build!=null && $floor!=null && $flat!=null && $tenant!=null && $landlord==null)
         {
            $pro_data = Contract::where('project',$pro)->where('building_id',$build)->where('floor_id',$floor)->where('flat_id',$flat)->where('tenant',$tenant)->get();
         }
         else  if($pro!=null && $build!=null && $floor!=null && $flat!=null && $tenant==null && $landlord==null)
         {
            $pro_data = Contract::where('project',$pro)->where('building_id',$build)->where('floor_id',$floor)->where('flat_id',$flat)->get();
         }





        else if($pro==null && $build==null && $floor==null && $flat==null && $tenant!=null && $landlord==null)
         {
            $pro_data = Contract::where('tenant',$tenant)->get();
         }
         else if($pro==null && $build==null && $floor==null && $flat==null && $tenant==null && $landlord!=null)
         {
            $pro_data = Contract::where('lanlord_name',$landlord)->get();
         }
         else if($pro==null && $build==null && $floor==null && $flat==null && $tenant!=null && $landlord!=null)
         {
            $pro_data = Contract::where('lanlord_name',$landlord)->where('tenant',$tenant)->get();
         }





        else if($pro!=null && $build!=null && $floor!=null && $flat==null && $tenant!=null && $landlord!=null)
         {
             $pro_data = Contract::where('project',$pro)->where('building_id',$build)->where('floor_id',$floor)->where('lanlord_name',$landlord)->where('tenant',$tenant)->get();
         }
        else if($pro!=null && $build!=null && $floor!=null && $flat==null && $tenant==null && $landlord==null)
         {
             $pro_data = Contract::where('project',$pro)->where('building_id',$build)->where('floor_id',$floor)->get();
         }
        else if($pro!=null && $build!=null && $floor!=null && $flat==null && $tenant==null && $landlord!=null)
         {
             $pro_data = Contract::where('project',$pro)->where('building_id',$build)->where('floor_id',$floor)->where('lanlord_name',$landlord)->get();
         }
        else if($pro!=null && $build!=null && $floor!=null && $flat==null && $tenant!=null && $landlord==null)
         {
             $pro_data = Contract::where('project',$pro)->where('building_id',$build)->where('floor_id',$floor)->where('tenant',$tenant)->get();
         }






      else   if($pro!=null && $build!=null && $floor==null && $flat==null && $tenant!=null && $landlord==null)
         {
            $pro_data = Contract::where('project',$pro)->where('building_id',$build)->where('tenant',$tenant)->get();
         }
       else  if($pro!=null && $build!=null && $floor==null && $flat==null && $tenant==null && $landlord!=null)
         {
            $pro_data = Contract::where('project',$pro)->where('building_id',$build)->where('lanlord_name',$landlord)->get();
         }
       else   if($pro!=null && $build!=null && $floor==null && $flat==null && $tenant!=null && $landlord!=null)
         {
            $pro_data = Contract::where('project',$pro)->where('building_id',$build)->where('lanlord_name',$landlord)->where('tenant',$tenant)->get();
         }
        else if($pro!=null && $build!=null && $floor==null && $flat==null && $tenant==null && $landlord==null)
         {
            $pro_data = Contract::where('project',$pro)->where('building_id',$build)->get();
         }

       else  if($pro!=null && $build==null && $floor==null && $flat==null && $tenant==null && $landlord==null)
         {
            $pro_data = Contract::where('project',$pro)->get();
         }



        foreach($pro_data as $datas)
        {
          if($request->check==1 && $st!=null && $end!=null )
          {
           $pro1 = InstallmentCheck::where('contract_id',$datas->id)->where('check_issue', 'like', '%' . $request->search . '%')->whereBetween('check_date', [$st, $end])->get();
          }
            else if($request->check==1 && $st==null && $end==null)
          {
           $pro1 = InstallmentCheck::where('contract_id',$datas->id)->where('check_issue', 'like', '%' . $request->search . '%')->get();
          }





        else if($request->check==2 && $st!=null && $end!=null)
        {
            $pro1 = InstallmentCheck::where('contract_id',$datas->id)->where('amount',$request->search)->whereBetween('check_date', [$st, $end])->get();
        }
        else if($request->check==2 && $st==null && $end==null)
        {
            $pro1 = InstallmentCheck::where('contract_id',$datas->id)->where('amount',$request->search)->get();
        }



         else if($request->check==3 && $st!=null && $end!=null)
        {
            $pro1 = InstallmentCheck::where('contract_id',$datas->id)->where('id',$request->search)->whereBetween('check_date', [$st, $end])->get();
        }
         else if($request->check==3 && $st==null && $end==null)
        {
            $pro1 = InstallmentCheck::where('contract_id',$datas->id)->where('id',$request->search)->get();
        }





         else if($request->check==4 && $st!=null && $end!=null)
        {
            $pro1 = InstallmentCheck::where('contract_id',$datas->id)->where('check_no',$request->search)->whereBetween('check_date', [$st, $end])->get();
        }
          else if($request->check==4 && $st==null && $end==null)
        {
            $pro1 = InstallmentCheck::where('contract_id',$datas->id)->where('check_no',$request->search)->get();
        }




         else if($request->check==5 && $st!=null && $end!=null)
        {
            $pro1 = InstallmentCheck::whereBetween('check_date', [$st, $end])->get();
        }
          else if($request->check==5 && $st==null && $end==null)
        {
            $pro1 = InstallmentCheck::all();
        }



        else{
            $pro1 = InstallmentCheck::where('contract_id',$datas->id)->get();
        }





                //  $pro1 =  InstallmentCheck::where('contract_id',$datas->id)->get();
                foreach ($pro1 as $key => $value) {
            $count++;
            // dd($datas->);
            $tbl .="<tr>";
            $tbl .="<td>".$count."</td>";
            $tbl .="<td>".$value->installment_no."</td>";
            $tbl .="<td>".$value->party_name."</td>";
            $tbl .="<td>".$datas->Building_Name->name."</td>";
            $tbl .="<td>".$datas->flat_id."</td>";
            $tbl .="<td>".$value->particular."</td>";
            $tbl .="<td>".$value->check_no."</td>";
            $tbl .="<td>".$value->pay_type."</td>";
            $tbl .="<td>".$value->check_date."</td>";
            $tbl .="<td>".$value->check_issue."</td>";
            $tbl .="<td>".$value->amount."</td>";
            $tbl .="<td>".$value->drop_down_opt."</td>";
             if($value->status=='Open'){
            $tbl .="<td><span style='color:red'>".$value->status."</span></td>";
            }
            else{
            $tbl .="<td><span style='color:green'>".$value->status."</span></td>";
            }
            //   $tbl .="<td>".$value->type."</td>";
              if($value->is_show==0){
            $tbl .="<td><div class='dropdown'>";
            $tbl .="<button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions</button>";
            $tbl .="<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>";
            // $tbl .="<button class='dropdown-item' >New PDC List</button>";
            // $tbl .="<button class='dropdown-item' >PDC in Bank Safe Box</button>";
              if($value->hold_release==0){

            $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,1)'>Cleared PDC</button>";
            // $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,2)'>Cleared PDC (Discounted)</button>";
            $tbl .="<button class='dropdown-item' >Pending PDC Submitted To Landlord</button>";
            $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,6)' >PDC Submitted To Landlord</button>";
            $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,3)'>Bounced PDC</button>";
            $tbl .="<button class='dropdown-item' >Re-Submitted PDC</button>";
            $tbl .="<button class='dropdown-item' value='".$value->contract_id."*".$value->check_date."*".$value->id."*".$value->check_no."' onclick='replacement(this.value)'>Replacement PDC</button>";
            $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,4)'>Bounced Cleared  PDC</button>";
            $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,5)'>Bounced Cheque</button>";
            $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,7)'>Hold</button>";

            }

            else if($value->hold_release==1){
                $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,8)'>Release</button>";
            }
            $tbl .="</div></div></td>";
              }
              else{
                $tbl .="<td></td>";
            }
            $tbl .="</tr>";
            }
          }
        }

        else if($pro==null && $build==null && $floor==null && $flat==null && $tenant==null && $landlord==null)
        {
            if($request->check==1 && $st!=null && $end!=null && $status==null)
        {
           $pro1 = InstallmentCheck::where('check_issue', 'like', '%' . $request->search . '%')->whereBetween('check_date', [$st, $end])->get();
        }
         else  if($request->check==1 && $st!=null && $end!=null && $status!=null)
        {
           $pro1 = InstallmentCheck::where('status',$status)->where('check_issue', 'like', '%' . $request->search . '%')->whereBetween('check_date', [$st, $end])->get();
        }


        else if($request->check==1 && $st==null && $end==null && $status==null)
        {
           $pro1 = InstallmentCheck::where('check_issue', 'like', '%' . $request->search . '%')->get();
        }
        else if($request->check==1 && $st==null && $end==null && $status!=null)
        {
           $pro1 = InstallmentCheck::where('status',$status)->where('check_issue', 'like', '%' . $request->search . '%')->get();
        }


        else if($request->check==2 && $st!=null && $end!=null && $status==null)
        {
            $pro1 = InstallmentCheck::where('amount',$request->search)->whereBetween('check_date', [$st, $end])->get();
        }
         else if($request->check==2 && $st!=null && $end!=null && $status!=null)
        {
            $pro1 = InstallmentCheck::where('status',$status)->where('amount',$request->search)->whereBetween('check_date', [$st, $end])->get();
        }


        else  if($request->check==2 && $st==null && $end==null && $status==null)
        {
            $pro1 = InstallmentCheck::where('amount',$request->search)->get();
        }
         else  if($request->check==2 && $st==null && $end==null && $status!=null)
        {
            $pro1 = InstallmentCheck::where('status',$status)->where('amount',$request->search)->get();
        }




         else if($request->check==3 && $st!=null && $end!=null && $status==null)
        {
            $pro1 = InstallmentCheck::where('id',$request->search)->whereBetween('check_date', [$st, $end])->get();
        }
          else if($request->check==3 && $st!=null && $end!=null && $status!=null)
        {
            $pro1 = InstallmentCheck::where('status',$status)->where('id',$request->search)->whereBetween('check_date', [$st, $end])->get();
        }


         else if($request->check==3 && $st==null && $end==null && $status==null)
        {
            $pro1 = InstallmentCheck::where('id',$request->search)->get();
        }
         else if($request->check==3 && $st==null && $end==null && $status!=null)
        {
            $pro1 = InstallmentCheck::where('status',$status)->where('id',$request->search)->get();
        }




        else  if($request->check==4 && $st!=null && $end!=null && $status==null)
        {
            $pro1 = InstallmentCheck::where('check_no',$request->search)->whereBetween('check_date', [$st, $end])->get();
        }
        else  if($request->check==4 && $st!=null && $end!=null && $status!=null)
        {
            $pro1 = InstallmentCheck::where('status',$status)->where('check_no',$request->search)->whereBetween('check_date', [$st, $end])->get();
        }


       else   if($request->check==4 && $st==null && $end==null && $status==null)
        {
            $pro1 = InstallmentCheck::where('check_no',$request->search)->get();
        }
         else   if($request->check==4 && $st==null && $end==null && $status!=null)
        {
            $pro1 = InstallmentCheck::where('status',$status)->where('check_no',$request->search)->get();
        }




       else   if($request->check==5 && $st!=null && $end!=null && $status==null)
        {
            $pro1 = InstallmentCheck::whereBetween('check_date', [$st, $end])->get();
        }
        else   if($request->check==5 && $st!=null && $end!=null && $status!=null)
        {
            $pro1 = InstallmentCheck::where('status',$status)->whereBetween('check_date', [$st, $end])->get();
        }


      else   if($request->check==5 && $st==null && $end==null && $status==null)
        {
            $pro1 = InstallmentCheck::all();
        }
        else   if($request->check==5 && $st==null && $end==null && $status!=null)
        {
            $pro1 = InstallmentCheck::where('status',$status)->get();
        }


        else   if($request->check==null && $st==null && $end==null && $status!=null)
        {
            $pro1 = InstallmentCheck::where('status',$status)->get();
        }

         else   if($request->check==null && $st!=null && $end!=null && $status==null)
        {
            $pro1 = InstallmentCheck::all();
        }






            foreach ($pro1 as $key => $value) {
                $count++;

            $tbl .="<tr>";
            $tbl .="<td>".$count."</td>";
            $tbl .="<td>".$value->installment_no."</td>";
            $tbl .="<td>".$value->party_name."</td>";
            $tbl .="<td>".$value->Contract_data->Building_Name->name."</td>";
            $tbl .="<td>".$value->Contract_data->flat_id."</td>";
            $tbl .="<td>".$value->particular."</td>";
            $tbl .="<td>".$value->check_no."</td>";
            $tbl .="<td>".$value->pay_type."</td>";
            $tbl .="<td>".$value->check_date."</td>";
            $tbl .="<td>".$value->check_issue."</td>";
            $tbl .="<td>".$value->amount."</td>";
            $tbl .="<td>".$value->drop_down_opt."</td>";
            if($value->status=='Open'){
            $tbl .="<td><span style='color:red'>".$value->status."</span></td>";
            }
            else{
            $tbl .="<td><span style='color:green'>".$value->status."</span></td>";
            }
            //  $tbl .="<td>".$value->type."</td>";
             if($value->is_show==0){

            $tbl .="<td><div class='dropdown '>";
            $tbl .="<button class='btn btn-secondary dropdown-toggle' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions</button>";
            $tbl .="<div class='dropdown-menu' aria-labelledby='dropdownMenuLink'>";
            // $tbl .="<button class='dropdown-item' >New PDC List</button>";
            // $tbl .="<button class='dropdown-item' >PDC in Bank Safe Box</button>";
             if($value->hold_release==0){

            $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,1)'>Cleared PDC</button>";
            // $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,2)'>Cleared PDC (Discounted)</button>";
            $tbl .="<button class='dropdown-item' >Pending PDC Submitted To Landlord</button>";
            $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,6)' >PDC Submitted To Landlord</button>";
            $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,3)'>Bounced PDC</button>";
            $tbl .="<button class='dropdown-item' >Re-Submitted PDC</button>";
            $tbl .="<button class='dropdown-item' value='".$value->contract_id."*".$value->check_date."*".$value->id."*".$value->check_no."' onclick='replacement(this.value)'>Replacement PDC</button>";
            $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,4)'>Bounced Cleared  PDC</button>";
            $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,5)'>Bounced Cheque</button>";
            $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,7)'>Hold</button>";

            }

            else if($value->hold_release==1){
                $tbl .="<button class='dropdown-item' value='".$value->id."' onclick='status_change(this.value,8)'>Release</button>";
            }


            $tbl .="</div></div></td>";
             }
            else{
                $tbl .="<td></td>";
            }
            $tbl .="</tr>";
            }

        }

      echo $tbl;
    }


    public function change_status(Request $request)
    {
        $check = $request->check_val;
        $update="";
        $status="";
       $update = InstallmentCheck::where('id',$request->installment_id)->with('Contract_data')->first();
        if($check==1)
        {
            OwnerReport::create([
                'contract_id' => $update->Contract_data->id,
                'owner_id' => $update->Contract_data->lanlord_name,
                'user_id' => $update->Contract_data->tenant,
                'credit' => $update->amount
            ]);
            $status = "Cleared PDC";

        }
      else  if($check==2)
        {
           $status = "Cleared PDC (Discount)";
        }
      else   if($check==3)
        {
           $status = "Bounded PDC ";
        }
      else   if($check==4)
        {
            $status = "Cleared Cleared PDC";
        }
      else  if($check==5)
        {
            $status = "Bounced Cheque";
        }
         else  if($check==6)
        {
            $status = "PDC Submitted To Landlord";
        }
         else  if($check==7)
        {
            $status = "Hold";
            $update->hold_release =1;
        }
         else  if($check==8)
        {
            $status = "Release";
            $update->hold_release =0;
        }




         $update->status = $status;
         $update->save();
    }

    public function save_replacement_pdc(Request $request)
    {
        $update_show = InstallmentCheck::where('id',$request->installment)->first();
        $update_show->status = "Replaced By Cheque";
        $update_show->is_show =1;
        $update_show->save();




        $install                 = new InstallmentCheck();
        $install->contract_id    = $request->contract_id;
        $install->installment_no = $request->installment_no;
        $install->amount         = $request->amount;
        $install->particular     = $request->particular;
        $install->pay_type       = $request->pay_type;
        $install->check_no       = $request->check_no;
        $install->check_date     = $request->check_date;
        $install->check_issue    = $request->check_issue;
        $install->party_name     = $request->party_name;
        $install->drop_down_opt  = $request->deposit_to;
        $install->type           = "Replacement";
        $install->status         = "Replaced";
        $install->other_charges  = $request->other_charges;
        $install->save();


        return back()->withStatus(__('Replacement PDC Saved successfully.'));
    }


    public function Add_Account()
    {
        $data['counter'] = 1;
        $data['account'] = Account::all();
        return view('admin.accounts',$data);
    }

    public function save_account(Request $request)
    {
        $type = $request->account_type;
        $acc_type="";
        if($type==1){
            $acc_type = "Income";
        }
        else if($type==2){
            $acc_type = "Expense";
        }
        $account                     = new Account();
        $account->account_name       = $request->account_name;
        $account->account_type       = $acc_type;
        $account->asset_liabilitie   = $request->asset_liabilitie;
        $account->other_account_type = $request->other_account_type;
        $account->description        = $request->description;
        $account->note               = $request->note;
        $account->vat_code           = $request->vat_code;
        if($request->sub_account_type)
        {
             $account->type = "Subaccount";

        }
        else
        {
           $account->type = "Main";
        }
        $account->parent_id          = $request->sub_account_type;
        $account->save();
        return back()->withStatus(__('Account Added successfully.'));

    }

    public function Owner_report()
    {
        $owners = User::where('role','=','Landlord')->get();
        return view('admin.owner_report',[
            'owners' => $owners
        ]);
    }

    public function Owner_Flats($owner_id)
    {
        $owners_flats = Flat_list::where('owner_id','=',$owner_id)->get();
        return collect([
            'status' => true,
            'data' => $owners_flats
        ]);
    }

    public function Owner_Post(Request $request) {
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $arr = [];
//        if($request->owner_id != '' && $request->flat_id != 'all') {
//            $data = OwnerReport::where('owner_id','=',$request->owner_id)->with('owner','Flat_lists')->get();
//                foreach($data as $d) {
//                    if($d->debit != '' || $d->credit !='') {
//                    $a = array(
//                        'id' => $d->contract_id,
//                        'owner_name' => $d->owner->name,
//                        'flat_code' => $d->Flat_lists->unit_code,
//                        'description' => null,
//                        'debit' => isset($d->debit) ? $d->debit : null,
//                        'credit' => isset($d->credit) ? $d->credit : null,
//                        'created_at' => $d->created_at
//                    );
//                    array_push($arr, $a);
//                }
//            }
//        } else if($request->owner_id != '' && $request->flat_id == 'all') {
//            $data = Flat_list::where('owner_id','=',$request->owner_id)->with('Landlord_name','owner_report')->get();
//            foreach($data as $d) {
//                if($d->owner_report->debit != '' || $d->owner_report->credit !='') {
//                    $a = array(
//                        'id' => $d->owner_report->contract_id,
//                        'owner_name' => $d->Landlord_name->name,
//                        'flat_code' => $d->unit_code,
//                        'description' => null,
//                        'debit' => isset($d->owner_report->debit) ? $d->owner_report->debit : null,
//                        'credit' => isset($d->owner_report->credit) ? $d->owner_report->credit : null,
//                        'created_at' => $d->created_at
//                    );
//                    array_push($arr, $a);
//                }
//            }
//        } else
        if($request->owner_id != '' && $request->flat_id != 'all' && $request->start_date != '' && $request->end_date != '') {
            $data = OwnerReport::where('owner_id','=',$request->owner_id)
                ->whereBetween('created_at', [$start_date, $end_date])->with('owner','Flat_lists')->get();
            foreach($data as $d) {
                if($d->debit != '' || $d->credit !='') {
                    $a = array(
                        'id' => $d->contract_id,
                        'owner_name' => $d->owner->name,
                        'flat_code' => $d->Flat_lists->unit_code,
                        'description' => null,
                        'debit' => isset($d->debit) ? $d->debit : '',
                        'credit' => isset($d->credit) ? $d->credit : '',
                        'created_at' => Carbon::parse($d->created_at)->format('M d Y')
                    );
                    array_push($arr, $a);
                }
            }
        } else if($request->owner_id != '' && $request->flat_id == 'all' && $request->start_date != '' && $request->end_date != '') {
            $data = OwnerReport::where('owner_id','=',$request->owner_id)
                ->whereBetween('created_at', [$start_date, $end_date])
                ->with('owner','Flat_lists')
                ->get();
            foreach($data as $d) {
                if($d->debit != '' || $d->credit !='') {
                    $a = array(
                        'id' => $d->contract_id,
                        'owner_name' => $d->owner->name,
                        'flat_code' => $d->Flat_lists->unit_code,
                        'description' => null,
                        'debit' => isset($d->debit) ? $d->debit : '',
                        'credit' => isset($d->credit) ? $d->credit : '',
                        'created_at' => Carbon::parse($d->created_at)->format('M d Y')
                    );
                    array_push($arr, $a);
                }
            }
        }
//        else if($request->owner_id != '') {
//            $data = OwnerReport::where('owner_id','=',$request->owner_id)
//                ->with('owner','Flat_lists')
//                ->get();
//            foreach($data as $d) {
//                if($d->debit != '' || $d->credit !='') {
//                    $a = array(
//                        'id' => $d->contract_id,
//                        'owner_name' => $d->owner->name,
//                        'flat_code' => $d->Flat_lists->unit_code,
//                        'description' => null,
//                        'debit' => isset($d->debit) ? $d->debit : null,
//                        'credit' => isset($d->credit) ? $d->credit : null,
//                        'created_at' => $d->created_at
//                    );
//                    array_push($arr, $a);
//                }
//            }
//        } else if($request->start_date != '' && $request->end_date != '') {
//            $data = OwnerReport::whereBetween('created_at', [$start_date, $end_date])
//                ->with('owner','Flat_lists')
//                ->get();
//            foreach($data as $d) {
//                if($d->debit != '' || $d->credit !='') {
//                    $a = array(
//                        'id' => $d->contract_id,
//                        'owner_name' => $d->owner->name,
//                        'flat_code' => $d->Flat_lists->unit_code,
//                        'description' => null,
//                        'debit' => isset($d->debit) ? $d->debit : null,
//                        'credit' => isset($d->credit) ? $d->credit : null,
//                        'created_at' => $d->created_at
//                    );
//                    array_push($arr, $a);
//                }
//            }
//        }
        return collect([
           'status' => true,
           'data' => $arr
        ]);
    }

}

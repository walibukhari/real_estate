@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'add_landlords'
])

@section('content')
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
             @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('error') !!}</strong>
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
        <style type="text/css">
            .filelabel {
    width: 100%;
    border: 2px dashed grey;
    border-radius: 5px;
    display: block;
    padding: 5px;
    transition: border 300ms ease;
    cursor: pointer;
    text-align: center;
    margin: 0;
}
.filelabel i {
    display: block;
    font-size: 30px;
    padding-bottom: 5px;
}
.filelabel i,
.filelabel .title {
  color: grey;
  transition: 200ms color;
}
.filelabel:hover {
  border: 2px solid #1665c4;
}
.filelabel:hover i,
.filelabel:hover .title {
  color: #1665c4;
}
#FileInput{
    display:none;
}
        </style>
            <div class="row">
  <div class="col-12">    
  <div class="row">
      <div class="col-md-3">
          
      </div>
       <div class="col-md-3">
          
      </div>
       <div class="col-md-4">
          
      </div>
       <div class="col-md-2">
            <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"> Create New</button>
      </div>
  </div>     
    <div class="material-card card">
      <div class="card-body">

        <h4 class="card-title">Landlords  List</h4>
        <h6 class="card-subtitle">
        </h6><br>
        <div class="table-responsive">
         <!--  <table id="complex_header" class="table table-striped table-bordered display"
          style="width:100%">
          <thead>

            <tr>
              <th>Sr#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
          <tr>
            
          </tr>
      </tbody>
      <tfoot>
       <tr>
              <th>Sr#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
      </tfoot>
     </table> -->
    </div>
   </div>
  </div>
 </div>
</div>
  <div class="modal fade" id="myModal"  role="dialog" >
    <div class="modal-dialog modal-lg" >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create New</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
                   <div class="p-5">
               <form class="form" method="POST" action="{{ route('register') }}">
                                @csrf


                                         <div class="container">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="text-primary">Acc No(Debitor)</label>
                                                    <input type="number" name="acc_no_db" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                   <label class="text-primary">Acc No(Creditor)</label>
                                                    <input type="number" name="acc_no_cr" class="form-control">  
                                                </div>
                                                  <div class="col-md-4 ">
                                        <label class="text-primary">Title</label>    
                                        <select class="form-control"  name="title">
                                            <option value="" >Select One</option>
                                            <option value="Mr">Mr</option>
                                            <option value="Mrs">Mrs</option>
                                        </select>
                                    </div> 
                                            </div>
                                    <div class="row">
                                        
                                         
                                      <div class=" col-md-4 ">
                                        <label class="text-primary">Ref No</label>
                                        <input type="text" value="{{ old('ref_no') }}" name="ref_no" class="form-control " placeholder="Ref no"
                                            aria-label="Name">
                                            
                                    </div>
                                      <div class="col-md-4">
                                           <label class="text-primary">Booking Number</label> 
                                           <input type="text" name="booking_no" value="{{ old('booking_no') }}" placeholder="e.g 123" class="form-control" name="card_no">
                                        </div>
                                
                                    <div class="col-md-4">
                                           <label class="text-primary">Phone</label> 
                                           <input type="number" value="{{ old('phone') }}"  placeholder="123" class="form-control" name="phone">
                                        </div>  
                                  
                                    </div>

                                     <div class="row">
                                           <div class="col-md-4{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <label class="text-primary">Name</label>
                                    <div class="input-group-prepend">
                                   <!--      <span class="input-group-text">
                                            <i class="nc-icon nc-single-02"></i>
                                        </span> -->
                                    </div>
                                    <input name="name" type="text" class="form-control" placeholder="Name" value="{{ old('name') }}" required >
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                            <div class="col-md-4 ">
                                             <label class="text-primary">Name in Arabic</label>
                                        <input type="text" value="{{ old('name_in_arabic') }}" name="name_in_arabic" class="form-control " placeholder="Name in Arabic"
                                            aria-label="Name">
                                    </div>
                                         <div class="col-md-4{{ $errors->has('email') ? ' has-danger' : '' }}">

                                        <label class="text-primary">Email</label>
                                    <div class="input-group-prepend">
                                     <!--    <span class="input-group-text">
                                            <i class="nc-icon nc-email-85"></i>
                                        </span> -->
                                    </div>
                                    <input name="email" type="email" class="form-control" placeholder="Email" required value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                               
                                     
                                         
                                    </div>
                                    
                                    <div class="row">
                                         <div class="col-md-4{{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label class="text-primary">Password</label>
                                    <div class="input-group-prepend">
                                       <!--  <span class="input-group-text">
                                            <i class="nc-icon nc-key-25"></i>
                                        </span> -->
                                    </div>
                                    <input name="password" type="password" class="form-control" placeholder="Password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                       
                                            <div class="col-md-4">
                                            <label class="text-primary">Confirm Password</label>
                                    <div class="input-group-prepend">
                                      <!--   <span class="input-group-text">
                                            <i class="nc-icon nc-key-25"></i>
                                        </span> -->
                                    </div>
                                    <input name="password_confirmation" type="password" class="form-control" placeholder="Password confirmation" required>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                 <div class=" col-md-4 ">
                                       <label class="text-primary">Company</label>
                                        <input type="text" value="{{ old('company') }}" name="company" class="form-control " placeholder="Copmpany"
                                            aria-label="Name">
                                    </div>
                                    </div>
                                     <div class="row">
                                        <div class="col-md-4">
                                           <label class="text-primary">Location</label> 
                                           <input type="text" value="{{ old('location') }}" name="location" placeholder="Location" class="form-control">
                                        </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">PO box</label> 
                                           <input type="text" value="{{ old('po_box') }}" class="form-control" placeholder="PO Box" name="po_box">
                                        </div>
                                          <div class="col-md-4">
                                           <label class="text-primary">Fax</label>
                                            <input type="text" value="{{ old('fax') }}" name="fax" placeholder="Fax No" class="form-control">
                                       </div>
                                       
                                    </div>
                                     <div class="row">
                                          <div class="col-md-4">
                                           <label class="text-primary">Country</label>
                                            <select class="form-control"  name="country" >
                                             <option value="" >Select One</option>
                                             <option value="1">Pakistan</option>
                                             <option value="3">Iran</option>
                                             <option value="2">India</option>
                                             <option value="5">China</option>
                                         </select>
                                       </div>
                                        <div class="col-md-4">
                                           <label class="text-primary">City</label>
                                            <select class="form-control" name="city" >
                                             <option value="" >Select One</option>
                                             <option value="2">Multan</option>
                                             <option value="1">Lahore</option>
                                             <option value="5">Karachi</option>
                                         </select>
                                       </div>
                                                                           
                                    </div>
                                    <hr>
                                    <h4 class="text-primary">Contact Person Details</h4>
                                    <div class="row">
                                        <br>
                                          <div class="col-md-4">
                                           <label class="text-primary">Name</label>
                                           <input type="text" value="{{ old('contact_per_name') }}"    name="contact_per_name" placeholder="Name" class="form-control">
                                       </div>
                                            <div class="col-md-4">
                                          <label class="text-primary">Designation</label>  
                                         <input type="text" value="{{ old('designation') }}"  name="designation" class="form-control" placeholder="">
                                        </div>
                                        <div class="col-md-4">
                                           <label class="text-primary">Phone</label>
                                       <input type="number" value="{{ old('contact_per_phone') }}"  name="contact_per_phone"  class="form-control" placeholder="Enter phone number...">
                                       </div>
                                     
                                     
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                           <label class="text-primary">Mobile</label>
                                       <input type="number" value="{{ old('contact_per_mobile') }}" name="contact_per_mobile"  class="form-control" placeholder="Enter Mobile number...">
                                       </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">Remarks</label>
                                       <input type="text" value="{{ old('contact_per_remarks') }}" name="contact_per_remarks"  class="form-control" placeholder="">
                                       </div>
                                       
                                    </div>
                                    <hr>

                                    <div class="row">
                                          <div class="col-md-4">
                                           <label class="text-primary">Card No</label>
                                           <input type="number"  value="{{ old('card_no') }}" placeholder="e.g 45325" name="card_no"  class="form-control">
                                       </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">ID No.</label>
                                           <input type="text" placeholder="e.g. 36302-xxxxxxx-x" value="{{ old('id_no') }}" name="id_no"  class="form-control">
                                       </div>
                                         <div class="col-md-4">
                                           <label class="text-primary">Designation</label>
                                           <input type="text" value="{{ old('designation') }}" placeholder="" name="designation"  class="form-control">
                                       </div>
                                    </div>


                                       <div class="row">
                                        <div class="col-md-4">
                                           <label class="text-primary">Nationality</label>
                                           <input type="text" value="{{ old('nationality') }}" placeholder="" name="nationality"  class="form-control">
                                       </div>
                                       <div class="col-md-4">
                                           <label class="text-primary">VAT Country</label>
                                         <select class="form-control" name="vat_country" >
                                             <option value="" >Select One</option>
                                             <option value="testvatcountry">Test Vat Country</option>
                                         </select>
                                       </div>
                                       <div class="col-md-4">
                                           <label class="text-primary">Profession</label>
                                           <input type="text" value="{{ old('profession') }}" name="profession" placeholder="Profession" class="form-control">
                                       </div>
                                    </div>
                                    <div class="row">
                                           <div class="col-md-4">
                                           <label class="text-primary">Address 1</label>
                                            <input type="text" value="{{ old('address1') }}" name="address1" placeholder="Enter address" class="form-control">
                                       </div>
                                        <div class="col-md-4">
                                          <label class="text-primary">Address 2</label>
                                            <input type="text"  value="{{ old('address2') }}" name="address2" placeholder="Enter address" class="form-control">
                                        </div>
                                        <div class="col-md-4">

                                            <label class="text-primary">VAT Reg No</label>
                                            <input type="hidden" name="type" value="landlord">
                                            <input type="hidden" name="visa_no" value="">
                                            <input type="hidden" name="passport_no" value="">
                                            <input type="hidden" name="pass_expiry_date" value="">
                                            <input type="hidden" name="visa_expiry_date" value="">
                                            <input type="hidden" name="trade_license_no" value="">
                                            <input type="hidden" name="mobile" value="">
                                            <input type="hidden" name="trade_license_expiry_date" value="">
                                            <input type="hidden" name="initial_occupation_date" value="">
                                            <input type="hidden" name="is_show" value="0">
                                            <input type="text"  value="{{ old('vat_reg_no') }}" class="form-control" name="vat_reg_no" placeholder="Enter VAT reg no">
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">

                                           <label class="text-primary">VAT Emirates</label>
                                          <select class="form-control" name="vat_emirates" >
                                             <option value="" >Select One</option>
                                             <option value="2">hello</option>
                                         </select>
                                       </div>
                                    </div>
                                    <br>
                                
                                      <div class="row">
                                        <div class="col-md-1">
                                            <a  class="btn btn-primary">Find</a> 
                                        </div>
                                         <div class="col-md-1">
                                           <a  class="btn btn-primary">Add</a> 
                                        </div>
                                         <div class="col-md-1">
                                            <a  class="btn btn-primary">Edit</a> 
                                        </div>
                                         <div class="col-md-1">
                                          <a  class="btn btn-primary">Update</a> 
                                        </div>
                                         <div class="col-md-1">
                                             <a  class="btn btn-primary">Cancel</a> 
                                        </div>
                                           <div class="col-md-1">
                                             <a  class="btn btn-primary">Delete</a> 
                                        </div>
                                         <div class="col-md-1">
                                           <a  class="btn btn-primary">Close</a> 
                                        </div>
                                         <div class="col-md-2">
                                             <a  class="btn btn-primary">Attachment</a> 
                                        </div>
                                    </div>
                                     <div class="row">
                                       
                                      
                                        
                                    </div>
                                </div>
                                
                            <div class="row">
                             <div class="col-md-4">
                                  <div class="form-check text-left">
                                    <label class="form-check-label">
                                        <input class="form-check-input" name="agree_terms_and_conditions" type="checkbox">
                                        <span class="form-check-sign"></span>
                                            {{ __('I agree to the') }}
                                        <a href="#something">{{ __('terms and conditions') }}</a>.
                                    </label>
                                    @if ($errors->has('agree_terms_and_conditions'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('agree_terms_and_conditions') }}</strong>
                                        </span>
                                    @endif
                                </div>
                             </div>
                         </div>
                                    <div class="text-center mt-4">
                                        <button class="btn btn-started"> Get Started</button>
                                    </div>
                                </form>
           </div>
        
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>

    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
    $("#FileInput").on('change',function (e) {
            var labelVal = $(".title").text();
            var oldfileName = $(this).val();
                fileName = e.target.value.split( '\\' ).pop();

                if (oldfileName == fileName) {return false;}
                var extension = fileName.split('.').pop();

            if ($.inArray(extension,['jpg','jpeg','png']) >= 0) {
                $(".filelabel i").removeClass().addClass('fa fa-file-image-o');
                $(".filelabel i, .filelabel .title").css({'color':'#208440'});
                $(".filelabel").css({'border':' 2px solid #208440'});
            }
            else if(extension == 'pdf'){
                $(".filelabel i").removeClass().addClass('fa fa-file-pdf-o');
                $(".filelabel i, .filelabel .title").css({'color':'red'});
                $(".filelabel").css({'border':' 2px solid red'});

            }
  else if(extension == 'doc' || extension == 'docx'){
            $(".filelabel i").removeClass().addClass('fa fa-file-word-o');
            $(".filelabel i, .filelabel .title").css({'color':'#2388df'});
            $(".filelabel").css({'border':' 2px solid #2388df'});
        }
            else{
                $(".filelabel i").removeClass().addClass('fa fa-file-o');
                $(".filelabel i, .filelabel .title").css({'color':'black'});
                $(".filelabel").css({'border':' 2px solid black'});
            }

            if(fileName ){
                if (fileName.length > 10){
                    $(".filelabel .title").text(fileName.slice(0,4)+'...'+extension);
                }
                else{
                    $(".filelabel .title").text(fileName);
                }
            }
            else{
                $(".filelabel .title").text(labelVal);
            }
        });
</script>

@endsection

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMigrationTableAddTwoCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_security_deposites', function (Blueprint $table) {
            $table->string('particular')->nullable();
            $table->string('cash')->nullable();
            $table->string('check')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_security_deposites', function (Blueprint $table) {
            //
        });
    }
}

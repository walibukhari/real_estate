<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanLordBankDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lan_lord_bank_details', function (Blueprint $table) {
            $table->id();
            $table->integer('lanlord_id');
            $table->string('name')->nullable();
            $table->string('branch')->nullable();
            $table->string('address')->nullable();
            $table->string('title')->nullable();
            $table->integer('account_number')->nullable();
            $table->integer('iban_number')->nullable();
            $table->integer('swift_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lan_lord_bank_details');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstallmentChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installment_checks', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('contract_id');
            $table->bigInteger('installment_no');
            $table->bigInteger('amount')->nullable();
            $table->bigInteger('check_no')->nullable();
            $table->dateTime('check_date')->nullable();
            $table->string('check_issue')->nullable();
            $table->string('party_name')->nullable();
            $table->string('drop_down_opt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installment_checks');
    }
}

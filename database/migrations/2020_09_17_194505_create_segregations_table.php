<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSegregationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('segregations', function (Blueprint $table) {
            $table->id();
            $table->integer('building_id');
            $table->integer('project_id');
            $table->integer('flat_id');
            $table->integer('user_id');
            $table->integer('status')->nullable();
            $table->string('user_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('segregations');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractSecurityDepositesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_security_deposites', function (Blueprint $table) {
            $table->id();
            $table->integer('contract_id');
            $table->integer('serial_no')->nullable();
            $table->integer('check_no')->nullable();
            $table->dateTime('check_date')->nullable();
            $table->longText('check_issue')->nullable();
            $table->string('party_name')->nullable();
            $table->string('check_deposite')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_security_deposites');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractOtherNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_other_names', function (Blueprint $table) {
            $table->id();
            $table->integer('contract_id');
            $table->string('sr_no')->nullable();
            $table->string('particular')->nullable();
            $table->bigInteger('amount')->nullable();
            $table->integer('vat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_other_names');
    }
}

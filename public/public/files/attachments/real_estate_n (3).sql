-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2020 at 03:52 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `real_estate_n`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `title` varchar(522) DEFAULT NULL,
  `name` varchar(225) NOT NULL,
  `email` varchar(125) NOT NULL,
  `password` varchar(225) NOT NULL,
  `phone` varchar(125) DEFAULT NULL,
  `role` enum('Super Admin','Admin','General Manager','Landlord','Tenant') NOT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `title`, `name`, `email`, `password`, `phone`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Shahzaib', 'admin@gmail.com', '$2y$10$nDwjRaAHdbFUMc3Iq2/Yw.2A2cwylKVOBipO46iKcfN6gZ9DtElKO', '', 'Super Admin', 'Active', '2020-09-07 13:29:27', '2020-09-07 13:29:27'),
(10, NULL, 'Haseeb khan 1', 'ahmed.shahzaib.789@gmail.com', 'smbrothers', '0900402094', 'Landlord', 'Active', '2020-09-09 06:58:25', '2020-09-18 07:42:17'),
(15, NULL, 'Ahmad Shahzaib', 'ahmed.shahzaib.789@gmail.com', 'smbrothers', NULL, 'Landlord', 'Active', '2020-09-16 02:44:19', '2020-09-18 02:00:42'),
(16, 'Mr', 'Ahmad Shahzaib', 'ahmed.shahzaib.7899@gmail.com', '12345678', NULL, 'Tenant', 'Active', '2020-09-16 02:45:58', '2020-09-16 02:45:58'),
(17, NULL, 'Ahmad Shahzaib', 'ahmed.shahzaib.789@gmail.com', '12345678', NULL, 'Landlord', 'Active', '2020-09-16 02:48:40', '2020-09-17 04:02:54'),
(18, 'Mr', 'Ahmad Shahzaib', 'ahmed.shahzaib.7789@gmail.com', '12345678', NULL, 'Landlord', 'Active', '2020-09-16 02:49:06', '2020-09-16 02:49:06'),
(19, 'Mrs', 'Alis', 'a@a.com', '12345678', NULL, 'Landlord', 'Active', '2020-09-20 11:08:01', '2020-09-20 11:08:01'),
(20, 'Mr', 'Rohan', 'a@aa.com', '12345678', NULL, 'Landlord', 'Active', '2020-09-20 14:46:53', '2020-09-20 14:46:53'),
(21, 'Mr', 'ALi', 'FSD@ds.cwef', '12345678', NULL, 'Landlord', 'Active', '2020-09-20 14:55:53', '2020-09-20 14:55:53'),
(22, 'Mr', 'ALi', 'FSD@ds.cweef', '12345678', NULL, 'Landlord', 'Active', '2020-09-20 14:57:32', '2020-09-20 14:57:32'),
(23, 'Mrs', 'ALi khansd', 'FSD@ds.cweeffdddd', '12345678', NULL, 'Landlord', 'Active', '2020-09-20 15:03:38', '2020-09-20 17:18:04');

-- --------------------------------------------------------

--
-- Table structure for table `building_facilties`
--

CREATE TABLE `building_facilties` (
  `id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `building_fac_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `building_facilties`
--

INSERT INTO `building_facilties` (`id`, `building_id`, `building_fac_id`, `created_at`, `updated_at`) VALUES
(3, 11, 10, '2020-09-10 09:14:02', '2020-09-10 09:14:02'),
(4, 11, 11, '2020-09-10 09:14:02', '2020-09-10 09:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `building_files`
--

CREATE TABLE `building_files` (
  `id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `file_link` varchar(225) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `building_files`
--

INSERT INTO `building_files` (`id`, `building_id`, `file_link`, `created_at`, `updated_at`) VALUES
(7, 11, 'public/files/images/lab6.pdf', '2020-09-10 09:14:02', '2020-09-10 09:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `building_lists`
--

CREATE TABLE `building_lists` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `manager_id` int(11) NOT NULL,
  `name` varchar(125) NOT NULL,
  `address` text DEFAULT NULL,
  `img_link` varchar(225) NOT NULL,
  `no_floors` int(11) DEFAULT NULL,
  `no_flats` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `parking` enum('Yes','No') NOT NULL,
  `status` enum('Pending','Approved','Disapproved') DEFAULT 'Pending',
  `created_by_id` int(11) DEFAULT NULL,
  `aprroved_by_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `building_lists`
--

INSERT INTO `building_lists` (`id`, `project_id`, `manager_id`, `name`, `address`, `img_link`, `no_floors`, `no_flats`, `type_id`, `parking`, `status`, `created_by_id`, `aprroved_by_id`, `created_at`, `updated_at`) VALUES
(11, 13, 10, 'abc building', 'asfgsdjl', 'public/files/images/images (13).jpg', 4, 43, 10, 'Yes', 'Approved', NULL, NULL, '2020-09-10 09:14:01', '2020-09-10 09:14:01');

-- --------------------------------------------------------

--
-- Table structure for table `building_of_facilties`
--

CREATE TABLE `building_of_facilties` (
  `id` int(11) NOT NULL,
  `name` varchar(125) NOT NULL,
  `description` text DEFAULT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Deactive',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `building_of_facilties`
--

INSERT INTO `building_of_facilties` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(10, 'sd', 'sd', 'Deactive', '2020-09-10 09:08:58', '2020-09-10 09:10:42'),
(11, 'gasfa', 'gwsedfgsdg', 'Deactive', '2020-09-10 09:10:38', '2020-09-10 09:10:38');

-- --------------------------------------------------------

--
-- Table structure for table `building_types`
--

CREATE TABLE `building_types` (
  `id` int(11) NOT NULL,
  `name` varchar(125) NOT NULL,
  `description` text DEFAULT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Deactive',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `building_types`
--

INSERT INTO `building_types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(10, 'd', 'd', 'Active', '2020-09-10 09:11:11', '2020-09-18 02:01:38'),
(11, 'f', 'b', 'Deactive', '2020-09-10 09:12:00', '2020-09-10 09:12:00');

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE `contracts` (
  `id` int(11) NOT NULL,
  `building_id` int(11) DEFAULT NULL,
  `floor_id` int(11) DEFAULT NULL,
  `flat_id` int(11) DEFAULT NULL,
  `flat_security` int(11) DEFAULT NULL,
  `contract_from` varchar(522) DEFAULT NULL,
  `contract_to` varchar(522) DEFAULT NULL,
  `contract_amount` int(11) DEFAULT NULL,
  `installments_no` int(11) DEFAULT NULL,
  `terms_and_condition` varchar(522) DEFAULT NULL,
  `renew_terms_and_condition` varchar(522) DEFAULT NULL,
  `scanned_doc` varchar(522) DEFAULT NULL,
  `status` varchar(522) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `contract_attachments`
--

CREATE TABLE `contract_attachments` (
  `id` int(11) NOT NULL,
  `contract_id` int(11) DEFAULT NULL,
  `attachment` varchar(522) DEFAULT NULL,
  `comment` varchar(522) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `faciltie_of_flats`
--

CREATE TABLE `faciltie_of_flats` (
  `id` int(11) NOT NULL,
  `name` varchar(125) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Deactive',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faciltie_of_flats`
--

INSERT INTO `faciltie_of_flats` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(12, 'hello name 1', 'ioj;mdfgijkmlfrvsdmkld;fvlc', 'Deactive', '2020-09-10 08:54:34', '2020-09-10 08:57:31'),
(13, 'hello name 2', 'fhgvnm', 'Active', '2020-09-10 08:59:20', '2020-09-10 08:59:26');

-- --------------------------------------------------------

--
-- Table structure for table `flat_facilties`
--

CREATE TABLE `flat_facilties` (
  `id` int(11) NOT NULL,
  `flat_id` int(11) NOT NULL,
  `flat_fac_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `flat_facilties`
--

INSERT INTO `flat_facilties` (`id`, `flat_id`, `flat_fac_id`, `created_at`, `updated_at`) VALUES
(22, 8, 13, '2020-09-10 15:31:50', '2020-09-10 15:31:50'),
(23, 8, 13, '2020-09-10 15:31:50', '2020-09-10 15:31:50'),
(24, 8, 12, '2020-09-10 15:31:50', '2020-09-10 15:31:50');

-- --------------------------------------------------------

--
-- Table structure for table `flat_lists`
--

CREATE TABLE `flat_lists` (
  `id` int(11) NOT NULL,
  `flat_type_id` int(11) NOT NULL,
  `flat_pro_type_id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `unit_code` varchar(225) NOT NULL,
  `rent_range` varchar(125) DEFAULT NULL,
  `status` enum('Pending','Approved','Disapproved') NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `create_by_id` int(11) DEFAULT NULL,
  `approved_by_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `flat_lists`
--

INSERT INTO `flat_lists` (`id`, `flat_type_id`, `flat_pro_type_id`, `building_id`, `unit_code`, `rent_range`, `status`, `created_at`, `updated_at`, `create_by_id`, `approved_by_id`) VALUES
(8, 17, 12, 11, '324', '344', 'Approved', '2020-09-10 09:53:40', '2020-09-10 15:00:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `flat_pro_types`
--

CREATE TABLE `flat_pro_types` (
  `id` int(11) NOT NULL,
  `name` varchar(125) NOT NULL,
  `description` text DEFAULT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Deactive',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `flat_pro_types`
--

INSERT INTO `flat_pro_types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(11, 'haseeb', 'gasdvx', 'Deactive', '2020-09-10 09:00:35', '2020-09-10 09:03:30'),
(12, 'sdf', 'df', 'Deactive', '2020-09-10 09:03:24', '2020-09-10 09:03:24');

-- --------------------------------------------------------

--
-- Table structure for table `flat_types`
--

CREATE TABLE `flat_types` (
  `id` int(11) NOT NULL,
  `name` varchar(125) NOT NULL,
  `description` text DEFAULT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Deactive',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `flat_types`
--

INSERT INTO `flat_types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(16, 'asg', 'asdg', 'Deactive', '2020-09-10 09:07:11', '2020-09-10 09:08:38'),
(17, 'asdvvvv', 'v', 'Deactive', '2020-09-10 09:08:31', '2020-09-10 09:08:31');

-- --------------------------------------------------------

--
-- Table structure for table `floor_lists`
--

CREATE TABLE `floor_lists` (
  `id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `floor_type_id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `no_of_flats` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `floor_lists`
--

INSERT INTO `floor_lists` (`id`, `building_id`, `floor_type_id`, `name`, `no_of_flats`, `created_at`, `updated_at`) VALUES
(5, 11, 11, 'fsdfasdvcx', 4, '2020-09-10 09:52:05', '2020-09-10 09:52:15');

-- --------------------------------------------------------

--
-- Table structure for table `floor_types`
--

CREATE TABLE `floor_types` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `description` text DEFAULT NULL,
  `status` enum('Active','Deactive') DEFAULT 'Deactive',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `floor_types`
--

INSERT INTO `floor_types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(11, 'fdg', 'dsfg', 'Deactive', '2020-09-10 08:22:00', '2020-09-10 08:53:57');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(225) NOT NULL,
  `role` varchar(522) DEFAULT NULL,
  `phone` varchar(125) DEFAULT NULL,
  `status` enum('Active','Deactive') DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `first_per_title` varchar(255) DEFAULT NULL,
  `first_per_ref_no` varchar(255) DEFAULT NULL,
  `first_per_name_in_arabic` varchar(522) DEFAULT NULL,
  `first_per_company` varchar(255) DEFAULT NULL,
  `first_per_nationality` varchar(255) DEFAULT NULL,
  `first_per_trade_license_no` int(100) DEFAULT NULL,
  `first_per_license_authority` int(100) DEFAULT NULL,
  `first_per_reg_no` int(100) DEFAULT NULL,
  `first_per_vat_emirates` int(100) DEFAULT NULL,
  `first_per_vat_gcc` int(100) DEFAULT NULL,
  `first_per_phone` int(100) DEFAULT NULL,
  `first_per_contact1` int(100) DEFAULT NULL,
  `first_per_contact2` int(100) DEFAULT NULL,
  `first_per_country` int(11) DEFAULT NULL,
  `first_per_city` int(11) DEFAULT NULL,
  `first_per_location` varchar(255) DEFAULT NULL,
  `first_per_pobox` varchar(255) DEFAULT NULL,
  `first_per_passport_no` int(100) DEFAULT NULL,
  `first_per_passport_expiry_date` varchar(255) DEFAULT NULL,
  `first_per_visa_no` varchar(255) DEFAULT NULL,
  `first_per_visa_no_expiry_date` varchar(255) DEFAULT NULL,
  `first_per_remarks` varchar(255) DEFAULT NULL,
  `second_per_name` varchar(255) DEFAULT NULL,
  `second_per_name_in_arabic` varchar(255) DEFAULT NULL,
  `second_per_designation` varchar(522) DEFAULT NULL,
  `second_per_phone` int(100) DEFAULT NULL,
  `second_per_mobile` int(100) DEFAULT NULL,
  `second_per_card_no` int(100) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `second_per_id_no` int(100) DEFAULT NULL,
  `second_per_vat_country` varchar(255) DEFAULT NULL,
  `second_per_nationality` varchar(255) DEFAULT NULL,
  `second_per_profession` varchar(255) DEFAULT NULL,
  `second_per_address1` varchar(255) DEFAULT NULL,
  `second_per_address2` varchar(255) DEFAULT NULL,
  `second_per_vat_reg_no` int(100) DEFAULT NULL,
  `second_per_vat_emirates` int(100) DEFAULT NULL,
  `second_per_remarks` varchar(255) DEFAULT NULL,
  `acc_no_db` int(100) DEFAULT NULL,
  `acc_no_cr` int(100) DEFAULT NULL,
  `initial_occupation_date` varchar(255) DEFAULT NULL,
  `booking_no` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `role`, `phone`, `status`, `created_at`, `updated_at`, `first_per_title`, `first_per_ref_no`, `first_per_name_in_arabic`, `first_per_company`, `first_per_nationality`, `first_per_trade_license_no`, `first_per_license_authority`, `first_per_reg_no`, `first_per_vat_emirates`, `first_per_vat_gcc`, `first_per_phone`, `first_per_contact1`, `first_per_contact2`, `first_per_country`, `first_per_city`, `first_per_location`, `first_per_pobox`, `first_per_passport_no`, `first_per_passport_expiry_date`, `first_per_visa_no`, `first_per_visa_no_expiry_date`, `first_per_remarks`, `second_per_name`, `second_per_name_in_arabic`, `second_per_designation`, `second_per_phone`, `second_per_mobile`, `second_per_card_no`, `type`, `second_per_id_no`, `second_per_vat_country`, `second_per_nationality`, `second_per_profession`, `second_per_address1`, `second_per_address2`, `second_per_vat_reg_no`, `second_per_vat_emirates`, `second_per_remarks`, `acc_no_db`, `acc_no_cr`, `initial_occupation_date`, `booking_no`) VALUES
(25, 10, NULL, '233456', NULL, '2020-09-15 07:15:10', '2020-09-18 02:01:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 18, NULL, NULL, 'Active', '2020-09-16 02:49:06', '2020-09-16 02:49:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 17, NULL, NULL, 'Active', '2020-09-17 04:06:09', '2020-09-17 04:07:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 15, NULL, NULL, 'Active', '2020-09-18 02:00:42', '2020-09-18 02:00:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 19, NULL, '123', 'Active', '2020-09-20 11:08:01', '2020-09-20 11:08:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 20, NULL, NULL, 'Active', '2020-09-20 14:46:53', '2020-09-20 14:46:53', NULL, '123', 'khan', NULL, NULL, 123, NULL, NULL, 123, 123, 123, 123, 123, 1, 2, 'nothinh', 'kch bi', 123, '2020-09-21', '123', '2020-09-21', 'nill', 'haseeb', 'khan', 'nothing', 123, 123, 123, NULL, 123, 'testvatcountry', 'pta nahi', 'null', 'null', 'null', 123, 2, 'nothing', NULL, NULL, NULL, NULL),
(31, 21, NULL, NULL, 'Active', '2020-09-20 14:55:53', '2020-09-20 14:55:53', NULL, '32523', 'df', NULL, '3', 1234, 1234, 124125, 2142315, 5231, 23523, 423423, 523423, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 22, NULL, NULL, 'Active', '2020-09-20 14:57:32', '2020-09-20 14:57:32', NULL, '32523', 'df', NULL, '3', 1234, 1234, 124125, 2142315, 5231, 23523, 423423, 523423, 2, 1, 'kwlvsf,x', 'wfesd', 235234, '2020-10-02', '3523523', '2020-09-17', 'slrmdg', 'hello', 'wgksdvnm', 'ujinkvdm,xc', 32523, 34, 2345, NULL, 32, 'testvatcountry', 'ASDJKG M', 'fsdf', 'fsg', 'sdv', 2342, 2, 'sdf', NULL, NULL, NULL, NULL),
(33, 23, NULL, NULL, 'Active', '2020-09-20 15:03:38', '2020-09-20 17:22:22', NULL, '1122', 'df1122', NULL, NULL, 12341122, 12341122, 124125, 1122, 1212, 235231122, 1122, 1122, NULL, NULL, 'kwlvsf,x1122', '1122', 1122, '2020-10-02', '3523523', '2020-09-17', 'slrmdg', 'hello', 'wgksdvnm', 'ujinkvdm,xc', 32523, 34, 2345, NULL, 32, NULL, 'ASDJKG M', 'fsdf', 'fsg', 'sdv', 234212, NULL, 'sdf', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `s_date` date NOT NULL,
  `e_date` date NOT NULL,
  `location` text DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('Pending','Approved','Disapproved') NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `s_date`, `e_date`, `location`, `created_by_id`, `manager_id`, `created_at`, `updated_at`, `status`) VALUES
(13, 'National Highway', '2020-09-10', '2020-09-26', 'Gulgasht', 1, 10, '2020-09-10 01:38:21', '2020-09-10 09:51:31', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `project_files`
--

CREATE TABLE `project_files` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `file_link` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project_files`
--

INSERT INTO `project_files` (`id`, `project_id`, `file_link`, `created_at`, `updated_at`) VALUES
(10, 13, 'public/files/images/DCN Lab Paper.pdf', '2020-09-10 01:38:22', '2020-09-10 09:37:01');

-- --------------------------------------------------------

--
-- Table structure for table `segregations`
--

CREATE TABLE `segregations` (
  `id` int(11) NOT NULL,
  `building_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `flat_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_type` varchar(522) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `segregations`
--

INSERT INTO `segregations` (`id`, `building_id`, `project_id`, `flat_id`, `user_id`, `status`, `user_type`, `created_at`, `updated_at`) VALUES
(3, 11, 13, 324, 15, 2, 'Landlord', '2020-09-18 12:41:53', '2020-09-18 07:41:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Shahzaib', 'ahmed.shahzaib.789@gmail.comss', '2020-09-03 12:40:24', '$2y$10$nDwjRaAHdbFUMc3Iq2/Yw.2A2cwylKVOBipO46iKcfN6gZ9DtElKO', NULL, '2020-09-03 12:40:24', '2020-09-03 12:47:19');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `building_facilties`
--
ALTER TABLE `building_facilties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `building_id` (`building_id`),
  ADD KEY `building_fac_id` (`building_fac_id`);

--
-- Indexes for table `building_files`
--
ALTER TABLE `building_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `building_id` (`building_id`);

--
-- Indexes for table `building_lists`
--
ALTER TABLE `building_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aprroved_by_id` (`aprroved_by_id`),
  ADD KEY `create_by_id` (`created_by_id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `manager_id` (`manager_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `building_of_facilties`
--
ALTER TABLE `building_of_facilties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `building_types`
--
ALTER TABLE `building_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contract_attachments`
--
ALTER TABLE `contract_attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faciltie_of_flats`
--
ALTER TABLE `faciltie_of_flats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_facilties`
--
ALTER TABLE `flat_facilties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `flat_id` (`flat_id`),
  ADD KEY `flat_fac_id` (`flat_fac_id`);

--
-- Indexes for table `flat_lists`
--
ALTER TABLE `flat_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `approved_by_id` (`approved_by_id`),
  ADD KEY `create_by_id` (`create_by_id`),
  ADD KEY `building_id` (`building_id`),
  ADD KEY `flat_pro_type_id` (`flat_pro_type_id`),
  ADD KEY `flat_type_id` (`flat_type_id`);

--
-- Indexes for table `flat_pro_types`
--
ALTER TABLE `flat_pro_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_types`
--
ALTER TABLE `flat_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `floor_lists`
--
ALTER TABLE `floor_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `floor_type_id` (`floor_type_id`),
  ADD KEY `building_id` (`building_id`);

--
-- Indexes for table `floor_types`
--
ALTER TABLE `floor_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manager_id` (`manager_id`),
  ADD KEY `created_by_id` (`created_by_id`);

--
-- Indexes for table `project_files`
--
ALTER TABLE `project_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `segregations`
--
ALTER TABLE `segregations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `building_facilties`
--
ALTER TABLE `building_facilties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `building_files`
--
ALTER TABLE `building_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `building_lists`
--
ALTER TABLE `building_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `building_of_facilties`
--
ALTER TABLE `building_of_facilties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `building_types`
--
ALTER TABLE `building_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `contracts`
--
ALTER TABLE `contracts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contract_attachments`
--
ALTER TABLE `contract_attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faciltie_of_flats`
--
ALTER TABLE `faciltie_of_flats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `flat_facilties`
--
ALTER TABLE `flat_facilties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `flat_lists`
--
ALTER TABLE `flat_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `flat_pro_types`
--
ALTER TABLE `flat_pro_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `flat_types`
--
ALTER TABLE `flat_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `floor_lists`
--
ALTER TABLE `floor_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `floor_types`
--
ALTER TABLE `floor_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `project_files`
--
ALTER TABLE `project_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `segregations`
--
ALTER TABLE `segregations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `building_facilties`
--
ALTER TABLE `building_facilties`
  ADD CONSTRAINT `building_facilties_ibfk_1` FOREIGN KEY (`building_fac_id`) REFERENCES `building_of_facilties` (`id`),
  ADD CONSTRAINT `building_facilties_ibfk_2` FOREIGN KEY (`building_id`) REFERENCES `building_lists` (`id`);

--
-- Constraints for table `building_files`
--
ALTER TABLE `building_files`
  ADD CONSTRAINT `building_files_ibfk_1` FOREIGN KEY (`building_id`) REFERENCES `building_lists` (`id`);

--
-- Constraints for table `building_lists`
--
ALTER TABLE `building_lists`
  ADD CONSTRAINT `building_lists_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `building_lists_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `building_types` (`id`);

--
-- Constraints for table `flat_facilties`
--
ALTER TABLE `flat_facilties`
  ADD CONSTRAINT `flat_facilties_ibfk_1` FOREIGN KEY (`flat_fac_id`) REFERENCES `faciltie_of_flats` (`id`),
  ADD CONSTRAINT `flat_facilties_ibfk_2` FOREIGN KEY (`flat_id`) REFERENCES `flat_lists` (`id`);

--
-- Constraints for table `flat_lists`
--
ALTER TABLE `flat_lists`
  ADD CONSTRAINT `flat_lists_ibfk_1` FOREIGN KEY (`building_id`) REFERENCES `building_lists` (`id`),
  ADD CONSTRAINT `flat_lists_ibfk_2` FOREIGN KEY (`flat_pro_type_id`) REFERENCES `flat_pro_types` (`id`),
  ADD CONSTRAINT `flat_lists_ibfk_3` FOREIGN KEY (`flat_type_id`) REFERENCES `flat_types` (`id`);

--
-- Constraints for table `floor_lists`
--
ALTER TABLE `floor_lists`
  ADD CONSTRAINT `floor_lists_ibfk_1` FOREIGN KEY (`building_id`) REFERENCES `building_lists` (`id`),
  ADD CONSTRAINT `floor_lists_ibfk_2` FOREIGN KEY (`floor_type_id`) REFERENCES `floor_types` (`id`);

--
-- Constraints for table `project_files`
--
ALTER TABLE `project_files`
  ADD CONSTRAINT `project_files_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
